﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;

namespace Autotester
{
    class SQLConnector
    {
        /*PRODUZIONE : STRINGA DI CONNESSIONE AL SERVER*/
        //private static string connectionString = "Data Source = RALCOSRV4\\SQLEXPRESS;Initial Catalog = Autotest; User ID = sitem; Password=sitem";
        /*TEST : STRINGA DI CONNESSIONE AL PC LOCALE*/
        //private static string connectionString = "Data Source =SIMONE-PC\\CHAMELEON;Initial Catalog=Autotest;Integrated Security=True";
        private Object retObject;
        private List<Utente> users;
        private List<ConfigurazioneTest> testConfig = new List<ConfigurazioneTest>();
        private List<tipoRuota> tipoRuota = new List<tipoRuota>();
        private specificheTest st;
        private string singoloDatoString;
        private Int64 singoloDatoInt;

        public object CreateCommand(string queryString, Object obj)
        {
            try
            {
                string connectionString;
                Cursor.Current = Cursors.WaitCursor;
                singoloDatoString = "";
                singoloDatoInt = 0;
                if (ConfigurationManager.AppSettings["LOCAL_TEST"].Equals("false"))
                    connectionString = ConfigurationManager.ConnectionStrings["RALCO_DB"].ConnectionString;
                else
                    connectionString = ConfigurationManager.ConnectionStrings["LOCAL_DB"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    if (queryString.Contains("UPDATE") || queryString.Contains("INSERT") || queryString.Contains("DELETE"))
                    {
                        command.ExecuteNonQuery();
                    }    
                    else if (queryString.Contains("SELECT"))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (obj.GetType() == typeof(List<ConfigurazioneTest>))
                                {
                                    testConfig.Add(new ConfigurazioneTest());
                                    testConfig[testConfig.Count - 1].ID_tipoCollim = Convert.ToInt32(reader["ID_tipoCollim"]);
                                    testConfig[testConfig.Count - 1].nomeCollim = Convert.ToString(reader["nomeCollim"]);
                                    testConfig[testConfig.Count - 1].baudRate = Convert.ToString(reader["baudRate"]);
                                    testConfig[testConfig.Count - 1].baseID_Send = Convert.ToInt32(reader["baseID_Send"]);
                                    testConfig[testConfig.Count - 1].offsetID_Send = Convert.ToInt32(reader["offsetID_Send"]);
                                    testConfig[testConfig.Count - 1].baseID_Recv = Convert.ToInt32(reader["baseID_Recv"]);
                                    testConfig[testConfig.Count - 1].offsetID_Recv = Convert.ToInt32(reader["offsetID_Recv"]);
                                    testConfig[testConfig.Count - 1].nomePanel = Convert.ToString(reader["nomePanel"]);
                                    testConfig[testConfig.Count - 1].protocolName = Convert.ToString(reader["protocolName"]);
                                    testConfig[testConfig.Count - 1].numMotori = Convert.ToInt32(reader["numMotori"]);
                                    testConfig[testConfig.Count - 1].numLasers = Convert.ToInt32(reader["numLasers"]);
                                    testConfig[testConfig.Count - 1].FWAtteso = Convert.ToString(reader["FWAtteso"]);
                                    testConfig[testConfig.Count - 1].BLAtteso = Convert.ToString(reader["BLAtteso"]);
                                    testConfig[testConfig.Count - 1].PCBAtteso = Convert.ToString(reader["PCBAtteso"]);
                                    testConfig[testConfig.Count - 1].motore1 = Convert.ToString(reader["motore1"]);
                                    testConfig[testConfig.Count - 1].motore2 = Convert.ToString(reader["motore2"]);
                                    testConfig[testConfig.Count - 1].motore3 = Convert.ToString(reader["motore3"]);
                                    testConfig[testConfig.Count - 1].motore4 = Convert.ToString(reader["motore4"]);
                                    testConfig[testConfig.Count - 1].motore5 = Convert.ToString(reader["motore5"]);
                                    testConfig[testConfig.Count - 1].motore6 = Convert.ToString(reader["motore6"]);
                                    testConfig[testConfig.Count - 1].motore7 = Convert.ToString(reader["motore7"]);
                                    testConfig[testConfig.Count - 1].motore8 = Convert.ToString(reader["motore8"]);
                                    testConfig[testConfig.Count - 1].motore9 = Convert.ToString(reader["motore9"]);
                                    testConfig[testConfig.Count - 1].motore10 = Convert.ToString(reader["motore10"]);
                                    testConfig[testConfig.Count - 1].motore11 = Convert.ToString(reader["motore11"]);
                                    testConfig[testConfig.Count - 1].motore12 = Convert.ToString(reader["motore12"]);
                                    testConfig[testConfig.Count - 1].testMetro = Convert.ToBoolean(reader["testMetro"]);
                                    testConfig[testConfig.Count - 1].testLuce = Convert.ToBoolean(reader["testLuce"]);
                                    testConfig[testConfig.Count - 1].testDSC = Convert.ToBoolean(reader["testDSC"]);
                                    testConfig[testConfig.Count - 1].testLaser = Convert.ToBoolean(reader["testLaser"]);
                                    testConfig[testConfig.Count - 1].testBtnFiltro = Convert.ToBoolean(reader["testBtnFiltro"]);
                                    testConfig[testConfig.Count - 1].testBtnLuce = Convert.ToBoolean(reader["testBtnLuce"]);
                                    testConfig[testConfig.Count - 1].testBtnIride = Convert.ToBoolean(reader["testBtnIride"]);
                                    testConfig[testConfig.Count - 1].testBtnMembrana = Convert.ToBoolean(reader["testBtnMembrana"]);
                                    testConfig[testConfig.Count - 1].testChiave = Convert.ToBoolean(reader["testChiave"]);
                                    testConfig[testConfig.Count - 1].testVita = Convert.ToBoolean(reader["testVita"]);
                                    testConfig[testConfig.Count - 1].testReset = Convert.ToBoolean(reader["testReset"]);
                                    testConfig[testConfig.Count - 1].testInclin = Convert.ToBoolean(reader["testInclin"]);
                                }
                                else if (obj.GetType() == typeof(List<tipoRuota>))
                                {
                                    tipoRuota.Add(new tipoRuota());
                                    tipoRuota[tipoRuota.Count - 1].IDRuota = reader.GetInt32(0);
                                    tipoRuota[tipoRuota.Count - 1].baseIDRuota = reader.GetInt32(1);
                                    tipoRuota[tipoRuota.Count - 1].protocolloRuota = reader.GetInt32(2);
                                    tipoRuota[tipoRuota.Count - 1].nomeRuota = reader.GetString(3);
                                    tipoRuota[tipoRuota.Count - 1].baudRate = reader.GetString(4);
                                    tipoRuota[tipoRuota.Count - 1].passiAngolo = reader.GetInt32(5);
                                    tipoRuota[tipoRuota.Count - 1].passiAngoloGiro = reader.GetInt32(6);
                                    tipoRuota[tipoRuota.Count - 1].minutiAttesaRotazione = reader.GetInt32(7);
                                }
                                else if (obj.GetType() == typeof(specificheTest))
                                {
                                    st = new specificheTest();
                                    st.modelloCollim = reader.GetString(0);
                                    st.IDFlow = reader.GetInt16(1);
                                    st.anno = reader.GetInt16(2);
                                }
                                else if (obj.GetType() == typeof(List<Utente>))
                                {
                                    if (users == null)
                                        users = new List<Utente>();
                                    users.Add(new Utente());
                                    users[users.Count - 1].idUtente = Convert.ToInt16(reader["idUtente"]);
                                    users[users.Count - 1].idApplicativo = Convert.ToInt16(reader["idApplicativo"]);
                                    users[users.Count - 1].nome = Convert.ToString(reader["nome"]);
                                    users[users.Count - 1].cognome = Convert.ToString(reader["cognome"]);
                                    users[users.Count - 1].password = Convert.ToString(reader["password"]);
                                    users[users.Count - 1].credenziale = Convert.ToString(reader["credenziale"]);
                                    users[users.Count - 1].location = Convert.ToString(reader["location"]);
                                    users[users.Count - 1].dipartimento = Convert.ToString(reader["dipartimento"]);
                                }
                                else if (obj.GetType() == typeof(string))
                                    singoloDatoString = reader.GetString(0);
                                else if (obj.GetType() == typeof(Int64))
                                    singoloDatoInt = reader.GetInt64(0);
                            }
                        }
                        if (obj.GetType() == typeof(List<ConfigurazioneTest>))
                        {
                            retObject = null;
                            Convert.ChangeType(retObject, typeof(List<ConfigurazioneTest>));
                            retObject = testConfig;
                        }
                        else if (obj.GetType() == typeof(List<tipoRuota>))
                        {
                            retObject = null;
                            Convert.ChangeType(retObject, typeof(List<tipoRuota>));
                            retObject = tipoRuota;
                        }
                        else if (obj.GetType() == typeof(specificheTest))
                        {
                            retObject = null;
                            Convert.ChangeType(st, typeof(specificheTest));
                            retObject = st;
                        }
                        else if (obj.GetType() == typeof(List<Utente>))
                        {
                            retObject = null;
                            Convert.ChangeType(retObject, typeof(List<Utente>));
                            retObject = users;
                        }
                        else if (obj.GetType() == typeof(string))
                        {
                            retObject = null;
                            Convert.ChangeType(retObject, typeof(string));
                            retObject = singoloDatoString;
                        }
                        else if (obj.GetType() == typeof(Int64))
                        {
                            retObject = 0;
                            Convert.ChangeType(retObject, typeof(Int64));
                            retObject = singoloDatoInt;
                        }
                        return (retObject);
                    }
                }
                return (null);
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (null);
            }
        }
    }
}
