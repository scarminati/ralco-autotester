﻿namespace Autotester
{
    partial class FormCANSystemErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ledErrMot1 = new Autotester.LedBulb();
            this.ledErrMot2 = new Autotester.LedBulb();
            this.ledErrMot3 = new Autotester.LedBulb();
            this.ledErr270 = new Autotester.LedBulb();
            this.ledErr180 = new Autotester.LedBulb();
            this.ledErr90 = new Autotester.LedBulb();
            this.ledErr0 = new Autotester.LedBulb();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ledErrMot4 = new Autotester.LedBulb();
            this.label7 = new System.Windows.Forms.Label();
            this.ledErrMot8 = new Autotester.LedBulb();
            this.label8 = new System.Windows.Forms.Label();
            this.ledErrMot7 = new Autotester.LedBulb();
            this.ledErrMot6 = new Autotester.LedBulb();
            this.ledErrMot5 = new Autotester.LedBulb();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.ledErrMot12 = new Autotester.LedBulb();
            this.label15 = new System.Windows.Forms.Label();
            this.ledErrMot11 = new Autotester.LedBulb();
            this.ledErrMot10 = new Autotester.LedBulb();
            this.ledErrMot9 = new Autotester.LedBulb();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.ledErrRxCAN = new Autotester.LedBulb();
            this.label20 = new System.Windows.Forms.Label();
            this.ledErrReset = new Autotester.LedBulb();
            this.label21 = new System.Windows.Forms.Label();
            this.ledTempHigh = new Autotester.LedBulb();
            this.label22 = new System.Windows.Forms.Label();
            this.ledTempLow = new Autotester.LedBulb();
            this.label23 = new System.Windows.Forms.Label();
            this.ledErrContEncoder = new Autotester.LedBulb();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl_formNonRaggMot12 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot11 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot10 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot9 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot8 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot7 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot6 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot5 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot4 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot3 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot2 = new System.Windows.Forms.Label();
            this.lbl_formNonRaggMot1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl_passiMot12 = new System.Windows.Forms.Label();
            this.lbl_passiMot11 = new System.Windows.Forms.Label();
            this.lbl_passiMot10 = new System.Windows.Forms.Label();
            this.lbl_passiMot9 = new System.Windows.Forms.Label();
            this.lbl_passiMot8 = new System.Windows.Forms.Label();
            this.lbl_passiMot7 = new System.Windows.Forms.Label();
            this.lbl_passiMot6 = new System.Windows.Forms.Label();
            this.lbl_passiMot5 = new System.Windows.Forms.Label();
            this.lbl_passiMot4 = new System.Windows.Forms.Label();
            this.lbl_passiMot3 = new System.Windows.Forms.Label();
            this.lbl_passiMot2 = new System.Windows.Forms.Label();
            this.lbl_passiMot1 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(169, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Motore 3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(93, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Motore 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Motore 1";
            // 
            // ledErrMot1
            // 
            this.ledErrMot1.Color = System.Drawing.Color.Red;
            this.ledErrMot1.Location = new System.Drawing.Point(32, 51);
            this.ledErrMot1.Name = "ledErrMot1";
            this.ledErrMot1.On = true;
            this.ledErrMot1.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot1.TabIndex = 37;
            this.ledErrMot1.Text = "ledBulb1";
            // 
            // ledErrMot2
            // 
            this.ledErrMot2.Color = System.Drawing.Color.Red;
            this.ledErrMot2.Location = new System.Drawing.Point(105, 51);
            this.ledErrMot2.Name = "ledErrMot2";
            this.ledErrMot2.On = true;
            this.ledErrMot2.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot2.TabIndex = 38;
            this.ledErrMot2.Text = "ledBulb2";
            // 
            // ledErrMot3
            // 
            this.ledErrMot3.Color = System.Drawing.Color.Red;
            this.ledErrMot3.Location = new System.Drawing.Point(182, 51);
            this.ledErrMot3.Name = "ledErrMot3";
            this.ledErrMot3.On = true;
            this.ledErrMot3.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot3.TabIndex = 39;
            this.ledErrMot3.Text = "ledBulb3";
            // 
            // ledErr270
            // 
            this.ledErr270.Color = System.Drawing.Color.Red;
            this.ledErr270.Location = new System.Drawing.Point(259, 47);
            this.ledErr270.Name = "ledErr270";
            this.ledErr270.On = true;
            this.ledErr270.Size = new System.Drawing.Size(23, 23);
            this.ledErr270.TabIndex = 57;
            this.ledErr270.Text = "ledBulb9";
            // 
            // ledErr180
            // 
            this.ledErr180.Color = System.Drawing.Color.Red;
            this.ledErr180.Location = new System.Drawing.Point(182, 47);
            this.ledErr180.Name = "ledErr180";
            this.ledErr180.On = true;
            this.ledErr180.Size = new System.Drawing.Size(23, 23);
            this.ledErr180.TabIndex = 56;
            this.ledErr180.Text = "ledBulb4";
            // 
            // ledErr90
            // 
            this.ledErr90.Color = System.Drawing.Color.Red;
            this.ledErr90.Location = new System.Drawing.Point(105, 47);
            this.ledErr90.Name = "ledErr90";
            this.ledErr90.On = true;
            this.ledErr90.Size = new System.Drawing.Size(23, 23);
            this.ledErr90.TabIndex = 55;
            this.ledErr90.Text = "ledBulb5";
            // 
            // ledErr0
            // 
            this.ledErr0.Color = System.Drawing.Color.Red;
            this.ledErr0.Location = new System.Drawing.Point(32, 47);
            this.ledErr0.Name = "ledErr0";
            this.ledErr0.On = true;
            this.ledErr0.Size = new System.Drawing.Size(23, 23);
            this.ledErr0.TabIndex = 54;
            this.ledErr0.Text = "ledBulb6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "0°";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "90°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(178, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "180°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(259, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "270°";
            // 
            // ledErrMot4
            // 
            this.ledErrMot4.Color = System.Drawing.Color.Red;
            this.ledErrMot4.Location = new System.Drawing.Point(259, 51);
            this.ledErrMot4.Name = "ledErrMot4";
            this.ledErrMot4.On = true;
            this.ledErrMot4.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot4.TabIndex = 64;
            this.ledErrMot4.Text = "ledBulb3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(247, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "Motore 4";
            // 
            // ledErrMot8
            // 
            this.ledErrMot8.Color = System.Drawing.Color.Red;
            this.ledErrMot8.Location = new System.Drawing.Point(259, 114);
            this.ledErrMot8.Name = "ledErrMot8";
            this.ledErrMot8.On = true;
            this.ledErrMot8.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot8.TabIndex = 72;
            this.ledErrMot8.Text = "ledBulb3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(247, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 71;
            this.label8.Text = "Motore 8";
            // 
            // ledErrMot7
            // 
            this.ledErrMot7.Color = System.Drawing.Color.Red;
            this.ledErrMot7.Location = new System.Drawing.Point(182, 114);
            this.ledErrMot7.Name = "ledErrMot7";
            this.ledErrMot7.On = true;
            this.ledErrMot7.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot7.TabIndex = 70;
            this.ledErrMot7.Text = "ledBulb3";
            // 
            // ledErrMot6
            // 
            this.ledErrMot6.Color = System.Drawing.Color.Red;
            this.ledErrMot6.Location = new System.Drawing.Point(105, 114);
            this.ledErrMot6.Name = "ledErrMot6";
            this.ledErrMot6.On = true;
            this.ledErrMot6.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot6.TabIndex = 69;
            this.ledErrMot6.Text = "ledBulb2";
            // 
            // ledErrMot5
            // 
            this.ledErrMot5.Color = System.Drawing.Color.Red;
            this.ledErrMot5.Location = new System.Drawing.Point(32, 114);
            this.ledErrMot5.Name = "ledErrMot5";
            this.ledErrMot5.On = true;
            this.ledErrMot5.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot5.TabIndex = 68;
            this.ledErrMot5.Text = "ledBulb1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(169, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "Motore 7";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(93, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "Motore 6";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 65;
            this.label14.Text = "Motore 5";
            // 
            // ledErrMot12
            // 
            this.ledErrMot12.Color = System.Drawing.Color.Red;
            this.ledErrMot12.Location = new System.Drawing.Point(259, 178);
            this.ledErrMot12.Name = "ledErrMot12";
            this.ledErrMot12.On = true;
            this.ledErrMot12.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot12.TabIndex = 80;
            this.ledErrMot12.Text = "ledBulb3";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(247, 152);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 79;
            this.label15.Text = "Motore 12";
            // 
            // ledErrMot11
            // 
            this.ledErrMot11.Color = System.Drawing.Color.Red;
            this.ledErrMot11.Location = new System.Drawing.Point(182, 178);
            this.ledErrMot11.Name = "ledErrMot11";
            this.ledErrMot11.On = true;
            this.ledErrMot11.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot11.TabIndex = 78;
            this.ledErrMot11.Text = "ledBulb3";
            // 
            // ledErrMot10
            // 
            this.ledErrMot10.Color = System.Drawing.Color.Red;
            this.ledErrMot10.Location = new System.Drawing.Point(105, 178);
            this.ledErrMot10.Name = "ledErrMot10";
            this.ledErrMot10.On = true;
            this.ledErrMot10.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot10.TabIndex = 77;
            this.ledErrMot10.Text = "ledBulb2";
            // 
            // ledErrMot9
            // 
            this.ledErrMot9.Color = System.Drawing.Color.Red;
            this.ledErrMot9.Location = new System.Drawing.Point(32, 178);
            this.ledErrMot9.Name = "ledErrMot9";
            this.ledErrMot9.On = true;
            this.ledErrMot9.Size = new System.Drawing.Size(23, 23);
            this.ledErrMot9.TabIndex = 76;
            this.ledErrMot9.Text = "ledBulb1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(169, 152);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 75;
            this.label16.Text = "Motore 11";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(93, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 74;
            this.label17.Text = "Motore 10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 152);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 73;
            this.label18.Text = "Motore 9";
            // 
            // ledErrRxCAN
            // 
            this.ledErrRxCAN.Color = System.Drawing.Color.Red;
            this.ledErrRxCAN.Location = new System.Drawing.Point(32, 73);
            this.ledErrRxCAN.Name = "ledErrRxCAN";
            this.ledErrRxCAN.On = true;
            this.ledErrRxCAN.Size = new System.Drawing.Size(23, 23);
            this.ledErrRxCAN.TabIndex = 83;
            this.ledErrRxCAN.Text = "ledBulb1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 39);
            this.label20.TabIndex = 82;
            this.label20.Text = "Timeout\r\nRx Mex \r\nCAN\r\n";
            // 
            // ledErrReset
            // 
            this.ledErrReset.Color = System.Drawing.Color.Red;
            this.ledErrReset.Location = new System.Drawing.Point(105, 73);
            this.ledErrReset.Name = "ledErrReset";
            this.ledErrReset.On = true;
            this.ledErrReset.Size = new System.Drawing.Size(23, 23);
            this.ledErrReset.TabIndex = 85;
            this.ledErrReset.Text = "ledBulb2";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(94, 27);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 39);
            this.label21.TabIndex = 84;
            this.label21.Text = "Timeout\r\nRx Mex \r\nRESET";
            // 
            // ledTempHigh
            // 
            this.ledTempHigh.Color = System.Drawing.Color.Red;
            this.ledTempHigh.Location = new System.Drawing.Point(259, 73);
            this.ledTempHigh.Name = "ledTempHigh";
            this.ledTempHigh.On = true;
            this.ledTempHigh.Size = new System.Drawing.Size(23, 23);
            this.ledTempHigh.TabIndex = 89;
            this.ledTempHigh.Text = "ledBulb3";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(248, 27);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 39);
            this.label22.TabIndex = 88;
            this.label22.Text = "Temperatura\r\nTroppo\r\nAlta";
            // 
            // ledTempLow
            // 
            this.ledTempLow.Color = System.Drawing.Color.Red;
            this.ledTempLow.Location = new System.Drawing.Point(181, 73);
            this.ledTempLow.Name = "ledTempLow";
            this.ledTempLow.On = true;
            this.ledTempLow.Size = new System.Drawing.Size(23, 23);
            this.ledTempLow.TabIndex = 87;
            this.ledTempLow.Text = "ledBulb4";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(170, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 39);
            this.label23.TabIndex = 86;
            this.label23.Text = "Temperatura\r\nTroppo\r\nBassa";
            // 
            // ledErrContEncoder
            // 
            this.ledErrContEncoder.Color = System.Drawing.Color.Red;
            this.ledErrContEncoder.Location = new System.Drawing.Point(32, 154);
            this.ledErrContEncoder.Name = "ledErrContEncoder";
            this.ledErrContEncoder.On = true;
            this.ledErrContEncoder.Size = new System.Drawing.Size(23, 23);
            this.ledErrContEncoder.TabIndex = 91;
            this.ledErrContEncoder.Text = "ledBulb1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(21, 108);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 39);
            this.label24.TabIndex = 90;
            this.label24.Text = "Errore\r\nConteggio\r\nEncoder";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ledErrMot12);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.ledErrMot1);
            this.groupBox1.Controls.Add(this.ledErrMot2);
            this.groupBox1.Controls.Add(this.ledErrMot3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.ledErrMot4);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.ledErrMot5);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.ledErrMot6);
            this.groupBox1.Controls.Add(this.ledErrMot11);
            this.groupBox1.Controls.Add(this.ledErrMot7);
            this.groupBox1.Controls.Add(this.ledErrMot10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ledErrMot9);
            this.groupBox1.Controls.Add(this.ledErrMot8);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 214);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allarmi Motore";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ledTempHigh);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.ledErrContEncoder);
            this.groupBox2.Controls.Add(this.ledErrRxCAN);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.ledErrReset);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.ledTempLow);
            this.groupBox2.Location = new System.Drawing.Point(12, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 194);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Allarmi Vari";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ledErr270);
            this.groupBox3.Controls.Add(this.ledErr0);
            this.groupBox3.Controls.Add(this.ledErr90);
            this.groupBox3.Controls.Add(this.ledErr180);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(12, 432);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(317, 89);
            this.groupBox3.TabIndex = 94;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rotazione Collimatore";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot12);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot11);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot10);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot9);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot8);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot7);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot6);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot5);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot4);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot3);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot2);
            this.groupBox4.Controls.Add(this.lbl_formNonRaggMot1);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Location = new System.Drawing.Point(335, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(317, 214);
            this.groupBox4.TabIndex = 95;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Evento Motore : Formato Non Raggiungibile";
            // 
            // lbl_formNonRaggMot12
            // 
            this.lbl_formNonRaggMot12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot12.Location = new System.Drawing.Point(250, 178);
            this.lbl_formNonRaggMot12.Name = "lbl_formNonRaggMot12";
            this.lbl_formNonRaggMot12.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot12.TabIndex = 91;
            // 
            // lbl_formNonRaggMot11
            // 
            this.lbl_formNonRaggMot11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot11.Location = new System.Drawing.Point(172, 178);
            this.lbl_formNonRaggMot11.Name = "lbl_formNonRaggMot11";
            this.lbl_formNonRaggMot11.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot11.TabIndex = 90;
            // 
            // lbl_formNonRaggMot10
            // 
            this.lbl_formNonRaggMot10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot10.Location = new System.Drawing.Point(96, 178);
            this.lbl_formNonRaggMot10.Name = "lbl_formNonRaggMot10";
            this.lbl_formNonRaggMot10.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot10.TabIndex = 89;
            // 
            // lbl_formNonRaggMot9
            // 
            this.lbl_formNonRaggMot9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot9.Location = new System.Drawing.Point(24, 178);
            this.lbl_formNonRaggMot9.Name = "lbl_formNonRaggMot9";
            this.lbl_formNonRaggMot9.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot9.TabIndex = 88;
            // 
            // lbl_formNonRaggMot8
            // 
            this.lbl_formNonRaggMot8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot8.Location = new System.Drawing.Point(250, 118);
            this.lbl_formNonRaggMot8.Name = "lbl_formNonRaggMot8";
            this.lbl_formNonRaggMot8.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot8.TabIndex = 87;
            // 
            // lbl_formNonRaggMot7
            // 
            this.lbl_formNonRaggMot7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot7.Location = new System.Drawing.Point(172, 118);
            this.lbl_formNonRaggMot7.Name = "lbl_formNonRaggMot7";
            this.lbl_formNonRaggMot7.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot7.TabIndex = 86;
            // 
            // lbl_formNonRaggMot6
            // 
            this.lbl_formNonRaggMot6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot6.Location = new System.Drawing.Point(96, 118);
            this.lbl_formNonRaggMot6.Name = "lbl_formNonRaggMot6";
            this.lbl_formNonRaggMot6.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot6.TabIndex = 85;
            // 
            // lbl_formNonRaggMot5
            // 
            this.lbl_formNonRaggMot5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot5.Location = new System.Drawing.Point(24, 118);
            this.lbl_formNonRaggMot5.Name = "lbl_formNonRaggMot5";
            this.lbl_formNonRaggMot5.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot5.TabIndex = 84;
            // 
            // lbl_formNonRaggMot4
            // 
            this.lbl_formNonRaggMot4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot4.Location = new System.Drawing.Point(250, 51);
            this.lbl_formNonRaggMot4.Name = "lbl_formNonRaggMot4";
            this.lbl_formNonRaggMot4.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot4.TabIndex = 83;
            // 
            // lbl_formNonRaggMot3
            // 
            this.lbl_formNonRaggMot3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot3.Location = new System.Drawing.Point(172, 51);
            this.lbl_formNonRaggMot3.Name = "lbl_formNonRaggMot3";
            this.lbl_formNonRaggMot3.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot3.TabIndex = 82;
            // 
            // lbl_formNonRaggMot2
            // 
            this.lbl_formNonRaggMot2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot2.Location = new System.Drawing.Point(96, 51);
            this.lbl_formNonRaggMot2.Name = "lbl_formNonRaggMot2";
            this.lbl_formNonRaggMot2.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot2.TabIndex = 81;
            // 
            // lbl_formNonRaggMot1
            // 
            this.lbl_formNonRaggMot1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_formNonRaggMot1.Location = new System.Drawing.Point(24, 51);
            this.lbl_formNonRaggMot1.Name = "lbl_formNonRaggMot1";
            this.lbl_formNonRaggMot1.Size = new System.Drawing.Size(40, 19);
            this.lbl_formNonRaggMot1.TabIndex = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Motore 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(93, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Motore 2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(169, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "Motore 3";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(247, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 63;
            this.label25.Text = "Motore 4";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(21, 88);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "Motore 5";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(93, 88);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 13);
            this.label27.TabIndex = 66;
            this.label27.Text = "Motore 6";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(169, 88);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 13);
            this.label28.TabIndex = 67;
            this.label28.Text = "Motore 7";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(247, 152);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 13);
            this.label29.TabIndex = 79;
            this.label29.Text = "Motore 12";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(247, 88);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(49, 13);
            this.label30.TabIndex = 71;
            this.label30.Text = "Motore 8";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(169, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 13);
            this.label31.TabIndex = 75;
            this.label31.Text = "Motore 11";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(21, 152);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(49, 13);
            this.label32.TabIndex = 73;
            this.label32.Text = "Motore 9";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(93, 152);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(55, 13);
            this.label33.TabIndex = 74;
            this.label33.Text = "Motore 10";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl_passiMot12);
            this.groupBox5.Controls.Add(this.lbl_passiMot11);
            this.groupBox5.Controls.Add(this.lbl_passiMot10);
            this.groupBox5.Controls.Add(this.lbl_passiMot9);
            this.groupBox5.Controls.Add(this.lbl_passiMot8);
            this.groupBox5.Controls.Add(this.lbl_passiMot7);
            this.groupBox5.Controls.Add(this.lbl_passiMot6);
            this.groupBox5.Controls.Add(this.lbl_passiMot5);
            this.groupBox5.Controls.Add(this.lbl_passiMot4);
            this.groupBox5.Controls.Add(this.lbl_passiMot3);
            this.groupBox5.Controls.Add(this.lbl_passiMot2);
            this.groupBox5.Controls.Add(this.lbl_passiMot1);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Location = new System.Drawing.Point(335, 232);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(317, 203);
            this.groupBox5.TabIndex = 96;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Evento Motore : Perdita Passi";
            // 
            // lbl_passiMot12
            // 
            this.lbl_passiMot12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot12.Location = new System.Drawing.Point(250, 174);
            this.lbl_passiMot12.Name = "lbl_passiMot12";
            this.lbl_passiMot12.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot12.TabIndex = 91;
            // 
            // lbl_passiMot11
            // 
            this.lbl_passiMot11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot11.Location = new System.Drawing.Point(172, 174);
            this.lbl_passiMot11.Name = "lbl_passiMot11";
            this.lbl_passiMot11.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot11.TabIndex = 90;
            // 
            // lbl_passiMot10
            // 
            this.lbl_passiMot10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot10.Location = new System.Drawing.Point(96, 174);
            this.lbl_passiMot10.Name = "lbl_passiMot10";
            this.lbl_passiMot10.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot10.TabIndex = 89;
            // 
            // lbl_passiMot9
            // 
            this.lbl_passiMot9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot9.Location = new System.Drawing.Point(24, 174);
            this.lbl_passiMot9.Name = "lbl_passiMot9";
            this.lbl_passiMot9.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot9.TabIndex = 88;
            // 
            // lbl_passiMot8
            // 
            this.lbl_passiMot8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot8.Location = new System.Drawing.Point(250, 114);
            this.lbl_passiMot8.Name = "lbl_passiMot8";
            this.lbl_passiMot8.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot8.TabIndex = 87;
            // 
            // lbl_passiMot7
            // 
            this.lbl_passiMot7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot7.Location = new System.Drawing.Point(172, 114);
            this.lbl_passiMot7.Name = "lbl_passiMot7";
            this.lbl_passiMot7.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot7.TabIndex = 86;
            // 
            // lbl_passiMot6
            // 
            this.lbl_passiMot6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot6.Location = new System.Drawing.Point(96, 114);
            this.lbl_passiMot6.Name = "lbl_passiMot6";
            this.lbl_passiMot6.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot6.TabIndex = 85;
            // 
            // lbl_passiMot5
            // 
            this.lbl_passiMot5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot5.Location = new System.Drawing.Point(24, 114);
            this.lbl_passiMot5.Name = "lbl_passiMot5";
            this.lbl_passiMot5.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot5.TabIndex = 84;
            // 
            // lbl_passiMot4
            // 
            this.lbl_passiMot4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot4.Location = new System.Drawing.Point(250, 47);
            this.lbl_passiMot4.Name = "lbl_passiMot4";
            this.lbl_passiMot4.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot4.TabIndex = 83;
            // 
            // lbl_passiMot3
            // 
            this.lbl_passiMot3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot3.Location = new System.Drawing.Point(172, 47);
            this.lbl_passiMot3.Name = "lbl_passiMot3";
            this.lbl_passiMot3.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot3.TabIndex = 82;
            // 
            // lbl_passiMot2
            // 
            this.lbl_passiMot2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot2.Location = new System.Drawing.Point(96, 47);
            this.lbl_passiMot2.Name = "lbl_passiMot2";
            this.lbl_passiMot2.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot2.TabIndex = 81;
            // 
            // lbl_passiMot1
            // 
            this.lbl_passiMot1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiMot1.Location = new System.Drawing.Point(24, 47);
            this.lbl_passiMot1.Name = "lbl_passiMot1";
            this.lbl_passiMot1.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiMot1.TabIndex = 80;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(21, 21);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(49, 13);
            this.label46.TabIndex = 30;
            this.label46.Text = "Motore 1";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(93, 21);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(49, 13);
            this.label47.TabIndex = 31;
            this.label47.Text = "Motore 2";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(169, 21);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(49, 13);
            this.label48.TabIndex = 35;
            this.label48.Text = "Motore 3";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(247, 21);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(49, 13);
            this.label49.TabIndex = 63;
            this.label49.Text = "Motore 4";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(21, 84);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(49, 13);
            this.label50.TabIndex = 65;
            this.label50.Text = "Motore 5";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(93, 84);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(49, 13);
            this.label51.TabIndex = 66;
            this.label51.Text = "Motore 6";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(169, 84);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(49, 13);
            this.label52.TabIndex = 67;
            this.label52.Text = "Motore 7";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(247, 148);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(55, 13);
            this.label53.TabIndex = 79;
            this.label53.Text = "Motore 12";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(247, 84);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(49, 13);
            this.label54.TabIndex = 71;
            this.label54.Text = "Motore 8";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(169, 148);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(55, 13);
            this.label55.TabIndex = 75;
            this.label55.Text = "Motore 11";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(21, 148);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(49, 13);
            this.label56.TabIndex = 73;
            this.label56.Text = "Motore 9";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(93, 148);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(55, 13);
            this.label57.TabIndex = 74;
            this.label57.Text = "Motore 10";
            // 
            // FormCANSystemErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 533);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormCANSystemErrors";
            this.Text = "CANSystem Errori";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private LedBulb ledErrMot1;
        private LedBulb ledErrMot2;
        private LedBulb ledErrMot3;
        private LedBulb ledErr270;
        private LedBulb ledErr180;
        private LedBulb ledErr90;
        private LedBulb ledErr0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private LedBulb ledErrMot4;
        private System.Windows.Forms.Label label7;
        private LedBulb ledErrMot8;
        private System.Windows.Forms.Label label8;
        private LedBulb ledErrMot7;
        private LedBulb ledErrMot6;
        private LedBulb ledErrMot5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private LedBulb ledErrMot12;
        private System.Windows.Forms.Label label15;
        private LedBulb ledErrMot11;
        private LedBulb ledErrMot10;
        private LedBulb ledErrMot9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private LedBulb ledErrRxCAN;
        private System.Windows.Forms.Label label20;
        private LedBulb ledErrReset;
        private System.Windows.Forms.Label label21;
        private LedBulb ledTempHigh;
        private System.Windows.Forms.Label label22;
        private LedBulb ledTempLow;
        private System.Windows.Forms.Label label23;
        private LedBulb ledErrContEncoder;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lbl_formNonRaggMot12;
        private System.Windows.Forms.Label lbl_formNonRaggMot11;
        private System.Windows.Forms.Label lbl_formNonRaggMot10;
        private System.Windows.Forms.Label lbl_formNonRaggMot9;
        private System.Windows.Forms.Label lbl_formNonRaggMot8;
        private System.Windows.Forms.Label lbl_formNonRaggMot7;
        private System.Windows.Forms.Label lbl_formNonRaggMot6;
        private System.Windows.Forms.Label lbl_formNonRaggMot5;
        private System.Windows.Forms.Label lbl_formNonRaggMot4;
        private System.Windows.Forms.Label lbl_formNonRaggMot3;
        private System.Windows.Forms.Label lbl_formNonRaggMot2;
        private System.Windows.Forms.Label lbl_formNonRaggMot1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lbl_passiMot12;
        private System.Windows.Forms.Label lbl_passiMot11;
        private System.Windows.Forms.Label lbl_passiMot10;
        private System.Windows.Forms.Label lbl_passiMot9;
        private System.Windows.Forms.Label lbl_passiMot8;
        private System.Windows.Forms.Label lbl_passiMot7;
        private System.Windows.Forms.Label lbl_passiMot6;
        private System.Windows.Forms.Label lbl_passiMot5;
        private System.Windows.Forms.Label lbl_passiMot4;
        private System.Windows.Forms.Label lbl_passiMot3;
        private System.Windows.Forms.Label lbl_passiMot2;
        private System.Windows.Forms.Label lbl_passiMot1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
    }
}