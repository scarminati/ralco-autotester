﻿namespace Autotester
{
    partial class P225Panel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMetro = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnErrori = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.btnLuce = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCenter = new System.Windows.Forms.Button();
            this.btnSid = new System.Windows.Forms.Button();
            this.lblCross = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblLong = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblFilter = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.grpMetro = new System.Windows.Forms.GroupBox();
            this.ledMetro = new Autotester.LedBulb();
            this.grpLuce = new System.Windows.Forms.GroupBox();
            this.ledLuce = new Autotester.LedBulb();
            this.ledLuceTest = new Autotester.LedBulb();
            this.grpDSC = new System.Windows.Forms.GroupBox();
            this.ledCrossDSCTest = new Autotester.LedBulb();
            this.ledCrossDSCActive = new Autotester.LedBulb();
            this.ledLongDSCActive = new Autotester.LedBulb();
            this.ledLongDSCTest = new Autotester.LedBulb();
            this.grpVita = new System.Windows.Forms.GroupBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.grpLaser = new System.Windows.Forms.GroupBox();
            this.ledCenter = new Autotester.LedBulb();
            this.label22 = new System.Windows.Forms.Label();
            this.ledSID = new Autotester.LedBulb();
            this.lblFC = new System.Windows.Forms.Label();
            this.lblCNC = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblCollimatore = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cmb_tipoTest = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.ledError = new Autotester.LedBulb();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPCB = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBootloader = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFirmware = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.grpMetro.SuspendLayout();
            this.grpLuce.SuspendLayout();
            this.grpDSC.SuspendLayout();
            this.grpVita.SuspendLayout();
            this.grpLaser.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(4, 4);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(15, 14);
            this.chkEnable.TabIndex = 0;
            this.chkEnable.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Metro";
            // 
            // lblMetro
            // 
            this.lblMetro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMetro.Location = new System.Drawing.Point(13, 76);
            this.lblMetro.Name = "lblMetro";
            this.lblMetro.Size = new System.Drawing.Size(36, 23);
            this.lblMetro.TabIndex = 11;
            this.lblMetro.Text = "0";
            this.lblMetro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "DSC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Attivo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Cross";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Long";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(78, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Test";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1101, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Errori";
            // 
            // btnErrori
            // 
            this.btnErrori.Location = new System.Drawing.Point(1161, 35);
            this.btnErrori.Name = "btnErrori";
            this.btnErrori.Size = new System.Drawing.Size(75, 41);
            this.btnErrori.TabIndex = 23;
            this.btnErrori.Text = "Mostra errori";
            this.btnErrori.UseVisualStyleBackColor = true;
            this.btnErrori.Click += new System.EventHandler(this.btnErrori_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(37, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Luce";
            // 
            // btnLuce
            // 
            this.btnLuce.Location = new System.Drawing.Point(6, 33);
            this.btnLuce.Name = "btnLuce";
            this.btnLuce.Size = new System.Drawing.Size(54, 23);
            this.btnLuce.TabIndex = 25;
            this.btnLuce.Text = "Accendi";
            this.btnLuce.UseVisualStyleBackColor = true;
            this.btnLuce.Click += new System.EventHandler(this.btnLuce_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(39, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Laser";
            // 
            // btnCenter
            // 
            this.btnCenter.Location = new System.Drawing.Point(6, 46);
            this.btnCenter.Name = "btnCenter";
            this.btnCenter.Size = new System.Drawing.Size(85, 23);
            this.btnCenter.TabIndex = 28;
            this.btnCenter.Text = "CENTER On";
            this.btnCenter.UseVisualStyleBackColor = true;
            this.btnCenter.Click += new System.EventHandler(this.btnCenterOn_Click);
            // 
            // btnSid
            // 
            this.btnSid.Location = new System.Drawing.Point(6, 76);
            this.btnSid.Name = "btnSid";
            this.btnSid.Size = new System.Drawing.Size(85, 23);
            this.btnSid.TabIndex = 30;
            this.btnSid.Text = "SID On";
            this.btnSid.UseVisualStyleBackColor = true;
            this.btnSid.Click += new System.EventHandler(this.btnSidOn_Click);
            // 
            // lblCross
            // 
            this.lblCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCross.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCross.Location = new System.Drawing.Point(74, 31);
            this.lblCross.Name = "lblCross";
            this.lblCross.Size = new System.Drawing.Size(69, 16);
            this.lblCross.TabIndex = 35;
            this.lblCross.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Cross (cm)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLong
            // 
            this.lblLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLong.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLong.Location = new System.Drawing.Point(74, 56);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(69, 16);
            this.lblLong.TabIndex = 37;
            this.lblLong.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Long (cm)";
            // 
            // lblFilter
            // 
            this.lblFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilter.Location = new System.Drawing.Point(74, 80);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(69, 16);
            this.lblFilter.TabIndex = 39;
            this.lblFilter.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 81);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "Filtro";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(73, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Test";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(51, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 48;
            this.label15.Text = "Test Vita";
            // 
            // grpMetro
            // 
            this.grpMetro.Controls.Add(this.ledMetro);
            this.grpMetro.Controls.Add(this.label9);
            this.grpMetro.Controls.Add(this.lblMetro);
            this.grpMetro.Enabled = false;
            this.grpMetro.Location = new System.Drawing.Point(401, 0);
            this.grpMetro.Name = "grpMetro";
            this.grpMetro.Size = new System.Drawing.Size(61, 110);
            this.grpMetro.TabIndex = 49;
            this.grpMetro.TabStop = false;
            // 
            // ledMetro
            // 
            this.ledMetro.Location = new System.Drawing.Point(13, 30);
            this.ledMetro.Name = "ledMetro";
            this.ledMetro.On = true;
            this.ledMetro.Size = new System.Drawing.Size(36, 39);
            this.ledMetro.TabIndex = 10;
            this.ledMetro.Text = "ledBulb1";
            // 
            // grpLuce
            // 
            this.grpLuce.Controls.Add(this.btnLuce);
            this.grpLuce.Controls.Add(this.label13);
            this.grpLuce.Controls.Add(this.ledLuce);
            this.grpLuce.Controls.Add(this.label16);
            this.grpLuce.Controls.Add(this.ledLuceTest);
            this.grpLuce.Enabled = false;
            this.grpLuce.Location = new System.Drawing.Point(487, 0);
            this.grpLuce.Name = "grpLuce";
            this.grpLuce.Size = new System.Drawing.Size(111, 110);
            this.grpLuce.TabIndex = 50;
            this.grpLuce.TabStop = false;
            // 
            // ledLuce
            // 
            this.ledLuce.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLuce.Location = new System.Drawing.Point(15, 63);
            this.ledLuce.Name = "ledLuce";
            this.ledLuce.On = true;
            this.ledLuce.Size = new System.Drawing.Size(36, 39);
            this.ledLuce.TabIndex = 26;
            this.ledLuce.Text = "ledBulb1";
            // 
            // ledLuceTest
            // 
            this.ledLuceTest.Location = new System.Drawing.Point(76, 69);
            this.ledLuceTest.Name = "ledLuceTest";
            this.ledLuceTest.On = true;
            this.ledLuceTest.Size = new System.Drawing.Size(25, 23);
            this.ledLuceTest.TabIndex = 41;
            this.ledLuceTest.Text = "ledBulb4";
            // 
            // grpDSC
            // 
            this.grpDSC.Controls.Add(this.ledCrossDSCTest);
            this.grpDSC.Controls.Add(this.label2);
            this.grpDSC.Controls.Add(this.label3);
            this.grpDSC.Controls.Add(this.label5);
            this.grpDSC.Controls.Add(this.label10);
            this.grpDSC.Controls.Add(this.ledCrossDSCActive);
            this.grpDSC.Controls.Add(this.ledLongDSCActive);
            this.grpDSC.Controls.Add(this.label11);
            this.grpDSC.Controls.Add(this.ledLongDSCTest);
            this.grpDSC.Enabled = false;
            this.grpDSC.Location = new System.Drawing.Point(622, 0);
            this.grpDSC.Name = "grpDSC";
            this.grpDSC.Size = new System.Drawing.Size(121, 110);
            this.grpDSC.TabIndex = 51;
            this.grpDSC.TabStop = false;
            // 
            // ledCrossDSCTest
            // 
            this.ledCrossDSCTest.Location = new System.Drawing.Point(81, 47);
            this.ledCrossDSCTest.Name = "ledCrossDSCTest";
            this.ledCrossDSCTest.On = true;
            this.ledCrossDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSCTest.TabIndex = 19;
            this.ledCrossDSCTest.Text = "ledBulb4";
            // 
            // ledCrossDSCActive
            // 
            this.ledCrossDSCActive.Color = System.Drawing.Color.Yellow;
            this.ledCrossDSCActive.Location = new System.Drawing.Point(11, 47);
            this.ledCrossDSCActive.Name = "ledCrossDSCActive";
            this.ledCrossDSCActive.On = true;
            this.ledCrossDSCActive.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSCActive.TabIndex = 16;
            this.ledCrossDSCActive.Text = "ledBulb1";
            // 
            // ledLongDSCActive
            // 
            this.ledLongDSCActive.Color = System.Drawing.Color.Yellow;
            this.ledLongDSCActive.Location = new System.Drawing.Point(11, 77);
            this.ledLongDSCActive.Name = "ledLongDSCActive";
            this.ledLongDSCActive.On = true;
            this.ledLongDSCActive.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSCActive.TabIndex = 17;
            this.ledLongDSCActive.Text = "ledBulb2";
            // 
            // ledLongDSCTest
            // 
            this.ledLongDSCTest.Location = new System.Drawing.Point(81, 77);
            this.ledLongDSCTest.Name = "ledLongDSCTest";
            this.ledLongDSCTest.On = true;
            this.ledLongDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSCTest.TabIndex = 20;
            this.ledLongDSCTest.Text = "ledBulb3";
            // 
            // grpVita
            // 
            this.grpVita.Controls.Add(this.label15);
            this.grpVita.Controls.Add(this.label17);
            this.grpVita.Controls.Add(this.lblCross);
            this.grpVita.Controls.Add(this.label19);
            this.grpVita.Controls.Add(this.lblLong);
            this.grpVita.Controls.Add(this.label21);
            this.grpVita.Controls.Add(this.lblFilter);
            this.grpVita.Enabled = false;
            this.grpVita.Location = new System.Drawing.Point(920, 0);
            this.grpVita.Name = "grpVita";
            this.grpVita.Size = new System.Drawing.Size(154, 110);
            this.grpVita.TabIndex = 52;
            this.grpVita.TabStop = false;
            // 
            // grpLaser
            // 
            this.grpLaser.Controls.Add(this.ledCenter);
            this.grpLaser.Controls.Add(this.label22);
            this.grpLaser.Controls.Add(this.ledSID);
            this.grpLaser.Controls.Add(this.label14);
            this.grpLaser.Controls.Add(this.btnCenter);
            this.grpLaser.Controls.Add(this.btnSid);
            this.grpLaser.Enabled = false;
            this.grpLaser.Location = new System.Drawing.Point(765, 0);
            this.grpLaser.Name = "grpLaser";
            this.grpLaser.Size = new System.Drawing.Size(139, 110);
            this.grpLaser.TabIndex = 53;
            this.grpLaser.TabStop = false;
            // 
            // ledCenter
            // 
            this.ledCenter.Location = new System.Drawing.Point(100, 47);
            this.ledCenter.Name = "ledCenter";
            this.ledCenter.On = true;
            this.ledCenter.Size = new System.Drawing.Size(25, 23);
            this.ledCenter.TabIndex = 32;
            this.ledCenter.Text = "ledBulb4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(97, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Test";
            // 
            // ledSID
            // 
            this.ledSID.Location = new System.Drawing.Point(100, 77);
            this.ledSID.Name = "ledSID";
            this.ledSID.On = true;
            this.ledSID.Size = new System.Drawing.Size(25, 23);
            this.ledSID.TabIndex = 33;
            this.ledSID.Text = "ledBulb3";
            // 
            // lblFC
            // 
            this.lblFC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFC.Location = new System.Drawing.Point(69, 84);
            this.lblFC.Name = "lblFC";
            this.lblFC.Size = new System.Drawing.Size(74, 21);
            this.lblFC.TabIndex = 72;
            // 
            // lblCNC
            // 
            this.lblCNC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNC.Location = new System.Drawing.Point(184, 85);
            this.lblCNC.Name = "lblCNC";
            this.lblCNC.Size = new System.Drawing.Size(74, 21);
            this.lblCNC.TabIndex = 71;
            // 
            // lblSN
            // 
            this.lblSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSN.Location = new System.Drawing.Point(184, 52);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(74, 21);
            this.lblSN.TabIndex = 70;
            // 
            // lblCollimatore
            // 
            this.lblCollimatore.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCollimatore.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollimatore.Location = new System.Drawing.Point(69, 21);
            this.lblCollimatore.Name = "lblCollimatore";
            this.lblCollimatore.Size = new System.Drawing.Size(189, 20);
            this.lblCollimatore.TabIndex = 69;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(147, 86);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 68;
            this.label25.Text = "CNC";
            // 
            // cmb_tipoTest
            // 
            this.cmb_tipoTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tipoTest.FormattingEnabled = true;
            this.cmb_tipoTest.Items.AddRange(new object[] {
            "Analisi",
            "Produzione",
            "Riparazione",
            "Test"});
            this.cmb_tipoTest.Location = new System.Drawing.Point(69, 51);
            this.cmb_tipoTest.Name = "cmb_tipoTest";
            this.cmb_tipoTest.Size = new System.Drawing.Size(74, 21);
            this.cmb_tipoTest.TabIndex = 67;
            this.cmb_tipoTest.SelectedIndexChanged += new System.EventHandler(this.cmb_tipoTest_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 66;
            this.label26.Text = "Tipo Test";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 24);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 13);
            this.label27.TabIndex = 65;
            this.label27.Text = "Collimatore";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(20, 13);
            this.label28.TabIndex = 64;
            this.label28.Text = "FC";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(147, 55);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 13);
            this.label29.TabIndex = 63;
            this.label29.Text = "S/N";
            // 
            // ledError
            // 
            this.ledError.Color = System.Drawing.Color.Red;
            this.ledError.Location = new System.Drawing.Point(1086, 30);
            this.ledError.Name = "ledError";
            this.ledError.On = true;
            this.ledError.Size = new System.Drawing.Size(69, 67);
            this.ledError.TabIndex = 22;
            this.ledError.Text = "ledBulb1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblPCB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblBootloader);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblFirmware);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(263, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 105);
            this.panel1.TabIndex = 73;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Firmware Version";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPCB
            // 
            this.lblPCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCB.Location = new System.Drawing.Point(48, 81);
            this.lblPCB.Name = "lblPCB";
            this.lblPCB.Size = new System.Drawing.Size(55, 16);
            this.lblPCB.TabIndex = 5;
            this.lblPCB.Text = "-1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "PCB";
            // 
            // lblBootloader
            // 
            this.lblBootloader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBootloader.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBootloader.Location = new System.Drawing.Point(48, 53);
            this.lblBootloader.Name = "lblBootloader";
            this.lblBootloader.Size = new System.Drawing.Size(55, 16);
            this.lblBootloader.TabIndex = 3;
            this.lblBootloader.Text = "-1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "BL";
            // 
            // lblFirmware
            // 
            this.lblFirmware.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFirmware.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirmware.Location = new System.Drawing.Point(48, 27);
            this.lblFirmware.Name = "lblFirmware";
            this.lblFirmware.Size = new System.Drawing.Size(55, 16);
            this.lblFirmware.TabIndex = 1;
            this.lblFirmware.Text = "-1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "FW";
            // 
            // P225Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblFC);
            this.Controls.Add(this.lblCNC);
            this.Controls.Add(this.lblSN);
            this.Controls.Add(this.lblCollimatore);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.cmb_tipoTest);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.grpLaser);
            this.Controls.Add(this.grpVita);
            this.Controls.Add(this.grpDSC);
            this.Controls.Add(this.grpLuce);
            this.Controls.Add(this.grpMetro);
            this.Controls.Add(this.btnErrori);
            this.Controls.Add(this.ledError);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.chkEnable);
            this.Name = "P225Panel";
            this.Size = new System.Drawing.Size(1246, 115);
            this.Tag = "Philips";
            this.Load += new System.EventHandler(this.P225Panel_Load);
            this.grpMetro.ResumeLayout(false);
            this.grpMetro.PerformLayout();
            this.grpLuce.ResumeLayout(false);
            this.grpLuce.PerformLayout();
            this.grpDSC.ResumeLayout(false);
            this.grpDSC.PerformLayout();
            this.grpVita.ResumeLayout(false);
            this.grpVita.PerformLayout();
            this.grpLaser.ResumeLayout(false);
            this.grpLaser.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEnable;
        private System.Windows.Forms.Label label9;
        private LedBulb ledMetro;
        private System.Windows.Forms.Label lblMetro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private LedBulb ledCrossDSCActive;
        private LedBulb ledLongDSCActive;
        private System.Windows.Forms.Label label11;
        private LedBulb ledLongDSCTest;
        private LedBulb ledCrossDSCTest;
        private System.Windows.Forms.Label label12;
        private LedBulb ledError;
        private System.Windows.Forms.Button btnErrori;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnLuce;
        private LedBulb ledLuce;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnCenter;
        private System.Windows.Forms.Button btnSid;
        private System.Windows.Forms.Label lblCross;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblLong;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label16;
        private LedBulb ledLuceTest;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox grpMetro;
        private System.Windows.Forms.GroupBox grpLuce;
        private System.Windows.Forms.GroupBox grpDSC;
        private System.Windows.Forms.GroupBox grpVita;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox grpLaser;
        private LedBulb ledCenter;
        private System.Windows.Forms.Label label22;
        private LedBulb ledSID;
        private System.Windows.Forms.Label lblFC;
        private System.Windows.Forms.Label lblCNC;
        private System.Windows.Forms.Label lblSN;
        private System.Windows.Forms.Label lblCollimatore;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmb_tipoTest;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBootloader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFirmware;
        private System.Windows.Forms.Label label7;
    }
}
