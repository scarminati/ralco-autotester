﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormR225Errors : Form
    {
        public FormR225Errors()
        {
            InitializeComponent();
        }

        public void setErrors(R225ProtocolAnalyzer protocol)
        {
            if (protocol.passiCross.Equals(-1))
                lbl_deltaPassiCross.Text = protocol.passiCross.ToString();
            else
                lbl_deltaPassiCross.Text = (Math.Abs(50 - protocol.passiCross)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiCross.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiCross.Text) > 50))
                lbl_deltaPassiCross.BackColor = Color.Red;
            if (protocol.passiLong.Equals(-1))
                lbl_deltaPassiLong.Text = protocol.passiLong.ToString();
            else
                lbl_deltaPassiLong.Text = (Math.Abs(50 - protocol.passiLong)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiLong.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiLong.Text) > 50))
                lbl_deltaPassiLong.BackColor = Color.Red;
            if (protocol.passiIride.Equals(-1))
                lbl_deltaPassiIride.Text = protocol.passiIride.ToString();
            else
                lbl_deltaPassiIride.Text = (Math.Abs(50 - protocol.passiIride)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiIride.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiIride.Text) > 50))
                lbl_deltaPassiIride.BackColor = Color.Red;
            lbl_numRiposizFiltro.Text = protocol.tentativiPosFiltro.ToString();
            if (protocol.tentativiPosFiltro > 0)
                lbl_numRiposizFiltro.BackColor = Color.Red;
            ledErr0.On = protocol.error0;
            ledErr90.On = protocol.error90;
            ledErr180.On = protocol.error180;
            ledErr270.On = protocol.error270;
        }
    }
}
