﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    public class Utente
    {
        public int idUtente { get; set; }
        public int idApplicativo { get; set; }
        public string nome { get; set; }
        public string cognome { get; set; }
        public string password { get; set; }
        public string credenziale { get; set; }
        public string location { get; set; }
        public string dipartimento { get; set; }
    }
}
