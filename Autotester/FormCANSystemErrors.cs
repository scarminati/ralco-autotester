﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormCANSystemErrors : Form
    {
        public FormCANSystemErrors()
        {
            InitializeComponent();
        }

        public void setErrors(CANSystemProtocolAnalyzer protocol)
        {
            ledErrMot1.On = protocol.ErrMot1;
            ledErrMot2.On = protocol.ErrMot2;
            ledErrMot3.On = protocol.ErrMot3;
            ledErrMot4.On = protocol.ErrMot4;
            ledErrMot5.On = protocol.ErrMot5;
            ledErrMot6.On = protocol.ErrMot6;
            ledErrMot7.On = protocol.ErrMot7;
            ledErrMot8.On = protocol.ErrMot8;
            ledErrMot9.On = protocol.ErrMot9;
            ledErrMot10.On = protocol.ErrMot10;
            ledErrMot11.On = protocol.ErrMot11;
            ledErrMot12.On = protocol.ErrMot12;

            lbl_formNonRaggMot1.Text = protocol.ErrFormNonRaggMot1.ToString();
            lbl_formNonRaggMot2.Text = protocol.ErrFormNonRaggMot2.ToString();
            lbl_formNonRaggMot3.Text = protocol.ErrFormNonRaggMot3.ToString();
            lbl_formNonRaggMot4.Text = protocol.ErrFormNonRaggMot4.ToString();
            lbl_formNonRaggMot5.Text = protocol.ErrFormNonRaggMot5.ToString();
            lbl_formNonRaggMot6.Text = protocol.ErrFormNonRaggMot6.ToString();
            lbl_formNonRaggMot7.Text = protocol.ErrFormNonRaggMot7.ToString();
            lbl_formNonRaggMot8.Text = protocol.ErrFormNonRaggMot8.ToString();
            lbl_formNonRaggMot9.Text = protocol.ErrFormNonRaggMot9.ToString();
            lbl_formNonRaggMot10.Text = protocol.ErrFormNonRaggMot10.ToString();
            lbl_formNonRaggMot11.Text = protocol.ErrFormNonRaggMot11.ToString();
            lbl_formNonRaggMot12.Text = protocol.ErrFormNonRaggMot12.ToString();

            ledErrRxCAN.On = protocol.ErrTimeoutRxCAN;
            ledErrReset.On = protocol.ErrReset;
            ledTempLow.On = protocol.ErrTempLow;
            ledTempHigh.On = protocol.ErrTempHigh;
            ledErrContEncoder.On = protocol.ErrContEncoder;

            ledErr0.On = protocol.error0;
            ledErr90.On = protocol.error90;
            ledErr180.On = protocol.error180;
            ledErr270.On = protocol.error270;

            //lbl_passiMot1.Text = protocol.PassiMot1.ToString();
            //lbl_passiMot2.Text = protocol.PassiMot2.ToString();
            //lbl_passiMot3.Text = protocol.PassiMot3.ToString();
            //lbl_passiMot4.Text = protocol.PassiMot4.ToString();
            //lbl_passiMot5.Text = protocol.PassiMot5.ToString();
            //lbl_passiMot6.Text = protocol.PassiMot6.ToString();
            //lbl_passiMot7.Text = protocol.PassiMot7.ToString();
            //lbl_passiMot8.Text = protocol.PassiMot8.ToString();
            //lbl_passiMot9.Text = protocol.PassiMot9.ToString();
            //lbl_passiMot10.Text = protocol.PassiMot10.ToString();
            //lbl_passiMot11.Text = protocol.PassiMot11.ToString();
            //lbl_passiMot12.Text = protocol.PassiMot12.ToString();


            if (protocol.PassiMot1.Equals(-1))
                lbl_passiMot1.Text = protocol.PassiMot1.ToString();
            else
                lbl_passiMot1.Text = (Math.Abs(50 - protocol.PassiMot1)).ToString();
            if ((Convert.ToInt32(lbl_passiMot1.Text) == -1) || (Convert.ToInt32(lbl_passiMot1.Text) > 50))
                lbl_passiMot1.BackColor = Color.Red;
            if (protocol.PassiMot2.Equals(-1))
                lbl_passiMot2.Text = protocol.PassiMot2.ToString();
            else
                lbl_passiMot2.Text = (Math.Abs(50 - protocol.PassiMot2)).ToString();
            if ((Convert.ToInt32(lbl_passiMot2.Text) == -1) || (Convert.ToInt32(lbl_passiMot2.Text) > 50))
                lbl_passiMot2.BackColor = Color.Red;
            if (protocol.PassiMot3.Equals(-1))
                lbl_passiMot3.Text = protocol.PassiMot3.ToString();
            else
                lbl_passiMot3.Text = (Math.Abs(50 - protocol.PassiMot3)).ToString();
            if ((Convert.ToInt32(lbl_passiMot3.Text) == -1) || (Convert.ToInt32(lbl_passiMot3.Text) > 50))
                lbl_passiMot3.BackColor = Color.Red;
            if (protocol.PassiMot4.Equals(-1))
                lbl_passiMot4.Text = protocol.PassiMot4.ToString();
            else
                lbl_passiMot4.Text = (Math.Abs(50 - protocol.PassiMot4)).ToString();
            if ((Convert.ToInt32(lbl_passiMot4.Text) == -1) || (Convert.ToInt32(lbl_passiMot4.Text) > 50))
                lbl_passiMot4.BackColor = Color.Red;
            if (protocol.PassiMot5.Equals(-1))
                lbl_passiMot5.Text = protocol.PassiMot5.ToString();
            else
                lbl_passiMot5.Text = (Math.Abs(50 - protocol.PassiMot5)).ToString();
            if ((Convert.ToInt32(lbl_passiMot5.Text) == -1) || (Convert.ToInt32(lbl_passiMot5.Text) > 50))
                lbl_passiMot5.BackColor = Color.Red;
            if (protocol.PassiMot6.Equals(-1))
                lbl_passiMot6.Text = protocol.PassiMot6.ToString();
            else
                lbl_passiMot6.Text = (Math.Abs(50 - protocol.PassiMot6)).ToString();
            if ((Convert.ToInt32(lbl_passiMot6.Text) == -1) || (Convert.ToInt32(lbl_passiMot6.Text) > 50))
                lbl_passiMot6.BackColor = Color.Red;
            if (protocol.PassiMot7.Equals(-1))
                lbl_passiMot7.Text = protocol.PassiMot7.ToString();
            else
                lbl_passiMot7.Text = (Math.Abs(50 - protocol.PassiMot7)).ToString();
            if ((Convert.ToInt32(lbl_passiMot7.Text) == -1) || (Convert.ToInt32(lbl_passiMot7.Text) > 50))
                lbl_passiMot7.BackColor = Color.Red;
            if (protocol.PassiMot8.Equals(-1))
                lbl_passiMot8.Text = protocol.PassiMot8.ToString();
            else
                lbl_passiMot8.Text = (Math.Abs(50 - protocol.PassiMot8)).ToString();
            if ((Convert.ToInt32(lbl_passiMot8.Text) == -1) || (Convert.ToInt32(lbl_passiMot8.Text) > 50))
                lbl_passiMot8.BackColor = Color.Red;
            if (protocol.PassiMot9.Equals(-1))
                lbl_passiMot9.Text = protocol.PassiMot9.ToString();
            else
                lbl_passiMot9.Text = (Math.Abs(50 - protocol.PassiMot9)).ToString();
            if ((Convert.ToInt32(lbl_passiMot9.Text) == -1) || (Convert.ToInt32(lbl_passiMot9.Text) > 50))
                lbl_passiMot9.BackColor = Color.Red;
            if (protocol.PassiMot10.Equals(-1))
                lbl_passiMot10.Text = protocol.PassiMot10.ToString();
            else
                lbl_passiMot10.Text = (Math.Abs(50 - protocol.PassiMot10)).ToString();
            if ((Convert.ToInt32(lbl_passiMot10.Text) == -1) || (Convert.ToInt32(lbl_passiMot10.Text) > 50))
                lbl_passiMot10.BackColor = Color.Red;
            if (protocol.PassiMot11.Equals(-1))
                lbl_passiMot11.Text = protocol.PassiMot11.ToString();
            else
                lbl_passiMot11.Text = (Math.Abs(50 - protocol.PassiMot11)).ToString();
            if ((Convert.ToInt32(lbl_passiMot11.Text) == -1) || (Convert.ToInt32(lbl_passiMot11.Text) > 50))
                lbl_passiMot11.BackColor = Color.Red;
            if (protocol.PassiMot12.Equals(-1))
                lbl_passiMot12.Text = protocol.PassiMot12.ToString();
            else
                lbl_passiMot12.Text = (Math.Abs(50 - protocol.PassiMot12)).ToString();
            if ((Convert.ToInt32(lbl_passiMot12.Text) == -1) || (Convert.ToInt32(lbl_passiMot12.Text) > 50))
                lbl_passiMot12.BackColor = Color.Red;
        }
    }
}
