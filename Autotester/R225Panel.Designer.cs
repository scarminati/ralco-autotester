﻿namespace Autotester
{
    partial class R225Panel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblChiave = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnErrori = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lblCross = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblLong = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblFilter = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.grpChiave = new System.Windows.Forms.GroupBox();
            this.ledChiave = new Autotester.LedBulb();
            this.grpTastoLuce = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ledTastoLuceActive = new Autotester.LedBulb();
            this.label25 = new System.Windows.Forms.Label();
            this.ledLuce = new Autotester.LedBulb();
            this.ledLuceTest = new Autotester.LedBulb();
            this.grpDSC = new System.Windows.Forms.GroupBox();
            this.ledCrossDSCTest = new Autotester.LedBulb();
            this.ledCrossDSCActive = new Autotester.LedBulb();
            this.ledLongDSCActive = new Autotester.LedBulb();
            this.ledLongDSCTest = new Autotester.LedBulb();
            this.grpVita = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lblIris = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label23 = new System.Windows.Forms.Label();
            this.cmb_tipoTest = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.grpTastoFiltro = new System.Windows.Forms.GroupBox();
            this.ledTastoFiltroActive = new Autotester.LedBulb();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.ledFiltroTest = new Autotester.LedBulb();
            this.grpTastoIride = new System.Windows.Forms.GroupBox();
            this.ledTastoIrideActive = new Autotester.LedBulb();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.ledIrideTest = new Autotester.LedBulb();
            this.lblCollimatore = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblCNC = new System.Windows.Forms.Label();
            this.lblFC = new System.Windows.Forms.Label();
            this.grpTastiMemb = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ledTastiMembActive = new Autotester.LedBulb();
            this.btnDettagliTastiMemb = new System.Windows.Forms.Button();
            this.ledTastiMemb = new Autotester.LedBulb();
            this.label32 = new System.Windows.Forms.Label();
            this.ledError = new Autotester.LedBulb();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPCB = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBootloader = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFirmware = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.grpChiave.SuspendLayout();
            this.grpTastoLuce.SuspendLayout();
            this.grpDSC.SuspendLayout();
            this.grpVita.SuspendLayout();
            this.grpTastoFiltro.SuspendLayout();
            this.grpTastoIride.SuspendLayout();
            this.grpTastiMemb.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(4, 4);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(15, 14);
            this.chkEnable.TabIndex = 0;
            this.chkEnable.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "S/N";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Chiave";
            // 
            // lblChiave
            // 
            this.lblChiave.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChiave.Location = new System.Drawing.Point(9, 76);
            this.lblChiave.Name = "lblChiave";
            this.lblChiave.Size = new System.Drawing.Size(36, 23);
            this.lblChiave.TabIndex = 11;
            this.lblChiave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "DSC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Attivo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Cross";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(38, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Long";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(72, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Test";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1101, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Errori";
            // 
            // btnErrori
            // 
            this.btnErrori.Location = new System.Drawing.Point(1161, 35);
            this.btnErrori.Name = "btnErrori";
            this.btnErrori.Size = new System.Drawing.Size(75, 41);
            this.btnErrori.TabIndex = 23;
            this.btnErrori.Text = "Mostra errori";
            this.btnErrori.UseVisualStyleBackColor = true;
            this.btnErrori.Click += new System.EventHandler(this.btnErrori_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(26, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Tasto Luce";
            // 
            // lblCross
            // 
            this.lblCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCross.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCross.Location = new System.Drawing.Point(74, 28);
            this.lblCross.Name = "lblCross";
            this.lblCross.Size = new System.Drawing.Size(69, 16);
            this.lblCross.TabIndex = 35;
            this.lblCross.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Cross (cm)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLong
            // 
            this.lblLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLong.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLong.Location = new System.Drawing.Point(74, 49);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(69, 16);
            this.lblLong.TabIndex = 37;
            this.lblLong.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Long (cm)";
            // 
            // lblFilter
            // 
            this.lblFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilter.Location = new System.Drawing.Point(74, 70);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(69, 16);
            this.lblFilter.TabIndex = 39;
            this.lblFilter.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "Filtro";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(78, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Test";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "FC";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 13);
            this.label20.TabIndex = 46;
            this.label20.Text = "Collimatore";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(51, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 48;
            this.label15.Text = "Test Vita";
            // 
            // grpChiave
            // 
            this.grpChiave.Controls.Add(this.ledChiave);
            this.grpChiave.Controls.Add(this.label9);
            this.grpChiave.Controls.Add(this.lblChiave);
            this.grpChiave.Enabled = false;
            this.grpChiave.Location = new System.Drawing.Point(757, 0);
            this.grpChiave.Name = "grpChiave";
            this.grpChiave.Size = new System.Drawing.Size(56, 110);
            this.grpChiave.TabIndex = 49;
            this.grpChiave.TabStop = false;
            // 
            // ledChiave
            // 
            this.ledChiave.Location = new System.Drawing.Point(10, 30);
            this.ledChiave.Name = "ledChiave";
            this.ledChiave.On = true;
            this.ledChiave.Size = new System.Drawing.Size(36, 39);
            this.ledChiave.TabIndex = 10;
            this.ledChiave.Text = "ledBulb1";
            // 
            // grpTastoLuce
            // 
            this.grpTastoLuce.Controls.Add(this.label31);
            this.grpTastoLuce.Controls.Add(this.ledTastoLuceActive);
            this.grpTastoLuce.Controls.Add(this.label25);
            this.grpTastoLuce.Controls.Add(this.label13);
            this.grpTastoLuce.Controls.Add(this.ledLuce);
            this.grpTastoLuce.Controls.Add(this.label16);
            this.grpTastoLuce.Controls.Add(this.ledLuceTest);
            this.grpTastoLuce.Enabled = false;
            this.grpTastoLuce.Location = new System.Drawing.Point(377, 0);
            this.grpTastoLuce.Name = "grpTastoLuce";
            this.grpTastoLuce.Size = new System.Drawing.Size(110, 110);
            this.grpTastoLuce.TabIndex = 50;
            this.grpTastoLuce.TabStop = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(43, 37);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 13);
            this.label31.TabIndex = 43;
            this.label31.Text = "Attivo";
            // 
            // ledTastoLuceActive
            // 
            this.ledTastoLuceActive.Color = System.Drawing.Color.Yellow;
            this.ledTastoLuceActive.Location = new System.Drawing.Point(47, 69);
            this.ledTastoLuceActive.Name = "ledTastoLuceActive";
            this.ledTastoLuceActive.On = true;
            this.ledTastoLuceActive.Size = new System.Drawing.Size(25, 23);
            this.ledTastoLuceActive.TabIndex = 44;
            this.ledTastoLuceActive.Text = "ledBulb1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 37);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 13);
            this.label25.TabIndex = 42;
            this.label25.Text = "Stato";
            // 
            // ledLuce
            // 
            this.ledLuce.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLuce.Location = new System.Drawing.Point(3, 63);
            this.ledLuce.Name = "ledLuce";
            this.ledLuce.On = true;
            this.ledLuce.Size = new System.Drawing.Size(36, 39);
            this.ledLuce.TabIndex = 26;
            this.ledLuce.Text = "ledBulb1";
            // 
            // ledLuceTest
            // 
            this.ledLuceTest.Location = new System.Drawing.Point(80, 69);
            this.ledLuceTest.Name = "ledLuceTest";
            this.ledLuceTest.On = true;
            this.ledLuceTest.Size = new System.Drawing.Size(25, 23);
            this.ledLuceTest.TabIndex = 41;
            this.ledLuceTest.Text = "ledBulb4";
            // 
            // grpDSC
            // 
            this.grpDSC.Controls.Add(this.ledCrossDSCTest);
            this.grpDSC.Controls.Add(this.label2);
            this.grpDSC.Controls.Add(this.label3);
            this.grpDSC.Controls.Add(this.label5);
            this.grpDSC.Controls.Add(this.label10);
            this.grpDSC.Controls.Add(this.ledCrossDSCActive);
            this.grpDSC.Controls.Add(this.ledLongDSCActive);
            this.grpDSC.Controls.Add(this.label11);
            this.grpDSC.Controls.Add(this.ledLongDSCTest);
            this.grpDSC.Enabled = false;
            this.grpDSC.Location = new System.Drawing.Point(819, 0);
            this.grpDSC.Name = "grpDSC";
            this.grpDSC.Size = new System.Drawing.Size(103, 110);
            this.grpDSC.TabIndex = 51;
            this.grpDSC.TabStop = false;
            // 
            // ledCrossDSCTest
            // 
            this.ledCrossDSCTest.Location = new System.Drawing.Point(75, 47);
            this.ledCrossDSCTest.Name = "ledCrossDSCTest";
            this.ledCrossDSCTest.On = true;
            this.ledCrossDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSCTest.TabIndex = 19;
            this.ledCrossDSCTest.Text = "ledBulb4";
            // 
            // ledCrossDSCActive
            // 
            this.ledCrossDSCActive.Color = System.Drawing.Color.Yellow;
            this.ledCrossDSCActive.Location = new System.Drawing.Point(7, 47);
            this.ledCrossDSCActive.Name = "ledCrossDSCActive";
            this.ledCrossDSCActive.On = true;
            this.ledCrossDSCActive.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSCActive.TabIndex = 16;
            this.ledCrossDSCActive.Text = "ledBulb1";
            // 
            // ledLongDSCActive
            // 
            this.ledLongDSCActive.Color = System.Drawing.Color.Yellow;
            this.ledLongDSCActive.Location = new System.Drawing.Point(7, 77);
            this.ledLongDSCActive.Name = "ledLongDSCActive";
            this.ledLongDSCActive.On = true;
            this.ledLongDSCActive.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSCActive.TabIndex = 17;
            this.ledLongDSCActive.Text = "ledBulb2";
            // 
            // ledLongDSCTest
            // 
            this.ledLongDSCTest.Location = new System.Drawing.Point(75, 77);
            this.ledLongDSCTest.Name = "ledLongDSCTest";
            this.ledLongDSCTest.On = true;
            this.ledLongDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSCTest.TabIndex = 20;
            this.ledLongDSCTest.Text = "ledBulb3";
            // 
            // grpVita
            // 
            this.grpVita.Controls.Add(this.label30);
            this.grpVita.Controls.Add(this.lblIris);
            this.grpVita.Controls.Add(this.label15);
            this.grpVita.Controls.Add(this.label17);
            this.grpVita.Controls.Add(this.lblCross);
            this.grpVita.Controls.Add(this.label19);
            this.grpVita.Controls.Add(this.lblLong);
            this.grpVita.Controls.Add(this.label21);
            this.grpVita.Controls.Add(this.lblFilter);
            this.grpVita.Enabled = false;
            this.grpVita.Location = new System.Drawing.Point(927, 0);
            this.grpVita.Name = "grpVita";
            this.grpVita.Size = new System.Drawing.Size(154, 110);
            this.grpVita.TabIndex = 52;
            this.grpVita.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(11, 91);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(49, 13);
            this.label30.TabIndex = 49;
            this.label30.Text = "Iride (cm)";
            // 
            // lblIris
            // 
            this.lblIris.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblIris.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIris.Location = new System.Drawing.Point(74, 90);
            this.lblIris.Name = "lblIris";
            this.lblIris.Size = new System.Drawing.Size(69, 16);
            this.lblIris.TabIndex = 50;
            this.lblIris.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 13);
            this.label23.TabIndex = 54;
            this.label23.Text = "Tipo Test";
            // 
            // cmb_tipoTest
            // 
            this.cmb_tipoTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tipoTest.FormattingEnabled = true;
            this.cmb_tipoTest.Items.AddRange(new object[] {
            "Analisi",
            "Produzione",
            "Riparazione",
            "Test"});
            this.cmb_tipoTest.Location = new System.Drawing.Point(69, 51);
            this.cmb_tipoTest.Name = "cmb_tipoTest";
            this.cmb_tipoTest.Size = new System.Drawing.Size(74, 21);
            this.cmb_tipoTest.TabIndex = 55;
            this.cmb_tipoTest.SelectedIndexChanged += new System.EventHandler(this.cmb_tipoTest_SelectedIndexChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(147, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 13);
            this.label24.TabIndex = 56;
            this.label24.Text = "CNC";
            // 
            // grpTastoFiltro
            // 
            this.grpTastoFiltro.Controls.Add(this.ledTastoFiltroActive);
            this.grpTastoFiltro.Controls.Add(this.label14);
            this.grpTastoFiltro.Controls.Add(this.label22);
            this.grpTastoFiltro.Controls.Add(this.label26);
            this.grpTastoFiltro.Controls.Add(this.ledFiltroTest);
            this.grpTastoFiltro.Enabled = false;
            this.grpTastoFiltro.Location = new System.Drawing.Point(491, 0);
            this.grpTastoFiltro.Name = "grpTastoFiltro";
            this.grpTastoFiltro.Size = new System.Drawing.Size(75, 110);
            this.grpTastoFiltro.TabIndex = 57;
            this.grpTastoFiltro.TabStop = false;
            // 
            // ledTastoFiltroActive
            // 
            this.ledTastoFiltroActive.Color = System.Drawing.Color.Yellow;
            this.ledTastoFiltroActive.Location = new System.Drawing.Point(9, 69);
            this.ledTastoFiltroActive.Name = "ledTastoFiltroActive";
            this.ledTastoFiltroActive.On = true;
            this.ledTastoFiltroActive.Size = new System.Drawing.Size(25, 23);
            this.ledTastoFiltroActive.TabIndex = 43;
            this.ledTastoFiltroActive.Text = "ledBulb1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 42;
            this.label14.Text = "Attivo";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 24;
            this.label22.Text = "Tasto Filtro";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(43, 37);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(28, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Test";
            // 
            // ledFiltroTest
            // 
            this.ledFiltroTest.Location = new System.Drawing.Point(46, 69);
            this.ledFiltroTest.Name = "ledFiltroTest";
            this.ledFiltroTest.On = true;
            this.ledFiltroTest.Size = new System.Drawing.Size(25, 23);
            this.ledFiltroTest.TabIndex = 41;
            this.ledFiltroTest.Text = "ledBulb4";
            // 
            // grpTastoIride
            // 
            this.grpTastoIride.Controls.Add(this.ledTastoIrideActive);
            this.grpTastoIride.Controls.Add(this.label27);
            this.grpTastoIride.Controls.Add(this.label28);
            this.grpTastoIride.Controls.Add(this.label29);
            this.grpTastoIride.Controls.Add(this.ledIrideTest);
            this.grpTastoIride.Enabled = false;
            this.grpTastoIride.Location = new System.Drawing.Point(572, 0);
            this.grpTastoIride.Name = "grpTastoIride";
            this.grpTastoIride.Size = new System.Drawing.Size(76, 110);
            this.grpTastoIride.TabIndex = 58;
            this.grpTastoIride.TabStop = false;
            // 
            // ledTastoIrideActive
            // 
            this.ledTastoIrideActive.Color = System.Drawing.Color.Yellow;
            this.ledTastoIrideActive.Location = new System.Drawing.Point(14, 69);
            this.ledTastoIrideActive.Name = "ledTastoIrideActive";
            this.ledTastoIrideActive.On = true;
            this.ledTastoIrideActive.Size = new System.Drawing.Size(25, 23);
            this.ledTastoIrideActive.TabIndex = 44;
            this.ledTastoIrideActive.Text = "ledBulb1";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(8, 37);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 42;
            this.label27.Text = "Attivo";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 13);
            this.label28.TabIndex = 24;
            this.label28.Text = "Tasto Iride";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(43, 37);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 13);
            this.label29.TabIndex = 40;
            this.label29.Text = "Test";
            // 
            // ledIrideTest
            // 
            this.ledIrideTest.Location = new System.Drawing.Point(46, 69);
            this.ledIrideTest.Name = "ledIrideTest";
            this.ledIrideTest.On = true;
            this.ledIrideTest.Size = new System.Drawing.Size(25, 23);
            this.ledIrideTest.TabIndex = 41;
            this.ledIrideTest.Text = "ledBulb4";
            // 
            // lblCollimatore
            // 
            this.lblCollimatore.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCollimatore.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollimatore.Location = new System.Drawing.Point(69, 21);
            this.lblCollimatore.Name = "lblCollimatore";
            this.lblCollimatore.Size = new System.Drawing.Size(189, 20);
            this.lblCollimatore.TabIndex = 59;
            // 
            // lblSN
            // 
            this.lblSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSN.Location = new System.Drawing.Point(184, 52);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(74, 20);
            this.lblSN.TabIndex = 60;
            // 
            // lblCNC
            // 
            this.lblCNC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNC.Location = new System.Drawing.Point(184, 85);
            this.lblCNC.Name = "lblCNC";
            this.lblCNC.Size = new System.Drawing.Size(74, 20);
            this.lblCNC.TabIndex = 61;
            // 
            // lblFC
            // 
            this.lblFC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFC.Location = new System.Drawing.Point(69, 84);
            this.lblFC.Name = "lblFC";
            this.lblFC.Size = new System.Drawing.Size(74, 20);
            this.lblFC.TabIndex = 62;
            // 
            // grpTastiMemb
            // 
            this.grpTastiMemb.Controls.Add(this.label34);
            this.grpTastiMemb.Controls.Add(this.label33);
            this.grpTastiMemb.Controls.Add(this.ledTastiMembActive);
            this.grpTastiMemb.Controls.Add(this.btnDettagliTastiMemb);
            this.grpTastiMemb.Controls.Add(this.ledTastiMemb);
            this.grpTastiMemb.Controls.Add(this.label32);
            this.grpTastiMemb.Enabled = false;
            this.grpTastiMemb.Location = new System.Drawing.Point(653, 0);
            this.grpTastiMemb.Name = "grpTastiMemb";
            this.grpTastiMemb.Size = new System.Drawing.Size(99, 110);
            this.grpTastiMemb.TabIndex = 63;
            this.grpTastiMemb.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(52, 30);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 41;
            this.label34.Text = "Test";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(9, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(34, 13);
            this.label33.TabIndex = 25;
            this.label33.Text = "Attivo";
            // 
            // ledTastiMembActive
            // 
            this.ledTastiMembActive.Color = System.Drawing.Color.Yellow;
            this.ledTastiMembActive.Location = new System.Drawing.Point(14, 49);
            this.ledTastiMembActive.Name = "ledTastiMembActive";
            this.ledTastiMembActive.On = true;
            this.ledTastiMembActive.Size = new System.Drawing.Size(25, 23);
            this.ledTastiMembActive.TabIndex = 26;
            this.ledTastiMembActive.Text = "ledBulb1";
            // 
            // btnDettagliTastiMemb
            // 
            this.btnDettagliTastiMemb.Location = new System.Drawing.Point(16, 77);
            this.btnDettagliTastiMemb.Name = "btnDettagliTastiMemb";
            this.btnDettagliTastiMemb.Size = new System.Drawing.Size(66, 23);
            this.btnDettagliTastiMemb.TabIndex = 11;
            this.btnDettagliTastiMemb.Text = "Dettagli";
            this.btnDettagliTastiMemb.UseVisualStyleBackColor = true;
            this.btnDettagliTastiMemb.Click += new System.EventHandler(this.btnDettagliTastiMemb_Click);
            // 
            // ledTastiMemb
            // 
            this.ledTastiMemb.Location = new System.Drawing.Point(55, 49);
            this.ledTastiMemb.Name = "ledTastiMemb";
            this.ledTastiMemb.On = true;
            this.ledTastiMemb.Size = new System.Drawing.Size(25, 23);
            this.ledTastiMemb.TabIndex = 10;
            this.ledTastiMemb.Text = "ledBulb1";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(1, 11);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(97, 13);
            this.label32.TabIndex = 9;
            this.label32.Text = "Tasti Membrana";
            // 
            // ledError
            // 
            this.ledError.Color = System.Drawing.Color.Red;
            this.ledError.Location = new System.Drawing.Point(1086, 30);
            this.ledError.Name = "ledError";
            this.ledError.On = true;
            this.ledError.Size = new System.Drawing.Size(69, 67);
            this.ledError.TabIndex = 22;
            this.ledError.Text = "ledBulb1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblPCB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblBootloader);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblFirmware);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(263, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 105);
            this.panel1.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Firmware Version";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPCB
            // 
            this.lblPCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCB.Location = new System.Drawing.Point(48, 81);
            this.lblPCB.Name = "lblPCB";
            this.lblPCB.Size = new System.Drawing.Size(55, 16);
            this.lblPCB.TabIndex = 5;
            this.lblPCB.Text = "-1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "PCB";
            // 
            // lblBootloader
            // 
            this.lblBootloader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBootloader.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBootloader.Location = new System.Drawing.Point(48, 53);
            this.lblBootloader.Name = "lblBootloader";
            this.lblBootloader.Size = new System.Drawing.Size(55, 16);
            this.lblBootloader.TabIndex = 3;
            this.lblBootloader.Text = "-1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "BL";
            // 
            // lblFirmware
            // 
            this.lblFirmware.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFirmware.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirmware.Location = new System.Drawing.Point(48, 27);
            this.lblFirmware.Name = "lblFirmware";
            this.lblFirmware.Size = new System.Drawing.Size(55, 16);
            this.lblFirmware.TabIndex = 1;
            this.lblFirmware.Text = "-1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "FW";
            // 
            // R225Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grpTastiMemb);
            this.Controls.Add(this.lblFC);
            this.Controls.Add(this.lblCNC);
            this.Controls.Add(this.lblSN);
            this.Controls.Add(this.lblCollimatore);
            this.Controls.Add(this.grpTastoIride);
            this.Controls.Add(this.grpTastoFiltro);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.cmb_tipoTest);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.grpVita);
            this.Controls.Add(this.grpDSC);
            this.Controls.Add(this.grpTastoLuce);
            this.Controls.Add(this.grpChiave);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btnErrori);
            this.Controls.Add(this.ledError);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkEnable);
            this.Name = "R225Panel";
            this.Size = new System.Drawing.Size(1246, 117);
            this.Tag = "Philips";
            this.Load += new System.EventHandler(this.R225Panel_Load);
            this.grpChiave.ResumeLayout(false);
            this.grpChiave.PerformLayout();
            this.grpTastoLuce.ResumeLayout(false);
            this.grpTastoLuce.PerformLayout();
            this.grpDSC.ResumeLayout(false);
            this.grpDSC.PerformLayout();
            this.grpVita.ResumeLayout(false);
            this.grpVita.PerformLayout();
            this.grpTastoFiltro.ResumeLayout(false);
            this.grpTastoFiltro.PerformLayout();
            this.grpTastoIride.ResumeLayout(false);
            this.grpTastoIride.PerformLayout();
            this.grpTastiMemb.ResumeLayout(false);
            this.grpTastiMemb.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEnable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private LedBulb ledChiave;
        private System.Windows.Forms.Label lblChiave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private LedBulb ledCrossDSCActive;
        private LedBulb ledLongDSCActive;
        private System.Windows.Forms.Label label11;
        private LedBulb ledLongDSCTest;
        private LedBulb ledCrossDSCTest;
        private System.Windows.Forms.Label label12;
        private LedBulb ledError;
        private System.Windows.Forms.Button btnErrori;
        private System.Windows.Forms.Label label13;
        private LedBulb ledLuce;
        private System.Windows.Forms.Label lblCross;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblLong;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label16;
        private LedBulb ledLuceTest;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox grpChiave;
        private System.Windows.Forms.GroupBox grpTastoLuce;
        private System.Windows.Forms.GroupBox grpDSC;
        private System.Windows.Forms.GroupBox grpVita;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmb_tipoTest;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox grpTastoFiltro;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private LedBulb ledFiltroTest;
        private System.Windows.Forms.GroupBox grpTastoIride;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private LedBulb ledIrideTest;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblIris;
        private System.Windows.Forms.Label label31;
        private LedBulb ledTastoLuceActive;
        private LedBulb ledTastoFiltroActive;
        private LedBulb ledTastoIrideActive;
        private System.Windows.Forms.Label lblCollimatore;
        private System.Windows.Forms.Label lblSN;
        private System.Windows.Forms.Label lblCNC;
        private System.Windows.Forms.Label lblFC;
        private System.Windows.Forms.GroupBox grpTastiMemb;
        private System.Windows.Forms.Button btnDettagliTastiMemb;
        private LedBulb ledTastiMemb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private LedBulb ledTastiMembActive;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBootloader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFirmware;
        private System.Windows.Forms.Label label7;
    }
}
