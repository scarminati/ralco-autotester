﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Autotester
{

    public class P225ProtocolAnalyzer : IProtocolDataChanges<ProtocolChange>, IProtocolAnalyzer
    {
        private CANusb_Connector connector = null;

        private Timer timeoutTimer;
        public string protocolName { get { return "CAN-CAL"; } }
        // stato interno collimatore
        private int status;
        private int currentMsg;

        // attributi collimatore
        private bool collimationDone;
        private bool collimationFailed;
        private bool dscCrossActive;
        private bool dscLongActive;

        private bool crossJam;
        private bool longJam;
        private bool filterJam;
        private bool crossCalib;
        private bool longCalib;
        private bool filterCalib;
        private bool crossPot;
        private bool longPot;

        //errori aggiunti

        private bool rom_failed;
        private bool ram_failed;
        private bool wd_failed;
        private bool can_failed;
        private bool power_failed;
        private bool power_xl_failed;
        private bool hsi_not_supported;
        private bool sw_exception;
        private bool hw_wd_timeout;
        private bool unexpected_nmi;
        private bool unexpected_int;
        private bool unexpected_input;
        private bool sw_wd_timeout;
        private bool stack_underflow;
        private bool stack_overflow;

        private bool ext_rx_overrun;
        private bool ext_tx_overrun;
        private bool rx_queue_overrun;
        private bool tx_queue_overrun;
        private bool can_error;
        private bool guarding_again;

        private bool no_hw_access;
        private bool no_current;
        private bool excessive_position;
        private bool range_check_failed;
        private bool time_out;
        private bool excessive_time;
        private bool mcu_time_out;

        private bool calibration_failed;

        private bool temp_sensor_fault;
        private bool defect;

        private bool light;
        private int meter;
        private int maxSpeed;
        private int filter;
        private int crossPos;
        private int longPos;
        private int temperature;

        private int crossEncoder;
        private int longEncoder;

        private string fwCollimator;
        private string fwBootloader;
        private string pcbCollimator;

        private bool heartbeatStatus;
        private bool heartbeatError;

        public int passiLong { get; set; }
        public int deltaPotenziometriLong { get; set; }
        public int deltaEncodersLong { get; set; }
        public int passiCross { get; set; }
        public int deltaPotenziometriCross { get; set; }
        public int deltaEncodersCross { get; set; }
        public int tentativiPosFiltro { get; set; }

        public bool error0 { get; set; }
        public bool error90 { get; set; }
        public bool error180 { get; set; }
        public bool error270 { get; set; }
        public int filterWhenError { get; set; }
        public int crossPosWhenError { get; set; }
        public int longPosWhenError { get; set; }
        public int passiIride { get; set; }
        public int passiSpatialFilter { get; set; }

        public int SpatialFilter1_Pos { get; } //spostamento filtro spaziale 1
        public int SpatialFilter1_Rot { get; } //rotazione filtro spaziale 1
        public int IrisPos { get; } //spostamento iride

        public bool TastiMembrActive { get; set; }
        public bool TestTastiMemb { get; set; }


        private int _counterIteration;

        public int CounterIteration
        {
            get { return _counterIteration; }
            set
            {
                if (this._counterIteration != value)
                {
                    this._counterIteration = value;
                    emitChanges(ProtocolChange.Counter);
                }

            }
        }

        private uint[,] m = new uint[,] {
              {2026, 0x01, 0x42, 0x4C, 0x44, 0x31, 0x44, 0x56, 0x31},
              {2025, 0x01, 0xE8, 0x03, 0x06, 0x02, 0xFF, 0x00, 0x00},
              {2026, 0x02, 0x04, 0xE4, 0x06, 0xE8, 0x03, 0x06, 0x02},
              {2025, 0x02, 0x04, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00},
              {2026, 0x03, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x02, 0x23, 0x42, 0x4C, 0x44, 0x31, 0x43, 0x54},
              {2024, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x03, 0x52, 0x4C, 0x31, 0x73, 0x63, 0x73, 0x58},
              {2024, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x04, 0x04, 0x08, 0x00, 0x02, 0x02, 0xF4, 0x01},
              {2024, 0x04, 0x00, 0x00, 0xC7, 0x01, 0x02, 0x00, 0x00},
              {2023, 0x02, 0x23, 0x42, 0x4C, 0x44, 0x31, 0x43, 0x54},
              {2024, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x03, 0x52, 0x4C, 0x5F, 0x73, 0x63, 0x73, 0x58},
              {2024, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x04, 0x04, 0x08, 0x00, 0x02, 0x02, 0xF4, 0x01},
              {2024, 0x04, 0x00, 0x00, 0xC6, 0x01, 0x02, 0x00, 0x00},
              {2023, 0x02, 0x23, 0x42, 0x4C, 0x44, 0x31, 0x43, 0x54},
              {2024, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x03, 0x52, 0x4C, 0x31, 0x66, 0x63, 0x73, 0x58},
              {2024, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x04, 0x04, 0x08, 0x01, 0x02, 0x02, 0xF4, 0x01},
              {2024, 0x04, 0x00, 0x00, 0xC8, 0x01, 0x02, 0x00, 0x00},
              {2023, 0x02, 0x23, 0x42, 0x4C, 0x44, 0x31, 0x4E, 0x6F},
              {2024, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x03, 0x64, 0x65, 0x5F, 0x53, 0x44, 0x4F, 0x43},
              {2024, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x04, 0x04, 0x08, 0x00, 0x01, 0x07, 0x0A, 0x00},
              {2024, 0x04, 0x00, 0x00, 0x84, 0x05, 0x06, 0x00, 0x00},
              {2023, 0x02, 0x23, 0x42, 0x4C, 0x44, 0x31, 0x4E, 0x6F},
              {2024, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x03, 0x64, 0x65, 0x5F, 0x53, 0x44, 0x4F, 0x53},
              {2024, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
              {2023, 0x04, 0x04, 0x08, 0x01, 0x04, 0x07, 0x0A, 0x00},
              {2024, 0x04, 0x00, 0x00, 0x04, 0x06, 0x06, 0x00, 0x00},
              {2025, 0x03, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
              };

        public bool CollimationDone
        {
            get { return collimationDone; }
        }

        public bool CollimationFailed
        {
            get { return collimationFailed; }
        }

        public bool DscCrossActive
        {
            get { return dscCrossActive; }
        }

        public bool DscLongActive
        {
            get { return dscLongActive; }
        }

        public bool CrossJam
        {
            get { return crossJam; }
        }

        public bool LongJam
        {
            get { return longJam; }
        }

        public bool FilterJam
        {
            get { return filterJam; }
        }

        public bool CrossCalib
        {
            get { return crossCalib; }
        }

        public bool LongCalib
        {
            get { return longCalib; }
        }

        public bool FilterCalib
        {
            get { return filterCalib; }
        }

        //non implementato nel protocollo P225...
        public bool POSTerror
        { get { return false; } }

        public bool CrossPot
        {
            get { return crossPot; }
        }

        public bool LongPot
        {
            get { return longPot; }
        }

        public bool Light
        {
            get { return light; }
        }

        public int Meter
        {
            get { return meter; }
        }

        public int MaxSpeed
        {
            get { return maxSpeed; }
        }

        public int Filter
        {
            get { return filter; }
        }

        public int CrossPos
        {
            get { return crossPos; }
        }

        public int LongPos
        {
            get { return longPos; }
        }

        public int Temperature
        {
            get { return temperature; }
        }

        public int CrossEncoder
        {
            get { return crossEncoder; }
        }

        public int LongEncoder
        {
            get { return longEncoder; }
        }

        public string FwCollimator
        {
            get { return fwCollimator; }
        }

        public string FwBootloader
        {
            get { return fwBootloader; }
        }

        public string PcbCollimator
        {
            get { return pcbCollimator; }
        }

        public bool HeartbeatStatus
        {
            get { return heartbeatStatus; }
        }

        public bool HeartbeatError
        {
            get { return heartbeatError; }
        }

        public bool generalError
        {
            get { return (rom_failed | ram_failed | wd_failed | can_failed | power_failed | power_xl_failed | hsi_not_supported | sw_exception | hw_wd_timeout | unexpected_nmi | unexpected_int | unexpected_input | sw_wd_timeout | stack_underflow | stack_overflow); }
        }

        public bool communicationError
        {
            get { return (ext_rx_overrun | ext_tx_overrun | rx_queue_overrun | tx_queue_overrun | can_error | guarding_again); }
        }

        public bool AEPError
        {
            get { return calibration_failed; }
        }

        public List<CalibTable> CalibTable { get; }

        public CANusb_Connector Connector
        {
            get { return connector; }
            set
            {
                if (value == null)
                {
                    if (this.connector != null)
                    {
                        connector.Handler -= ConnectorOnHandler();
                    }
                    connector = null;

                }
                else
                {
                    connector = value;
                    connector.Handler += ConnectorOnHandler();                    
                }
            }
        }

        public testCollimatore testColl { get; set; }

        public void setDBTmessages(string deviceName)
        {
           if (deviceName.Equals("P 225/078/ACS DHHS"))
            {
                m[0, 5] = 0x32;
                m[5, 6] = 0x32;
                m[11, 6] = 0x32;
                m[17, 6] = 0x32;
                m[23, 6] = 0x32;
                m[29, 6] = 0x32;
            }
            else
            {
                m[0, 5] = 0x31;
                m[5, 6] = 0x31;
                m[11, 6] = 0x31;
                m[17, 6] = 0x31;
                m[23, 6] = 0x31;
                m[29, 6] = 0x31;
            }
        }


        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate(CANCommand command) { this.processMessage(command); };
        }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<ProtocolChange> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<ProtocolChange> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<ProtocolChange> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<ProtocolChange>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */

        public void timeoutReached(Object param)
        {
            if (status == 3)
            {
                status = 1;
                currentMsg = 0;
                sendDbtMsg();
            }
            
        }

        private void sendDbtMsg()
        {
            CANCommand msg = new CANCommand(m[currentMsg, 0], 0, 8, (byte)m[currentMsg, 1], (byte)m[currentMsg, 2], (byte)m[currentMsg, 3], (byte)m[currentMsg, 4],
                (byte)m[currentMsg, 5], (byte)m[currentMsg, 6], (byte)m[currentMsg, 7], (byte)m[currentMsg, 8]);
            connector.enqueueMessage(msg);
            currentMsg++;
        }


        public void initCollimator()
        {
            status = 3;
            CANCommand msg = new CANCommand(0x1C7, 0, 8, 0x73, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            timeoutTimer = new Timer(this.timeoutReached, null, 1000, Timeout.Infinite);

        }

        private void processDbtMsg(CANCommand msg)
        {
            if (msg.getDLC() == 1)
            {
                // e' il messaggio di heartbeat. lo ignoro
                return;
            }
            if (msg.getID() != m[currentMsg, 0])
            {
                emitChanges(ProtocolChange.DBTError);
                status = -1;
            }
            else
            {
                //LAWICEL.SplitUlong val;
                //val.data = 0;
                //val.d0 = (byte)m[currentMsg, 1];
                //val.d1 = (byte)m[currentMsg, 2];
                //val.d2 = (byte)m[currentMsg, 3];
                //val.d3 = (byte)m[currentMsg, 4];
                //val.d4 = (byte)m[currentMsg, 5];
                //val.d5 = (byte)m[currentMsg, 6];
                //val.d6 = (byte)m[currentMsg, 7];
                //val.d7 = (byte)m[currentMsg, 8];

                /*
                if (msg.dato.data != val.data)
                {
                    MessageBox.Show("Ricevuto messaggio non aspettato", "DBT config", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    status = -1;
                }
                 * */
            }

            if (status == 1)
            {
                currentMsg++;
                if (currentMsg < 36)
                {
                    sendDbtMsg();
                }
                else
                {
                    sendNmtStart();
                }
            }
        }

        public void readCalibTable()
        {

        }

        public void setCollimatorForShipment()
        {
            //lamelle tutte chiuse
            CANCommand msg = new CANCommand(0x1C6, 0, 8, 0x01, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFE);
            connector.enqueueMessage(msg);
            //filtro in posizione 0
            msg = new CANCommand(0x1C6, 0, 8, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            //Luce Spenta
            msg = new CANCommand(0x1C6, 0, 8, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
        }

        public void playSoundEndTest()
        {

        }

        private void sendNmtStart()
        {
            status = 2;
            emitChanges(ProtocolChange.ConfigurationComplete);
            this.filterWhenError = -1;
            this.crossPosWhenError = -1;
            this.longPosWhenError = -1;
            CANCommand msg = new CANCommand(0, 0, 2, 1, 4, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);

            sendVersionRequests();
            prepareTestPassi();
            
            //msg = new CANCommand(0x1C7, 0, 8, 0x73, 0, 0, 0, 0, 0, 0, 0);
            //connector.enqueueMessage(msg);
        }

        private void processMessage(CANCommand msg)
        {
            if (status == 1)
            {
                processDbtMsg(msg);
            } 
            else if (status == 3)
            {
                if (msg.getID() == 0x1C8 && msg.getDatum(0) == 0x75)
                {
                    emitChanges(ProtocolChange.ConfigurationComplete);
                    status = 2;
                    processReadMsg(msg);
                }
            } 
            else if (status == 2)
            {
                processReadMsg(msg);
            }
        }

        private void processReadMsg(CANCommand msg)
        {
            bool bLedCross = false;
            bool bLedLong = false;
            bool bLedIris = false;
            bool bLedFiltro = false;
            bool bLedLuce = false;
            bool bLedMetro = false;
            bool bLedAEP = false;
            bool bLedGeneral = false;
            bool bLedCommunication = false;
            bool bLedFan = false;
            bool bLedTemp0 = false;
            bool bLedTemp1 = false;
            bool bLedTemp2 = false;

            Int16 enc;
            short temp;

            //Console.WriteLine("###DEBUG : Messaggio letto = " + msg.id + " " + msg.len + " " + msg.dato.d0 + " " + msg.dato.d1 + " " + msg.dato.d2 + " " + msg.dato.d3 + " " + msg.dato.d4 + " " + msg.dato.d5 + " " + msg.dato.d6 + " " + msg.dato.d7);

            if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x08))
            {
                switch (msg.getDatum(0))
                {
                    case 0x7E:
                        switch (msg.getDatum(1))
                        {
                            case 0x50:
                                //EV_PASSI_PHOTO
                                if (msg.getDatum(2) == 1)
                                {
                                    this.passiCross = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                    if (this.passiCross.Equals(-1) || Math.Abs(this.passiCross -50) > 50)
                                        bLedCross = true;
                                }  
                                else if (msg.getDatum(2) == 2)
                                {
                                    this.passiLong = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                    if (this.passiLong.Equals(-1) || Math.Abs(this.passiLong - 50) > 50)
                                        bLedLong = true;
                                }
                                //emitChanges(ProtocolChange.TestPassi);
                                if (bLedCross | bLedLong)
                                    emitChanges(ProtocolChange.Errors);
                                break;

                            case 0x51:
                                //EV_POT_DELTA
                                if (msg.getDatum(2) == 1)
                                    this.deltaPotenziometriCross = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                else if (msg.getDatum(2) == 2)
                                    this.deltaPotenziometriLong = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                //emitChanges(ProtocolChange.Potentiometers);
                                break;

                            case 0x52:
                                //EV_ENCODER_DELTA
                                if (msg.getDatum(2) == 1)
                                    this.deltaEncodersCross = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                else if (msg.getDatum(2) == 2)
                                    this.deltaEncodersLong = msg.getDatum(7) << 24 | msg.getDatum(7) << 16 | msg.getDatum(6) << 8 | msg.getDatum(5);
                                //emitChanges(ProtocolChange.Encoders);
                                break;
                        }
                        break;

                    case 0x70:
                        switch (msg.getDatum(1))
                        {
                            case 7:
                                if (msg.getDatum(2) == 4)
                                {
                                    collimationDone = true;
                                    collimationFailed = false;
                                }
                                else if (msg.getDatum(2) == 8)
                                {
                                    collimationDone = false;
                                    collimationFailed = true;
                                }
                                emitChanges(ProtocolChange.Shutters);
                                break;

                            case 8:
                                switch (msg.getDatum(2))
                                {
                                    case 11:
                                        dscCrossActive = true;
                                        collimationDone = false;
                                        collimationFailed = false;
                                        break;

                                    case 12:
                                        dscCrossActive = false;
                                        break;

                                    case 13:
                                        dscLongActive = true;
                                        collimationDone = false;
                                        collimationFailed = false;
                                        break;

                                    case 14:
                                        dscLongActive = false;
                                        break;
                                };
                                //if (ledDscLong.On || ledDscCross.On)
                                //{
                                //    tmrEncoder.Start();
                                //}
                                //else
                                //{
                                //    tmrEncoder.Stop();
                                //}
                                emitChanges(ProtocolChange.DSC);
                                break;
                        };
                        break;

                    case 0x71:
                        switch (msg.getDatum(2))//NI_AREA_TYPE
                        {
                            case 0://NI_SHUTTER_AREA 
                                bLedCross = true;
                                bLedLong = true;
                                break;

                            case 1://NI_SHUTTER_X_AREA 
                                bLedCross = true;
                                break;

                            case 2://NI_SHUTTER_Y_AREA 
                                bLedLong = true;
                                break;

                            case 3://NI_SHUTTER_I_AREA 
                                bLedIris = true;
                                break;

                            case 0x20://NI_FILTER_AREA
                                bLedFiltro = true;
                                break;

                            case 0x30://NI_XLIGHT_AREA
                                bLedLuce = true;
                                break;

                            case 0x50://NI_RULER_AREA 
                                bLedMetro = true;
                                break;

                            case 0x60://NI_AEP_AREA 
                                bLedAEP = true;
                                break;

                            case 0x70://NI_GENERAL_AREA 
                                bLedGeneral = true;
                                break;

                            case 0x80://NI_COMMUNICATION_AREA 
                                bLedCommunication = true;
                                break;

                            case 0x90://NI_FAN_AREA 
                                bLedFan = true;
                                break;

                            case 0xA0://NI_TEMPERATURE_AREA 
                                bLedTemp0 = true;
                                break;

                            case 0xA1://NI_TEMPERATURE_AREA1
                                bLedTemp1 = true;
                                break;

                            case 0xA2://NI_TEMPERATURE_AREA2
                                bLedTemp2 = true;
                                break;
                        };

                        switch (msg.getDatum(1))//ERROR_ID
                        {
                            case 1:
                                if (bLedGeneral)
                                    rom_failed = true;
                                break;
                            case 2:
                                if (bLedGeneral)
                                    ram_failed = true;
                                break;
                            case 3:
                                if (bLedGeneral)
                                    wd_failed = true;
                                break;
                            case 4:
                                if (bLedGeneral)
                                    can_failed = true;
                                break;
                            case 5:
                                if (bLedGeneral)
                                    power_failed = true;
                                break;
                            case 6:
                                if (bLedGeneral)
                                    power_xl_failed = true;
                                break;
                            case 7:
                                if (bLedGeneral)
                                    hsi_not_supported = true;
                                break;
                            case 8:
                                if (bLedGeneral)
                                    sw_exception = true;
                                break;
                            case 11:
                                if (bLedGeneral)
                                    hw_wd_timeout = true;
                                break;
                            case 12:
                                if (bLedGeneral)
                                    unexpected_nmi = true;
                                break;
                            case 13:
                                if (bLedGeneral)
                                    unexpected_int = true;
                                break;
                            case 14:
                                if (bLedGeneral)
                                    unexpected_input = true;
                                break;
                            case 15:
                                if (bLedGeneral)
                                    sw_wd_timeout = true;
                                break;
                            case 16:
                                if (bLedGeneral)
                                    stack_underflow = true;
                                break;
                            case 17:
                                if (bLedGeneral)
                                    stack_overflow = true;
                                break;
                            case 21:
                                if (bLedCommunication)
                                    ext_rx_overrun = true;
                                break;
                            case 22:
                                if (bLedCommunication)
                                    ext_tx_overrun = true;
                                break;
                            case 23:
                                if (bLedCommunication)
                                    rx_queue_overrun = true;
                                break;
                            case 24:
                                if (bLedCommunication)
                                    tx_queue_overrun = true;
                                break;
                            case 25:
                                if (bLedCommunication)
                                    can_error = true;
                                break;
                            case 26:
                                if (bLedCommunication)
                                    guarding_again = true;
                                break;
                            case 32://NO_ENCODER_COUNT
                                if (bLedCross)
                                {
                                    crossPot = true;
                                }
                                if (bLedLong)
                                {
                                    longPot = true;
                                }
                                break;
                            case 34://MOTOR_JAMMED
                                if (bLedCross)
                                {
                                    crossJam = true;
                                }
                                if (bLedLong)
                                {
                                    longJam = true;
                                }
                                if (bLedFiltro)
                                {
                                    filterJam = true;
                                }
                                break;

                            case 35://MOTOR_CALIBRATION
                                if (bLedCross)
                                {
                                    crossCalib = true;
                                }
                                if (bLedLong)
                                {
                                    longCalib = true;
                                }
                                if (bLedFiltro)
                                {
                                    filterCalib = true;
                                }
                                break;
                            case 41:
                                if (bLedAEP)
                                    calibration_failed = true;
                                break;
                        }

                        emitChanges(ProtocolChange.Errors);
                        break;

                    case 0x30:
                        light = (msg.getDatum(1) == 1);
                        emitChanges(ProtocolChange.Light);
                        break;

                    case 0x50:
                    case 0x51:
                        meter = msg.getDatum(2) << 8 | msg.getDatum(1);
                        emitChanges(ProtocolChange.Meter);
                        break;

                    case 0x58:
                        maxSpeed = msg.getDatum(2) << 8 | msg.getDatum(1);
                        emitChanges(ProtocolChange.Info);
                        break;

                    case 0x20:
                        filter = msg.getDatum(1);
                        emitChanges(ProtocolChange.Filter);
                        break;

                    case 0x00:
                        crossPos = msg.getDatum(2) << 8 | msg.getDatum(1);
                        longPos = msg.getDatum(4) << 8 | msg.getDatum(3);
                        emitChanges(ProtocolChange.Shutters);
                        break;

                    case 0x72:
                        fwCollimator = msg.getDatum(1).ToString() + "." + msg.getDatum(2).ToString("00") + "." + msg.getDatum(3).ToString("00");                        
                        //LAWICEL.sendMessage(hCanBus, 0x1C7, 0, 8, 0x76, 0, 0, 0, 0, 0, 0, 0);
                        emitChanges(ProtocolChange.Info);
                        break;

                    case 0x74:
                        temperature = msg.getDatum(1);
                        emitChanges(ProtocolChange.Temperature);
                        break;

                    case 0x75:
                        pcbCollimator = msg.getDatum(1).ToString();
                        emitChanges(ProtocolChange.Info);

                        //LAWICEL.sendMessage(hCanBus, 0x1C7, 0, 8, 0x70, 0, 0, 0, 0, 0, 0, 0);
                        break;

                    case 0x77:
                        fwBootloader = msg.getDatum(1).ToString() + "." + msg.getDatum(2).ToString("00") + "." + msg.getDatum(3).ToString("00");
                        emitChanges(ProtocolChange.Info);
                        break;

                    case 0x79:
                        break;

                    //case 0x0C:
                    //    crossEncoder = msg.getDatum(2) << 8 | msg.getDatum(1);
                    //    //logger.Debug("Cross DSC Encoder= {0}", enc);
                    //    emitChanges(ProtocolChange.Encoders);
                    //    break;

                    //case 0x0D:
                    //    longEncoder = msg.getDatum(2) << 8 | msg.getDatum(1);
                    //    emitChanges(ProtocolChange.Encoders);
                    //    //logger.Debug("Long DSC Encoder= {0}", enc);
                    //    break;

                };
            }
            else if (msg.getID() == 0x6E4)
            {
                //switch(msg.dato.d0) {)
                if ((msg.getDatum(0) & 0x80) == 0x80)
                {
                    if (heartbeatStatus)
                    {
                        heartbeatError = true;
                    }
                    heartbeatStatus = true;
                }
                else
                {
                    if (!heartbeatStatus)
                    {
                        heartbeatError = true;
                    }
                    heartbeatStatus = false;
                }
                emitChanges(ProtocolChange.Heartbeat);
            }
        }

        private void emitChanges(ProtocolChange value)
        {
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
            {
                LogManager.GetLogger(connector.Logger.Name).Warn(connector.Logger.Name + " processor messages not available ");
            }
        }

        public void sendLightCommand(bool status)
        {
            CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x06), 0, 8, 0x31, (byte)(status ? 1 : 0), 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
        }

        public void sendLaserCommand(byte laser, bool status)
        {
            CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x06), 0, 8, 0x34, laser, (byte)(status ? 1 : 0), 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);            
        }

        public void sendShutterOpenCommand()
        {
            CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x06), 0, 8, 0x01, 0xFF, 0xFD , 0x07, 0, 0xFF, 0xFF, 0xFE);
            connector.enqueueMessage(msg);
        }

        public void sendVersionRequests()
        {

            CANCommand msg = new CANCommand(0x1C7, 0, 8, 0x76, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            
            msg = new CANCommand(0x1C7, 0, 8, 0x70, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);

            msg = new CANCommand(0x1C7, 0, 8, 0x73, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
        }

        private void prepareTestPassi()
        {
            CANCommand msg = new CANCommand(0x1C7, 0, 8, 0x7F, 0x01, 0x48, 0x59, 0x26, 0x37, 0, 0);
            connector.enqueueMessage(msg);

            msg = new CANCommand(0x1C6, 0, 8, 0x00, 0, 0x02, 0, 0x02, 0, 0, 0);
            connector.enqueueMessage(msg);

        }
    }
}
