﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormS6055Errors : Form
    {
        public FormS6055Errors()
        {
            InitializeComponent();
        }

        public void setErrors(S605ProtocolAnalyzer protocol)
        {

            ledPOST.On = protocol.POSTerror;

            ledErr0.On = protocol.error0;
            ledErr90.On = protocol.error90;
            ledErr180.On = protocol.error180;
            ledErr270.On = protocol.error270;

            ledSpFiltJam.On = protocol.SpFiltJam;
            ledIrisJam.On = protocol.IrisJam;

            if (protocol.passiSpatialFilter.Equals(-1))
                lbl_deltaPassiLamelle.Text = protocol.passiSpatialFilter.ToString();
            else
                lbl_deltaPassiLamelle.Text = (Math.Abs(200 - protocol.passiSpatialFilter)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiLamelle.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiLamelle.Text) > 200))
                lbl_deltaPassiLamelle.BackColor = Color.Red;
            if (protocol.passiIride.Equals(-1))
                lbl_deltaPassiIride.Text = protocol.passiIride.ToString();
            else
                lbl_deltaPassiIride.Text = (Math.Abs(50 - protocol.passiIride)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiIride.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiIride.Text) > 50))
                lbl_deltaPassiIride.BackColor = Color.Red;
        }
    }
}
