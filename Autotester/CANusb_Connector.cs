﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using Lawicel;
using System.Windows.Forms;
using System.Timers;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace Autotester
{
    public class CANusb_Connector : ICanReaderDelegate<CANCommand>
    {
        //private string connectorName;
        private Logger logger;
        private Thread CANusb_Reading_Thread = null;
        private Thread CANusb_Writing_Thread = null;
        private DateTime CANusb_startingConnectionTime;
        private TimeSpan CANusb_ElapsedTime;
        private CANCommand cmd = new CANCommand();//generico messaggio da inviare

        private bool killListener = false;

        private ManualResetEvent msgsSendWait = new ManualResetEvent(false);
        private ConcurrentQueue<CANCommand> msgsToSendQueue = null;

        public Logger Logger
        {
            get { return logger; }
        }

        public uint connectorID { get; set; }
        public string connectorName { get; set; }
        public string baudRate { get; set; }
        public bool isConnected { get; set; }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<CANCommand> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<CANCommand> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<CANCommand> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<CANCommand> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<CANCommand>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<CANCommand> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<CANCommand> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<CANCommand> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */

        public DateTime getConnectorStartingTime()
        {
            return (this.CANusb_startingConnectionTime);
        }

        public TimeSpan getConnectorElapsedTime()
        {
            return (this.CANusb_ElapsedTime);
        }

        //Configurazione File LOG
        private void nlogConfiguration(string interfaceName)
        {
        //    // Step 1. Create configuration object 
        //    var config = new LoggingConfiguration();

        //    // Step 2. Create targets and add them to the configuration 

        //    var fileTarget = new FileTarget();
        //    config.AddTarget("file", fileTarget);
        //    // Step 3. Set target properties 
        //    string dir = Environment.CurrentDirectory + @"\logs\";
        //    fileTarget.FileName = dir + "log_Events_" + getConnectorName() + "_" + DateTime.Now.ToString(@"ddMMyy_HHmmss") + ".txt";
        //    fileTarget.Layout = "${date:format=yyyy/MM/dd HH.mm.ss.ffffff} | ${message}";
        //    fileTarget.ArchiveEvery = FileArchivePeriod.Day;
        //    fileTarget.ArchiveNumbering = ArchiveNumberingMode.Rolling;
        //    fileTarget.MaxArchiveFiles = 60;
        //    // Step 4. Define rules

        //    var rule2 = new LoggingRule("*", LogLevel.Trace, fileTarget);
        //    config.LoggingRules.Add(rule2);

        //    // Step 5. Activate the configuration
        //    LogManager.Configuration = config;

            logger = LogManager.GetLogger("Autotester.canusb." + interfaceName);
        }


        /*Gestione Dispositivi CANusb*/

        public void CANusb_connect(string baudRate)
        {
            this.connectorID = (CANUSB.canusb_Open(connectorName,
            baudRate,
            CANUSB.CANUSB_ACCEPTANCE_CODE_ALL,
            CANUSB.CANUSB_ACCEPTANCE_MASK_ALL,
            CANUSB.CANUSB_FLAG_TIMESTAMP));
            if (this.connectorID > 0)
            {
                killListener = false;
                CANusb_startingConnectionTime = DateTime.Now;
                nlogConfiguration(connectorName);
                this.CANusb_Reading_Thread = new Thread(new ThreadStart(this.threadPoolingMsg));//timer usato per la lettura dei messaggi lungo il canale CANBUS

                CANusb_Reading_Thread.IsBackground = true;
                CANusb_Reading_Thread.Start();

                this.msgsToSendQueue = new ConcurrentQueue<CANCommand>();
                this.msgsSendWait.Reset();
                this.CANusb_Writing_Thread = new Thread(new ThreadStart(this.processSendMessage));
                CANusb_Writing_Thread.IsBackground = true;
                this.CANusb_Writing_Thread.Start();
            }
        }

        public void CANusb_disconnect()
        {
            if (CANusb_Reading_Thread == null && CANusb_Writing_Thread == null)
                this.connectorID = 1;

            killListener = true;

            if (CANusb_Reading_Thread  != null)
                CANusb_Reading_Thread.Interrupt();
            CANusb_Reading_Thread = null;

            this.msgsSendWait.Set();
            if (CANusb_Writing_Thread != null)
                CANusb_Writing_Thread.Interrupt();

            CANusb_Writing_Thread = null;

            this.connectorID = (uint) CANUSB.canusb_Close(this.connectorID);
        }

        /*Timer*/

        private void threadPoolingMsg()
        {
            CANusb_ElapsedTime = DateTime.Now.Subtract(getConnectorStartingTime());
            int iCont = 1;
            // Read one CANData message
            CANCommand ret = new CANCommand();

            try
            {
                while (true && !killListener)
                {
                    ret = readMessage(this.connectorID);

                    if (CANUSB.ERROR_CANUSB_OK == ret.getRetCode())
                    {
                        logger.Trace(logger.Name + " Rx | " + "0x" + ret.getID().ToString("X3") + " " + ret.getDLC().ToString() + " " + "0x" + ret.getDatum(0).ToString("X2") + " " + "0x" + ret.getDatum(1).ToString("X2") + " " + "0x" + ret.getDatum(2).ToString("X2") + " " + "0x" + ret.getDatum(3).ToString("X2") + " " + "0x" + ret.getDatum(4).ToString("X2") + " " + "0x" + ret.getDatum(5).ToString("X2") + " " + "0x" + ret.getDatum(6).ToString("X2") + " " + "0x" + ret.getDatum(7).ToString("X2"));
                        //analisi del messaggio
                        //analyzeMessage(ret);
                        CheckDisposed();

                        var current = _delegates;
                        if (current != null)
                        {
                            current(ret);
                        }
                        else
                        {
                            logger.Warn(logger.Name + " processor messages not available ");
                        }

                    }
                    else if (CANUSB.ERROR_CANUSB_NO_MESSAGE == ret.getRetCode())
                    {
                        iCont = 0;
                    }
                    else
                    {
                        iCont = 0;
                        //ReadLabel.Text = "Failed to read message.";
                    }

                    Thread.Sleep(10);
                }
                
            }
            catch (ThreadInterruptedException ex)
            {
                logger.Trace(logger.Name + " Thread send halted");
            }

        }

        /*PURPOSE : reads messages retrieved by the CANusb connector*/
        private CANCommand readMessage(uint handle)
        {
            CANCommand ret = new CANCommand();
            CANUSB.CANMsg msg = new CANUSB.CANMsg();
            msg.data = ret.getDataUlong();
            int rv = CANUSB.canusb_Read(handle, out msg);
            if (CANUSB.ERROR_CANUSB_OK == rv)
            {
                ret.setRetCode(rv);
                ret.setID(msg.id);
                ret.setFlags(msg.flags);
                ret.setDLC(msg.len);
                ret.setData(msg.data);
                ret.cleanData();
                ret.setTimeStamp(msg.timestamp);
            }
            return ret;
        }

        public void enqueueMessage(CANCommand msgSend)
        {
            //logger.Info("request enqueue message");
            if (this.msgsToSendQueue != null)
            {
                //logger.Trace(logger.Name + " Enqueue message: | " + "0x" + msgSend.getID().ToString("X3") + " " + msgSend.getDLC().ToString() + " " + "0x" + msgSend.getDatum(0).ToString("X2") + " " + "0x" + msgSend.getDatum(1).ToString("X2") + " " + "0x" + msgSend.getDatum(2).ToString("X2") + " " + "0x" + msgSend.getDatum(3).ToString("X2") + " " + "0x" + msgSend.getDatum(4).ToString("X2") + " " + "0x" + msgSend.getDatum(5).ToString("X2") + " " + "0x" + msgSend.getDatum(6).ToString("X2") + " " + "0x" + msgSend.getDatum(7).ToString("X2"));
                this.msgsToSendQueue.Enqueue(msgSend);
                
                this.msgsSendWait.Set();
            }
                
        }

        private void processSendMessage()
        {
            if (this.msgsToSendQueue == null)
            {
                CANusb_Writing_Thread.Abort();
                return;
            }

            CANCommand msg;

            try
            {
                while (true && !killListener)
                {
                    this.msgsSendWait.WaitOne();
                    
                    while (msgsToSendQueue.TryDequeue(out msg))
                    {
                        this.sendMessage(msg);
                        Thread.Sleep(1);
                    }
                    this.msgsSendWait.Reset();
                }

            }
            catch (ThreadInterruptedException ex)
            {
                logger.Trace(logger.Name + " Thread send halted");
            }
        }

        //Invia un messaggio al CANusb indicato
        private int sendMessage(CANCommand msgSend)
        {
            int rv;//return value
            CANUSB.CANMsg msg = new CANUSB.CANMsg();
            msg.id = msgSend.getID();
            msg.len = msgSend.getDLC();
            msg.flags = msgSend.getFlags();
            msg.data = msgSend.getDataUlong();
            rv = CANUSB.canusb_Write(this.connectorID, ref msg);
            logger.Trace(logger.Name + " Tx | " + "0x" + msgSend.getID().ToString("X3") + " " + msgSend.getDLC().ToString() + " " + "0x" + msgSend.getDatum(0).ToString("X2") + " " + "0x" + msgSend.getDatum(1).ToString("X2") + " " + "0x" + msgSend.getDatum(2).ToString("X2") + " " + "0x" + msgSend.getDatum(3).ToString("X2") + " " + "0x" + msgSend.getDatum(4).ToString("X2") + " " + "0x" + msgSend.getDatum(5).ToString("X2") + " " + "0x" + msgSend.getDatum(6).ToString("X2") + " " + "0x" + msgSend.getDatum(7).ToString("X2"));
            //LogManager.GetLogger(logger.Name).Trace(logger.Name + " | " + "0x" + msgSend.getID().ToString("X3") + " " + msgSend.len.ToString() + " " + "0x" + msgSend.dato.d0.ToString("X2") + " " + "0x" + msgSend.dato.d1.ToString("X2") + " " + "0x" + msgSend.dato.d2.ToString("X2") + " " + "0x" + msgSend.dato.d3.ToString("X2") + " " + "0x" + msgSend.dato.d4.ToString("X2") + " " + "0x" + msgSend.dato.d5.ToString("X2") + " " + "0x" + msgSend.dato.d6.ToString("X2") + " " + "0x" + msgSend.dato.d7.ToString("X2"));
            return rv;
        }
    }
}
