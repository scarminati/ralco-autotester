﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    public enum ProtocolChange
    {
        //movimentazioni
        Shutters, Filter, Light, Laser, Laser1, Laser2, DSC, Meter, SpatialFilter1Pos, SpatialFilter1Rot, Iris, FocalSpot,Motori1_4, Motori5_8, Motori9_12,Incl,
        //passi
        StepsCross, StepsLong, StepsIris, StepsSpatialFilter1,
        //eventi/errori
        Errors, DBTError, Info, Temperature, Encoders, Heartbeat, Potentiometers,
        //tasti
        TastoFiltro,TastoLuce, Key,
        //test tasti membrana
        TastiMembrana,
        //configurazioni
        Counter, ConfigurationComplete,
        //varie
        TestPassi,TestReset
    }

    interface IProtocolDataChanges<out ProtocolChange>
    {
        event Action<ProtocolChange> Handler;
    }
}
