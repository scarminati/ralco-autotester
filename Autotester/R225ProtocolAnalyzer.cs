﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Autotester
{
    public class R225ProtocolAnalyzer : IProtocolDataChanges<ProtocolChange>, IProtocolAnalyzer
    {
        private CANusb_Connector connector = null;
        public string protocolName { get { return "CAN-BUS"; } }
        private int flag = -1;//variabile usata per determinare se i messaggi da analizzare contengono le stringhe per la versione FW, BL, PCB

        // attributi collimatore
        List<CalibTable> listCalTable = new List<CalibTable>();
        private bool requestForCalTable = false;

        // stato interno collimatore
        private int status;
        private int currentMsg;

        //messaggi di errore passi cross e long da escludere prima che il test sia effettivo.
        int maxCrossStepError;
        int maxLongStepError;

        // variabili locali

        private bool configComplete = false;
        private string fwCollimator;
        private string fwBootloader;
        private string pcbCollimator;
        private string serialNumber;
        private bool light;
        private bool key;
        private int filter;
        private bool tastoFiltro;
        private bool tastoLuce;
        private bool tastoIride;
        private bool dscCrossActive;
        private bool dscLongActive;
        private bool crossJam;
        private bool longJam;
        private int crossPos;
        private int longPos;
        private int oldCrossPos;
        private int oldLongPos;
        private int irisPos;
        private byte testTastiMembr = 0x00;
        private bool postError = false;
        private bool emcyBufferFull = false;
        private bool busOff = false;
        private bool memoryError = false;
        private bool overVoltage = false;
        private bool underVoltage = false;
        private bool tempOutOfRange = false;
        private bool canOverRun = false;
        private bool errMotCross = false;
        private bool errMotLong = false;
        private bool errMotIride = false;
        private bool errMotFiltro = false;
        private bool errHomCross = false;
        private bool errHomLong = false;
        private bool errHomIride = false;
        private bool errHomFiltro = false;
        private bool errLimHWCross = false;
        private bool errLimHWLong = false;
        private bool errLimHWIride = false;
        private bool errLimHWFiltro = false;

        //variabili pubbliche implementazioni dell'interfaccia

        public int SpatialFilter1_Pos { get; } //spostamento filtro spaziale 1
        public int SpatialFilter1_Rot { get; } //rotazione filtro spaziale 1
        public bool error0 { get; set; }
        public bool error90 { get; set; }
        public bool error180 { get; set; }
        public bool error270 { get; set; }
        public int filterWhenError { get; set; }
        public int crossPosWhenError { get; set; }
        public int longPosWhenError { get; set; }
        public int passiLong { get; set; }
        public int deltaPotenziometriLong { get; set; }
        public int passiCross { get; set; }
        public int deltaPotenziometriCross { get; set; }
        public int passiIride { get; set; }
        public int passiSpatialFilter { get; set; }
        public int tentativiPosFiltro { get; set; }

        //tasti membrana
        public bool TastiMembrActive { get; set; }
        public bool TestTastiMemb { get; set; }
        public byte TastiMembPressed { get; set; }

        public int CounterIteration {get;set;}

        public bool CollimationDone { get;}

        public bool CollimationFailed {get;}

        public bool DscCrossActive { get { return dscCrossActive; } }

        public bool DscLongActive { get { return dscLongActive; } }



        //non implementato nel protocollo R225...
        public bool POSTerror{ get; }

        public bool CrossJam
        {
            get;
        }

        public bool LongJam
        {
            get;
        }

        public bool IrisJam
        {
            get;
        }

        public bool FilterJam
        {
            get;
        }

        public bool CrossCalib
        {
            get;
        }

        public bool LongCalib
        {
            get;
        }

        public bool FilterCalib
        {
            get;
        }

        public bool CrossPot
        {
            get;
        }

        public bool LongPot
        {
            get;
        }

        public bool Light
        {
            get { return light; }
        }

        public bool Key
        {
            get { return key; }
        }

        public bool TastoFiltro
        {
            get { return tastoFiltro; }
        }

        public bool TastoLuce
        {
            get { return tastoLuce; }
        }

        public bool TastoIride
        {
            get { return tastoIride; }
        }

        public int Meter
        {
            get;
        }

        public int MaxSpeed
        {
            get;
        }

        public int Filter
        {
            get { return filter; }
        }

        public int CrossPos
        {
            get { return crossPos; }
        }

        public int LongPos
        {
            get { return longPos; }
        }
        public int IrisPos
        {
            get { return irisPos; }
        }


        public int Temperature
        {
            get;
        }

        public int CrossEncoder
        {
            get;
        }

        public int LongEncoder
        {
            get;
        }

        public string FwCollimator
        {
            get { return fwCollimator; }
        }

        public string FwBootloader
        {
            get { return fwBootloader; }
        }

        public string PcbCollimator
        {
            get { return pcbCollimator; }
        }

        public string SerialNumber
        {
            get { return serialNumber; }
        }

        public List<CalibTable> CalibTable { get { return listCalTable; } }

        public CANusb_Connector Connector
        {
            get { return connector; }
            set
            {
                if (value == null)
                {
                    if (this.connector != null)
                    {
                        connector.Handler -= ConnectorOnHandler();
                    }
                    connector = null;

                }
                else
                {
                    connector = value;
                    connector.Handler += ConnectorOnHandler();                    
                }
            }
        }

        public testCollimatore  testColl { get; set; }


        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate(CANCommand command) { this.processMessage(command); };
        }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<ProtocolChange> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<ProtocolChange> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<ProtocolChange> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<ProtocolChange>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */
        
        public void initCollimator()
        {
            if (testColl.configTest.protocolName.Equals("CAN-BUS"))
            {
                /*configuro il numero di errori massimo sui passi che si può commettere prima di invalidare il test*/
                maxCrossStepError = 2;
                maxLongStepError = 2;
                /*Configurazioni fatte perchè alcuni collimatori potrebbero escluderle, e quindi l'autotest non funzionerebbe.Le configurazioni solo solo temporanee. 
                 Al power off del collimatore vengono perse*/
                //SID Laterale SX CAN
                CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x0A, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //VERIFICARE SE VA BENE ANCHE PER ALTRI COLLIMATORI; NEL CASO RIMUOVERE LA LINEA
                //Mando un mex di spostamento automatico delle lamelle al formato 500x500mm (le lamelle di fatto non si muovono, ma si attivano i mex di stato)
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x40, 0x00, 0x00, 0x64, 1, 0xF4, 1, 0xF4);
                connector.enqueueMessage(msg);
                //Recettore Laterale SX CAN
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x0B, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Recettore Laterale DX CAN
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x0C, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //SID Verticale CAN
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x0D, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Recettore Verticale CAN
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x0E, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //SID Laterale SX CAN
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0F), 0, 8, 0x47, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Recettore Laterale SX CAN
                //Setta i limiti fisici a 500x500mm
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x08), 0, 8, 1, 0xF4, 1, 0xF4, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Abilita la generazione degli eventi
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0xF0, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Abilita test passi fotocellula
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0xF1, 0x01, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Abilita messaggi di stato
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x07), 0, 8, 1, 0, 1, 0, 1, 0, 1, 0);
                connector.enqueueMessage(msg);
                //Mando un mex di spostamento manuale delle lamelle al formato 500x500mm (le lamelle di fatto non si muovono, ma si attivano i mex di stato)
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x80, 0x00, 0x00, 0x64, 1, 0xF4, 1, 0xF4);
                connector.enqueueMessage(msg);
                //Abilito tutti i messaggi di stato (0x7F0, 0x7F1, 0x7F9, 0x7FC)
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x07), 0, 8, 0x03, 0xe8, 0x03, 0xe8, 0x03, 0xe8, 0x03, 0xe8);
                connector.enqueueMessage(msg);
                //Setto iride indipendente dalle lamelle (impostazione temporanea)
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x11, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
            }
            else if (testColl.configTest.protocolName.Equals("CAN-OPEN"))
            {
                //Enable Heart Beat
                CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send), 0, 8, 0x2B, 0x17, 0x10, 0x00, 0xE8, 0x03, 0x00, 0x00);
                connector.enqueueMessage(msg);
                //Enable Symmetric Shutters
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send), 0, 8, 0x2F, 0x10, 0x60, 0x01, 0x11, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                //Enable Iris
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send), 0, 8, 0x2F, 0x30, 0x60, 0x01, 0x11, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                //Enable Spectral Filter Wheel
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send), 0, 8, 0x2F, 0x40, 0x60, 0x01, 0x01, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                //Set SFD to 10000 0.1mm
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send), 0, 8, 0x2B, 0x00, 0x60, 0x00, 0x10, 0x27, 0x00, 0x00);
                connector.enqueueMessage(msg);
                //NMT Start
                msg = new CANCommand(0x000, 0, 2, 0x01, 0x40, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
            }
            emitChanges(ProtocolChange.ConfigurationComplete);
            sendVersionRequests();
        }

        public void readCalibTable()
        {
            if (!requestForCalTable)
            {
                requestForCalTable = true;
                for (byte motore = 1; motore <= testColl.configTest.numMotori; motore++)
                {
                    listCalTable.Add(new CalibTable());
                    //Richiesta numero punti di taratura motore
                    CANCommand msg = new CANCommand(0x607, 0, 8, 0x40, 0x50, 0x28, motore, 0, 0, 0, 0);
                    connector.enqueueMessage(msg);
                }
            }
        }

        public void setCollimatorForShipment()
        {

        }

        public void playSoundEndTest()
        {

        }

        private void processMessage(CANCommand msg)
        {
            processReadMsg(msg);
        }

        //valorizza le variabili di istanza del protocollo che vengono poi analizzate dalla R225Panel
        private void processReadMsg(CANCommand msg)
        {
            if (testColl.configTest.protocolName.Equals("CAN-BUS"))
            {
                if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x06))//configurazione standard o Villa
                {
                    fwCollimator = Convert.ToChar(msg.getDatum(5)) + "." + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                    emitChanges(ProtocolChange.Info);
                }
                else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x00))//configurazione standard o Villa
                {
                    oldCrossPos = crossPos;
                    oldLongPos = longPos;
                    crossPos = msg.getDatum(4) << 8 | msg.getDatum(5);
                    longPos = msg.getDatum(6) << 8 | msg.getDatum(7);
                    emitChanges(ProtocolChange.Shutters);
                    if (!crossPos.Equals(oldCrossPos))
                        dscCrossActive = true;
                    else
                        dscCrossActive = false;
                    if (!longPos.Equals(oldLongPos))
                        dscLongActive = true;
                    else
                        dscLongActive = false;
                    emitChanges(ProtocolChange.DSC);
                }
                else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x09))//configurazione standard o Villa
                {
                    irisPos = msg.getDatum(5) << 8 | msg.getDatum(7);
                    emitChanges(ProtocolChange.Iris);
                }
                //else if (msg.getID() == 0x7F2)
                //{
                //    serialNumber = "" + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                //    emitChanges(ProtocolChange.Info);
                //}
                else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x04))//configurazione standard o Villa
                {
                    switch (msg.getDLC())
                    {
                        case 1:
                            switch (msg.getDatum(0))
                            {
                                case 0x00:
                                    TastiMembrActive = false;
                                    break;
                                default:
                                    TastiMembrActive = true;
                                    testTastiMembr = (byte)(testTastiMembr | msg.getDatum(0));
                                    TastiMembPressed = testTastiMembr;
                                    switch (testTastiMembr)
                                    {
                                        case 0x3F:
                                            TestTastiMemb = true;
                                            break;
                                        default:
                                            TestTastiMemb = false;
                                            break;
                                    }
                                    break;
                            }
                            emitChanges(ProtocolChange.TastiMembrana);
                            break;
                        case 8:
                            switch (msg.getDatum(0))
                            {
                                //EV_LIGHT
                                case 0x08:
                                    switch (msg.getDatum(7))
                                    {
                                        case 0x00:
                                            light = false;
                                            break;
                                        case 0x01:
                                            light = true;
                                            break;
                                    }
                                    emitChanges(ProtocolChange.Light);
                                    break;
                                //EV_CHIAVE
                                case 0x0C:
                                    switch (msg.getDatum(7))
                                    {
                                        case 0x00:
                                            key = false;
                                            break;
                                        case 0x01:
                                            key = true;
                                            break;
                                    }
                                    emitChanges(ProtocolChange.Key);
                                    break;
                                //EV_FILTER_READY
                                case 0x0D:
                                    filter = msg.getDatum(7);
                                    emitChanges(ProtocolChange.Filter);
                                    break;
                                //EV_PASSI_PHOTO
                                case 0x0E:
                                    switch (msg.getDatum(1))
                                    {
                                        case 0x01://CROSS
                                            passiCross = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                            emitChanges(ProtocolChange.StepsCross);
                                            //Console.WriteLine("STEP CROSS = " + stepsCross);
                                            if (passiCross.Equals(-1) || (Math.Abs(50 - passiCross) > 50))
                                            {
                                                maxCrossStepError = maxCrossStepError - 1;
                                                //Console.WriteLine("STEP CROSS = " + stepsCross);
                                                //Console.WriteLine("ERRORI CROSS = " + maxCrossStepError);
                                                if (maxCrossStepError < 0)
                                                    crossJam = true;
                                            }
                                            break;
                                        case 0x02://LONG
                                            passiLong = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                            emitChanges(ProtocolChange.StepsLong);
                                            //Console.WriteLine("STEP LONG = " + stepsLong);
                                            if (passiLong.Equals(-1) || (Math.Abs(50 - passiLong) > 50))
                                            {
                                                maxLongStepError = maxLongStepError - 1;
                                                //Console.WriteLine("STEP LONG = " + stepsLong);
                                                //Console.WriteLine("ERRORI LONG = " + maxLongStepError);
                                                if (maxLongStepError < 0)
                                                    longJam = true;
                                            }
                                            break;
                                    }
                                    if (crossJam || longJam)
                                        emitChanges(ProtocolChange.Errors);
                                    break;
                                //EV_TASTO_FRONTALE
                                case 0x10:
                                    switch (msg.getDatum(1))
                                    {
                                        case 0x01://TASTO FILTRO
                                            switch (msg.getDatum(7))
                                            {
                                                case 0x00:
                                                    tastoFiltro = false;
                                                    break;
                                                case 0x01:
                                                    tastoFiltro = true;
                                                    break;
                                            }
                                            emitChanges(ProtocolChange.TastoFiltro);
                                            break;
                                        case 0x02://TASTO LUCE
                                            switch (msg.getDatum(7))
                                            {
                                                case 0x00:
                                                    tastoLuce = false;
                                                    break;
                                                case 0x01:
                                                    tastoLuce = true;
                                                    break;
                                            }
                                            emitChanges(ProtocolChange.TastoLuce);
                                            break;
                                    }
                                    break;
                                case 0x40://EV_ERROR
                                    switch (msg.getDatum(2))
                                    {
                                        case 0x02://FILTRO
                                            if (msg.getDatum(7) > 0)
                                            {
                                                tentativiPosFiltro = Convert.ToInt32(msg.getDatum(7));
                                                emitChanges(ProtocolChange.Errors);
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                }
            }
            else if (testColl.configTest.protocolName.Equals("CAN-OPEN"))
            {
                if (msg.getID().Equals(0x087))
                {
                    if (msg.getDatum(1) == 0xF0 && msg.getDatum(0) == 0x70)
                    {
                        postError = true;
                    }
                    if (msg.getDatum(1) == 0x10 && msg.getDatum(0) == 0x00)
                    {
                        emcyBufferFull = true;
                    }
                    if (msg.getDatum(1) == 0x81 && msg.getDatum(0) == 0x40)
                    {
                        busOff = true;
                    }
                    if (msg.getDatum(1) == 0x51 && msg.getDatum(0) == 0x00)
                    {
                        memoryError = true;
                    }
                    if (msg.getDatum(1) == 0x31 && msg.getDatum(0) == 0x00)
                    {
                        overVoltage = true;
                    }
                    if (msg.getDatum(1) == 0x31 && msg.getDatum(0) == 0x01)
                    {
                        underVoltage = true;
                    }
                    if (msg.getDatum(1) == 0x42 && msg.getDatum(0) == 0x00)
                    {
                        tempOutOfRange = true;
                    }
                    if (msg.getDatum(1) == 0x81 && msg.getDatum(0) == 0x10)
                    {
                        canOverRun = true;
                    }
                    if (msg.getDatum(1) == 0x53 && msg.getDatum(0) == 0x00)
                    {
                        if ((msg.getDatum(6) & 0x01).Equals(0x01))
                            errMotCross = true;
                        if ((msg.getDatum(6) & 0x02).Equals(0x02))
                            errMotLong = true;
                        if ((msg.getDatum(6) & 0x04).Equals(0x04))
                            errMotIride = true;
                        if ((msg.getDatum(6) & 0x08).Equals(0x08))
                            errMotFiltro = true;
                    }
                    if (msg.getDatum(1) == 0x52 && msg.getDatum(0) == 0x00)
                    {
                        if ((msg.getDatum(6) & 0x01).Equals(0x01))
                            errHomCross = true;
                        if ((msg.getDatum(6) & 0x02).Equals(0x02))
                            errHomLong = true;
                        if ((msg.getDatum(6) & 0x04).Equals(0x04))
                            errHomIride = true;
                        if ((msg.getDatum(6) & 0x08).Equals(0x08))
                            errHomFiltro = true;
                    }
                    if (msg.getDatum(1) == 0xA0 && msg.getDatum(0) == 0x30)
                    {
                        if ((msg.getDatum(4) & 0x01).Equals(0x01))
                            errLimHWCross = true;
                        if ((msg.getDatum(4) & 0x02).Equals(0x02))
                            errLimHWLong = true;
                        if ((msg.getDatum(4) & 0x04).Equals(0x04))
                            errLimHWIride = true;
                        if ((msg.getDatum(4) & 0x08).Equals(0x08))
                            errLimHWFiltro = true;
                    }
                    emitChanges(ProtocolChange.Errors);
                }
                else if (msg.getID().Equals(0x187))
                {
                    //front = (Int16)Convert.ToInt32(msg.getDatum(0) + (msg.getDatum(1) << 8));
                    //left = (Int16)Convert.ToInt32(msg.getDatum(2) + (msg.getDatum(3) << 8));
                    //rear = (Int16)Convert.ToInt32(msg.getDatum(4) + (msg.getDatum(5) << 8));
                    //right = (Int16)Convert.ToInt32(msg.getDatum(6) + (msg.getDatum(7) << 8));
                    //emitChanges(ProtocolChange.Shutters);
                }
                else if (msg.getID().Equals(0x287))
                {
                    //filter = Convert.ToInt32(msg.getDatum(0));
                    //emitChanges(ProtocolChange.Filter);
                }
                else if (msg.getID().Equals(0x387))
                {
                    //light = (msg.getDatum(4) == 0x01 ? true : false);
                    //emitChanges(ProtocolChange.Light);
                }
                else if (msg.getID().Equals(0x487))
                {
                    //focalSpot = Convert.ToInt32(msg.getDatum(0));
                    //emitChanges(ProtocolChange.FocalSpot);
                }
                //Generali
                else if (msg.getID().Equals(0x5C0))
                {
                    switch (msg.getDatum(0))
                    {
                        case (0x41):
                            switch (msg.getDatum(1))
                            {
                                case (0x09):
                                    switch (msg.getDatum(2))
                                    {
                                        case (0x10):
                                            pcbCollimator = "";
                                            flag = 0;//Versione PCB
                                            break;
                                        case (0x22):
                                            fwBootloader = "";
                                            flag = 1;//Versione Bootloader
                                            break;
                                    }
                                    break;
                                case (0x0A):
                                    fwCollimator = "";
                                    flag = 2;//Versione Firmware
                                    break;
                            }
                            break;
                        case (0x43)://4 byte
                            {
                                //Aperture
                                if (msg.getDatum(2) == 0x28)
                                {
                                    listCalTable[msg.getDatum(1) - 1].Passi.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                    Console.WriteLine("Passi motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                                }

                                //Passi
                                if (msg.getDatum(2) == 0x29)
                                {
                                    listCalTable[msg.getDatum(1) - 1].Aperture.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                    Console.WriteLine("Apertura motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                                }


                            }
                            break;
                        case (0x47)://3 byte
                            break;
                        case (0x4B)://2 byte
                            break;
                        case (0x4F)://1 byte
                            {
                                //numero punti di taratura
                                if (msg.getDatum(2) == 0x28 && msg.getDatum(1) == 0x50)
                                {
                                    listCalTable[msg.getDatum(3) - 1].PuntiTaratura = msg.getDatum(4);
                                    for (byte i = 1; i <= msg.getDatum(4); i++)
                                    {
                                        //Richiesta aperture motore per il punto di taratura
                                        CANCommand mex = new CANCommand(0x607, 0, 8, 0x40, msg.getDatum(3), 0x28, i, 0, 0, 0, 0);
                                        connector.enqueueMessage(mex);
                                        //Richiesta passi motore per il punto di taratura
                                        mex = new CANCommand(0x607, 0, 8, 0x40, msg.getDatum(3), 0x29, i, 0, 0, 0, 0);
                                        connector.enqueueMessage(mex);
                                    }
                                }
                            }
                            break;
                        default:
                            switch (flag)
                            {
                                case 0:
                                    pcbCollimator = pcbCollimator + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                    pcbCollimator = pcbCollimator.Replace('\0', ' ').Trim();
                                    break;
                                case 1:
                                    fwBootloader = fwBootloader + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                    fwBootloader = fwBootloader.Replace('\0', ' ').Trim(); ;
                                    break;
                                case 2:
                                    fwCollimator = fwCollimator + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                    fwCollimator = fwCollimator.Replace('\0', ' ').Trim(); ;
                                    break;
                            }
                            emitChanges(ProtocolChange.Info);
                            break;
                    }
                    ////POST error
                    //if (msg.getDatum(0) == 0x70 && msg.getDatum(1) == 0xF0 && msg.getDatum(2) == 0x00 && msg.getDatum(3) == 0x02)
                    //{
                    //    postError = true;
                    //    emitChanges(ProtocolChange.Errors);
                    //}
                }
                else if (msg.getID().Equals(0x740))
                {
                    switch (msg.getDatum(0))
                    {
                        case 0x05:
                            if (!configComplete)
                            {
                                readCalibTable();
                                configComplete = !configComplete;
                                emitChanges(ProtocolChange.ConfigurationComplete);
                            }
                            break;
                    }
                }
            }
        }

        private void emitChanges(ProtocolChange value)
        {
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
            {
                LogManager.GetLogger(connector.Logger.Name).Warn(connector.Logger.Name + " processor messages not available ");
            }
        }

        public void sendVersionRequests()
        {
            if (testColl.configTest.protocolName.Equals("CAN-BUS"))
            {
                //Richiesta Versione Firmware
                CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x06), Lawicel.CANUSB.CANMSG_RTR, 8, 0, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Richiesto Serial Number
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x06), 0, 8, 2, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //emitChanges(ProtocolChange.Info);
            }
            else if (testColl.configTest.protocolName.Equals("CAN-OPEN"))
            {
                //Richiesta Versione Firmware
                CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x40, 0x0A, 0x10, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Richiesta Versione PCB
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x40, 0x09, 0x10, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x70, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                //Richiesta Versione Bootloader
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x40, 0x09, 0x22, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x00), 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
                connector.enqueueMessage(msg);
            }
        }
    }
}
