﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Xceed.Words.NET;

namespace Autotester
{
    public partial class R915SPanel : UserControl, ICollimatorPanel
    {
        private R915SProtocolAnalyzer protocol;
        private ConfigurazioneTest cfg;
        private FormControls formRuota = null;
        private int testLuce = 0;
        private int numCicli;

        private int _numPanel;

        public int NumPanel { get { return _numPanel; } set { this._numPanel = value; }  }

        public R915SPanel()
        {
            InitializeComponent();
        }

        //public void setConnector(CANusb_Connector connector, string deviceName)
        //{
        //    R915SProtocolAnalyzer R915SProtocol = new R915SProtocolAnalyzer();
        //    R915SProtocol.Connector = connector;

        //    R915SProtocol.Handler += ConnectorOnHandler();

        //    protocol = R915SProtocol;
        //}

        public void setConnector(testCollimatore t)
        {
            R915SProtocolAnalyzer R915SProtocol = new R915SProtocolAnalyzer();

            R915SProtocol.testColl = t;

            R915SProtocol.Connector = t.connettore;

            R915SProtocol.Handler += ConnectorOnHandler();

            protocol = R915SProtocol;
        }

        public string collimatorName { get; set; }

        public void setFormRuota(FormControls formRuota)
        {
            this.formRuota = formRuota;
        }

        public void enableControl(ConfigurazioneTest cfg)
        {
            this.cfg = cfg;
            if (cfg.testLuce.Equals(true))
            {
                grpLuce.Enabled = true;
            }
            if (cfg.testVita.Equals(true))
            {
                grpVita.Enabled = true;
            }
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return protocol;
        }

        public bool isManualTestCompleted()
        {
            bool rv = true;
            if (grpLuce.Enabled)
                rv = rv & ledLuceTest.On;
            return (rv);
        }

        public string getTipoTest()
        {
            if (cmb_tipoTest.SelectedItem == null)
                return "";
            else
                return cmb_tipoTest.SelectedItem.ToString();
        }

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public string pathScriptFile { get; set; }


        public List<List<SingoloComando>> scripts { get; set; }

        public bool testMetro()
        {
            return true;
        }


        private Action<ProtocolChange> ConnectorOnHandler()
        {
            return delegate(ProtocolChange value) { this.CollimatorValuesChanged(value); };
        }

        private void CollimatorValuesChanged(ProtocolChange value)
        {
            int tmp;

            switch (value)
            {
                case ProtocolChange.Shutters:
                    changeShutters(protocol.FrontPos, protocol.RightPos, protocol.RearPos, protocol.LeftPos);
                    break;

                case ProtocolChange.Filter:
                    changeFilter(protocol.Filter);
                    break;

                case ProtocolChange.Light:
                    if (protocol.Light)
                    {
                        changeButtonLight("Spegni");
                    }
                    else
                    {
                        changeButtonLight("Accendi");
                    }
                    break;

                case ProtocolChange.FocalSpot:
                    changeFocalSpot(protocol.FocalSpot);
                    break;

                case ProtocolChange.TestReset:
                    //ledTestReset.On = true;
                    protocol.initCollimator();
                    break;

                case ProtocolChange.Errors:
                    ledError.On = true;
                    
                    if (this.formRuota != null)
                    {
                        switch (this.formRuota.getActualAngleRotation())
                        {
                            case 0:
                                protocol.error0 = true;
                                break;
                            case 90:
                                protocol.error90 = true;
                                break;
                            case 180:
                                protocol.error180 = true;
                                break;
                            case 270:
                                protocol.error270 = true;
                                break;
                        }
                    }
                    changeChkEnableValue(false);
                   // this.chkEnable.Checked = false;
                    break;
                    
                case ProtocolChange.Info:
                    if (!protocol.FwCollimator.Equals(""))
                        changeFirmware(protocol.FwCollimator);
                    if (!protocol.FwBootloader.Equals(""))
                        changeBootloader(protocol.FwBootloader);
                    if (!protocol.PcbCollimator.Equals(""))
                        changePCB(protocol.PcbCollimator);
                    testLuce = 0;
                    break;

                case ProtocolChange.Counter:
                    changeCicli(protocol.CounterIteration);
                    break;

                case ProtocolChange.DBTError:
                    MessageBox.Show("Problemi nella configurazione del collimatore: " + this._numPanel);
                    break;

                case ProtocolChange.ConfigurationComplete:
                    protocol.sendVersionRequests();
                    break;

                default:
                    break;
            }
        }

        private void changeChkEnableValue(bool value)
        {
            if (this.chkEnable.InvokeRequired)
            {
                this.chkEnable.BeginInvoke((MethodInvoker)delegate () { this.chkEnable.Checked = value ; });
            }
            else
            {
                this.chkEnable.Checked = value; 
            }
        }

        private void changeCicli(int value)
        {
            this.numCicli = value;
        }

        public int getCicli()
        {
            return this.numCicli;
        }

        private void changeFirmware(string value)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate () {
                    this.lblFirmware.Text = value;
                    if (!lblFirmware.Text.Equals(""))
                    {
                        if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblFirmware.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblFirmware.Text = value;
                if (!lblFirmware.Text.Equals(""))
                {
                    if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblFirmware.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeBootloader(string value)
        {
            if (this.lblBootloader.InvokeRequired)
            {
                this.lblBootloader.BeginInvoke((MethodInvoker)delegate () {
                    this.lblBootloader.Text = value;
                    if (!lblBootloader.Text.Equals(""))
                    {
                        if (!lblBootloader.Text.Equals(cfg.BLAtteso) && lblBootloader.Text.Length.Equals(7))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblBootloader.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblBootloader.Text = value;
                if (!lblBootloader.Text.Equals(""))
                {
                    if (!lblBootloader.Text.Equals(cfg.BLAtteso) && lblBootloader.Text.Length.Equals(7))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblBootloader.BackColor = Color.Red;
                    }
                }
            }
        }


        private void changePCB(string value)
        {
            if (this.lblPCB.InvokeRequired)
            {
                this.lblPCB.BeginInvoke((MethodInvoker)delegate () {
                    this.lblPCB.Text = value;
                    if (!lblPCB.Text.Equals("") && lblPCB.Text.Length.Equals(9))
                    {
                        if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblPCB.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblPCB.Text = value;
                if (!lblPCB.Text.Equals("") && lblPCB.Text.Length.Equals(9))
                {
                    if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblPCB.BackColor = Color.Red;
                    }
                }
            }
        }

        //private void changeFirmware(string value)
        //{
        //    if (this.lblFirmware.InvokeRequired)
        //    {
        //        this.lblFirmware.BeginInvoke((MethodInvoker)delegate() { this.lblFirmware.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblFirmware.Text = value;
        //    }
        //}

        //private void changeBootloader(string value)
        //{
        //    if (this.lblBootloader.InvokeRequired)
        //    {
        //        this.lblBootloader.BeginInvoke((MethodInvoker)delegate() { this.lblBootloader.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblBootloader.Text = value;
        //    }
        //}


        //private void changePCB(string value)
        //{
        //    if (this.lblPCB.InvokeRequired)
        //    {
        //        this.lblPCB.BeginInvoke((MethodInvoker)delegate() { this.lblPCB.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblPCB.Text = value;
        //    }
        //}

        private void changeButtonLight(string value)
        {
            if (this.btnLuce.InvokeRequired)
            {
                this.btnLuce.BeginInvoke((MethodInvoker)delegate () {
                    testLuce++;
                    this.btnLuce.Text = value;
                    ledLuce.On = protocol.Light;
                    if (testLuce >= 2)
                    {
                        ledLuceTest.On = true;
                    }
                });
            }
            else
            {
                testLuce++;
                this.btnLuce.Text = value;
                ledLuce.On = protocol.Light;
                if (testLuce >= 2)
                {
                    ledLuceTest.On = true;
                }
            }
        }

        private void changeShutters(int frontPos, int rightPos, int rearPos, int leftPos)
        {
            if (this.lblFront.InvokeRequired || this.lblRight.InvokeRequired || this.lblRear.InvokeRequired || this.lblLeft.InvokeRequired)
            {
                this.lblFront.BeginInvoke((MethodInvoker)delegate() { this.lblFront.Text = frontPos.ToString(); });
                this.lblRight.BeginInvoke((MethodInvoker)delegate () { this.lblRight.Text = rightPos.ToString(); });
                this.lblRear.BeginInvoke((MethodInvoker)delegate () { this.lblRear.Text = rearPos.ToString(); });
                this.lblLeft.BeginInvoke((MethodInvoker)delegate () { this.lblLeft.Text = leftPos.ToString(); });
            }
            else
            {
                this.lblFront.Text = frontPos.ToString();
                this.lblRight.Text = rightPos.ToString();
                this.lblRear.Text = rearPos.ToString();
                this.lblLeft.Text = leftPos.ToString();
            }
        }

        private void changeFilter(int value)
        {
            if (this.lblFiltro.InvokeRequired)
            {
                this.lblFiltro.BeginInvoke((MethodInvoker)delegate () { this.lblFiltro.Text = value.ToString(); });
            }
            else
            {
                this.lblFiltro.Text = value.ToString(); ;
            }
        }

        private void changeFocalSpot(int value)
        {
            if (this.lblPuntoFocale.InvokeRequired)
            {
                this.lblPuntoFocale.BeginInvoke((MethodInvoker)delegate() { this.lblPuntoFocale.Text = value.ToString(); });
            }
            else
            {
                this.lblPuntoFocale.Text = value.ToString(); ;
            }
        }

        private void R915SPanel_Load(object sender, EventArgs e)
        {
            initValues();
        }

        private void initValues()
        {
            ledError.On = false;
            ledLuce.On = false;
            ledLuceTest.On = false;

            lblCollimatore.Text = collimatorName;
            lblSN.Text = SN;
            lblFC.Text = FC;
            lblCNC.Text = CNC;


            lblFirmware.Text = "";
            lblBootloader.Text = "";
            lblPCB.Text = "";
            //lblCicli.Text = "";

            lblFront.Text = "";
            lblRight.Text = "";
            lblRear.Text = "";
            lblLeft.Text = "";
            lblFiltro.Text = "";
            lblPuntoFocale.Text = "";

            this.scripts = new List<List<SingoloComando>>();
            //if (this.collimatorName.Equals("R 915 S DHHS"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R915S\scripts.txt";
            //else if (this.collimatorName.Equals("R 915 S DHHS_Prototipo"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R915S_Prototipo\scripts.txt";

            if (protocol != null)
            {
                protocol.setSerialNumberOnPCB(lblSN.Text.PadLeft(8, '0'));
                protocol.initCollimator();
            }

            chkEnable.Checked = true;


        }

        public bool isChecked()
        {
            return chkEnable.Checked;
        }

        public bool hasErrors()
        {
            return ledError.On;
        }

        private void btnErrori_Click(object sender, EventArgs e)
        {
            FormR915SErrors form = new FormR915SErrors();

            form.setErrors(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        private void cmb_tipoTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tipoTest = cmb_tipoTest.SelectedItem.ToString();
        }

        private void btnLuce_Click(object sender, EventArgs e)
        {
            protocol.sendLightCommand(!protocol.Light);
        }

        public void saveCalibTable()
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                string fileName = "";
                if (infoCNC[0].Equals("    "))
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt";
                else
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt";
                File.AppendAllText(fileName, DateTime.Now + "\n");
                for (int i = 0; i < ProtocolAnalyzer().CalibTable.Count; i++)
                {
                    File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].PuntiTaratura + "\n");
                    for (int j = 0; j < ProtocolAnalyzer().CalibTable[i].PuntiTaratura; j++)
                        File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].Aperture[j].ToString() + "," + ProtocolAnalyzer().CalibTable[i].Passi[j] + "\n");
                }
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt",true);
                    }

                    else
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt", true);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //salva il report sul server
        public void saveReport(Utente user)
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string testPass = "PASS";
                string errorDescription = "";
                var doc = DocX.Load(@"\\ralcosrv2\Public\Templates_Autotest\\TemplateFinalReport_" + templateReportName + ".docx");
                #region Test Report
                if (!lblFirmware.Text.Equals(-1) && !lblBootloader.Equals(-1) && !lblPCB.Equals(-1))
                    doc.ReplaceText("@@GENTEST@@", "PASS");
                else
                {
                    doc.ReplaceText("@@GENTEST@@", "FAIL");
                    testPass = "FAIL";
                }
                if (grpLuce.Enabled.Equals(true))
                {
                    if (ledLuceTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@LIGTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIGTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpVita.Enabled.Equals(true))
                {
                    if (ledError.On.Equals(false))
                    {
                        doc.ReplaceText("@@LIFTEST@@", "PASS");
                        doc.ReplaceText("@@LIFNOTES@@", "");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIFTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";
                        if (protocol.POSTerror)
                            errorDescription += " POST Error";
                        if (protocol.EMCYBufferFull)
                            errorDescription += " Buffer Full Error";
                        if (protocol.BUSOff)
                            errorDescription += " Bus OFF Error";
                        if (protocol.MemoryError)
                            errorDescription += " Memory Error";
                        if (protocol.OverVoltage)
                            errorDescription += " Over Voltage Error";
                        if (protocol.UnderVoltage)
                            errorDescription += " Under Voltage Error";
                        if (protocol.TempOutOfRange)
                            errorDescription += " PCB Temperature out of range";
                        if (protocol.CANOverRun)
                            errorDescription += " CAN Overrun Error";
                        if (protocol.ErrMotFront)
                            errorDescription += " Front Motor Error";
                        if (protocol.ErrMotLeft)
                            errorDescription += " Left Motor Error";
                        if (protocol.ErrMotRear)
                            errorDescription += " Rear Motor Error";
                        if (protocol.ErrMotRight)
                            errorDescription += " Right Motor Error";
                        if (protocol.ErrMotFiltro)
                            errorDescription += " Filter Motor Error";
                        if (protocol.ErrHomFront)
                            errorDescription += " Front Homing Error";
                        if (protocol.ErrHomLeft)
                            errorDescription += " Left Homing Error";
                        if (protocol.ErrHomRear)
                            errorDescription += " Rear Homing Error";
                        if (protocol.ErrHomRight)
                            errorDescription += " Right Homing Error";
                        if (protocol.ErrHomFiltro)
                            errorDescription += " Filter Homing Error";
                        if (protocol.ErrLimHWFront)
                            errorDescription += " Front Hardware Limit Error";
                        if (protocol.ErrLimHWLeft)
                            errorDescription += " Left Hardware Limit Error";
                        if (protocol.ErrLimHWRear)
                            errorDescription += " Rear Hardware Limit Error";
                        if (protocol.ErrLimHWRight)
                            errorDescription += " Right Hardware Limit Error";
                        if (protocol.ErrLimHWFiltro)
                            errorDescription += " Filter Hardware Limit Error";
                        doc.ReplaceText("@@LIFNOTES@@", errorDescription);
                    }
                }
                #endregion
                #region General Information
                doc.ReplaceText("@@MODEL@@", lblCollimatore.Text);
                doc.ReplaceText("@@SERNR@@", lblSN.Text);
                doc.ReplaceText("@@FWVER@@", lblFirmware.Text);
                doc.ReplaceText("@@BLVER@@", lblBootloader.Text);
                doc.ReplaceText("@@PCBVER@@", lblPCB.Text);
                doc.ReplaceText("@@FLOW@@", lblFC.Text);
                doc.ReplaceText("@@CNC@@", lblCNC.Text);
                doc.ReplaceText("@@DATE@@", DateTime.Now.ToString());
                doc.ReplaceText("@@OUTCOME@@", testPass);
                #endregion
                #region Operator
                doc.ReplaceText("@@OPERATOR@@", user.nome + " " + user.cognome);
                #endregion
                string filePathFirma = Utility.searchPathDigitalSignatureOperator(user);

                Xceed.Words.NET.Image img;
                if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                {
                    //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                    img = doc.AddImage(filePathFirma);
                    Xceed.Words.NET.Picture picture = img.CreatePicture();
                    Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                    p1.AppendPicture(picture);
                    p1.Alignment = Alignment.right;
                }

                //salvo in locale il report
                //infoFC[0] = numero FC; infoFC[1] = anno FC;
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                //Console.WriteLine(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                if (infoCNC[0].Equals("    "))
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                else
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                //salvo il report sul server solo se il tipo di test è diverso da Test
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                    }

                    else
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                    }
                    SQLConnector conn = new SQLConnector();
                    conn.CreateCommand("INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[FW_Version],[BL_Version],[PCB_Version],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + collimatorName + "','" + SN + "','" + ProtocolAnalyzer().FwCollimator + "','" + ProtocolAnalyzer().FwBootloader + "','" + ProtocolAnalyzer().PcbCollimator + "','" + FC + "','" + CNC + "','" + Convert.ToString(DateTime.Now.Year) + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errorDescription + "','" + tipoTest + "','" + user.nome + " " + user.cognome + "','" + user.location + "')", null);
                    //executeSQLCommand("Data Source=RALCOSRV4\\SQLEXPRESS;Initial Catalog=Autotest;Integrated Security=True;User ID=SCarminati;Password=RalCar028", "INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + getCollimatorType() + "','" + p.getSN() + "','" + p.getFC() + "','" + p.getCNC() + "','" + p.getAnno() + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errors + "','" + p.getTipoTest() + "','" + txtTester.Text + "','" + getLocation() + "')");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
