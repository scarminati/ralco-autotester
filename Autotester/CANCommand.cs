﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Autotester
{
    public class CANCommand : IEquatable<CANCommand>
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct SplitUlong
        {
            [FieldOffset(0)]
            public byte d0;
            [FieldOffset(1)]
            public byte d1;
            [FieldOffset(2)]
            public byte d2;
            [FieldOffset(3)]
            public byte d3;
            [FieldOffset(4)]
            public byte d4;
            [FieldOffset(5)]
            public byte d5;
            [FieldOffset(6)]
            public byte d6;
            [FieldOffset(7)]
            public byte d7;
            [FieldOffset(0)]
            public ulong data;

            //[FieldOffset(0)] public byte d[8];
        }

        private SplitUlong dato; //Dati del messaggio CAN (D0...D7)
        private int retCode=0;
        private uint id = 0x000;
        private byte flags = 0x00;
        private byte dlc = 0;
        private byte[] data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        private uint timeStamp = 0;
        private int nextMessage = 0;


        //costruttore di default
        public CANCommand() {}

        //overload costruttore 

        public CANCommand(uint id, byte flags, byte dlc, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7, uint timeStamp = 0, int nextMessage = 0)
        {
            this.retCode = 0;
            this.id = id;
            this.flags = flags;
            this.dlc = dlc;
            this.data = new byte[] {d0, d1, d2, d3, d4, d5, d6, d7};            
            this.dato.d0 = d0;
            this.dato.d1 = d1;
            this.dato.d2 = d2;
            this.dato.d3 = d3;
            this.dato.d4 = d4;
            this.dato.d5 = d5;
            this.dato.d6 = d6;
            this.dato.d7 = d7;
            this.timeStamp = timeStamp;
            this.nextMessage = nextMessage;
        }
        public void setRetCode(int retCode)
        {
            this.retCode = retCode;
        }
        public int getRetCode()
        {
            return (this.retCode);
        }
        public void setID(uint id)
        {
            this.id = id;
        }
        public uint getID()
        {
            return (this.id);
        }
        public void setFlags(byte flags)
        {
            this.flags = flags;
        }
        public byte getFlags()
        {
            return (this.flags);
        }
        public void setDLC(byte dlc)
        {
            this.dlc = dlc;
        }
        public byte getDLC()
        {
            return (this.dlc);
        }
        public void setTimeStamp(uint timeStamp)
        {
            this.timeStamp = timeStamp;
        }
        public uint getTimeStamp()
        {
            return (this.timeStamp);
        }
        public void setIndexNextMsg(int idx_next)
        {
            this.nextMessage = idx_next;
        }
        public int getIndexNextMsg()
        {
            return (this.nextMessage);
        }
        /*METODI PER IL SETTING ED IL GETTING DEI DATI DEL MESSAGGIO (DO...D7), per ogni protocollo possibile (CANopen, CAN-CAL, CANBUS, CANSYSTEM)*/
        public void setData(byte[] data)
        {
            this.data = data;
            dato.data = 0;
            dato.d0 = this.data[0];
            dato.d1 = this.data[1];
            dato.d2 = this.data[2];
            dato.d3 = this.data[3];
            dato.d4 = this.data[4];
            dato.d5 = this.data[5];
            dato.d6 = this.data[6];
            dato.d7 = this.data[7];

        }
        public byte[] getData()
        {
            return (this.data);
        }
        public void setData(ulong data)
        {
            this.dato.data = data;
            this.data[0] = dato.d0;
            this.data[1] = dato.d1;
            this.data[2] = dato.d2;
            this.data[3] = dato.d3;
            this.data[4] = dato.d4;
            this.data[5] = dato.d5;
            this.data[6] = dato.d6;
            this.data[7] = dato.d7;
        }
        public ulong getDataUlong()
        {
            return this.dato.data;
        }
        public byte getDatum(byte idxDatum)
        {
            return (this.data[idxDatum]);
        }
        public void setDatum(byte idxDatum, byte datum)
        {
            this.data[idxDatum] = datum;
        }

        public void setData(int protocol, int operation, params int[] values)
        {
            byte[] data = new byte[8] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            switch (protocol)
            {
                case Utility.PROTOCOL_CAN_STEP:
                    switch (operation)
                    {
                        case Utility.OPERATION_MOVE:
                            data[0] = 0x01;
                            byte[] bytes = new byte[4];
                            bytes = getBytes(values[0], 4, 0);
                            data[1] = bytes[0];
                            data[2] = bytes[1];
                            data[3] = bytes[2];
                            data[4] = bytes[3];
                            break;
                        case Utility.OPERATION_RESET:
                            //i dati sono già a zero, quindi non faccio niente...
                            break;
                    }
                    break;
            }
            setData(data);
        }

        /* INPUT : value = valore intero da convertire;
         *         bytesNumber = numero di bytes dell'array restituito
         * OUTPUT : bytes = value convertito in byte*/
        public byte[] getBytes(int value, int bytesNumber, int bytesOrder)
        {
            byte[] bytes = new byte[bytesNumber];
            switch (bytesOrder)
            {
                case 0://big endian : MSB ... LSB
                    for (int i = bytesNumber - 1; i >= 0; i--)
                    {
                        bytes[(bytesNumber - 1) - i] = Convert.ToByte(((ushort)(value >> (i * 8)) & 0xFF));
                    }
                    break;
                case 1://little endian : LSB ... MSB
                    for (int i = 0 ; i <= bytesNumber - 1; i++)
                    {
                        bytes[i] = Convert.ToByte(((ushort)(value >> (i * 8)) & 0xFF));
                    }
                    break;
            }
            return bytes;
        }

        /*DESCRIZIONE : Se DLC<8, mette a zero tutti i Data Bytes successivi
         *              ES : DLC = 6 -> D6 e D7 vengono inizializzati a zero
          INPUT : CANCommand : messaggio CAN da ripulire
          OUTPUT : None (il messaggio */
        public void cleanData()
        {
            if (this.getDLC() < 8)
            {
                for (int i = this.getDLC(); i < 8; i++)
                {
                    this.setDatum((byte)i, 0);
                }
            }
        }

        public bool Equals(CANCommand cmd)
        {
            if (this.getID() == cmd.getID() && this.getDLC() == cmd.getDLC() && this.getData()[0] == cmd.getData()[0] && this.getData()[1] == cmd.getData()[1]
                 && this.getData()[2] == cmd.getData()[2] && this.getData()[3] == cmd.getData()[3] && this.getData()[4] == cmd.getData()[4] && this.getData()[5] == cmd.getData()[5]
                 && this.getData()[6] == cmd.getData()[6] && this.getData()[7] == cmd.getData()[7])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
