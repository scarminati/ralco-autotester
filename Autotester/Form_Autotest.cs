﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lawicel;
using System.Windows.Forms;
using System.DirectoryServices.AccountManagement;
using System.Threading;
using System.IO;
using System.Xml;

namespace Autotester
{
    public partial class Form_Autotest : Form
    {
        private List<CANusb_Connector> totConnectorsHUB;
        private CANusb_Connector connectorRuota;//connettore CANUSB per la ruota
        private List<CANusb_Connector> connectorsCollList;//connettori CANUSB per i collimatori
        private List<ConfigurazioneTest> testConfig;
        private List<tipoRuota> ruota;
        private List<LedBulb> ledCollList;
        private List<ComboBox> cmbCollModelList;
        private List<testCollimatore> testCollim;
        private List<MaskedTextBox> SNlist;
        private List<MaskedTextBox> FClist;
        private List<MaskedTextBox> CNClist;
        private List<GroupBox> grpList;
        private SQLConnector conn;
        private Utente user;
        private int idxGbSelected = -1;//indice usato per sapere in quale TB scrivere il SN (in modalità barcode)
        public Form_Autotest(Utente user)
        {
            try
            {
                InitializeComponent();
                this.user = user;
                setFormLayout();
                checkForUpdates();
                ledCollList = new List<LedBulb>() { ledBulbTest1, ledBulbTest2, ledBulbTest3, ledBulbTest4, ledBulbTest5 };
                cmbCollModelList = new List<ComboBox>() { cmb_ModelloTest1, cmb_ModelloTest2, cmb_ModelloTest3, cmb_ModelloTest4, cmb_ModelloTest5 };
                SNlist = new List<MaskedTextBox>() { tb_SNTest1, tb_SNTest2, tb_SNTest3, tb_SNTest4, tb_SNTest5 };
                FClist = new List<MaskedTextBox>() { tb_FCTest1, tb_FCTest2, tb_FCTest3, tb_FCTest4, tb_FCTest5 };
                CNClist = new List<MaskedTextBox>() { tb_CNCTest1, tb_CNCTest2, tb_CNCTest3, tb_CNCTest4, tb_CNCTest5 };
                grpList = new List<GroupBox>() { grp_Test1, grp_Test2, grp_Test3, grp_Test4, grp_Test5 };
                connectorRuota = new CANusb_Connector();
                connectorsCollList = new List<CANusb_Connector>() { new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector() };
                totConnectorsHUB = new List<CANusb_Connector>() { new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector(), new CANusb_Connector() };
                testCollim = new List<testCollimatore>() { new testCollimatore(), new testCollimatore(), new testCollimatore(), new testCollimatore(), new testCollimatore() };
                testConfig = new List<ConfigurazioneTest>();
                ruota = new List<tipoRuota>();
                conn = new SQLConnector();
                try
                {
                    ruota = (List<tipoRuota>)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[tipoRuotaTest]", ruota);
                    foreach (tipoRuota r in ruota)
                        cmb_Ruota.Items.Add(r.nomeRuota);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                try
                {
                    testConfig = (List<ConfigurazioneTest>)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[testAttivi]", testConfig);
                    foreach (ConfigurazioneTest t in testConfig)
                        foreach (ComboBox c in cmbCollModelList)
                            c.Items.Add(t.nomeCollim);
                    foreach (ComboBox c in cmbCollModelList)
                        c.Sorted = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                loadCANUSB();
                btn_inputMode_Click(null, null);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setFormLayout()
        {
            try
            {
                if (user.dipartimento.Equals("ControlloFinale"))
                {
                    this.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + @"\img\testRuota.jpg");
                    grp_Test1.Location = new Point(311, 93);
                    grp_Test2.Location = new Point(32, 334);
                    grp_Test3.Location = new Point(311, 334);
                    grp_Test4.Location = new Point(562, 334);
                    grp_Test5.Location = new Point(311, 570);
                    grp_Ruota.Location = new Point(603, 5);
                }

                else if (user.dipartimento.Equals("Mammografia"))
                {
                    this.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + @"\img\testRuotaMammo.jpg");
                    grp_Test1.Location = new Point(268, 548);
                    grp_Test2.Location = new Point(12, 334);
                    grp_Test3.Location = new Point(53, 12);
                    grp_Test4.Location = new Point(439, 12);
                    grp_Test5.Location = new Point(558, 321);
                    grp_Ruota.Location = new Point(283, 356);
                    cb_quickTest.Visible = true;
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //carica gli USB connessi all'HUB
        private void loadCANUSB()
        {
            try
            {
                ledBulbRuota.On = false;
                cmb_Ruota.SelectedIndex = -1;
                foreach (LedBulb l in ledCollList)
                    l.On = false;
                foreach (ComboBox c in cmbCollModelList)
                    c.SelectedIndex = -1;
                StringBuilder buf = new StringBuilder(32);
                int numCANusbDetectedToHUB = CANUSB.canusb_getFirstAdapter(buf, 32);
                if (numCANusbDetectedToHUB > 0)
                {
                    totConnectorsHUB[0].connectorName = buf.ToString();
                    for (int i = 1; i < numCANusbDetectedToHUB; i++)
                    {
                        if (CANUSB.canusb_getNextAdapter(buf, 32) > 0)
                        {
                            totConnectorsHUB[i].connectorName = buf.ToString();
                        }
                    }
                }
                if (!numCANusbDetectedToHUB.Equals(0))
                {
                    //Ordino la lista per nome del connettore, così l'assegnazione è sempre la stessa. (Connettore[0] = ruota; Connettore[1...5] = Test 1...Test 5
                    totConnectorsHUB.OrderBy(o => o.connectorName).ToList();
                    connectorRuota = totConnectorsHUB[0];
                    ledBulbRuota.On = true;
                    ledBulbRuota.Color = Color.LightBlue;
                    for (int i = 1; i < numCANusbDetectedToHUB; i++)
                    {
                        connectorsCollList[i - 1] = totConnectorsHUB[i];
                        ledCollList[i - 1].On = true;
                        ledCollList[i - 1].Color = Color.LightBlue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool connectCANUSB()
        {
            try
            {
                bool connected = false;//diventa true se connetto almeno un dispositivo tra quelli dei collimatori
                                       //connessione ruota, che avviene se ho selezionato la ruota e se il connettore è stato riconosciuto ed è funzionante
                if (cmb_Ruota.SelectedItem != null && ledBulbRuota.Color != Color.Red)
                {
                    connectorRuota.baudRate = ruota.Single(r => r.nomeRuota.Equals(cmb_Ruota.SelectedItem.ToString())).baudRate;
                    connectorRuota.CANusb_connect(connectorRuota.baudRate);
                    if (connectorRuota.connectorID > 0)
                    {
                        ledBulbRuota.Color = Color.Green;
                        connectorRuota.isConnected = true;
                    }
                    else
                    {
                        ledBulbRuota.Color = Color.Red;
                        connectorRuota.isConnected = false;
                    }
                }
                //connessione collimatori
                bool dettagliForm = true;
                foreach (CANusb_Connector c in connectorsCollList)
                {
                    if (cmbCollModelList[connectorsCollList.IndexOf(c)].SelectedItem != null && ledCollList[connectorsCollList.IndexOf(c)].Color != Color.Red)
                    {
                        if (!SNlist[connectorsCollList.IndexOf(c)].MaskFull)
                        {
                            MessageBox.Show("Inserire il numero seriale del collimatore in posizione " + (connectorsCollList.IndexOf(c) + 1) + " prima di procedere con il test", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            dettagliForm = false;
                        }
                        else if (!FClist[connectorsCollList.IndexOf(c)].MaskFull)
                        {
                            dettagliForm = false;
                            MessageBox.Show("Inserire il numero di Flow Chart del collimatore in posizione " + (connectorsCollList.IndexOf(c) + 1) + " prima di procedere con il test", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        if (dettagliForm.Equals(false))
                            break;
                    }
                }
                if (dettagliForm.Equals(true))
                {
                    foreach (CANusb_Connector c in connectorsCollList)
                    {
                        if (cmbCollModelList[connectorsCollList.IndexOf(c)].SelectedItem != null && ledCollList[connectorsCollList.IndexOf(c)].Color != Color.Red)
                        {
                            c.baudRate = testConfig.Single(m => m.nomeCollim.Equals(cmbCollModelList[connectorsCollList.IndexOf(c)].SelectedItem.ToString())).baudRate;
                            c.CANusb_connect(c.baudRate);
                            if (c.connectorID > 0)
                            {
                                ledCollList[connectorsCollList.IndexOf(c)].Color = Color.Green;
                                c.isConnected = true;
                                connected = true;
                                testCollim[connectorsCollList.IndexOf(c)].connettore = c;
                                testCollim[connectorsCollList.IndexOf(c)].configTest = testConfig.Single(m => m.nomeCollim.Equals(cmbCollModelList[connectorsCollList.IndexOf(c)].SelectedItem.ToString()));
                                testCollim[connectorsCollList.IndexOf(c)].SN = SNlist[connectorsCollList.IndexOf(c)].Text;
                                testCollim[connectorsCollList.IndexOf(c)].FC = FClist[connectorsCollList.IndexOf(c)].Text;
                                testCollim[connectorsCollList.IndexOf(c)].CNC = CNClist[connectorsCollList.IndexOf(c)].Text;
                            }
                            else
                            {
                                ledCollList[connectorsCollList.IndexOf(c)].Color = Color.Red;
                                c.isConnected = false;
                            }
                        }
                    }
                }
                return (connected);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void disconnectCANUSB()
        {
            try
            {
                if (connectorRuota.isConnected.Equals(true))
                {
                    connectorRuota.CANusb_disconnect();
                    if (connectorRuota.connectorID.Equals(1))
                        ledBulbRuota.Color = Color.LightBlue;
                    else
                        ledBulbRuota.Color = Color.Red;
                    testCollim = new List<testCollimatore>() { new testCollimatore(), new testCollimatore(), new testCollimatore(), new testCollimatore(), new testCollimatore() };
                }
                //disconnessione collimatori
                foreach (CANusb_Connector c in connectorsCollList)
                {
                    if (c.isConnected.Equals(true))
                    {
                        c.CANusb_disconnect();
                        if (c.connectorID.Equals(1))
                            ledCollList[connectorsCollList.IndexOf(c)].Color = Color.LightBlue;
                        else
                            ledCollList[connectorsCollList.IndexOf(c)].Color = Color.Red;
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Go_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_Go.Tag.Equals("Start"))
                {
                    if (connectCANUSB())
                    {
                        btn_Go.Tag = "Stop";
                        timerBarcode.Stop();
                        btn_Go.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + "\\img\\stop.png");
                        btn_Refresh.Enabled = false;
                        FormTestMain form = new FormTestMain(this.user);
                        bool ok = false;
                        foreach (testCollimatore t in testCollim)
                            if (t.connettore != null)
                            {
                                Thread.Sleep(500);
                                form.setPanelTester(testCollim.IndexOf(t) + 1, t);
                                ok = true;
                            }
                        Thread.Sleep(100);
                        if (ok)
                        {
                            if (connectorRuota.isConnected)
                            {
                                form.ExtDevConnector = connectorRuota;
                                form.NameExtDevConnector = cmb_Ruota.SelectedItem.ToString();
                            }
                            form.Show(this);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Connettiti ad almeno un dispositivo prima di iniziare il test", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (btn_Go.Tag.Equals("Stop"))
                {
                    btn_Go.Tag = "Start";
                    btn_Go.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + "\\img\\start.png");
                    disconnectCANUSB();
                    btn_Refresh.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            try
            {
                loadCANUSB();
                checkForUpdates();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form_Autotest_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_inputMode_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_inputMode.Tag.Equals("Barcode"))
                {
                    btn_inputMode.Tag = "Tastiera";
                    timerBarcode.Stop();
                    idxGbSelected = -1;
                    tbBarcode.Clear();
                    tbBarcode.Enabled = false;
                    btn_inputMode.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + "\\img\\tastiera.ico");
                    foreach (GroupBox g in grpList)
                        g.Enabled = true;
                }
                else if (btn_inputMode.Tag.Equals("Tastiera"))
                {
                    btn_inputMode.Tag = "Barcode";
                    timerBarcode.Start();
                    btn_inputMode.BackgroundImage = Image.FromFile(Environment.CurrentDirectory + "\\img\\barcode.ico");
                    foreach (GroupBox g in grpList)
                        g.Enabled = false;
                    tbBarcode.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void timerBarcode_Tick(object sender, EventArgs e)
        {
            try
            {
                tbBarcode.Focus();
                switch (tbBarcode.Text.Trim())
                {
                    case ("R1P1"):
                        idxGbSelected = 0;
                        cleanTestGroup(idxGbSelected);
                        break;
                    case ("R1P2"):
                        idxGbSelected = 1;
                        cleanTestGroup(idxGbSelected);
                        break;
                    case ("R1P3"):
                        idxGbSelected = 2;
                        cleanTestGroup(idxGbSelected);
                        break;
                    case ("R1P4"):
                        idxGbSelected = 3;
                        cleanTestGroup(idxGbSelected);
                        break;
                    case ("R1P5"):
                        idxGbSelected = 4;
                        cleanTestGroup(idxGbSelected);
                        break;
                    default:
                        {
                            if (tbBarcode.Text.Equals(""))
                                break;
                            else
                            {
                                if (idxGbSelected.Equals(-1))
                                {
                                    timerBarcode.Stop();
                                    DialogResult result = MessageBox.Show("Selezionare la posizione del collimatore sulla ruota", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    if (result == DialogResult.OK)
                                        timerBarcode.Start();
                                }
                                else
                                {
                                    try
                                    {
                                        specificheTest st = new specificheTest();
                                        if (tbBarcode.Text.Contains("CNC"))
                                        {
                                            string value = "";
                                            value = (string)conn.CreateCommand("SELECT [modelloColl] FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + tbBarcode.Text.Replace("-", "/") + "'", value);
                                            cmbCollModelList[idxGbSelected].SelectedItem = value;
                                            value = (string)conn.CreateCommand("SELECT [serialeColl] FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + tbBarcode.Text.Replace("-", "/") + "'", value);
                                            SNlist[idxGbSelected].Text = value;
                                            value = (string)conn.CreateCommand("SELECT [numFlowChart] FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + tbBarcode.Text.Replace("-", "/") + "'", value);
                                            FClist[idxGbSelected].Text = value;
                                            CNClist[idxGbSelected].Text = tbBarcode.Text.Replace("-", "/");
                                            grpList[idxGbSelected].BackColor = SystemColors.Control;
                                            idxGbSelected = -1;
                                        } 
                                        else
                                        {
                                            st = (specificheTest)conn.CreateCommand("SELECT [Mod_Coll],[IDFlow],[anno] FROM [archivi].[dbo].[listaFlowChart] WHERE [archivi].[dbo].[listaFlowChart].[Da_N] <= " + tbBarcode.Text.PadLeft(8, '0') + " AND [archivi].[dbo].[listaFlowChart].[A_N] >= " + tbBarcode.Text.PadLeft(8, '0'), st);
                                            if (st != null)
                                            {
                                                if (st.modelloCollim.Contains('-'))
                                                {
                                                    string[] tmp = st.modelloCollim.Split('-');
                                                    st.modelloCollim = tmp[0];
                                                }
                                                cmbCollModelList[idxGbSelected].SelectedItem = st.modelloCollim;
                                                SNlist[idxGbSelected].Text = tbBarcode.Text;
                                                FClist[idxGbSelected].Text = st.IDFlow.ToString().PadLeft(4, '0') + "/" + st.anno.ToString();
                                                grpList[idxGbSelected].BackColor = SystemColors.Control;
                                                idxGbSelected = -1;
                                            }
                                        }

                                    }
                                    catch (Exception exc)
                                    {
                                        MessageBox.Show(exc.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                        break;
                }
                tbBarcode.Text = "";
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cleanTestGroup(int index)
        {
            try
            {
                cmbCollModelList[idxGbSelected].SelectedItem = null;
                SNlist[idxGbSelected].Text = "";
                FClist[idxGbSelected].Text = "";
                CNClist[idxGbSelected].Text = "";
                grpList[idxGbSelected].BackColor = Color.Yellow;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //private void tbBarcode_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (tbBarcode.Text.Equals(""))
        //            return;
        //        else
        //        {
        //            if (tbBarcode.Text.Equals("R1P1"))
        //            {
        //                idxGbSelected = 0;
        //                grpList[idxGbSelected].BackColor = Color.Yellow;
        //            }
        //            else if (tbBarcode.Text.Equals("R1P2"))
        //            {
        //                idxGbSelected = 1;
        //                grpList[idxGbSelected].BackColor = Color.Yellow;
        //            }
        //            else if (tbBarcode.Text.Equals("R1P3"))
        //            {
        //                idxGbSelected = 2;
        //                grpList[idxGbSelected].BackColor = Color.Yellow;
        //            }
        //            else if (tbBarcode.Text.Equals("R1P4"))
        //            {
        //                idxGbSelected = 3;
        //                grpList[idxGbSelected].BackColor = Color.Yellow;
        //            }
        //            else if (tbBarcode.Text.Equals("R1P5"))
        //            {
        //                idxGbSelected = 4;
        //                grpList[idxGbSelected].BackColor = Color.Yellow;
        //            }
        //            else
        //            {
        //                if (idxGbSelected.Equals(-1))
        //                    MessageBox.Show("Selezionare la posizione del collimatore sulla ruota", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                else
        //                {
        //                    SNlist[idxGbSelected].Text = tbBarcode.Text;
        //                    specificheTest st = new specificheTest();
        //                    st = (specificheTest)conn.CreateCommand("SELECT [Modello],[IDFlow],[anno] FROM [archivi].[dbo].[listaFlowChart] WHERE [archivi].[dbo].[listaFlowChart].[Da_N] <= " + Convert.ToInt32(SNlist[idxGbSelected].Text) + " AND [archivi].[dbo].[listaFlowChart].[A_N] >= " + +Convert.ToInt32(SNlist[idxGbSelected].Text), st);
        //                    if (st.modelloCollim.Contains('-'))
        //                    {
        //                        string[] tmp = st.modelloCollim.Split('-');
        //                        st.modelloCollim = tmp[0];
        //                    }
        //                    cmbCollModelList[idxGbSelected].SelectedItem = st.modelloCollim;
        //                    FClist[idxGbSelected].Text = st.IDFlow.ToString().PadLeft(4,'0') + "/" + st.anno.ToString();
        //                    coloraGroupbox(idxGbSelected);
        //                    idxGbSelected = -1;
        //                }
        //            }
        //            tbBarcode.Focus();
        //            tbBarcode.Clear();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //do nothing...
        //        tbBarcode.Focus();
        //        tbBarcode.Clear();
        //    }
        //}

        //private void coloraGroupbox(int index)
        //{
        //    foreach (GroupBox g in grpList)
        //        g.BackColor = SystemColors.Control;
        //}

        /// <summary>
        /// Se viene messa una versione più recente del file Autotester.exe nel path \\ralcosrv2\Public\Applicativi\Autotester, allora compare il bottone NEW per l'aggiornamento dell'applicativo.
        /// </summary>
        #region Aggiornamento Automatico
        private void checkForUpdates()
        {
            try
            {
                removeBakFiles();
                XmlDocument config = readXmlDocument("config.xml");
                XmlElement root = config.DocumentElement;
                XmlNodeList xmlNodeList = root.GetElementsByTagName("updates");
                if (xmlNodeList.Count.Equals(1))
                {
                    string[] dirs = Directory.GetFiles(xmlNodeList.Item(0).InnerXml.ToString(), "Autotester.exe");
                    if (dirs.Length.Equals(1))
                    {
                        FileInfo serverFileInfo = new FileInfo(dirs[0]);
                        FileInfo appFileInfo = new FileInfo(Environment.CurrentDirectory + @"\Autotester.exe");
                        if (serverFileInfo.LastWriteTime > appFileInfo.LastWriteTime)
                        {
                            btn_updates.Visible = true;
                            btn_updates.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //restituisce tutti gli elementi del file XML il cui tag è uguale ad elementName
        public XmlDocument readXmlDocument(string fileName)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(fileName);
                return (xml);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (null);
            }
        }

        private void removeBakFiles()
        {
            try
            {
                string[] dirs = Directory.GetFiles(Environment.CurrentDirectory, "Autotester.bak");
                if (!dirs.Length.Equals(0))
                    File.Delete(dirs[0]);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_updates_Click(object sender, EventArgs e)
        {
            try
            {
                File.Move(Environment.CurrentDirectory + @"\Autotester.exe", Environment.CurrentDirectory + @"\Autotester.bak");
                XmlDocument config = readXmlDocument("config.xml");
                XmlElement root = config.DocumentElement;
                XmlNodeList xmlNodeList = root.GetElementsByTagName("updates");
                string[] dirs = Directory.GetFiles(xmlNodeList.Item(0).InnerXml.ToString(), "Autotester.exe");
                File.Copy(dirs[0], Environment.CurrentDirectory + @"\Autotester.exe");
                DialogResult result = MessageBox.Show("Il programma verrà riavviato per consentire l\'installazione degli aggiornamenti.\nPremere OK per continuare...", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (result.Equals(DialogResult.OK))
                {
                    //File.Delete(Environment.CurrentDirectory + @"\Ralco_ServiceManager.bak");
                    this.Dispose();
                    Application.Restart();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void cb_quickTest_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_quickTest.Checked)
                {
                    if (user.dipartimento.Equals("ControlloFinale"))
                    {
                    }
                    else if (user.dipartimento.Equals("Mammografia"))
                    {
                        cmb_ModelloTest1.SelectedItem = "R 915 S DHHS";
                        cmb_ModelloTest2.SelectedItem = "R 915 S DHHS";
                        cmb_ModelloTest3.SelectedItem = "R 915 S DHHS";
                        cmb_ModelloTest4.SelectedItem = "R 915 S DHHS";
                        cmb_ModelloTest5.SelectedItem = "R 915 S DHHS";
                        tb_SNTest1.Text = "0000000";
                        tb_SNTest2.Text = "0000000";
                        tb_SNTest3.Text = "0000000";
                        tb_SNTest4.Text = "0000000";
                        tb_SNTest5.Text = "0000000";
                        tb_FCTest1.Text = "00000000";
                        tb_FCTest2.Text = "00000000";
                        tb_FCTest3.Text = "00000000";
                        tb_FCTest4.Text = "00000000";
                        tb_FCTest5.Text = "00000000";
                    }
                }
                else
                {
                    cmb_ModelloTest1.SelectedIndex = -1;
                    cmb_ModelloTest2.SelectedIndex = -1;
                    cmb_ModelloTest3.SelectedIndex = -1;
                    cmb_ModelloTest4.SelectedIndex = -1;
                    cmb_ModelloTest5.SelectedIndex = -1;
                    tb_SNTest1.Text = "";
                    tb_SNTest2.Text = "";
                    tb_SNTest3.Text = "";
                    tb_SNTest4.Text = "";
                    tb_SNTest5.Text = "";
                    tb_FCTest1.Text = "";
                    tb_FCTest2.Text = "";
                    tb_FCTest3.Text = "";
                    tb_FCTest4.Text = "";
                    tb_FCTest5.Text = "";
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form_Autotest_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            Utility.visualizeSwInformation();
        }
    }
}
