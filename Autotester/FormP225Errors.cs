﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormP225Errors : Form
    {
        public FormP225Errors()
        {
            InitializeComponent();
        }

        public void setErrors(P225ProtocolAnalyzer protocol)
        {
            ledCrossJam.On = protocol.CrossJam;
            ledCrossCalib.On = protocol.CrossCalib;
            ledCrossPot.On = protocol.CrossPot;

            ledLongJam.On = protocol.LongJam;
            ledLongCalib.On = protocol.LongCalib;
            ledLongPot.On = protocol.LongPot;

            ledFilterJam.On = protocol.FilterJam;
            ledFilterCalib.On = protocol.FilterCalib;

            ledGeneral.On = protocol.generalError;
            ledAEP.On = protocol.AEPError;
            ledComunicazione.On = protocol.communicationError;

            ledErr0.On = protocol.error0;
            ledErr90.On = protocol.error90;
            ledErr180.On = protocol.error180;
            ledErr270.On = protocol.error270;

            if (protocol.passiCross.Equals(-1))
                lbl_deltaPassiCross.Text = protocol.passiCross.ToString();
            else
                lbl_deltaPassiCross.Text = (Math.Abs(50 - protocol.passiCross)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiCross.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiCross.Text) > 50))
                lbl_deltaPassiCross.BackColor = Color.Red;
            if (protocol.passiLong.Equals(-1))
                lbl_deltaPassiLong.Text = protocol.passiLong.ToString();
            else
                lbl_deltaPassiLong.Text = (Math.Abs(50 - protocol.passiLong)).ToString();
            if ((Convert.ToInt32(lbl_deltaPassiLong.Text) == -1) || (Convert.ToInt32(lbl_deltaPassiLong.Text) > 50))
                lbl_deltaPassiLong.BackColor = Color.Red;
            lbl_deltaPotCross.Text = protocol.deltaPotenziometriCross.ToString();
            if (protocol.CrossJam)
                lbl_deltaPotCross.BackColor = Color.Red;
            lbl_deltaPotLong.Text = protocol.deltaPotenziometriLong.ToString();
            if (protocol.LongJam)
                lbl_deltaPotLong.BackColor = Color.Red;
            lbl_deltaEncCross.Text = protocol.deltaEncodersCross.ToString();
            if (protocol.CrossJam)
                lbl_deltaEncCross.BackColor = Color.Red;
            lbl_deltaEncLong.Text = protocol.deltaEncodersLong.ToString();
            if (protocol.LongJam)
                lbl_deltaEncLong.BackColor = Color.Red;
        }
    }
}
