﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Lawicel;
using System.DirectoryServices.AccountManagement;
using Newtonsoft.Json;
using NLog;
using System.Data.SqlClient;

//classe deprecata
namespace Autotester
{
    public partial class FormCANusb_HUB : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private UserPrincipal user;
        private bool isLocalTest = false;
        private Dictionary<string, ConfigurazioneTest[]> configurations;
        private Dictionary<string, Dictionary<int, string>> CANUSBmapping;
        private Dictionary<string, Dictionary<int, string>> CANUSBmapping_TMP;
        private int numCANusbDetectedToHUB;//numero di dongle CANUSB connessi all'HUB
        private Dictionary<string, Dictionary<int, CANusb_Connector>> connector = new Dictionary<string, Dictionary<int, CANusb_Connector>>();
        List<string> connectorsDetected;//lista dei nomi dei connettori effettivamente rilevati
        private Dictionary<string, Dictionary<int, Label>> labelDict;// = new Dictionary<string, Dictionary<int, TextBox>>(); 
        private Dictionary<string, Dictionary<int, LedBulb>> ledBulbDict;// = new Dictionary<string, Dictionary<int, TextBox>>(); 
        private string[] deviceType = new string[] { "Collimators", "External Devices" };
        /*Forms comunicanti con il CANusb HUB*/
        FormControls formControls;

        public FormCANusb_HUB()
        {
            InitializeComponent();
            try
            {
                configurations = new Dictionary<string, ConfigurazioneTest[]>();
                string json = Environment.CurrentDirectory + @"\config\extDevicesConfigurations.txt";
                configurations = JsonConvert.DeserializeObject<Dictionary<string, ConfigurazioneTest[]>>(File.ReadAllText(json));
                selectCollimatorsFromDB();
                foreach (var e in configurations)
                {
                    //if (e.Key.Equals("Collimators"))
                    //    for (int i = 0; i < e.Value.Length; i++)
                    //        cmb_collType.Items.Add(e.Value[i].deviceName);
                    //else if (e.Key.Equals("External Devices"))
                    //    for (int i = 0; i < e.Value.Length; i++)
                    //        cmb_extDevType.Items.Add(e.Value[i].deviceName);
                }
                json = Environment.CurrentDirectory + @"\config\CANUSBMapping.txt";
                CANUSBmapping = new Dictionary<string, Dictionary<int, string>>();
                CANUSBmapping = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<int, string>>>(File.ReadAllText(json));
                //inizializzazione del dizionario mapping
                labelDict = new Dictionary<string, Dictionary<int, Label>> 
                { { "Collimators", new Dictionary<int, Label> { { 1, lbl_CANusb_Coll1 }, { 2, lbl_CANusb_Coll2 }, { 3, lbl_CANusb_Coll3 }, { 4, lbl_CANusb_Coll4 }, { 5, lbl_CANusb_Coll5 } } },
                { "External Devices", new Dictionary<int, Label> { { 1, lbl_CANusb_ExtDev1 } } } };
                ledBulbDict = new Dictionary<string, Dictionary<int, LedBulb>> 
                { { "Collimators", new Dictionary<int, LedBulb> { { 1, ledBulbColl1 }, { 2, ledBulbColl2 }, { 3, ledBulbColl3 }, { 4, ledBulbColl4 }, { 5, ledBulbColl5 } } },
                { "External Devices", new Dictionary<int, LedBulb> { { 1, ledBulbExtDev1 } } } };
                loadCANUSB();
                user = UserPrincipal.Current;
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show(e.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }  
        }

        private void selectCollimatorsFromDB()
        {
            try
            {
                string strConn = "";
                //imposto la connessione al database
                if (isLocalTest.Equals(false))
                    strConn = "Data Source =RALCOSRV4\\SQLEXPRESS;Initial Catalog=Autotest;Integrated Security=True;User ID=SCarminati;Password=RalCar028";
                else
                    strConn = "Data Source =SIMONE-PC\\CHAMELEON;Initial Catalog=Autotest;Integrated Security=True";
                SqlConnection conn = new SqlConnection(strConn);
                //Creo il comando da eseguire mediante SQLCommand
                SqlCommand count = new SqlCommand("SELECT COUNT ([nomeCollim]) FROM [AutoTest].[dbo].[testAttivi]", conn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM [AutoTest].[dbo].[testAttivi]", conn);
                //apro la connessione
                conn.Open();
                //eseguo la query
                count.ExecuteNonQuery();
                int numColl = Convert.ToInt32(count.ExecuteScalar());
                cmd.ExecuteNonQuery();
                //chiudo la connessione
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    configurations.Add("Collimators", new ConfigurazioneTest[numColl]);
                    int i = 0;
                    while (reader.Read())
                    {
                        configurations["Collimators"][i] = new ConfigurazioneTest();
                        //configurations["Collimators"][i].deviceName = reader.GetString(1);
                        //configurations["Collimators"][i].baudRate = reader.GetString(2);
                        //configurations["Collimators"][i].panelName = reader.GetString(3);
                        configurations["Collimators"][i].protocolName = reader.GetString(4);
                        configurations["Collimators"][i].testMetro = reader.GetBoolean(5);
                        configurations["Collimators"][i].testLuce = reader.GetBoolean(6);
                        configurations["Collimators"][i].testDSC = reader.GetBoolean(7);
                        configurations["Collimators"][i].testLaser = reader.GetBoolean(8);
                        configurations["Collimators"][i].testBtnFiltro = reader.GetBoolean(9);
                        configurations["Collimators"][i].testBtnLuce = reader.GetBoolean(10);
                        configurations["Collimators"][i].testBtnIride = reader.GetBoolean(11);
                        configurations["Collimators"][i].testChiave = reader.GetBoolean(12);
                        configurations["Collimators"][i].testVita = reader.GetBoolean(13);
                        i++;
                    }
                }
                else
                {
                    MessageBox.Show("No Collimators into the Database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadCANUSB()
        {
            StringBuilder buf = new StringBuilder(32);
            connectorsDetected = new List<string>();
            int idx = 0;
            //inizializzo i led a Grey e le textboxes a valore vuoto
            foreach (var v in labelDict)
            {
                foreach (var v2 in v.Value)
                {
                    connectorManager(v.Key, v2.Key, 0);
                }
            }
            numCANusbDetectedToHUB = CANUSB.canusb_getFirstAdapter(buf, 32);
            if (numCANusbDetectedToHUB > 0)
            {
                connectorsDetected.Add(buf.ToString());
                for (int i = 1; i < numCANusbDetectedToHUB; i++ )
                {
                    if (CANUSB.canusb_getNextAdapter(buf, 32) > 0)
                    {
                        idx++;
                        connectorsDetected.Add(buf.ToString());
                    }
                }
            }
            foreach (var v in CANUSBmapping)
            {
                foreach (var v2 in v.Value)
                {
                    if (connectorsDetected.Contains(v2.Value))
                    {
                        connectorManager(v.Key, v2.Key, 1, v2.Value);
                    }
                }                    
            }
        }

        /*OBIETTIVO : setta i parametri dell'HUB (textboxes, led,...) a seconda del valore di main ed esegue le operazioni di connessione / disconnessione
         *       deviceType = Collimators, External Devices
         *       deviceNumber = 1,2,3,...
         *       main = 0 -> setta i parametri per channel Not Present
         *              1 -> setta i parametri per channel in stand-by
         *              2 -> esegue la connect e setta i parametri per channel connesso
         *              3 -> esegue la disconnect e setta i parametri per channel disconnesso
         OUTPUT : / */
        private void connectorManager(string deviceType, int deviceNumber, int main, params string[] CANUSB_name)
        {
            switch (main)
            {
                case 0://Connector Not Present
                    labelDict[deviceType][deviceNumber].Text = "";
                    ledBulbDict[deviceType][deviceNumber].On = false;
                    ledBulbDict[deviceType][deviceNumber].Color = Color.Gray;
                    if (deviceType.Equals("Collimators"))
                        btn_ConnectCollimators.Enabled = false;
                    else if (deviceType.Equals("External Devices"))
                        btn_ConnectExtDev.Enabled = false;
                    break;
                case 1://Connector Detected
                    labelDict[deviceType][deviceNumber].Text = CANUSB_name[0];
                    ledBulbDict[deviceType][deviceNumber].On = true;
                    ledBulbDict[deviceType][deviceNumber].Color = Color.LightBlue;
                    if (!connector.ContainsKey(deviceType))
                    {
                        connector.Add(deviceType, null);
                        connector[deviceType] = new Dictionary<int, CANusb_Connector> { { deviceNumber, new CANusb_Connector() } };
                    }
                    else
                    {
                        if (!connector[deviceType].ContainsKey(deviceNumber))
                        {
                            connector[deviceType][deviceNumber] = new CANusb_Connector();
                        }

                    }
                    connector[deviceType][deviceNumber].connectorName = CANUSB_name[0];
                    if (deviceType.Equals("Collimators"))
                        btn_ConnectCollimators.Enabled = true;
                    else if (deviceType.Equals("External Devices"))
                        btn_ConnectExtDev.Enabled = true;
                    break;
                case 2://Connection
                    if (deviceType.Equals("Collimators") && String.Compare(cmb_collType.GetItemText(cmb_collType.SelectedItem), "", false) == 0)
                        MessageBox.Show("Select a valid X-Ray Collimator Type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (deviceType.Equals("Collimators") && String.Compare(cmb_BaudRateCollimators.GetItemText(cmb_BaudRateCollimators.SelectedItem), "", false) == 0)
                        MessageBox.Show("Select a valid X-Ray Collimator Baud Rate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (deviceType.Equals("External Devices") && String.Compare(cmb_extDevType.GetItemText(cmb_extDevType.SelectedItem), "", false) == 0)
                        MessageBox.Show("Select a valid External Device Type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (deviceType.Equals("External Devices") && String.Compare(cmb_BaudRateExtDevices.GetItemText(cmb_BaudRateExtDevices.SelectedItem), "", false) == 0)
                        MessageBox.Show("Select a valid External Device Baud Rate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        string baud;
                        if (deviceType.Equals("Collimators"))
                        {
                            baud = cmb_BaudRateCollimators.GetItemText(cmb_BaudRateCollimators.SelectedItem);
                        }
                        else if (deviceType.Equals("External Devices"))
                        {
                            baud = cmb_BaudRateExtDevices.GetItemText(cmb_BaudRateExtDevices.SelectedItem);
                        }
                        else
                        {
                            return;
                        }

                        //if (connector[deviceType][deviceNumber].CANusb_connect(baud) >= 0)
                        //{
                        //    ledBulbDict[deviceType][deviceNumber].On = true;
                        //    ledBulbDict[deviceType][deviceNumber].Color = Color.LightGreen;
                        //    if (deviceType.Equals("Collimators"))
                        //        btn_ConnectCollimators.Text = "Disconnetti";
                        //    else if (deviceType.Equals("External Devices"))
                        //        btn_ConnectExtDev.Text = "Disconnetti";
                        //}
                        //else
                        //{
                        //    ledBulbDict[deviceType][deviceNumber].On = true;
                        //    ledBulbDict[deviceType][deviceNumber].Color = Color.Red;
                        //}
                    }
                    break;
                case 3://Disconnection
                    //if (connector[deviceType][deviceNumber].CANusb_disconnect().Equals(1))
                    //{
                    //    ledBulbDict[deviceType][deviceNumber].On = true;
                    //    ledBulbDict[deviceType][deviceNumber].Color = Color.LightBlue;
                    //    if (deviceType.Equals("Collimators"))
                    //        btn_ConnectCollimators.Text = "Connetti";
                    //    else if (deviceType.Equals("External Devices"))
                    //        btn_ConnectExtDev.Text = "Connetti";
                    //}
                    //else
                    //{
                    //    ledBulbDict[deviceType][deviceNumber].On = true;
                    //    ledBulbDict[deviceType][deviceNumber].Color = Color.Red;
                    //}
                    break;
            }
        }

        private void startTestForm(Button b) //int index, CANusb_Connector caNusbConnector)
        {
            //FormTestMain form = new FormTestMain();

            //bool ok = false;
            //foreach (var v in CANUSBmapping)
            //{
            //    if (v.Key.Equals(b.Tag))
            //    {
            //        foreach (var v2 in v.Value)
            //        {
            //            if (connectorsDetected.Contains(v2.Value) && !v2.Value.Equals(lbl_CANusb_ExtDev1.Text))
            //            {
            //                //connectorManager(v.Key, v2.Key, 2);
            //                if (ledBulbDict[v.Key][v2.Key].On)
            //                {
            //                    //form.setPanelTester(v2.Key, connector[v.Key][v2.Key], configurations["Collimators"][cmb_collType.SelectedIndex].panelName);
            //                    //form.setPanelTester(v2.Key, connector[v.Key][v2.Key], configurations["Collimators"][cmb_collType.SelectedIndex]);
            //                    ok = true;
            //                }
            //            }

            //        }

            //    }

            //}

            //if (ok)
            //{
            //    if (ledBulbExtDev1.On && (ledBulbExtDev1.Color == Color.LightGreen))
            //    {
            //        form.ExtDevConnector = connector[btn_ConnectExtDev.Tag.ToString()][1];
            //        form.NameExtDevConnector = cmb_extDevType.Text;
            //    }

            //    //form.setLabelsInfo(user.DisplayName, cmb_collType.GetItemText(cmb_collType.SelectedItem));
            //    form.Show(this);    
            //}
            
            //throw new NotImplementedException();
        }

        /*DESCRIZIONE : setta automaticamente il valore del baud rate indicato nella combobox 
          INPUT : main = 1 - setta automaticamente i valori di cmb_BaudRateCollimators
                  main = 2  - setta automaticamente i valori di cmb_BaudRateExtDevices*/
        private void setBaudRate(int main)
        {
            switch (main)
            {
                case 1:
                    cmb_BaudRateCollimators.Text = configurations["Collimators"][cmb_collType.SelectedIndex].baudRate.ToString();
                    break;
                case 2:
                    cmb_BaudRateExtDevices.Text = configurations["External Devices"][cmb_extDevType.SelectedIndex].baudRate.ToString();
                    break;
            }
            
        }

        //********** Gestione Combobox **********//

        private void cmb_collType_ALLCH_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBaudRate(1);
        }

        private void cmb_extDevType_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBaudRate(2);
        }

        //********** Gestione Bottoni **********//
        private void btn_ReloadAllCANUSB_Click(object sender, EventArgs e)
        {
            loadCANUSB();
        }
        //********** CONNECT **********//

        private void btn_ConnectCollimators_Click(object sender, EventArgs e)
        {
            connectDevices(btn_ConnectCollimators);
        }

        private void btn_ConnectExtDev_Click(object sender, EventArgs e)
        {
            connectDevices(btn_ConnectExtDev);
        }

        private void connectDevices(Button b)
        {
            if (b.Text.Equals("Connetti"))
            {
                foreach (var v in CANUSBmapping)
                {
                    if (v.Key.Equals(b.Tag))
                    {
                        foreach (var v2 in v.Value)
                        {
                            if (connectorsDetected.Contains(v2.Value))
                            {
                                connectorManager(v.Key, v2.Key, 2);
                            }
                                
                        }
                            
                    }
                        
                }

                // Se ho Disconnect -> avvio il test
                if (b.Text.Equals("Disconnetti") && !b.Tag.Equals("External Devices"))
                {
                    startTestForm(b);
                    btn_ReloadAllCANUSB.Enabled = false;
                    btn_ConfigureCANUSB.Enabled = false;
                }
                    
            }
            else if (b.Text.Equals("Disconnetti"))
            {
                foreach (var v in connector)
                {
                    if (v.Key.Equals(b.Tag))
                    {
                        foreach (var v2 in v.Value)
                        {
                            if (connectorsDetected.Contains(v2.Value.connectorName))
                            {
                                connectorManager(v.Key, v2.Key, 3);
                            }
                            
                        }                        
                    }                    
                }
                if (!b.Tag.Equals("External Devices"))
                {
                    btn_ReloadAllCANUSB.Enabled = true;
                    btn_ConfigureCANUSB.Enabled = true;
                    
                }
            }
        }

        //********** Gestione External Devices **********//

        private void testWheelP225ACSDHHSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formControls.Show();
        }

        private void testWheelR915SDHHSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formControls.Show();
        }

        private void FormCANusb_HUB_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
         //   Thread.Sleep(2000);
        }

        private void btn_ConfigureCANUSB_Click(object sender, EventArgs e)
        {
            loadCANUSB();
            FormCANUSBConfig formCANUSBConfig = new FormCANUSBConfig(connectorsDetected, CANUSBmapping);
            formCANUSBConfig.Show();
        }

        private void FormCANusb_HUB_Load(object sender, EventArgs e)
        {
            logger.Info("Start tester application");
        }

    }


}
