﻿namespace Autotester
{
    partial class FormCANusb_HUB
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_ConfigureCANUSB = new System.Windows.Forms.Button();
            this.btn_ReloadAllCANUSB = new System.Windows.Forms.Button();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.cmb_BaudRateCollimators = new System.Windows.Forms.ComboBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.cmb_collType = new System.Windows.Forms.ComboBox();
            this.btn_ConnectCollimators = new System.Windows.Forms.Button();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.lbl_CANusb_Coll5 = new System.Windows.Forms.Label();
            this.lbl_CANusb_Coll4 = new System.Windows.Forms.Label();
            this.lbl_CANusb_Coll3 = new System.Windows.Forms.Label();
            this.lbl_CANusb_Coll2 = new System.Windows.Forms.Label();
            this.lbl_CANusb_Coll1 = new System.Windows.Forms.Label();
            this.ledBulbColl5 = new Autotester.LedBulb();
            this.ledBulbColl4 = new Autotester.LedBulb();
            this.ledBulbColl3 = new Autotester.LedBulb();
            this.ledBulbColl2 = new Autotester.LedBulb();
            this.ledBulbColl1 = new Autotester.LedBulb();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmb_BaudRateExtDevices = new System.Windows.Forms.ComboBox();
            this.btn_ConnectExtDev = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmb_extDevType = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lbl_CANusb_ExtDev1 = new System.Windows.Forms.Label();
            this.ledBulbExtDev1 = new Autotester.LedBulb();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox34);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 465);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CANusb HUB Connection Manager";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_ConfigureCANUSB);
            this.groupBox4.Controls.Add(this.btn_ReloadAllCANUSB);
            this.groupBox4.Location = new System.Drawing.Point(6, 18);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(445, 54);
            this.groupBox4.TabIndex = 98;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Setup";
            // 
            // btn_ConfigureCANUSB
            // 
            this.btn_ConfigureCANUSB.Location = new System.Drawing.Point(115, 17);
            this.btn_ConfigureCANUSB.Name = "btn_ConfigureCANUSB";
            this.btn_ConfigureCANUSB.Size = new System.Drawing.Size(94, 30);
            this.btn_ConfigureCANUSB.TabIndex = 2;
            this.btn_ConfigureCANUSB.Tag = "Reload";
            this.btn_ConfigureCANUSB.Text = "Configura";
            this.btn_ConfigureCANUSB.UseVisualStyleBackColor = true;
            this.btn_ConfigureCANUSB.Click += new System.EventHandler(this.btn_ConfigureCANUSB_Click);
            // 
            // btn_ReloadAllCANUSB
            // 
            this.btn_ReloadAllCANUSB.Location = new System.Drawing.Point(6, 17);
            this.btn_ReloadAllCANUSB.Name = "btn_ReloadAllCANUSB";
            this.btn_ReloadAllCANUSB.Size = new System.Drawing.Size(94, 30);
            this.btn_ReloadAllCANUSB.TabIndex = 1;
            this.btn_ReloadAllCANUSB.Tag = "Reload";
            this.btn_ReloadAllCANUSB.Text = "Reload";
            this.btn_ReloadAllCANUSB.UseVisualStyleBackColor = true;
            this.btn_ReloadAllCANUSB.Click += new System.EventHandler(this.btn_ReloadAllCANUSB_Click);
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.groupBox3);
            this.groupBox34.Controls.Add(this.groupBox28);
            this.groupBox34.Location = new System.Drawing.Point(6, 252);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(445, 205);
            this.groupBox34.TabIndex = 96;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Console Collimatori";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox30);
            this.groupBox3.Controls.Add(this.groupBox31);
            this.groupBox3.Controls.Add(this.btn_ConnectCollimators);
            this.groupBox3.Location = new System.Drawing.Point(6, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(149, 175);
            this.groupBox3.TabIndex = 95;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Connection Manager";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.cmb_BaudRateCollimators);
            this.groupBox30.Location = new System.Drawing.Point(9, 65);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(135, 51);
            this.groupBox30.TabIndex = 2;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Baud Rate [kbit/s]";
            // 
            // cmb_BaudRateCollimators
            // 
            this.cmb_BaudRateCollimators.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_BaudRateCollimators.FormattingEnabled = true;
            this.cmb_BaudRateCollimators.Items.AddRange(new object[] {
            "1000",
            "800",
            "500",
            "250",
            "125",
            "100",
            "50"});
            this.cmb_BaudRateCollimators.Location = new System.Drawing.Point(10, 19);
            this.cmb_BaudRateCollimators.Name = "cmb_BaudRateCollimators";
            this.cmb_BaudRateCollimators.Size = new System.Drawing.Size(120, 21);
            this.cmb_BaudRateCollimators.TabIndex = 15;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.cmb_collType);
            this.groupBox31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox31.Location = new System.Drawing.Point(9, 13);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(135, 46);
            this.groupBox31.TabIndex = 1;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Tipo Collimatore";
            // 
            // cmb_collType
            // 
            this.cmb_collType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_collType.FormattingEnabled = true;
            this.cmb_collType.Location = new System.Drawing.Point(10, 19);
            this.cmb_collType.Name = "cmb_collType";
            this.cmb_collType.Size = new System.Drawing.Size(120, 21);
            this.cmb_collType.TabIndex = 8;
            this.cmb_collType.SelectedIndexChanged += new System.EventHandler(this.cmb_collType_ALLCH_SelectedIndexChanged);
            // 
            // btn_ConnectCollimators
            // 
            this.btn_ConnectCollimators.Enabled = false;
            this.btn_ConnectCollimators.Location = new System.Drawing.Point(19, 135);
            this.btn_ConnectCollimators.Name = "btn_ConnectCollimators";
            this.btn_ConnectCollimators.Size = new System.Drawing.Size(120, 23);
            this.btn_ConnectCollimators.TabIndex = 22;
            this.btn_ConnectCollimators.Tag = "Collimators";
            this.btn_ConnectCollimators.Text = "Connetti";
            this.btn_ConnectCollimators.UseVisualStyleBackColor = true;
            this.btn_ConnectCollimators.Click += new System.EventHandler(this.btn_ConnectCollimators_Click);
            // 
            // groupBox28
            // 
            this.groupBox28.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox28.Controls.Add(this.lbl_CANusb_Coll5);
            this.groupBox28.Controls.Add(this.lbl_CANusb_Coll4);
            this.groupBox28.Controls.Add(this.lbl_CANusb_Coll3);
            this.groupBox28.Controls.Add(this.lbl_CANusb_Coll2);
            this.groupBox28.Controls.Add(this.lbl_CANusb_Coll1);
            this.groupBox28.Controls.Add(this.ledBulbColl5);
            this.groupBox28.Controls.Add(this.ledBulbColl4);
            this.groupBox28.Controls.Add(this.ledBulbColl3);
            this.groupBox28.Controls.Add(this.ledBulbColl2);
            this.groupBox28.Controls.Add(this.ledBulbColl1);
            this.groupBox28.Controls.Add(this.label5);
            this.groupBox28.Controls.Add(this.label4);
            this.groupBox28.Controls.Add(this.label1);
            this.groupBox28.Controls.Add(this.label3);
            this.groupBox28.Controls.Add(this.label2);
            this.groupBox28.Location = new System.Drawing.Point(161, 21);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(279, 175);
            this.groupBox28.TabIndex = 94;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Collimatori";
            // 
            // lbl_CANusb_Coll5
            // 
            this.lbl_CANusb_Coll5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_Coll5.Location = new System.Drawing.Point(109, 138);
            this.lbl_CANusb_Coll5.Name = "lbl_CANusb_Coll5";
            this.lbl_CANusb_Coll5.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_Coll5.TabIndex = 96;
            this.lbl_CANusb_Coll5.Text = "CONNECTOR";
            this.lbl_CANusb_Coll5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANusb_Coll4
            // 
            this.lbl_CANusb_Coll4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_Coll4.Location = new System.Drawing.Point(109, 108);
            this.lbl_CANusb_Coll4.Name = "lbl_CANusb_Coll4";
            this.lbl_CANusb_Coll4.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_Coll4.TabIndex = 95;
            this.lbl_CANusb_Coll4.Text = "CONNECTOR";
            this.lbl_CANusb_Coll4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANusb_Coll3
            // 
            this.lbl_CANusb_Coll3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_Coll3.Location = new System.Drawing.Point(109, 78);
            this.lbl_CANusb_Coll3.Name = "lbl_CANusb_Coll3";
            this.lbl_CANusb_Coll3.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_Coll3.TabIndex = 94;
            this.lbl_CANusb_Coll3.Text = "CONNECTOR";
            this.lbl_CANusb_Coll3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANusb_Coll2
            // 
            this.lbl_CANusb_Coll2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_Coll2.Location = new System.Drawing.Point(109, 50);
            this.lbl_CANusb_Coll2.Name = "lbl_CANusb_Coll2";
            this.lbl_CANusb_Coll2.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_Coll2.TabIndex = 93;
            this.lbl_CANusb_Coll2.Text = "CONNECTOR";
            this.lbl_CANusb_Coll2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANusb_Coll1
            // 
            this.lbl_CANusb_Coll1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_Coll1.Location = new System.Drawing.Point(109, 20);
            this.lbl_CANusb_Coll1.Name = "lbl_CANusb_Coll1";
            this.lbl_CANusb_Coll1.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_Coll1.TabIndex = 36;
            this.lbl_CANusb_Coll1.Text = "CONNECTOR";
            this.lbl_CANusb_Coll1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ledBulbColl5
            // 
            this.ledBulbColl5.Location = new System.Drawing.Point(245, 139);
            this.ledBulbColl5.Name = "ledBulbColl5";
            this.ledBulbColl5.On = true;
            this.ledBulbColl5.Size = new System.Drawing.Size(24, 23);
            this.ledBulbColl5.TabIndex = 92;
            this.ledBulbColl5.Text = "ledBulb5";
            // 
            // ledBulbColl4
            // 
            this.ledBulbColl4.Location = new System.Drawing.Point(245, 109);
            this.ledBulbColl4.Name = "ledBulbColl4";
            this.ledBulbColl4.On = true;
            this.ledBulbColl4.Size = new System.Drawing.Size(24, 23);
            this.ledBulbColl4.TabIndex = 91;
            this.ledBulbColl4.Text = "ledBulb4";
            // 
            // ledBulbColl3
            // 
            this.ledBulbColl3.Location = new System.Drawing.Point(245, 79);
            this.ledBulbColl3.Name = "ledBulbColl3";
            this.ledBulbColl3.On = true;
            this.ledBulbColl3.Size = new System.Drawing.Size(24, 23);
            this.ledBulbColl3.TabIndex = 90;
            this.ledBulbColl3.Text = "ledBulb3";
            // 
            // ledBulbColl2
            // 
            this.ledBulbColl2.Location = new System.Drawing.Point(245, 49);
            this.ledBulbColl2.Name = "ledBulbColl2";
            this.ledBulbColl2.On = true;
            this.ledBulbColl2.Size = new System.Drawing.Size(24, 23);
            this.ledBulbColl2.TabIndex = 89;
            this.ledBulbColl2.Text = "ledBulb2";
            // 
            // ledBulbColl1
            // 
            this.ledBulbColl1.Location = new System.Drawing.Point(245, 19);
            this.ledBulbColl1.Name = "ledBulbColl1";
            this.ledBulbColl1.On = true;
            this.ledBulbColl1.Size = new System.Drawing.Size(24, 23);
            this.ledBulbColl1.TabIndex = 36;
            this.ledBulbColl1.Text = "ledBulb1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 88;
            this.label5.Text = "Collimatore 5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 87;
            this.label4.Text = "Collimatore 4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 84;
            this.label1.Text = "Collimatore 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 86;
            this.label3.Text = "Collimatore 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "Collimatore 2";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Location = new System.Drawing.Point(6, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(445, 180);
            this.groupBox2.TabIndex = 97;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Console Dispositivi Esterni";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox6);
            this.groupBox7.Controls.Add(this.btn_ConnectExtDev);
            this.groupBox7.Controls.Add(this.groupBox5);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(149, 156);
            this.groupBox7.TabIndex = 89;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Connection Manager";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cmb_BaudRateExtDevices);
            this.groupBox6.Location = new System.Drawing.Point(5, 71);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(135, 51);
            this.groupBox6.TabIndex = 88;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Baud Rate [kbit/s]";
            // 
            // cmb_BaudRateExtDevices
            // 
            this.cmb_BaudRateExtDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_BaudRateExtDevices.FormattingEnabled = true;
            this.cmb_BaudRateExtDevices.Items.AddRange(new object[] {
            "1000",
            "800",
            "500",
            "250",
            "125",
            "100",
            "50"});
            this.cmb_BaudRateExtDevices.Location = new System.Drawing.Point(10, 19);
            this.cmb_BaudRateExtDevices.Name = "cmb_BaudRateExtDevices";
            this.cmb_BaudRateExtDevices.Size = new System.Drawing.Size(120, 21);
            this.cmb_BaudRateExtDevices.TabIndex = 16;
            // 
            // btn_ConnectExtDev
            // 
            this.btn_ConnectExtDev.Enabled = false;
            this.btn_ConnectExtDev.Location = new System.Drawing.Point(15, 127);
            this.btn_ConnectExtDev.Name = "btn_ConnectExtDev";
            this.btn_ConnectExtDev.Size = new System.Drawing.Size(120, 23);
            this.btn_ConnectExtDev.TabIndex = 23;
            this.btn_ConnectExtDev.Tag = "External Devices";
            this.btn_ConnectExtDev.Text = "Connetti";
            this.btn_ConnectExtDev.UseVisualStyleBackColor = true;
            this.btn_ConnectExtDev.Click += new System.EventHandler(this.btn_ConnectExtDev_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cmb_extDevType);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(5, 17);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(135, 46);
            this.groupBox5.TabIndex = 87;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dispositivo Esterno";
            // 
            // cmb_extDevType
            // 
            this.cmb_extDevType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_extDevType.FormattingEnabled = true;
            this.cmb_extDevType.Location = new System.Drawing.Point(10, 19);
            this.cmb_extDevType.Name = "cmb_extDevType";
            this.cmb_extDevType.Size = new System.Drawing.Size(120, 21);
            this.cmb_extDevType.TabIndex = 9;
            this.cmb_extDevType.SelectedIndexChanged += new System.EventHandler(this.cmb_extDevType_SelectedIndexChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lbl_CANusb_ExtDev1);
            this.groupBox8.Controls.Add(this.ledBulbExtDev1);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Location = new System.Drawing.Point(161, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(275, 156);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Dispositivi Esterni";
            // 
            // lbl_CANusb_ExtDev1
            // 
            this.lbl_CANusb_ExtDev1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANusb_ExtDev1.Location = new System.Drawing.Point(105, 67);
            this.lbl_CANusb_ExtDev1.Name = "lbl_CANusb_ExtDev1";
            this.lbl_CANusb_ExtDev1.Size = new System.Drawing.Size(120, 20);
            this.lbl_CANusb_ExtDev1.TabIndex = 97;
            this.lbl_CANusb_ExtDev1.Text = "CONNECTOR";
            this.lbl_CANusb_ExtDev1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ledBulbExtDev1
            // 
            this.ledBulbExtDev1.Location = new System.Drawing.Point(245, 65);
            this.ledBulbExtDev1.Name = "ledBulbExtDev1";
            this.ledBulbExtDev1.On = true;
            this.ledBulbExtDev1.Size = new System.Drawing.Size(24, 23);
            this.ledBulbExtDev1.TabIndex = 86;
            this.ledBulbExtDev1.Text = "ledBulb1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "Dispositivo 1";
            // 
            // FormCANusb_HUB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 485);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormCANusb_HUB";
            this.Text = "Ralco Autotester - V01.00.00";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCANusb_HUB_FormClosing);
            this.Load += new System.EventHandler(this.FormCANusb_HUB_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox34.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button btn_ConnectCollimators;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.ComboBox cmb_BaudRateCollimators;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.ComboBox cmb_collType;
        private System.Windows.Forms.Button btn_ReloadAllCANUSB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn_ConnectExtDev;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cmb_BaudRateExtDevices;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cmb_extDevType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox8;
        private LedBulb ledBulbColl5;
        private LedBulb ledBulbColl4;
        private LedBulb ledBulbColl3;
        private LedBulb ledBulbColl2;
        private LedBulb ledBulbColl1;
        private LedBulb ledBulbExtDev1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn_ConfigureCANUSB;
        private System.Windows.Forms.Label lbl_CANusb_Coll5;
        private System.Windows.Forms.Label lbl_CANusb_Coll4;
        private System.Windows.Forms.Label lbl_CANusb_Coll3;
        private System.Windows.Forms.Label lbl_CANusb_Coll2;
        private System.Windows.Forms.Label lbl_CANusb_Coll1;
        private System.Windows.Forms.Label lbl_CANusb_ExtDev1;
    }
}

