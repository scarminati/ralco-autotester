﻿namespace Autotester
{
    partial class FormTestMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panCollimator1 = new System.Windows.Forms.Panel();
            this.panCollimator2 = new System.Windows.Forms.Panel();
            this.panCollimator3 = new System.Windows.Forms.Panel();
            this.panCollimator4 = new System.Windows.Forms.Panel();
            this.panCollimator5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.btnReoadScript = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDurata = new System.Windows.Forms.MaskedTextBox();
            this.txtNumCicli = new System.Windows.Forms.MaskedTextBox();
            this.optCicli = new System.Windows.Forms.RadioButton();
            this.optTempo = new System.Windows.Forms.RadioButton();
            this.btnTestVita = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tmrClock = new System.Windows.Forms.Timer(this.components);
            this.grpRuota = new System.Windows.Forms.GroupBox();
            this.btnRuota = new System.Windows.Forms.Button();
            this.chkTestRuota = new System.Windows.Forms.CheckBox();
            this.lblTipoRuota = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCicli = new System.Windows.Forms.Label();
            this.lblTester = new System.Windows.Forms.Label();
            this.btnTestReset = new System.Windows.Forms.Button();
            this.tmrTestReset = new System.Windows.Forms.Timer(this.components);
            this.chk_EscludiTestManuali = new System.Windows.Forms.CheckBox();
            this.chk_avviaAncheConErrori = new System.Windows.Forms.CheckBox();
            this.timerTestPausa = new System.Windows.Forms.Timer(this.components);
            this.chk_saveCalTable = new System.Windows.Forms.CheckBox();
            this.chk_saveTestReport = new System.Windows.Forms.CheckBox();
            this.btnPausaTestVita = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.grpRuota.SuspendLayout();
            this.SuspendLayout();
            // 
            // panCollimator1
            // 
            this.panCollimator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCollimator1.Location = new System.Drawing.Point(30, 85);
            this.panCollimator1.MaximumSize = new System.Drawing.Size(1242, 120);
            this.panCollimator1.MinimumSize = new System.Drawing.Size(1242, 120);
            this.panCollimator1.Name = "panCollimator1";
            this.panCollimator1.Size = new System.Drawing.Size(1242, 120);
            this.panCollimator1.TabIndex = 0;
            // 
            // panCollimator2
            // 
            this.panCollimator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCollimator2.Location = new System.Drawing.Point(30, 211);
            this.panCollimator2.Name = "panCollimator2";
            this.panCollimator2.Size = new System.Drawing.Size(1242, 120);
            this.panCollimator2.TabIndex = 2;
            // 
            // panCollimator3
            // 
            this.panCollimator3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCollimator3.Location = new System.Drawing.Point(30, 337);
            this.panCollimator3.Name = "panCollimator3";
            this.panCollimator3.Size = new System.Drawing.Size(1242, 120);
            this.panCollimator3.TabIndex = 4;
            // 
            // panCollimator4
            // 
            this.panCollimator4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCollimator4.Location = new System.Drawing.Point(30, 463);
            this.panCollimator4.Name = "panCollimator4";
            this.panCollimator4.Size = new System.Drawing.Size(1242, 120);
            this.panCollimator4.TabIndex = 6;
            // 
            // panCollimator5
            // 
            this.panCollimator5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCollimator5.Location = new System.Drawing.Point(30, 589);
            this.panCollimator5.Name = "panCollimator5";
            this.panCollimator5.Size = new System.Drawing.Size(1242, 120);
            this.panCollimator5.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Tester";
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.Filter = "Script|*.txt";
            // 
            // btnReoadScript
            // 
            this.btnReoadScript.Location = new System.Drawing.Point(866, 21);
            this.btnReoadScript.Name = "btnReoadScript";
            this.btnReoadScript.Size = new System.Drawing.Size(75, 50);
            this.btnReoadScript.TabIndex = 13;
            this.btnReoadScript.Text = "Ricarica Scripts";
            this.btnReoadScript.UseVisualStyleBackColor = true;
            this.btnReoadScript.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDurata);
            this.groupBox1.Controls.Add(this.txtNumCicli);
            this.groupBox1.Controls.Add(this.optCicli);
            this.groupBox1.Controls.Add(this.optTempo);
            this.groupBox1.Location = new System.Drawing.Point(668, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 73);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo test funzionale";
            // 
            // txtDurata
            // 
            this.txtDurata.Location = new System.Drawing.Point(130, 19);
            this.txtDurata.Mask = "00:00:00";
            this.txtDurata.Name = "txtDurata";
            this.txtDurata.Size = new System.Drawing.Size(50, 20);
            this.txtDurata.TabIndex = 30;
            this.txtDurata.ValidatingType = typeof(System.DateTime);
            // 
            // txtNumCicli
            // 
            this.txtNumCicli.Location = new System.Drawing.Point(130, 46);
            this.txtNumCicli.Mask = "9999999";
            this.txtNumCicli.Name = "txtNumCicli";
            this.txtNumCicli.Size = new System.Drawing.Size(50, 20);
            this.txtNumCicli.TabIndex = 31;
            this.txtNumCicli.ValidatingType = typeof(int);
            // 
            // optCicli
            // 
            this.optCicli.AutoSize = true;
            this.optCicli.Location = new System.Drawing.Point(7, 47);
            this.optCicli.Name = "optCicli";
            this.optCicli.Size = new System.Drawing.Size(84, 17);
            this.optCicli.TabIndex = 1;
            this.optCicli.TabStop = true;
            this.optCicli.Text = "Numero Cicli";
            this.optCicli.UseVisualStyleBackColor = true;
            // 
            // optTempo
            // 
            this.optTempo.AutoSize = true;
            this.optTempo.Location = new System.Drawing.Point(7, 20);
            this.optTempo.Name = "optTempo";
            this.optTempo.Size = new System.Drawing.Size(117, 17);
            this.optTempo.TabIndex = 0;
            this.optTempo.TabStop = true;
            this.optTempo.Text = "Tempo (GG:hh:mm)";
            this.optTempo.UseVisualStyleBackColor = true;
            // 
            // btnTestVita
            // 
            this.btnTestVita.Location = new System.Drawing.Point(953, 22);
            this.btnTestVita.Name = "btnTestVita";
            this.btnTestVita.Size = new System.Drawing.Size(75, 50);
            this.btnTestVita.TabIndex = 24;
            this.btnTestVita.Text = "Avvia Test Vita";
            this.btnTestVita.UseVisualStyleBackColor = true;
            this.btnTestVita.Click += new System.EventHandler(this.btnTestVita_Click);
            // 
            // lblTime
            // 
            this.lblTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTime.Location = new System.Drawing.Point(1172, 21);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(100, 23);
            this.lblTime.TabIndex = 26;
            this.lblTime.Text = "00:00:00";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1123, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Tempo";
            // 
            // tmrClock
            // 
            this.tmrClock.Interval = 1000;
            this.tmrClock.Tick += new System.EventHandler(this.tmrClock_Tick);
            // 
            // grpRuota
            // 
            this.grpRuota.Controls.Add(this.btnRuota);
            this.grpRuota.Controls.Add(this.chkTestRuota);
            this.grpRuota.Controls.Add(this.lblTipoRuota);
            this.grpRuota.Location = new System.Drawing.Point(316, 6);
            this.grpRuota.Name = "grpRuota";
            this.grpRuota.Size = new System.Drawing.Size(258, 73);
            this.grpRuota.TabIndex = 27;
            this.grpRuota.TabStop = false;
            this.grpRuota.Text = "Ruota";
            // 
            // btnRuota
            // 
            this.btnRuota.Location = new System.Drawing.Point(175, 15);
            this.btnRuota.Name = "btnRuota";
            this.btnRuota.Size = new System.Drawing.Size(75, 50);
            this.btnRuota.TabIndex = 5;
            this.btnRuota.Text = "Pannello Ruota";
            this.btnRuota.UseVisualStyleBackColor = true;
            this.btnRuota.Click += new System.EventHandler(this.btnRuota_Click);
            // 
            // chkTestRuota
            // 
            this.chkTestRuota.AutoSize = true;
            this.chkTestRuota.Location = new System.Drawing.Point(6, 47);
            this.chkTestRuota.Name = "chkTestRuota";
            this.chkTestRuota.Size = new System.Drawing.Size(114, 17);
            this.chkTestRuota.TabIndex = 3;
            this.chkTestRuota.Text = "Test con rotazione";
            this.chkTestRuota.UseVisualStyleBackColor = true;
            // 
            // lblTipoRuota
            // 
            this.lblTipoRuota.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTipoRuota.Location = new System.Drawing.Point(6, 16);
            this.lblTipoRuota.Name = "lblTipoRuota";
            this.lblTipoRuota.Size = new System.Drawing.Size(163, 22);
            this.lblTipoRuota.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1123, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Cicli";
            // 
            // lblCicli
            // 
            this.lblCicli.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCicli.Location = new System.Drawing.Point(1172, 48);
            this.lblCicli.Name = "lblCicli";
            this.lblCicli.Size = new System.Drawing.Size(100, 23);
            this.lblCicli.TabIndex = 29;
            this.lblCicli.Text = "0";
            this.lblCicli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTester
            // 
            this.lblTester.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTester.Location = new System.Drawing.Point(85, 12);
            this.lblTester.Name = "lblTester";
            this.lblTester.Size = new System.Drawing.Size(219, 20);
            this.lblTester.TabIndex = 30;
            // 
            // btnTestReset
            // 
            this.btnTestReset.Enabled = false;
            this.btnTestReset.Location = new System.Drawing.Point(583, 18);
            this.btnTestReset.Name = "btnTestReset";
            this.btnTestReset.Size = new System.Drawing.Size(75, 50);
            this.btnTestReset.TabIndex = 31;
            this.btnTestReset.Text = "Avvia Test Reset";
            this.btnTestReset.UseVisualStyleBackColor = true;
            this.btnTestReset.Click += new System.EventHandler(this.btnTestReset_Click);
            // 
            // tmrTestReset
            // 
            this.tmrTestReset.Interval = 45000;
            this.tmrTestReset.Tick += new System.EventHandler(this.tmrTestReset_Tick);
            // 
            // chk_EscludiTestManuali
            // 
            this.chk_EscludiTestManuali.AutoSize = true;
            this.chk_EscludiTestManuali.Location = new System.Drawing.Point(34, 40);
            this.chk_EscludiTestManuali.Name = "chk_EscludiTestManuali";
            this.chk_EscludiTestManuali.Size = new System.Drawing.Size(104, 17);
            this.chk_EscludiTestManuali.TabIndex = 32;
            this.chk_EscludiTestManuali.Text = "No Test Manuali";
            this.chk_EscludiTestManuali.UseVisualStyleBackColor = true;
            this.chk_EscludiTestManuali.Visible = false;
            // 
            // chk_avviaAncheConErrori
            // 
            this.chk_avviaAncheConErrori.AutoSize = true;
            this.chk_avviaAncheConErrori.Location = new System.Drawing.Point(200, 40);
            this.chk_avviaAncheConErrori.Name = "chk_avviaAncheConErrori";
            this.chk_avviaAncheConErrori.Size = new System.Drawing.Size(102, 17);
            this.chk_avviaAncheConErrori.TabIndex = 33;
            this.chk_avviaAncheConErrori.Text = "Avvia Con Errori";
            this.chk_avviaAncheConErrori.UseVisualStyleBackColor = true;
            this.chk_avviaAncheConErrori.Visible = false;
            // 
            // chk_saveCalTable
            // 
            this.chk_saveCalTable.AutoSize = true;
            this.chk_saveCalTable.Checked = true;
            this.chk_saveCalTable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_saveCalTable.Location = new System.Drawing.Point(34, 62);
            this.chk_saveCalTable.Name = "chk_saveCalTable";
            this.chk_saveCalTable.Size = new System.Drawing.Size(151, 17);
            this.chk_saveCalTable.TabIndex = 34;
            this.chk_saveCalTable.Text = "Salva Tabella Calibrazione";
            this.chk_saveCalTable.UseVisualStyleBackColor = true;
            this.chk_saveCalTable.Visible = false;
            // 
            // chk_saveTestReport
            // 
            this.chk_saveTestReport.AutoSize = true;
            this.chk_saveTestReport.Checked = true;
            this.chk_saveTestReport.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_saveTestReport.Location = new System.Drawing.Point(200, 62);
            this.chk_saveTestReport.Name = "chk_saveTestReport";
            this.chk_saveTestReport.Size = new System.Drawing.Size(112, 17);
            this.chk_saveTestReport.TabIndex = 35;
            this.chk_saveTestReport.Text = "Salva Test Report";
            this.chk_saveTestReport.UseVisualStyleBackColor = true;
            this.chk_saveTestReport.Visible = false;
            // 
            // btnPausaTestVita
            // 
            this.btnPausaTestVita.Enabled = false;
            this.btnPausaTestVita.Location = new System.Drawing.Point(1041, 22);
            this.btnPausaTestVita.Name = "btnPausaTestVita";
            this.btnPausaTestVita.Size = new System.Drawing.Size(75, 50);
            this.btnPausaTestVita.TabIndex = 36;
            this.btnPausaTestVita.Text = "Pausa Test Vita";
            this.btnPausaTestVita.UseVisualStyleBackColor = true;
            this.btnPausaTestVita.Click += new System.EventHandler(this.btnPausaTestVita_Click);
            // 
            // FormTestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 729);
            this.Controls.Add(this.btnPausaTestVita);
            this.Controls.Add(this.chk_saveTestReport);
            this.Controls.Add(this.chk_saveCalTable);
            this.Controls.Add(this.chk_avviaAncheConErrori);
            this.Controls.Add(this.chk_EscludiTestManuali);
            this.Controls.Add(this.btnTestReset);
            this.Controls.Add(this.lblTester);
            this.Controls.Add(this.btnReoadScript);
            this.Controls.Add(this.lblCicli);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.grpRuota);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnTestVita);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panCollimator5);
            this.Controls.Add(this.panCollimator4);
            this.Controls.Add(this.panCollimator3);
            this.Controls.Add(this.panCollimator2);
            this.Controls.Add(this.panCollimator1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormTestMain";
            this.Text = "Autotester";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTestMain_FormClosing);
            this.Load += new System.EventHandler(this.FormTestMain_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FormTestMain_HelpRequested);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpRuota.ResumeLayout(false);
            this.grpRuota.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panCollimator1;
        private RotatingLabel rotatingLabel1;
        private RotatingLabel rotatingLabel2;
        private System.Windows.Forms.Panel panCollimator2;
        private RotatingLabel rotatingLabel3;
        private System.Windows.Forms.Panel panCollimator3;
        private RotatingLabel rotatingLabel4;
        private System.Windows.Forms.Panel panCollimator4;
        private RotatingLabel rotatingLabel5;
        private System.Windows.Forms.Panel panCollimator5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
        private System.Windows.Forms.Button btnReoadScript;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox txtDurata;
        private System.Windows.Forms.MaskedTextBox txtNumCicli;
        private System.Windows.Forms.RadioButton optCicli;
        private System.Windows.Forms.RadioButton optTempo;
        private System.Windows.Forms.Button btnTestVita;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer tmrClock;
        private System.Windows.Forms.GroupBox grpRuota;
        private System.Windows.Forms.Label lblTipoRuota;
        private System.Windows.Forms.CheckBox chkTestRuota;
        private System.Windows.Forms.Button btnRuota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCicli;
        private System.Windows.Forms.Label lblTester;
        private System.Windows.Forms.Button btnTestReset;
        private System.Windows.Forms.Timer tmrTestReset;
        private System.Windows.Forms.CheckBox chk_EscludiTestManuali;
        private System.Windows.Forms.CheckBox chk_avviaAncheConErrori;
        private System.Windows.Forms.Timer timerTestPausa;
        private System.Windows.Forms.CheckBox chk_saveCalTable;
        private System.Windows.Forms.CheckBox chk_saveTestReport;
        private System.Windows.Forms.Button btnPausaTestVita;
    }
}