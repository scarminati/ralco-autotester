﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Xceed.Words.NET;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Autotester
{
    public partial class FormTestMain : Form
    {
        private ICollimatorPanel[] panels = new ICollimatorPanel[5];
        private string collimatorType;
        private string protocolName;
        private bool testVitaRunning;
        private string location = "Ralco IT";
        private SQLConnector conn;

        private string scriptMainFilename;

        private int durataMassima;
        private int time;
        private int tempoPausaThread;//tempo in secondi di esecuzione di una rotazione di un quarto di giro della ruota
        private int posRuota = 0;

        private CANusb_Connector extDevConnector;
        private string nameExtDevConnector;

        private FormControls formRuota;

        public CANusb_Connector ExtDevConnector
        {
            get { return extDevConnector; }
            set { extDevConnector = value; }
        }

        private Utente userForm { get; set; }

        public string NameExtDevConnector
        {
            get { return nameExtDevConnector; }
            set { nameExtDevConnector = value; }
        }

        private string getLocation()
        {
            return this.location;
        }

        private static string _includeCommand = "#INCLUDE";
        private static int _includeCommandLenght = _includeCommand.Length + 1;

        private Dictionary<int, List<ThreadTestVita>> threadsTestVita = new Dictionary<int, List<ThreadTestVita>>();


        public FormTestMain(Utente user)
        {
            try
            {
                InitializeComponent();
                userForm = user;
                lblTester.Text = userForm.nome + " " + userForm.cognome;
                conn = new SQLConnector();
                if (user.credenziale.Equals("admin"))
                {
                    chk_EscludiTestManuali.Visible = true;
                    chk_avviaAncheConErrori.Visible = true;
                    chk_saveCalTable.Visible = true;
                    chk_saveTestReport.Visible = true;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private ICollimatorPanel GetInstance(string strFullyQualifiedName)
        {
            try
            {
                Type parentType = typeof(ICollimatorPanel);
                Assembly assembly = Assembly.GetExecutingAssembly();
                Type[] types = assembly.GetTypes();
                IEnumerable<Type> imp = types.Where(t => t.GetInterfaces().Contains(parentType));

                foreach (Type type in imp)
                {
                    //                Console.WriteLine(type.Name);
                    if (type.Name == strFullyQualifiedName)
                    {
                        return (ICollimatorPanel)Activator.CreateInstance(type);
                    }
                }
                return null;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void setProtocolName(string protocolName)
        {
            try
            {
                this.protocolName = protocolName;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string getProtocolName()
        {
            try
            {
                return (this.protocolName);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public string getTemplateFileName(string collName)
        {
            try
            {
                string[] pattern = { "/", " " };
                Regex rgx = new Regex(pattern[0]);
                string[] replacement = { "_" };
                string line = collName;
                for (int i = 0; i < pattern.Length; i++)
                {
                    rgx = new Regex(pattern[i]);
                    line = rgx.Replace(line, replacement[0]);
                }
                return (line);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public void setPanelTester(int index, testCollimatore t)
        {
            try
            {
                ICollimatorPanel panel = GetInstance(t.configTest.nomePanel);
                if (panel == null)
                {
                    UnknowCollimatorPanel item = new UnknowCollimatorPanel();
                    item.setCollimatorCode(t.configTest.nomePanel);
                    panel = item;
                }
                panel.NumPanel = index;
                panel.collimatorName = t.configTest.nomeCollim;
                panel.SN = t.SN;
                panel.FC = t.FC;
                panel.CNC = t.CNC;
                //panel.setConnector(t.connettore, t.connettore.connectorName);
                panel.setConnector(t);
                panel.enableControl(t.configTest);
                panels[index - 1] = panel;
                //se esiste almeno un pannello con testReset a true, abilito il bottone
                if (t.configTest.testReset.Equals(true))
                    btnTestReset.Enabled = true;
                setProtocolName(t.configTest.protocolName);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormTestMain_Load(object sender, EventArgs e)
        {
            try
            {
                if (panels[0] != null)
                {
                    panCollimator1.Controls.Add((UserControl)panels[0]);
                }

                if (panels[1] != null)
                {
                    panCollimator2.Controls.Add((UserControl)panels[1]);
                }

                if (panels[2] != null)
                {
                    panCollimator3.Controls.Add((UserControl)panels[2]);
                }

                if (panels[3] != null)
                {
                    panCollimator4.Controls.Add((UserControl)panels[3]);
                }

                if (panels[4] != null)
                {
                    panCollimator5.Controls.Add((UserControl)panels[4]);
                }

                testVitaRunning = false;
                btnTestVita.Text = @"Avvia Test Funzionale";

                //String tmp = Application.ProductVersion;
                //// considero solo i primi 2 valori per la versione da visualizzare
                //int pos = tmp.IndexOf(@".");
                //pos = tmp.IndexOf(@".", pos + 1);

                this.Text = @"Ralco - Autotester - v. " + Application.ProductVersion;


                grpRuota.Visible = (this.extDevConnector != null);
                if (grpRuota.Visible)
                {
                    lblTipoRuota.Text = nameExtDevConnector;
                    chkTestRuota.Checked = true;
                    formRuota = new FormControls(extDevConnector, nameExtDevConnector);
                    foreach (var p in panels)
                        if (p != null && p.isChecked())
                            p.setFormRuota(formRuota);
                }
                else
                {
                    chkTestRuota.Checked = false;
                    formRuota = null;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            try
            {
                if (panels[0] != null)
                {
                    panels[0].scripts = Utility.loadScripts(panels[0].ProtocolAnalyzer(), panels[0].ProtocolAnalyzer().testColl.configTest);
                }

                if (panels[1] != null)
                {
                    panels[1].scripts = Utility.loadScripts(panels[1].ProtocolAnalyzer(), panels[1].ProtocolAnalyzer().testColl.configTest);
                }

                if (panels[2] != null)
                {
                    panels[2].scripts = Utility.loadScripts(panels[2].ProtocolAnalyzer(), panels[2].ProtocolAnalyzer().testColl.configTest);
                }

                if (panels[3] != null)
                {
                    panels[3].scripts = Utility.loadScripts(panels[3].ProtocolAnalyzer(), panels[3].ProtocolAnalyzer().testColl.configTest);
                }

                if (panels[4] != null)
                {
                    panels[4].scripts = Utility.loadScripts(panels[4].ProtocolAnalyzer(), panels[4].ProtocolAnalyzer().testColl.configTest);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTestVita_Click(object sender, EventArgs e)
        {
            try
            {
                btnReload_Click(null, null);
                List<ThreadTestVita> lista;
                bool ok = false;

                if (!testVitaRunning)
                {

                    if (!optCicli.Checked && !optTempo.Checked)
                    {
                        MessageBox.Show("Selezionare la modalità per il test funzionale.", "Test funzionale", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }


                    if (optTempo.Checked)
                    {
                        short tmp = 0;
                        ok = Int16.TryParse(txtDurata.Text.Substring(0, 2).Trim(), out tmp);
                        if (ok)
                        {
                            durataMassima = tmp * 24 * 60;
                        }
                        ok = Int16.TryParse(txtDurata.Text.Substring(3, 2).Trim(), out tmp);
                        if (ok)
                        {
                            durataMassima += (tmp * 60);
                        }
                        ok = Int16.TryParse(txtDurata.Text.Substring(6, 2).Trim(), out tmp);
                        if (ok)
                        {
                            durataMassima += tmp;
                        }

                        if (durataMassima == 0)
                        {
                            MessageBox.Show("Inserire il tempo giorni:ore:minuti di durata del test", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        if (durataMassima < 12 && chkTestRuota.Checked)
                        {
                            MessageBox.Show("Il tempo inserito e' minore di 12 minuti. Il test verrà eseguito senza la ruota.", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            chkTestRuota.Checked = false;
                        }

                        durataMassima *= 60;
                        if (lblTipoRuota.Text.Contains("R915S DHHS"))
                            tempoPausaThread = 0;
                        else
                            tempoPausaThread = 15;
                    }
                    else
                    {
                        short tmp = 0;
                        ok = Int16.TryParse(txtNumCicli.Text.Trim(), out tmp);
                        if (ok)
                        {
                            durataMassima = tmp;
                        }
                        if (durataMassima == 0)
                        {
                            MessageBox.Show("Inserire il numeri di cicli di durata del test", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        if (durataMassima < 100 && chkTestRuota.Checked)
                        {
                            MessageBox.Show("Il tempo inserito e' minore di 100 cicli. Il test verrà eseguito senza la ruota.", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            chkTestRuota.Checked = false;
                        }

                    }

                    ok = false;
                    foreach (var p in panels)
                    {
                        if (p != null)
                            ok |= p.isChecked();
                    }

                    if (!ok)
                    {
                        MessageBox.Show("Attivare almeno un collimatore per avviare il test.", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    ok = false;
                    foreach (var p in panels)
                    {
                        if (p != null)
                            if (p.isChecked())
                                ok |= p.hasErrors();

                    }

                    //funzionalità che consente l'avvio dei test automatici anche in presenza di errori - valida solo per utente admin
                    if (chk_avviaAncheConErrori.Checked.Equals(true))
                        ok = false;

                    if (ok)
                    {
                        MessageBox.Show("Almeno un collimatore presenta errori. Disattavare il collimatore in errore prima di ripartire.", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    ok = true;
                    foreach (var p in panels)
                    {
                        if (p != null)
                            if (p.isChecked())
                                ok &= p.isManualTestCompleted();
                    }

                    //funzionalità di esclusione dei test manuali - valida solo per utente admin
                    if (chk_EscludiTestManuali.Checked.Equals(true))
                        ok = true;

                    if (!ok)
                    {
                        MessageBox.Show("Completare i test manuali dei collimatori selezionati prima di procedere", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    ok = true;
                    foreach (var p in panels)
                    {
                        if (p != null && !p.hasErrors())
                            if (p.tipoTest == null)
                                ok &= false;
                    }

                    if (!ok)
                    {
                        MessageBox.Show("Inserire il tipo di test prima di procedere", this.Text.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (testVitaRunning)
                {
                    btnTestVita.Text = @"Avvia Test Funzionale";
                    btnPausaTestVita.Enabled = false;

                    tmrClock.Stop();
                    foreach (List<ThreadTestVita> testVitas in threadsTestVita.Values)
                    {
                        foreach (ThreadTestVita item in testVitas)
                        {
                            item.stopThread();
                        }
                    }
                    for (int i = 0; i < 5; i++)
                    {
                        if (panels[i] != null)
                        {
                            panels[i].saveReport(userForm);
                            panels[i].saveCalibTable();
                            panels[i].ProtocolAnalyzer().setCollimatorForShipment();
                        }
                    }
                }
                else
                {
                    btnTestVita.Text = @"Ferma Test Funzionale";
                    btnPausaTestVita.Enabled = true;

                    threadsTestVita.Clear();

                    posRuota = 0;
                    if (chkTestRuota.Checked)
                    {
                        formRuota.btn_rot0_Click(null, null);
                        posRuota = 1;
                    }

                    time = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        if (panels[i] != null)
                        {
                            if (panels[i].isChecked())
                            {
                                //lista = creaTestVitaPanel(panels[i].ProtocolAnalyzer(), panels[i].scripts);
                                lista = creaTestVitaPanel(panels[i]);
                                threadsTestVita.Add(i, lista);
                                foreach (ThreadTestVita item in lista)
                                {
                                    item.thread.Start();
                                }
                            }

                        }
                    }
                    tmrClock.Start();
                }
                testVitaRunning = !testVitaRunning;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPausaTestVita_Click(object sender, EventArgs e)
        {
            if (btnPausaTestVita.Text.Equals("Pausa Test Vita"))
            {
                pausaThreadTestVita(true);
                btnPausaTestVita.Text = "Riprendi Test Vita";
                tmrClock.Stop();
            }
            else if (btnPausaTestVita.Text.Equals("Riprendi Test Vita"))
            {
                pausaThreadTestVita(false);
                btnPausaTestVita.Text = "Pausa Test Vita";
                tmrClock.Start();
            }
        }

        private void executeSQLCommand(string SQLConn, string SQLCmd)
        {
            try
            {
                //imposto la connessione al database
                SqlConnection conn = new SqlConnection(SQLConn);
                //Creo il comando da eseguire mediante SQLCommand
                SqlCommand cmd = new SqlCommand(SQLCmd, conn);
                //apro la connessione
                conn.Open();
                //eseguo la query
                cmd.ExecuteNonQuery();
                //chiudo la connessione
                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<ThreadTestVita> creaTestVitaPanel(ICollimatorPanel panel)
        {
            try
            {
                ThreadTestVita threadTestVita;
                List<ThreadTestVita> lista = new List<ThreadTestVita>();

                for (int i = 0; i < panel.scripts.Count; i++)
                {
                    threadTestVita = new ThreadTestVita(i, panel.scripts[i]);
                    threadTestVita.Protocol = panel.ProtocolAnalyzer();

                    threadTestVita.thread = new Thread(new ThreadStart(threadTestVita.runTest));
                    //SC : metto i thread in background, così verranno chiusi automaticamente alla chiusura dell'applicativo
                    threadTestVita.thread.IsBackground = true;
                    lista.Add(threadTestVita);
                }

                return lista;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void tmrClock_Tick(object sender, EventArgs e)
        {
            try
            {
                time++;
                StringBuilder sb = new StringBuilder();
                int tempo = time;

                sb.Append(String.Format("{0:00}", (tempo % 60)));
                tempo /= 60;
                sb.Insert(0, String.Format("{0:00}", (tempo % 60)));
                sb.Insert(2, ":");
                tempo /= 60;
                sb.Insert(0, String.Format("{0:00}", (tempo % 24)));
                sb.Insert(2, ":");
                tempo /= 24;
                if (tempo > 0)
                {
                    sb.Insert(0, ":");
                    sb.Insert(0, tempo.ToString());
                }

                lblTime.Text = sb.ToString();


                if (optTempo.Checked)
                {
                    if (chkTestRuota.Checked)
                    {
                        if (time >= (durataMassima / 4 * (posRuota)))
                        {
                            switch (posRuota)
                            {
                                case 1:
                                    formRuota.btn_rot90_Click(null, null);
                                    posRuota++;
                                    break;
                                case 2:
                                    formRuota.btn_rot180_Click(null, null);
                                    posRuota++;
                                    break;
                                case 3:
                                    formRuota.btn_rot270_Click(null, null);
                                    posRuota++;
                                    break;
                                default:
                                    if (lblTipoRuota.Text.Contains("R915S DHHS"))
                                        formRuota.btn_rot180_Click(null, null);
                                    else
                                        formRuota.btn_rot0_Click(null, null);
                                    posRuota = 0;
                                    break;
                            }
                            if (tempoPausaThread != 0)
                                pausaThreadTestVita(true);
                        }

                    }
                    //se sono passati TEMPO_PAUSA_THREAD secondi da quando è avvenuta la rotazione, allora riprendi l'esecuzione dei thread
                    if (posRuota != 1 && time == tempoPausaThread + (durataMassima / 4 * (posRuota - 1)))
                        pausaThreadTestVita(false);
                    if (time >= durataMassima)
                    {
                        btnTestVita_Click(sender, e);
                    }
                }
                else if (optCicli.Checked)
                {
                    lblCicli.Text = ((panels[0]).getCicli()).ToString();
                    if (txtNumCicli.Text.Equals(lblCicli.Text))
                    {
                        btnTestVita_Click(sender, e);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                    {
                        if (!panels[i].isChecked())
                        {
                            if (this.threadsTestVita.ContainsKey(i))
                            {
                                List<ThreadTestVita> lista = this.threadsTestVita[i];
                                foreach (ThreadTestVita threadTestVita in lista)
                                {
                                    threadTestVita.stopThread();
                                }
                                threadsTestVita.Remove(i);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pausaThreadTestVita(bool pausa)
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                    {
                        if (panels[i].isChecked())
                        {
                            if (this.threadsTestVita.ContainsKey(i))
                            {
                                List<ThreadTestVita> lista = this.threadsTestVita[i];
                                foreach (ThreadTestVita threadTestVita in lista)
                                {
                                    threadTestVita.pauseThread(pausa);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRuota_Click(object sender, EventArgs e)
        {
            try
            {
                formRuota.ShowDialog(this);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormTestMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                foreach (List<ThreadTestVita> testVitas in threadsTestVita.Values)
                {
                    foreach (ThreadTestVita item in testVitas)
                    {
                        item.stopThread();
                    }
                }
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show (ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTestReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkTestRuota.Checked)
                {
                    btnTestReset.Text = @"Stop Test Reset";
                    bool ok = false;
                    foreach (var p in panels)
                    {
                        if (p != null)
                            if (p.isChecked() && !p.hasErrors() && (p.collimatorName.Equals("S 605 DASM DHHS") || p.collimatorName.Equals("S 605/146/DASM DHHS")))
                            {
                                p.scripts.Clear();
                                List<SingoloComando> elenco = new List<SingoloComando>();
                                SingoloComando msg = new SingoloComando();
                                msg.id = 0x000;
                                msg.dlc = 2;
                                msg.dati[0] = 0x81;
                                msg.dati[1] = 0x40;
                                msg.dati[2] = 0x00;
                                msg.dati[3] = 0x00;
                                msg.dati[4] = 0x00;
                                msg.dati[5] = 0x00;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                                msg.pause = 60000;
                                elenco.Add(msg);
                                p.scripts.Add(elenco);
                                ok = true;
                            }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Impossibile avviare il test reset sui collimatori", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        btnTestReset.Text = @"Start Test Reset";
                    }
                    else
                    {
                        btnTestReset.Enabled = false;
                        btnReoadScript.Enabled = false;
                        btnTestVita.Enabled = false;
                        posRuota = 0;
                        addThreadReset();
                        tmrTestReset.Start();
                    }
                }
                else
                    MessageBox.Show("Impossibile avviare il test di reset. Ruota non attiva.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tmrTestReset_Tick(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                    {
                        if (!panels[i].isChecked())
                        {
                            if (this.threadsTestVita.ContainsKey(i))
                            {
                                List<ThreadTestVita> lista = this.threadsTestVita[i];
                                foreach (ThreadTestVita threadTestVita in lista)
                                {
                                    threadTestVita.stopThread();
                                }
                                threadsTestVita.Remove(i);
                            }
                        }
                    }
                }
                bool testResetGoOn = false;
                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                    {
                        if (panels[i].isChecked())
                        {
                            testResetGoOn = true;
                            break;
                        }
                    }
                }
                if (!testResetGoOn)
                    posRuota = 9;
                switch (posRuota)
                {
                    case 0:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot0_Click(null, null);
                        posRuota++;
                        break;
                    case 1:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot90_Click(null, null);
                        posRuota++;
                        break;
                    case 2:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot180_Click(null, null);
                        posRuota++;
                        break;
                    case 3:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot270_Click(null, null);
                        posRuota++;
                        break;
                    case 4:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        tmrTestReset.Stop();
                        removeThreadReset();
                        DialogResult res = MessageBox.Show("Ruotare i collimatori di 90° sulle flange prima di procedere con il test", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (res.Equals(DialogResult.OK))
                        {
                            tmrTestReset.Start();
                            addThreadReset();
                            posRuota++;
                        }
                        break;
                    case 5:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot270_Click(null, null);
                        posRuota++;
                        break;
                    case 6:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot180_Click(null, null);
                        posRuota++;
                        break;
                    case 7:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot90_Click(null, null);
                        posRuota++;
                        break;
                    case 8:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot0_Click(null, null);
                        posRuota++;
                        break;
                    default:
                        //Console.WriteLine("######DEBUG : CASE" + posRuota);
                        formRuota.btn_rot0_Click(null, null);
                        posRuota = 0;
                        tmrTestReset.Stop();
                        removeThreadReset();
                        foreach (var p in panels)
                        {
                            if (p != null)
                                if (p.isChecked() && !p.hasErrors() && (p.collimatorName.Equals("S 605 DASM DHHS") || p.collimatorName.Equals("S 605/146/DASM DHHS")))
                                {
                                    p.ProtocolAnalyzer().initCollimator();
                                }
                        }
                        btnTestReset.Enabled = true;
                        btnReoadScript.Enabled = true;
                        btnTestVita.Enabled = true;
                        break;
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addThreadReset()
        {
            try
            {
                List<ThreadTestVita> lista;
                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                        if (panels[i].isChecked() && !panels[i].hasErrors() && (panels[i].collimatorName.Equals("S 605 DASM DHHS") || panels[i].collimatorName.Equals("S 605/146/DASM DHHS")))
                        {
                            lista = creaTestVitaPanel(panels[i]);
                            threadsTestVita.Add(i, lista);
                            foreach (ThreadTestVita item in lista)
                            {
                                item.thread.Start();
                            }
                        }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void removeThreadReset()
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    if (panels[i] != null)
                    {
                        if (panels[i].isChecked() && !panels[i].hasErrors() && (panels[i].collimatorName.Equals("S 605 DASM DHHS") || panels[i].collimatorName.Equals("S 605/146/DASM DHHS")))
                        {
                            if (this.threadsTestVita.ContainsKey(i))
                            {
                                List<ThreadTestVita> lista = this.threadsTestVita[i];
                                foreach (ThreadTestVita threadTestVita in lista)
                                {
                                    threadTestVita.stopThread();
                                }
                                threadsTestVita.Remove(i);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormTestMain_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            Utility.visualizeSwInformation();
        }
    }
}
