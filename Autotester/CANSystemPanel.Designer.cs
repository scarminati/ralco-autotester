﻿namespace Autotester
{
    partial class CANSystemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPCB = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBootloader = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFirmware = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnErrori = new System.Windows.Forms.Button();
            this.lblMot1Value = new System.Windows.Forms.Label();
            this.lblMot1 = new System.Windows.Forms.Label();
            this.lblMot2Value = new System.Windows.Forms.Label();
            this.lblMot2 = new System.Windows.Forms.Label();
            this.lblMot3Value = new System.Windows.Forms.Label();
            this.lblMot3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.grpVita = new System.Windows.Forms.GroupBox();
            this.lblMot12 = new System.Windows.Forms.Label();
            this.lblMot12Value = new System.Windows.Forms.Label();
            this.lblMot9 = new System.Windows.Forms.Label();
            this.lblMot9Value = new System.Windows.Forms.Label();
            this.lblMot10 = new System.Windows.Forms.Label();
            this.lblMot10Value = new System.Windows.Forms.Label();
            this.lblMot11 = new System.Windows.Forms.Label();
            this.lblMot11Value = new System.Windows.Forms.Label();
            this.lblMot8 = new System.Windows.Forms.Label();
            this.lblMot8Value = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMot5 = new System.Windows.Forms.Label();
            this.lblMot5Value = new System.Windows.Forms.Label();
            this.lblMot6 = new System.Windows.Forms.Label();
            this.lblMot6Value = new System.Windows.Forms.Label();
            this.lblMot7 = new System.Windows.Forms.Label();
            this.lblMot7Value = new System.Windows.Forms.Label();
            this.lblMot4 = new System.Windows.Forms.Label();
            this.lblMot4Value = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label23 = new System.Windows.Forms.Label();
            this.cmb_tipoTest = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.lblCollimatore = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblCNC = new System.Windows.Forms.Label();
            this.lblFC = new System.Windows.Forms.Label();
            this.grpLaser = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ledLaser2 = new Autotester.LedBulb();
            this.ledLaser1 = new Autotester.LedBulb();
            this.ledLaser1Test = new Autotester.LedBulb();
            this.label22 = new System.Windows.Forms.Label();
            this.ledLaser2Test = new Autotester.LedBulb();
            this.label14 = new System.Windows.Forms.Label();
            this.btnLaser1 = new System.Windows.Forms.Button();
            this.btnLaser2 = new System.Windows.Forms.Button();
            this.grpLuce = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLuce = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.ledLuce = new Autotester.LedBulb();
            this.label16 = new System.Windows.Forms.Label();
            this.ledLuceTest = new Autotester.LedBulb();
            this.grpDSC = new System.Windows.Forms.GroupBox();
            this.ledCrossDSCTest = new Autotester.LedBulb();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ledCrossDSC = new Autotester.LedBulb();
            this.ledLongDSC = new Autotester.LedBulb();
            this.label17 = new System.Windows.Forms.Label();
            this.ledLongDSCTest = new Autotester.LedBulb();
            this.grpInclin = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblinclX = new System.Windows.Forms.Label();
            this.lblinclY = new System.Windows.Forms.Label();
            this.lblinclZ = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.ledError = new Autotester.LedBulb();
            this.panel1.SuspendLayout();
            this.grpVita.SuspendLayout();
            this.grpLaser.SuspendLayout();
            this.grpLuce.SuspendLayout();
            this.grpDSC.SuspendLayout();
            this.grpInclin.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(4, 4);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(15, 14);
            this.chkEnable.TabIndex = 0;
            this.chkEnable.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "S/N";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblPCB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblBootloader);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblFirmware);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(263, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 105);
            this.panel1.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Firmware Version";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPCB
            // 
            this.lblPCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCB.Location = new System.Drawing.Point(48, 81);
            this.lblPCB.Name = "lblPCB";
            this.lblPCB.Size = new System.Drawing.Size(55, 16);
            this.lblPCB.TabIndex = 5;
            this.lblPCB.Text = "-1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "PCB";
            // 
            // lblBootloader
            // 
            this.lblBootloader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBootloader.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBootloader.Location = new System.Drawing.Point(48, 53);
            this.lblBootloader.Name = "lblBootloader";
            this.lblBootloader.Size = new System.Drawing.Size(55, 16);
            this.lblBootloader.TabIndex = 3;
            this.lblBootloader.Text = "-1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "BL";
            // 
            // lblFirmware
            // 
            this.lblFirmware.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFirmware.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirmware.Location = new System.Drawing.Point(48, 27);
            this.lblFirmware.Name = "lblFirmware";
            this.lblFirmware.Size = new System.Drawing.Size(55, 16);
            this.lblFirmware.TabIndex = 1;
            this.lblFirmware.Text = "-1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "FW";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1103, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Errori";
            // 
            // btnErrori
            // 
            this.btnErrori.Location = new System.Drawing.Point(1161, 41);
            this.btnErrori.Name = "btnErrori";
            this.btnErrori.Size = new System.Drawing.Size(75, 41);
            this.btnErrori.TabIndex = 23;
            this.btnErrori.Text = "Mostra errori";
            this.btnErrori.UseVisualStyleBackColor = true;
            this.btnErrori.Click += new System.EventHandler(this.btnErrori_Click);
            // 
            // lblMot1Value
            // 
            this.lblMot1Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot1Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot1Value.Location = new System.Drawing.Point(85, 27);
            this.lblMot1Value.Name = "lblMot1Value";
            this.lblMot1Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot1Value.TabIndex = 35;
            this.lblMot1Value.Text = "0";
            // 
            // lblMot1
            // 
            this.lblMot1.AutoSize = true;
            this.lblMot1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot1.Location = new System.Drawing.Point(3, 29);
            this.lblMot1.Name = "lblMot1";
            this.lblMot1.Size = new System.Drawing.Size(46, 13);
            this.lblMot1.TabIndex = 34;
            this.lblMot1.Text = "Motore1";
            this.lblMot1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMot2Value
            // 
            this.lblMot2Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot2Value.Location = new System.Drawing.Point(85, 48);
            this.lblMot2Value.Name = "lblMot2Value";
            this.lblMot2Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot2Value.TabIndex = 37;
            this.lblMot2Value.Text = "0";
            // 
            // lblMot2
            // 
            this.lblMot2.AutoSize = true;
            this.lblMot2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot2.Location = new System.Drawing.Point(3, 50);
            this.lblMot2.Name = "lblMot2";
            this.lblMot2.Size = new System.Drawing.Size(46, 13);
            this.lblMot2.TabIndex = 36;
            this.lblMot2.Text = "Motore2";
            // 
            // lblMot3Value
            // 
            this.lblMot3Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot3Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot3Value.Location = new System.Drawing.Point(85, 69);
            this.lblMot3Value.Name = "lblMot3Value";
            this.lblMot3Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot3Value.TabIndex = 39;
            this.lblMot3Value.Text = "0";
            // 
            // lblMot3
            // 
            this.lblMot3.AutoSize = true;
            this.lblMot3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot3.Location = new System.Drawing.Point(3, 71);
            this.lblMot3.Name = "lblMot3";
            this.lblMot3.Size = new System.Drawing.Size(46, 13);
            this.lblMot3.TabIndex = 38;
            this.lblMot3.Text = "Motore3";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "FC";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 13);
            this.label20.TabIndex = 46;
            this.label20.Text = "Collimatore";
            // 
            // grpVita
            // 
            this.grpVita.Controls.Add(this.lblMot12);
            this.grpVita.Controls.Add(this.lblMot12Value);
            this.grpVita.Controls.Add(this.lblMot9);
            this.grpVita.Controls.Add(this.lblMot9Value);
            this.grpVita.Controls.Add(this.lblMot10);
            this.grpVita.Controls.Add(this.lblMot10Value);
            this.grpVita.Controls.Add(this.lblMot11);
            this.grpVita.Controls.Add(this.lblMot11Value);
            this.grpVita.Controls.Add(this.lblMot8);
            this.grpVita.Controls.Add(this.lblMot8Value);
            this.grpVita.Controls.Add(this.label5);
            this.grpVita.Controls.Add(this.lblMot5);
            this.grpVita.Controls.Add(this.lblMot5Value);
            this.grpVita.Controls.Add(this.lblMot6);
            this.grpVita.Controls.Add(this.lblMot6Value);
            this.grpVita.Controls.Add(this.lblMot7);
            this.grpVita.Controls.Add(this.lblMot7Value);
            this.grpVita.Controls.Add(this.lblMot4);
            this.grpVita.Controls.Add(this.lblMot4Value);
            this.grpVita.Controls.Add(this.lblMot1);
            this.grpVita.Controls.Add(this.lblMot1Value);
            this.grpVita.Controls.Add(this.lblMot2);
            this.grpVita.Controls.Add(this.lblMot2Value);
            this.grpVita.Controls.Add(this.lblMot3);
            this.grpVita.Controls.Add(this.lblMot3Value);
            this.grpVita.Enabled = false;
            this.grpVita.Location = new System.Drawing.Point(726, 0);
            this.grpVita.Name = "grpVita";
            this.grpVita.Size = new System.Drawing.Size(362, 110);
            this.grpVita.TabIndex = 52;
            this.grpVita.TabStop = false;
            // 
            // lblMot12
            // 
            this.lblMot12.AutoSize = true;
            this.lblMot12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot12.Location = new System.Drawing.Point(237, 91);
            this.lblMot12.Name = "lblMot12";
            this.lblMot12.Size = new System.Drawing.Size(52, 13);
            this.lblMot12.TabIndex = 67;
            this.lblMot12.Text = "Motore12";
            // 
            // lblMot12Value
            // 
            this.lblMot12Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot12Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot12Value.Location = new System.Drawing.Point(322, 89);
            this.lblMot12Value.Name = "lblMot12Value";
            this.lblMot12Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot12Value.TabIndex = 68;
            this.lblMot12Value.Text = "0";
            // 
            // lblMot9
            // 
            this.lblMot9.AutoSize = true;
            this.lblMot9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot9.Location = new System.Drawing.Point(237, 29);
            this.lblMot9.Name = "lblMot9";
            this.lblMot9.Size = new System.Drawing.Size(46, 13);
            this.lblMot9.TabIndex = 60;
            this.lblMot9.Text = "Motore9";
            this.lblMot9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMot9Value
            // 
            this.lblMot9Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot9Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot9Value.Location = new System.Drawing.Point(322, 27);
            this.lblMot9Value.Name = "lblMot9Value";
            this.lblMot9Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot9Value.TabIndex = 61;
            this.lblMot9Value.Text = "0";
            // 
            // lblMot10
            // 
            this.lblMot10.AutoSize = true;
            this.lblMot10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot10.Location = new System.Drawing.Point(237, 50);
            this.lblMot10.Name = "lblMot10";
            this.lblMot10.Size = new System.Drawing.Size(52, 13);
            this.lblMot10.TabIndex = 62;
            this.lblMot10.Text = "Motore10";
            // 
            // lblMot10Value
            // 
            this.lblMot10Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot10Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot10Value.Location = new System.Drawing.Point(322, 48);
            this.lblMot10Value.Name = "lblMot10Value";
            this.lblMot10Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot10Value.TabIndex = 63;
            this.lblMot10Value.Text = "0";
            // 
            // lblMot11
            // 
            this.lblMot11.AutoSize = true;
            this.lblMot11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot11.Location = new System.Drawing.Point(237, 71);
            this.lblMot11.Name = "lblMot11";
            this.lblMot11.Size = new System.Drawing.Size(52, 13);
            this.lblMot11.TabIndex = 64;
            this.lblMot11.Text = "Motore11";
            // 
            // lblMot11Value
            // 
            this.lblMot11Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot11Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot11Value.Location = new System.Drawing.Point(322, 69);
            this.lblMot11Value.Name = "lblMot11Value";
            this.lblMot11Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot11Value.TabIndex = 65;
            this.lblMot11Value.Text = "0";
            // 
            // lblMot8
            // 
            this.lblMot8.AutoSize = true;
            this.lblMot8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot8.Location = new System.Drawing.Point(121, 91);
            this.lblMot8.Name = "lblMot8";
            this.lblMot8.Size = new System.Drawing.Size(46, 13);
            this.lblMot8.TabIndex = 58;
            this.lblMot8.Text = "Motore8";
            // 
            // lblMot8Value
            // 
            this.lblMot8Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot8Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot8Value.Location = new System.Drawing.Point(202, 89);
            this.lblMot8Value.Name = "lblMot8Value";
            this.lblMot8Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot8Value.TabIndex = 59;
            this.lblMot8Value.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(150, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 57;
            this.label5.Text = "Test Vita";
            // 
            // lblMot5
            // 
            this.lblMot5.AutoSize = true;
            this.lblMot5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot5.Location = new System.Drawing.Point(121, 29);
            this.lblMot5.Name = "lblMot5";
            this.lblMot5.Size = new System.Drawing.Size(46, 13);
            this.lblMot5.TabIndex = 51;
            this.lblMot5.Text = "Motore5";
            this.lblMot5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMot5Value
            // 
            this.lblMot5Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot5Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot5Value.Location = new System.Drawing.Point(202, 27);
            this.lblMot5Value.Name = "lblMot5Value";
            this.lblMot5Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot5Value.TabIndex = 52;
            this.lblMot5Value.Text = "0";
            // 
            // lblMot6
            // 
            this.lblMot6.AutoSize = true;
            this.lblMot6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot6.Location = new System.Drawing.Point(121, 50);
            this.lblMot6.Name = "lblMot6";
            this.lblMot6.Size = new System.Drawing.Size(46, 13);
            this.lblMot6.TabIndex = 53;
            this.lblMot6.Text = "Motore6";
            // 
            // lblMot6Value
            // 
            this.lblMot6Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot6Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot6Value.Location = new System.Drawing.Point(202, 48);
            this.lblMot6Value.Name = "lblMot6Value";
            this.lblMot6Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot6Value.TabIndex = 54;
            this.lblMot6Value.Text = "0";
            // 
            // lblMot7
            // 
            this.lblMot7.AutoSize = true;
            this.lblMot7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot7.Location = new System.Drawing.Point(121, 71);
            this.lblMot7.Name = "lblMot7";
            this.lblMot7.Size = new System.Drawing.Size(46, 13);
            this.lblMot7.TabIndex = 55;
            this.lblMot7.Text = "Motore7";
            // 
            // lblMot7Value
            // 
            this.lblMot7Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot7Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot7Value.Location = new System.Drawing.Point(202, 69);
            this.lblMot7Value.Name = "lblMot7Value";
            this.lblMot7Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot7Value.TabIndex = 56;
            this.lblMot7Value.Text = "0";
            // 
            // lblMot4
            // 
            this.lblMot4.AutoSize = true;
            this.lblMot4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot4.Location = new System.Drawing.Point(3, 91);
            this.lblMot4.Name = "lblMot4";
            this.lblMot4.Size = new System.Drawing.Size(46, 13);
            this.lblMot4.TabIndex = 49;
            this.lblMot4.Text = "Motore4";
            // 
            // lblMot4Value
            // 
            this.lblMot4Value.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMot4Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMot4Value.Location = new System.Drawing.Point(85, 89);
            this.lblMot4Value.Name = "lblMot4Value";
            this.lblMot4Value.Size = new System.Drawing.Size(35, 16);
            this.lblMot4Value.TabIndex = 50;
            this.lblMot4Value.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 13);
            this.label23.TabIndex = 54;
            this.label23.Text = "Tipo Test";
            // 
            // cmb_tipoTest
            // 
            this.cmb_tipoTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tipoTest.FormattingEnabled = true;
            this.cmb_tipoTest.Items.AddRange(new object[] {
            "Analisi",
            "Produzione",
            "Riparazione",
            "Test"});
            this.cmb_tipoTest.Location = new System.Drawing.Point(69, 51);
            this.cmb_tipoTest.Name = "cmb_tipoTest";
            this.cmb_tipoTest.Size = new System.Drawing.Size(74, 21);
            this.cmb_tipoTest.TabIndex = 55;
            this.cmb_tipoTest.SelectedIndexChanged += new System.EventHandler(this.cmb_tipoTest_SelectedIndexChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(147, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 13);
            this.label24.TabIndex = 56;
            this.label24.Text = "CNC";
            // 
            // lblCollimatore
            // 
            this.lblCollimatore.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCollimatore.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollimatore.Location = new System.Drawing.Point(69, 21);
            this.lblCollimatore.Name = "lblCollimatore";
            this.lblCollimatore.Size = new System.Drawing.Size(189, 20);
            this.lblCollimatore.TabIndex = 59;
            // 
            // lblSN
            // 
            this.lblSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSN.Location = new System.Drawing.Point(184, 52);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(74, 20);
            this.lblSN.TabIndex = 60;
            // 
            // lblCNC
            // 
            this.lblCNC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNC.Location = new System.Drawing.Point(184, 85);
            this.lblCNC.Name = "lblCNC";
            this.lblCNC.Size = new System.Drawing.Size(74, 20);
            this.lblCNC.TabIndex = 61;
            // 
            // lblFC
            // 
            this.lblFC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFC.Location = new System.Drawing.Point(69, 84);
            this.lblFC.Name = "lblFC";
            this.lblFC.Size = new System.Drawing.Size(74, 20);
            this.lblFC.TabIndex = 62;
            // 
            // grpLaser
            // 
            this.grpLaser.Controls.Add(this.label2);
            this.grpLaser.Controls.Add(this.ledLaser2);
            this.grpLaser.Controls.Add(this.ledLaser1);
            this.grpLaser.Controls.Add(this.ledLaser1Test);
            this.grpLaser.Controls.Add(this.label22);
            this.grpLaser.Controls.Add(this.ledLaser2Test);
            this.grpLaser.Controls.Add(this.label14);
            this.grpLaser.Controls.Add(this.btnLaser1);
            this.grpLaser.Controls.Add(this.btnLaser2);
            this.grpLaser.Enabled = false;
            this.grpLaser.Location = new System.Drawing.Point(548, 0);
            this.grpLaser.Name = "grpLaser";
            this.grpLaser.Size = new System.Drawing.Size(111, 110);
            this.grpLaser.TabIndex = 64;
            this.grpLaser.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Stato";
            // 
            // ledLaser2
            // 
            this.ledLaser2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLaser2.Location = new System.Drawing.Point(48, 77);
            this.ledLaser2.Name = "ledLaser2";
            this.ledLaser2.On = true;
            this.ledLaser2.Size = new System.Drawing.Size(25, 23);
            this.ledLaser2.TabIndex = 35;
            this.ledLaser2.Text = "ledBulb2";
            // 
            // ledLaser1
            // 
            this.ledLaser1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLaser1.Location = new System.Drawing.Point(48, 46);
            this.ledLaser1.Name = "ledLaser1";
            this.ledLaser1.On = true;
            this.ledLaser1.Size = new System.Drawing.Size(25, 23);
            this.ledLaser1.TabIndex = 34;
            this.ledLaser1.Text = "ledBulb1";
            // 
            // ledLaser1Test
            // 
            this.ledLaser1Test.Location = new System.Drawing.Point(79, 46);
            this.ledLaser1Test.Name = "ledLaser1Test";
            this.ledLaser1Test.On = true;
            this.ledLaser1Test.Size = new System.Drawing.Size(25, 23);
            this.ledLaser1Test.TabIndex = 32;
            this.ledLaser1Test.Text = "ledBulb4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(76, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Test";
            // 
            // ledLaser2Test
            // 
            this.ledLaser2Test.Location = new System.Drawing.Point(79, 77);
            this.ledLaser2Test.Name = "ledLaser2Test";
            this.ledLaser2Test.On = true;
            this.ledLaser2Test.Size = new System.Drawing.Size(25, 23);
            this.ledLaser2Test.TabIndex = 33;
            this.ledLaser2Test.Text = "ledBulb3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(40, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Laser";
            // 
            // btnLaser1
            // 
            this.btnLaser1.Location = new System.Drawing.Point(6, 46);
            this.btnLaser1.Name = "btnLaser1";
            this.btnLaser1.Size = new System.Drawing.Size(35, 23);
            this.btnLaser1.TabIndex = 28;
            this.btnLaser1.Text = "ON";
            this.btnLaser1.UseVisualStyleBackColor = true;
            this.btnLaser1.Click += new System.EventHandler(this.btnCenter_Click);
            // 
            // btnLaser2
            // 
            this.btnLaser2.Location = new System.Drawing.Point(6, 76);
            this.btnLaser2.Name = "btnLaser2";
            this.btnLaser2.Size = new System.Drawing.Size(35, 23);
            this.btnLaser2.TabIndex = 30;
            this.btnLaser2.Text = "ON";
            this.btnLaser2.UseVisualStyleBackColor = true;
            this.btnLaser2.Click += new System.EventHandler(this.btnSid_Click);
            // 
            // grpLuce
            // 
            this.grpLuce.Controls.Add(this.label3);
            this.grpLuce.Controls.Add(this.btnLuce);
            this.grpLuce.Controls.Add(this.label13);
            this.grpLuce.Controls.Add(this.ledLuce);
            this.grpLuce.Controls.Add(this.label16);
            this.grpLuce.Controls.Add(this.ledLuceTest);
            this.grpLuce.Enabled = false;
            this.grpLuce.Location = new System.Drawing.Point(479, 0);
            this.grpLuce.Name = "grpLuce";
            this.grpLuce.Size = new System.Drawing.Size(68, 110);
            this.grpLuce.TabIndex = 63;
            this.grpLuce.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Stato";
            // 
            // btnLuce
            // 
            this.btnLuce.Location = new System.Drawing.Point(18, 79);
            this.btnLuce.Name = "btnLuce";
            this.btnLuce.Size = new System.Drawing.Size(35, 23);
            this.btnLuce.TabIndex = 25;
            this.btnLuce.Text = "ON";
            this.btnLuce.UseVisualStyleBackColor = true;
            this.btnLuce.Click += new System.EventHandler(this.btnLuce_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Luce";
            // 
            // ledLuce
            // 
            this.ledLuce.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLuce.Location = new System.Drawing.Point(7, 49);
            this.ledLuce.Name = "ledLuce";
            this.ledLuce.On = true;
            this.ledLuce.Size = new System.Drawing.Size(25, 23);
            this.ledLuce.TabIndex = 26;
            this.ledLuce.Text = "ledBulb1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(34, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Test";
            // 
            // ledLuceTest
            // 
            this.ledLuceTest.Location = new System.Drawing.Point(38, 50);
            this.ledLuceTest.Name = "ledLuceTest";
            this.ledLuceTest.On = true;
            this.ledLuceTest.Size = new System.Drawing.Size(25, 23);
            this.ledLuceTest.TabIndex = 41;
            this.ledLuceTest.Text = "ledBulb4";
            // 
            // grpDSC
            // 
            this.grpDSC.Controls.Add(this.ledCrossDSCTest);
            this.grpDSC.Controls.Add(this.label9);
            this.grpDSC.Controls.Add(this.label10);
            this.grpDSC.Controls.Add(this.label11);
            this.grpDSC.Controls.Add(this.label15);
            this.grpDSC.Controls.Add(this.ledCrossDSC);
            this.grpDSC.Controls.Add(this.ledLongDSC);
            this.grpDSC.Controls.Add(this.label17);
            this.grpDSC.Controls.Add(this.ledLongDSCTest);
            this.grpDSC.Enabled = false;
            this.grpDSC.Location = new System.Drawing.Point(374, 0);
            this.grpDSC.Name = "grpDSC";
            this.grpDSC.Size = new System.Drawing.Size(103, 110);
            this.grpDSC.TabIndex = 65;
            this.grpDSC.TabStop = false;
            // 
            // ledCrossDSCTest
            // 
            this.ledCrossDSCTest.Location = new System.Drawing.Point(73, 47);
            this.ledCrossDSCTest.Name = "ledCrossDSCTest";
            this.ledCrossDSCTest.On = true;
            this.ledCrossDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSCTest.TabIndex = 19;
            this.ledCrossDSCTest.Text = "ledBulb4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(37, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "DSC";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Stato";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Cross";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 82);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Long";
            // 
            // ledCrossDSC
            // 
            this.ledCrossDSC.Color = System.Drawing.Color.Yellow;
            this.ledCrossDSC.Location = new System.Drawing.Point(9, 47);
            this.ledCrossDSC.Name = "ledCrossDSC";
            this.ledCrossDSC.On = true;
            this.ledCrossDSC.Size = new System.Drawing.Size(25, 23);
            this.ledCrossDSC.TabIndex = 16;
            this.ledCrossDSC.Text = "ledBulb1";
            // 
            // ledLongDSC
            // 
            this.ledLongDSC.Color = System.Drawing.Color.Yellow;
            this.ledLongDSC.Location = new System.Drawing.Point(9, 77);
            this.ledLongDSC.Name = "ledLongDSC";
            this.ledLongDSC.On = true;
            this.ledLongDSC.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSC.TabIndex = 17;
            this.ledLongDSC.Text = "ledBulb2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(70, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Test";
            // 
            // ledLongDSCTest
            // 
            this.ledLongDSCTest.Location = new System.Drawing.Point(73, 77);
            this.ledLongDSCTest.Name = "ledLongDSCTest";
            this.ledLongDSCTest.On = true;
            this.ledLongDSCTest.Size = new System.Drawing.Size(25, 23);
            this.ledLongDSCTest.TabIndex = 20;
            this.ledLongDSCTest.Text = "ledBulb3";
            // 
            // grpInclin
            // 
            this.grpInclin.Controls.Add(this.label29);
            this.grpInclin.Controls.Add(this.label28);
            this.grpInclin.Controls.Add(this.label27);
            this.grpInclin.Controls.Add(this.lblinclX);
            this.grpInclin.Controls.Add(this.lblinclY);
            this.grpInclin.Controls.Add(this.lblinclZ);
            this.grpInclin.Controls.Add(this.label21);
            this.grpInclin.Enabled = false;
            this.grpInclin.Location = new System.Drawing.Point(660, 0);
            this.grpInclin.Name = "grpInclin";
            this.grpInclin.Size = new System.Drawing.Size(65, 110);
            this.grpInclin.TabIndex = 66;
            this.grpInclin.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 86);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 13);
            this.label29.TabIndex = 45;
            this.label29.Text = "Z";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 61);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "Y";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 36);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "X";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblinclX
            // 
            this.lblinclX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblinclX.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinclX.Location = new System.Drawing.Point(25, 35);
            this.lblinclX.Name = "lblinclX";
            this.lblinclX.Size = new System.Drawing.Size(35, 16);
            this.lblinclX.TabIndex = 40;
            this.lblinclX.Text = "0";
            // 
            // lblinclY
            // 
            this.lblinclY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblinclY.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinclY.Location = new System.Drawing.Point(25, 60);
            this.lblinclY.Name = "lblinclY";
            this.lblinclY.Size = new System.Drawing.Size(35, 16);
            this.lblinclY.TabIndex = 41;
            this.lblinclY.Text = "0";
            // 
            // lblinclZ
            // 
            this.lblinclZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblinclZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinclZ.Location = new System.Drawing.Point(25, 85);
            this.lblinclZ.Name = "lblinclZ";
            this.lblinclZ.Size = new System.Drawing.Size(35, 16);
            this.lblinclZ.TabIndex = 42;
            this.lblinclZ.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(13, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Inclin.";
            // 
            // ledError
            // 
            this.ledError.Color = System.Drawing.Color.Red;
            this.ledError.Location = new System.Drawing.Point(1091, 30);
            this.ledError.Name = "ledError";
            this.ledError.On = true;
            this.ledError.Size = new System.Drawing.Size(69, 67);
            this.ledError.TabIndex = 22;
            this.ledError.Text = "ledBulb1";
            // 
            // CANSystemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.grpInclin);
            this.Controls.Add(this.grpDSC);
            this.Controls.Add(this.grpLaser);
            this.Controls.Add(this.grpLuce);
            this.Controls.Add(this.lblFC);
            this.Controls.Add(this.lblCNC);
            this.Controls.Add(this.lblSN);
            this.Controls.Add(this.lblCollimatore);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.cmb_tipoTest);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.grpVita);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btnErrori);
            this.Controls.Add(this.ledError);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkEnable);
            this.Name = "CANSystemPanel";
            this.Size = new System.Drawing.Size(1246, 117);
            this.Tag = "Philips";
            this.Load += new System.EventHandler(this.CANSystemPanel_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpVita.ResumeLayout(false);
            this.grpVita.PerformLayout();
            this.grpLaser.ResumeLayout(false);
            this.grpLaser.PerformLayout();
            this.grpLuce.ResumeLayout(false);
            this.grpLuce.PerformLayout();
            this.grpDSC.ResumeLayout(false);
            this.grpDSC.PerformLayout();
            this.grpInclin.ResumeLayout(false);
            this.grpInclin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEnable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBootloader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFirmware;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private LedBulb ledError;
        private System.Windows.Forms.Button btnErrori;
        private System.Windows.Forms.Label lblMot1Value;
        private System.Windows.Forms.Label lblMot1;
        private System.Windows.Forms.Label lblMot2Value;
        private System.Windows.Forms.Label lblMot2;
        private System.Windows.Forms.Label lblMot3Value;
        private System.Windows.Forms.Label lblMot3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox grpVita;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmb_tipoTest;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblMot4;
        private System.Windows.Forms.Label lblMot4Value;
        private System.Windows.Forms.Label lblCollimatore;
        private System.Windows.Forms.Label lblSN;
        private System.Windows.Forms.Label lblCNC;
        private System.Windows.Forms.Label lblFC;
        private System.Windows.Forms.Label lblMot12;
        private System.Windows.Forms.Label lblMot12Value;
        private System.Windows.Forms.Label lblMot9;
        private System.Windows.Forms.Label lblMot9Value;
        private System.Windows.Forms.Label lblMot10;
        private System.Windows.Forms.Label lblMot10Value;
        private System.Windows.Forms.Label lblMot11;
        private System.Windows.Forms.Label lblMot11Value;
        private System.Windows.Forms.Label lblMot8;
        private System.Windows.Forms.Label lblMot8Value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMot5;
        private System.Windows.Forms.Label lblMot5Value;
        private System.Windows.Forms.Label lblMot6;
        private System.Windows.Forms.Label lblMot6Value;
        private System.Windows.Forms.Label lblMot7;
        private System.Windows.Forms.Label lblMot7Value;
        private System.Windows.Forms.GroupBox grpLaser;
        private LedBulb ledLaser1Test;
        private System.Windows.Forms.Label label22;
        private LedBulb ledLaser2Test;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnLaser1;
        private System.Windows.Forms.Button btnLaser2;
        private System.Windows.Forms.GroupBox grpLuce;
        private System.Windows.Forms.Button btnLuce;
        private System.Windows.Forms.Label label13;
        private LedBulb ledLuce;
        private System.Windows.Forms.Label label16;
        private LedBulb ledLuceTest;
        private LedBulb ledLaser2;
        private LedBulb ledLaser1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grpDSC;
        private LedBulb ledCrossDSCTest;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private LedBulb ledCrossDSC;
        private LedBulb ledLongDSC;
        private System.Windows.Forms.Label label17;
        private LedBulb ledLongDSCTest;
        private System.Windows.Forms.GroupBox grpInclin;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblinclX;
        private System.Windows.Forms.Label lblinclY;
        private System.Windows.Forms.Label lblinclZ;
        private System.Windows.Forms.Label label21;
    }
}
