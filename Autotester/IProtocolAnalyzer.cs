﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    public interface IProtocolAnalyzer
    {
        //bool CollimationDone { get; }
        //bool CollimationFailed { get; }
        //bool DscCrossActive { get; }
        //bool DscLongActive { get; }
        //bool CrossJam { get; }
        //bool LongJam { get; }
        //bool CrossCalib { get; }
        //bool LongCalib { get; }
        //bool FilterCalib { get; }
        //bool CrossPot { get; }
        //bool LongPot { get; }
        //bool Light { get; }
        //bool POSTerror { get;}
        //int Meter { get; }
        //int MaxSpeed { get; }
        //int Filter { get; }
        //int CrossPos { get; }
        //int LongPos { get; }
        //int Temperature { get; }
        //int CrossEncoder { get; }
        //int LongEncoder { get; }
        //bool HeartbeatStatus { get; }
        //bool HeartbeatError { get; }
        //bool generalError { get;  }
        //bool communicationError { get; }
        //bool AEPError { get; }
        //int filterWhenError { get; set; }
        //int crossPosWhenError { get; set; }
        //int longPosWhenError { get; set; }
        //int passiLong { get; set; }
        //int deltaPotenziometriLong { get; set; }
        //int passiCross { get; set; }
        //int deltaPotenziometriCross { get; set; }
        //int passiIride { get; set; }
        //int passiSpatialFilter { get; set; }
        //int tentativiPosFiltro { get; set; }
        //bool TastiMembrActive { get; set; }
        //bool TestTastiMemb { get; set; }

        string protocolName { get; }
        string FwCollimator { get; }
        string FwBootloader { get; }
        string PcbCollimator { get; }
        bool error0 { get; set; }
        bool error90 { get; set; }
        bool error180 { get; set; }
        bool error270 { get; set; }

        testCollimatore testColl { get; set; }

        CANusb_Connector Connector { get; set; }

        int CounterIteration { get; set; }

        List<CalibTable> CalibTable { get; }

        void initCollimator();
        void readCalibTable();
        void setCollimatorForShipment();
        void playSoundEndTest();
    }
}
