﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Autotester
{
    public partial class FormControls : Form
    {
        private CANusb_Connector connector;//connettore usato per l'invio dei messaggi verso il dispositivo esterno.
        private CANCommand msg = new CANCommand();
        private uint BaseID;
        private int STEPS_GYRE;//numero di passi assoluti per compiere un giro completo
        private int STEPS_DEGREE;//numero di passi assoluti per compiere una rotazione di un grado
        private DateTime remainingTime;
        private int actualAngleRot;

        public void setSTEP_GYRE(int value)
        {
            this.STEPS_GYRE = value;
        }

        public int getSTEP_GYRE()
        {
            return (this.STEPS_GYRE);
        }

        public void setSTEP_DEGREE(int value)
        {
            this.STEPS_DEGREE = value;
        }

        public int getSTEP_DEGREE()
        {
            return (this.STEPS_DEGREE);
        }

        public void setActualAngleRotation(int actualAngleRot)
        {
            this.actualAngleRot = actualAngleRot;
        }

        public int getActualAngleRotation()
        {
            return (this.actualAngleRot);
        }

        public FormControls(CANusb_Connector connector, string testWheelType)
        {
            InitializeComponent();
            this.connector = connector;
            if (testWheelType.Equals("Test Wheel P225 ACS DHHS"))
            {
                grp_testWheel.Enabled = true;
                setSTEP_GYRE(357600);
                setSTEP_DEGREE(1000);
                this.BaseID = 0x700;
                this.numUpDw_rotTiming.Value = 30;
            }
            else if (testWheelType.Equals("Test Wheel R915S DHHS"))
            {
                grp_testWheel.Enabled = true;
                setSTEP_GYRE(245400);
                setSTEP_DEGREE(681);
                this.BaseID = 0x700;
                this.numUpDw_rotTiming.Value = 3;
            }
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_DEGREE);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_RESET);
            setActualAngleRotation(0);
            remainingTime = connector.getConnectorStartingTime().AddMinutes((double)this.numUpDw_rotTiming.Value);
            //this.rotationTimer.Start();
        }

        private void rotationTimer_Tick(object sender, EventArgs e)
        {
            tb_ROT_RemainingTime.Text = DateTime.Now.Subtract((this.remainingTime)).ToString(@"dd\d\ hh\h\ mm\m\ ss\s");
            if (DateTime.Now.Subtract((this.remainingTime)).ToString(@"dd\d\ hh\h\ mm\m\ ss\s").Equals("00d 00h 00m 00s"))
            {
                remainingTime = connector.getConnectorStartingTime().AddMinutes((double)this.numUpDw_rotTiming.Value) + connector.getConnectorElapsedTime();
                switch (getActualAngleRotation())
                {
                    case 0:
                        executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 4);
                        setActualAngleRotation(90);
                        break;
                    case 90:
                        executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 2);
                        setActualAngleRotation(180);
                        break;
                    case 180:
                        executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 4 * 3);
                        setActualAngleRotation(270);
                        break;
                    case 270:
                        executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_RESET);
                        setActualAngleRotation(0);
                        break;
                }
            }

        }

        private void btn_setTiming_Click(object sender, EventArgs e)
        {
            remainingTime = connector.getConnectorStartingTime().AddMinutes((double)this.numUpDw_rotTiming.Value);
        }

        private void executeMovement(int protocol, int operation, params int[] values)
        {
            msg.setID(this.BaseID + 0x10 + 0x02);
            msg.setDLC(8);
            msg.setData(protocol, operation, values);
            connector.enqueueMessage(msg);
        }
       
        public void btn_rot0_Click(object sender, EventArgs e)
        {
            setActualAngleRotation(0);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, 0);
        }

        public void btn_rot90_Click(object sender, EventArgs e)
        {
            setActualAngleRotation(90);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 4);
        }

        public void btn_rot180_Click(object sender, EventArgs e)
        {
            setActualAngleRotation(180);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 2);
        }

        public void btn_rot270_Click(object sender, EventArgs e)
        {
            setActualAngleRotation(270);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_GYRE / 4 * 3);
        }
       
        //faccio eseguire uno spostamento di un grado e poi mando il reset
        private void btn_reset_Click(object sender, EventArgs e)
        {
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_DEGREE);
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_RESET);
        }
       
        private void btn_rotVar_Click(object sender, EventArgs e)
        {
            executeMovement(Utility.PROTOCOL_CAN_STEP, Utility.OPERATION_MOVE, STEPS_DEGREE * Convert.ToInt32(this.numUpDw_rotAngle.Text));
        }

        //evento che si genera quando clicco la X del form per chiuderlo
        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
       
    }
}
