﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    interface ICollimatorPanel
    {
        void setConnector(testCollimatore t);
        void setFormRuota(FormControls formRuota);
        IProtocolAnalyzer ProtocolAnalyzer();

        bool isManualTestCompleted();

        string collimatorName { get; set; }

        string SN { get; set; }

        string FC { get; set; }

        string CNC { get; set; }

        string tipoTest { get; set; }

        bool isChecked();
        bool hasErrors();

        int getCicli();

        void enableControl(ConfigurazioneTest cfg);

        int NumPanel { get; set; }

        string pathScriptFile { get; set; }

        List<List<SingoloComando>> scripts { get; set; }

        void saveCalibTable();

        void saveReport(Utente user);

    }
}
