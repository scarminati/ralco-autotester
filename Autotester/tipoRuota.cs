﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    class tipoRuota
    {
        public int IDRuota { get; set; }
        public int baseIDRuota { get; set; }
        public int protocolloRuota { get; set; }
        public string nomeRuota { get; set; }
        public string baudRate { get; set; }
        public int passiAngolo { get; set; }
        public int passiAngoloGiro { get; set; }
        public int minutiAttesaRotazione { get; set; }
        public CANusb_Connector connettore { get; set; }
    }
}
