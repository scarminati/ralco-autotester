﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    interface ICanReaderDelegate<out CANCommand>
    {
        event Action<CANCommand> Handler;
    }
}
