﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace Autotester
{
    class ThreadTestVita
    {
        private List<SingoloComando> _comandi;
        private int counter;
        private int indexComando;
        private int scriptNumber;

        private bool running;
        private bool pause;

        public Thread thread;

        //private CANusb_Connector _connector = null;

        private IProtocolAnalyzer _protocol;

        public ThreadTestVita(int scriptNumber, List<SingoloComando> comandi)
        {
            this.scriptNumber = scriptNumber;
            this._comandi = comandi;
            reset();
        }

        //public CANusb_Connector Connector
        //{
        //    get { return _connector; }
        //    set { _connector = value; }
        //}

        public IProtocolAnalyzer Protocol
        {
            get { return _protocol; }
            set { _protocol = value; }
        }

        public void reset()
        {
            counter = 0;
            indexComando = 0;
            running = false;
        }

        public void stopThread()
        {
            running = false;
            pause = true;
        }

        public void pauseThread(bool pause)
        {
            this.pause = pause;
        }

        public void runTest()
        {
            running = true;
            pause = false;

            try
            {
                SingoloComando command = null;
                while (running)
                {
                    while (!pause)
                    {
                        command = _comandi[indexComando++];
                        if (indexComando >= _comandi.Count)
                        {
                            indexComando = 0;
                        }

                        CANCommand msg = new CANCommand(command.id, 0, command.dlc, command.dati[0], command.dati[1], command.dati[2], command.dati[3], command.dati[4],
                            command.dati[5], command.dati[6], command.dati[7]);
                        _protocol.Connector.enqueueMessage(msg);
                        if (command.increment)
                        {
                            counter++;
                            _protocol.CounterIteration = counter;
                        }
                        if (command.pause > 0)
                        {
                            Thread.Sleep(command.pause);
                        }
                    }
                }
            }
            catch (ThreadInterruptedException ex)
            {
                LogManager.GetLogger(_protocol.Connector.Logger.Name).Trace(_protocol.Connector.Logger.Name + " Thread " + this.scriptNumber + " Test halted");
            }
        }

    }
}
