﻿namespace Autotester
{
    partial class Form_Autotest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Autotest));
            this.tb_CNCTest2 = new System.Windows.Forms.MaskedTextBox();
            this.tb_FCTest2 = new System.Windows.Forms.MaskedTextBox();
            this.tb_SNTest2 = new System.Windows.Forms.MaskedTextBox();
            this.cmb_ModelloTest2 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.grp_Test2 = new System.Windows.Forms.GroupBox();
            this.ledBulbTest2 = new Autotester.LedBulb();
            this.grp_Test4 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cmb_ModelloTest4 = new System.Windows.Forms.ComboBox();
            this.tb_SNTest4 = new System.Windows.Forms.MaskedTextBox();
            this.tb_FCTest4 = new System.Windows.Forms.MaskedTextBox();
            this.tb_CNCTest4 = new System.Windows.Forms.MaskedTextBox();
            this.ledBulbTest4 = new Autotester.LedBulb();
            this.grp_Test3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmb_ModelloTest3 = new System.Windows.Forms.ComboBox();
            this.tb_SNTest3 = new System.Windows.Forms.MaskedTextBox();
            this.tb_FCTest3 = new System.Windows.Forms.MaskedTextBox();
            this.tb_CNCTest3 = new System.Windows.Forms.MaskedTextBox();
            this.ledBulbTest3 = new Autotester.LedBulb();
            this.grp_Test1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmb_ModelloTest1 = new System.Windows.Forms.ComboBox();
            this.tb_SNTest1 = new System.Windows.Forms.MaskedTextBox();
            this.tb_FCTest1 = new System.Windows.Forms.MaskedTextBox();
            this.tb_CNCTest1 = new System.Windows.Forms.MaskedTextBox();
            this.ledBulbTest1 = new Autotester.LedBulb();
            this.grp_Ruota = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmb_Ruota = new System.Windows.Forms.ComboBox();
            this.ledBulbRuota = new Autotester.LedBulb();
            this.grp_Test5 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmb_ModelloTest5 = new System.Windows.Forms.ComboBox();
            this.tb_SNTest5 = new System.Windows.Forms.MaskedTextBox();
            this.tb_FCTest5 = new System.Windows.Forms.MaskedTextBox();
            this.tb_CNCTest5 = new System.Windows.Forms.MaskedTextBox();
            this.ledBulbTest5 = new Autotester.LedBulb();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.btn_Go = new System.Windows.Forms.Button();
            this.btn_inputMode = new System.Windows.Forms.Button();
            this.tbBarcode = new System.Windows.Forms.TextBox();
            this.btn_updates = new System.Windows.Forms.Button();
            this.timerBarcode = new System.Windows.Forms.Timer(this.components);
            this.cb_quickTest = new System.Windows.Forms.CheckBox();
            this.grp_Test2.SuspendLayout();
            this.grp_Test4.SuspendLayout();
            this.grp_Test3.SuspendLayout();
            this.grp_Test1.SuspendLayout();
            this.grp_Ruota.SuspendLayout();
            this.grp_Test5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_CNCTest2
            // 
            this.tb_CNCTest2.Location = new System.Drawing.Point(59, 132);
            this.tb_CNCTest2.Mask = "9999/99";
            this.tb_CNCTest2.Name = "tb_CNCTest2";
            this.tb_CNCTest2.Size = new System.Drawing.Size(136, 20);
            this.tb_CNCTest2.TabIndex = 9;
            // 
            // tb_FCTest2
            // 
            this.tb_FCTest2.Location = new System.Drawing.Point(59, 99);
            this.tb_FCTest2.Mask = "9999/9999";
            this.tb_FCTest2.Name = "tb_FCTest2";
            this.tb_FCTest2.Size = new System.Drawing.Size(136, 20);
            this.tb_FCTest2.TabIndex = 8;
            // 
            // tb_SNTest2
            // 
            this.tb_SNTest2.Location = new System.Drawing.Point(59, 69);
            this.tb_SNTest2.Mask = "9999999";
            this.tb_SNTest2.Name = "tb_SNTest2";
            this.tb_SNTest2.Size = new System.Drawing.Size(136, 20);
            this.tb_SNTest2.TabIndex = 7;
            // 
            // cmb_ModelloTest2
            // 
            this.cmb_ModelloTest2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ModelloTest2.FormattingEnabled = true;
            this.cmb_ModelloTest2.Location = new System.Drawing.Point(59, 39);
            this.cmb_ModelloTest2.Name = "cmb_ModelloTest2";
            this.cmb_ModelloTest2.Size = new System.Drawing.Size(136, 21);
            this.cmb_ModelloTest2.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.Control;
            this.label17.Location = new System.Drawing.Point(8, 132);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 137;
            this.label17.Text = "CNC";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.Location = new System.Drawing.Point(8, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 136;
            this.label18.Text = "FC";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.Location = new System.Drawing.Point(8, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 135;
            this.label19.Text = "S/N";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.Location = new System.Drawing.Point(8, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 134;
            this.label20.Text = "Modello";
            // 
            // grp_Test2
            // 
            this.grp_Test2.Controls.Add(this.label20);
            this.grp_Test2.Controls.Add(this.label19);
            this.grp_Test2.Controls.Add(this.label18);
            this.grp_Test2.Controls.Add(this.label17);
            this.grp_Test2.Controls.Add(this.cmb_ModelloTest2);
            this.grp_Test2.Controls.Add(this.tb_SNTest2);
            this.grp_Test2.Controls.Add(this.ledBulbTest2);
            this.grp_Test2.Controls.Add(this.tb_FCTest2);
            this.grp_Test2.Controls.Add(this.tb_CNCTest2);
            this.grp_Test2.Enabled = false;
            this.grp_Test2.Location = new System.Drawing.Point(12, 334);
            this.grp_Test2.Name = "grp_Test2";
            this.grp_Test2.Size = new System.Drawing.Size(201, 160);
            this.grp_Test2.TabIndex = 102;
            this.grp_Test2.TabStop = false;
            // 
            // ledBulbTest2
            // 
            this.ledBulbTest2.Location = new System.Drawing.Point(6, 9);
            this.ledBulbTest2.Name = "ledBulbTest2";
            this.ledBulbTest2.On = true;
            this.ledBulbTest2.Size = new System.Drawing.Size(24, 23);
            this.ledBulbTest2.TabIndex = 145;
            this.ledBulbTest2.Text = "ledBulb3";
            // 
            // grp_Test4
            // 
            this.grp_Test4.Controls.Add(this.label22);
            this.grp_Test4.Controls.Add(this.label23);
            this.grp_Test4.Controls.Add(this.label24);
            this.grp_Test4.Controls.Add(this.label25);
            this.grp_Test4.Controls.Add(this.cmb_ModelloTest4);
            this.grp_Test4.Controls.Add(this.tb_SNTest4);
            this.grp_Test4.Controls.Add(this.tb_FCTest4);
            this.grp_Test4.Controls.Add(this.tb_CNCTest4);
            this.grp_Test4.Controls.Add(this.ledBulbTest4);
            this.grp_Test4.Enabled = false;
            this.grp_Test4.Location = new System.Drawing.Point(439, 12);
            this.grp_Test4.Name = "grp_Test4";
            this.grp_Test4.Size = new System.Drawing.Size(201, 160);
            this.grp_Test4.TabIndex = 104;
            this.grp_Test4.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.Control;
            this.label22.Location = new System.Drawing.Point(6, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 134;
            this.label22.Text = "Modello";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.Control;
            this.label23.Location = new System.Drawing.Point(6, 71);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(27, 13);
            this.label23.TabIndex = 135;
            this.label23.Text = "S/N";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.Location = new System.Drawing.Point(6, 101);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(20, 13);
            this.label24.TabIndex = 136;
            this.label24.Text = "FC";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.Control;
            this.label25.Location = new System.Drawing.Point(6, 131);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 137;
            this.label25.Text = "CNC";
            // 
            // cmb_ModelloTest4
            // 
            this.cmb_ModelloTest4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ModelloTest4.FormattingEnabled = true;
            this.cmb_ModelloTest4.Location = new System.Drawing.Point(58, 38);
            this.cmb_ModelloTest4.Name = "cmb_ModelloTest4";
            this.cmb_ModelloTest4.Size = new System.Drawing.Size(136, 21);
            this.cmb_ModelloTest4.TabIndex = 14;
            // 
            // tb_SNTest4
            // 
            this.tb_SNTest4.Location = new System.Drawing.Point(58, 68);
            this.tb_SNTest4.Mask = "9999999";
            this.tb_SNTest4.Name = "tb_SNTest4";
            this.tb_SNTest4.Size = new System.Drawing.Size(136, 20);
            this.tb_SNTest4.TabIndex = 15;
            // 
            // tb_FCTest4
            // 
            this.tb_FCTest4.Location = new System.Drawing.Point(58, 98);
            this.tb_FCTest4.Mask = "9999/9999";
            this.tb_FCTest4.Name = "tb_FCTest4";
            this.tb_FCTest4.Size = new System.Drawing.Size(136, 20);
            this.tb_FCTest4.TabIndex = 16;
            // 
            // tb_CNCTest4
            // 
            this.tb_CNCTest4.Location = new System.Drawing.Point(58, 131);
            this.tb_CNCTest4.Mask = "9999/99";
            this.tb_CNCTest4.Name = "tb_CNCTest4";
            this.tb_CNCTest4.Size = new System.Drawing.Size(136, 20);
            this.tb_CNCTest4.TabIndex = 17;
            // 
            // ledBulbTest4
            // 
            this.ledBulbTest4.Location = new System.Drawing.Point(6, 10);
            this.ledBulbTest4.Name = "ledBulbTest4";
            this.ledBulbTest4.On = true;
            this.ledBulbTest4.Size = new System.Drawing.Size(24, 23);
            this.ledBulbTest4.TabIndex = 142;
            this.ledBulbTest4.Text = "ledBulb1";
            // 
            // grp_Test3
            // 
            this.grp_Test3.Controls.Add(this.label5);
            this.grp_Test3.Controls.Add(this.label6);
            this.grp_Test3.Controls.Add(this.label7);
            this.grp_Test3.Controls.Add(this.label8);
            this.grp_Test3.Controls.Add(this.cmb_ModelloTest3);
            this.grp_Test3.Controls.Add(this.tb_SNTest3);
            this.grp_Test3.Controls.Add(this.tb_FCTest3);
            this.grp_Test3.Controls.Add(this.tb_CNCTest3);
            this.grp_Test3.Controls.Add(this.ledBulbTest3);
            this.grp_Test3.Enabled = false;
            this.grp_Test3.Location = new System.Drawing.Point(53, 12);
            this.grp_Test3.Name = "grp_Test3";
            this.grp_Test3.Size = new System.Drawing.Size(201, 160);
            this.grp_Test3.TabIndex = 103;
            this.grp_Test3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(9, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 134;
            this.label5.Text = "Modello";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(9, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 135;
            this.label6.Text = "S/N";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(9, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 136;
            this.label7.Text = "FC";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(9, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 137;
            this.label8.Text = "CNC";
            // 
            // cmb_ModelloTest3
            // 
            this.cmb_ModelloTest3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ModelloTest3.FormattingEnabled = true;
            this.cmb_ModelloTest3.Location = new System.Drawing.Point(59, 38);
            this.cmb_ModelloTest3.Name = "cmb_ModelloTest3";
            this.cmb_ModelloTest3.Size = new System.Drawing.Size(136, 21);
            this.cmb_ModelloTest3.TabIndex = 10;
            // 
            // tb_SNTest3
            // 
            this.tb_SNTest3.Location = new System.Drawing.Point(59, 68);
            this.tb_SNTest3.Mask = "9999999";
            this.tb_SNTest3.Name = "tb_SNTest3";
            this.tb_SNTest3.Size = new System.Drawing.Size(136, 20);
            this.tb_SNTest3.TabIndex = 11;
            // 
            // tb_FCTest3
            // 
            this.tb_FCTest3.Location = new System.Drawing.Point(59, 98);
            this.tb_FCTest3.Mask = "9999/9999";
            this.tb_FCTest3.Name = "tb_FCTest3";
            this.tb_FCTest3.Size = new System.Drawing.Size(136, 20);
            this.tb_FCTest3.TabIndex = 12;
            // 
            // tb_CNCTest3
            // 
            this.tb_CNCTest3.Location = new System.Drawing.Point(59, 131);
            this.tb_CNCTest3.Mask = "9999/99";
            this.tb_CNCTest3.Name = "tb_CNCTest3";
            this.tb_CNCTest3.Size = new System.Drawing.Size(136, 20);
            this.tb_CNCTest3.TabIndex = 13;
            // 
            // ledBulbTest3
            // 
            this.ledBulbTest3.Location = new System.Drawing.Point(6, 10);
            this.ledBulbTest3.Name = "ledBulbTest3";
            this.ledBulbTest3.On = true;
            this.ledBulbTest3.Size = new System.Drawing.Size(24, 23);
            this.ledBulbTest3.TabIndex = 142;
            this.ledBulbTest3.Text = "ledBulb1";
            // 
            // grp_Test1
            // 
            this.grp_Test1.Controls.Add(this.label9);
            this.grp_Test1.Controls.Add(this.label10);
            this.grp_Test1.Controls.Add(this.label11);
            this.grp_Test1.Controls.Add(this.label12);
            this.grp_Test1.Controls.Add(this.cmb_ModelloTest1);
            this.grp_Test1.Controls.Add(this.tb_SNTest1);
            this.grp_Test1.Controls.Add(this.tb_FCTest1);
            this.grp_Test1.Controls.Add(this.tb_CNCTest1);
            this.grp_Test1.Controls.Add(this.ledBulbTest1);
            this.grp_Test1.Enabled = false;
            this.grp_Test1.Location = new System.Drawing.Point(268, 548);
            this.grp_Test1.Name = "grp_Test1";
            this.grp_Test1.Size = new System.Drawing.Size(201, 160);
            this.grp_Test1.TabIndex = 101;
            this.grp_Test1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(6, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 134;
            this.label9.Text = "Modello";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(6, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 135;
            this.label10.Text = "S/N";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(6, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 136;
            this.label11.Text = "FC";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(6, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 137;
            this.label12.Text = "CNC";
            // 
            // cmb_ModelloTest1
            // 
            this.cmb_ModelloTest1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ModelloTest1.FormattingEnabled = true;
            this.cmb_ModelloTest1.Location = new System.Drawing.Point(58, 41);
            this.cmb_ModelloTest1.Name = "cmb_ModelloTest1";
            this.cmb_ModelloTest1.Size = new System.Drawing.Size(136, 21);
            this.cmb_ModelloTest1.TabIndex = 2;
            // 
            // tb_SNTest1
            // 
            this.tb_SNTest1.Location = new System.Drawing.Point(58, 71);
            this.tb_SNTest1.Mask = "9999999";
            this.tb_SNTest1.Name = "tb_SNTest1";
            this.tb_SNTest1.Size = new System.Drawing.Size(136, 20);
            this.tb_SNTest1.TabIndex = 3;
            // 
            // tb_FCTest1
            // 
            this.tb_FCTest1.Location = new System.Drawing.Point(59, 101);
            this.tb_FCTest1.Mask = "9999/9999";
            this.tb_FCTest1.Name = "tb_FCTest1";
            this.tb_FCTest1.Size = new System.Drawing.Size(136, 20);
            this.tb_FCTest1.TabIndex = 4;
            // 
            // tb_CNCTest1
            // 
            this.tb_CNCTest1.Location = new System.Drawing.Point(59, 134);
            this.tb_CNCTest1.Mask = "9999/99";
            this.tb_CNCTest1.Name = "tb_CNCTest1";
            this.tb_CNCTest1.Size = new System.Drawing.Size(136, 20);
            this.tb_CNCTest1.TabIndex = 5;
            // 
            // ledBulbTest1
            // 
            this.ledBulbTest1.Location = new System.Drawing.Point(4, 9);
            this.ledBulbTest1.Name = "ledBulbTest1";
            this.ledBulbTest1.On = true;
            this.ledBulbTest1.Size = new System.Drawing.Size(24, 23);
            this.ledBulbTest1.TabIndex = 142;
            this.ledBulbTest1.Text = "ledBulb1";
            // 
            // grp_Ruota
            // 
            this.grp_Ruota.Controls.Add(this.label13);
            this.grp_Ruota.Controls.Add(this.cmb_Ruota);
            this.grp_Ruota.Controls.Add(this.ledBulbRuota);
            this.grp_Ruota.Location = new System.Drawing.Point(283, 356);
            this.grp_Ruota.Name = "grp_Ruota";
            this.grp_Ruota.Size = new System.Drawing.Size(182, 57);
            this.grp_Ruota.TabIndex = 100;
            this.grp_Ruota.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.Control;
            this.label13.Location = new System.Drawing.Point(36, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 134;
            this.label13.Text = "Ruota";
            // 
            // cmb_Ruota
            // 
            this.cmb_Ruota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Ruota.FormattingEnabled = true;
            this.cmb_Ruota.Location = new System.Drawing.Point(78, 22);
            this.cmb_Ruota.Name = "cmb_Ruota";
            this.cmb_Ruota.Size = new System.Drawing.Size(90, 21);
            this.cmb_Ruota.TabIndex = 1;
            // 
            // ledBulbRuota
            // 
            this.ledBulbRuota.Location = new System.Drawing.Point(6, 19);
            this.ledBulbRuota.Name = "ledBulbRuota";
            this.ledBulbRuota.On = true;
            this.ledBulbRuota.Size = new System.Drawing.Size(24, 23);
            this.ledBulbRuota.TabIndex = 142;
            this.ledBulbRuota.Text = "ledBulb1";
            // 
            // grp_Test5
            // 
            this.grp_Test5.Controls.Add(this.label14);
            this.grp_Test5.Controls.Add(this.label15);
            this.grp_Test5.Controls.Add(this.label16);
            this.grp_Test5.Controls.Add(this.label21);
            this.grp_Test5.Controls.Add(this.cmb_ModelloTest5);
            this.grp_Test5.Controls.Add(this.tb_SNTest5);
            this.grp_Test5.Controls.Add(this.tb_FCTest5);
            this.grp_Test5.Controls.Add(this.tb_CNCTest5);
            this.grp_Test5.Controls.Add(this.ledBulbTest5);
            this.grp_Test5.Enabled = false;
            this.grp_Test5.Location = new System.Drawing.Point(558, 321);
            this.grp_Test5.Name = "grp_Test5";
            this.grp_Test5.Size = new System.Drawing.Size(201, 160);
            this.grp_Test5.TabIndex = 105;
            this.grp_Test5.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(8, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 134;
            this.label14.Text = "Modello";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(8, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 135;
            this.label15.Text = "S/N";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.Location = new System.Drawing.Point(8, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 136;
            this.label16.Text = "FC";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.Control;
            this.label21.Location = new System.Drawing.Point(8, 133);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 13);
            this.label21.TabIndex = 137;
            this.label21.Text = "CNC";
            // 
            // cmb_ModelloTest5
            // 
            this.cmb_ModelloTest5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ModelloTest5.FormattingEnabled = true;
            this.cmb_ModelloTest5.Location = new System.Drawing.Point(60, 40);
            this.cmb_ModelloTest5.Name = "cmb_ModelloTest5";
            this.cmb_ModelloTest5.Size = new System.Drawing.Size(136, 21);
            this.cmb_ModelloTest5.TabIndex = 18;
            // 
            // tb_SNTest5
            // 
            this.tb_SNTest5.Location = new System.Drawing.Point(60, 70);
            this.tb_SNTest5.Mask = "9999999";
            this.tb_SNTest5.Name = "tb_SNTest5";
            this.tb_SNTest5.Size = new System.Drawing.Size(136, 20);
            this.tb_SNTest5.TabIndex = 19;
            // 
            // tb_FCTest5
            // 
            this.tb_FCTest5.Location = new System.Drawing.Point(60, 100);
            this.tb_FCTest5.Mask = "9999/9999";
            this.tb_FCTest5.Name = "tb_FCTest5";
            this.tb_FCTest5.Size = new System.Drawing.Size(136, 20);
            this.tb_FCTest5.TabIndex = 20;
            // 
            // tb_CNCTest5
            // 
            this.tb_CNCTest5.Location = new System.Drawing.Point(60, 133);
            this.tb_CNCTest5.Mask = "9999/99";
            this.tb_CNCTest5.Name = "tb_CNCTest5";
            this.tb_CNCTest5.Size = new System.Drawing.Size(136, 20);
            this.tb_CNCTest5.TabIndex = 21;
            // 
            // ledBulbTest5
            // 
            this.ledBulbTest5.Location = new System.Drawing.Point(6, 9);
            this.ledBulbTest5.Name = "ledBulbTest5";
            this.ledBulbTest5.On = true;
            this.ledBulbTest5.Size = new System.Drawing.Size(24, 23);
            this.ledBulbTest5.TabIndex = 142;
            this.ledBulbTest5.Text = "ledBulb1";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Refresh.BackgroundImage")));
            this.btn_Refresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Refresh.Location = new System.Drawing.Point(676, 600);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(50, 50);
            this.btn_Refresh.TabIndex = 23;
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // btn_Go
            // 
            this.btn_Go.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Go.BackgroundImage")));
            this.btn_Go.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Go.Location = new System.Drawing.Point(735, 600);
            this.btn_Go.Name = "btn_Go";
            this.btn_Go.Size = new System.Drawing.Size(50, 50);
            this.btn_Go.TabIndex = 24;
            this.btn_Go.Tag = "Start";
            this.btn_Go.UseVisualStyleBackColor = true;
            this.btn_Go.Click += new System.EventHandler(this.btn_Go_Click);
            // 
            // btn_inputMode
            // 
            this.btn_inputMode.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_inputMode.BackgroundImage")));
            this.btn_inputMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_inputMode.Location = new System.Drawing.Point(617, 600);
            this.btn_inputMode.Name = "btn_inputMode";
            this.btn_inputMode.Size = new System.Drawing.Size(50, 50);
            this.btn_inputMode.TabIndex = 22;
            this.btn_inputMode.Tag = "Tastiera";
            this.btn_inputMode.UseVisualStyleBackColor = true;
            this.btn_inputMode.Click += new System.EventHandler(this.btn_inputMode_Click);
            // 
            // tbBarcode
            // 
            this.tbBarcode.AcceptsTab = true;
            this.tbBarcode.ForeColor = System.Drawing.SystemColors.Window;
            this.tbBarcode.Location = new System.Drawing.Point(616, 730);
            this.tbBarcode.Name = "tbBarcode";
            this.tbBarcode.Size = new System.Drawing.Size(169, 20);
            this.tbBarcode.TabIndex = 1000;
            // 
            // btn_updates
            // 
            this.btn_updates.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_updates.BackgroundImage")));
            this.btn_updates.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_updates.Enabled = false;
            this.btn_updates.Location = new System.Drawing.Point(735, 663);
            this.btn_updates.Name = "btn_updates";
            this.btn_updates.Size = new System.Drawing.Size(50, 50);
            this.btn_updates.TabIndex = 1001;
            this.btn_updates.UseVisualStyleBackColor = true;
            this.btn_updates.Visible = false;
            this.btn_updates.Click += new System.EventHandler(this.btn_updates_Click);
            // 
            // timerBarcode
            // 
            this.timerBarcode.Interval = 500;
            this.timerBarcode.Tick += new System.EventHandler(this.timerBarcode_Tick);
            // 
            // cb_quickTest
            // 
            this.cb_quickTest.AutoSize = true;
            this.cb_quickTest.Location = new System.Drawing.Point(617, 686);
            this.cb_quickTest.Name = "cb_quickTest";
            this.cb_quickTest.Size = new System.Drawing.Size(78, 17);
            this.cb_quickTest.TabIndex = 1002;
            this.cb_quickTest.Text = "Quick Test";
            this.cb_quickTest.UseVisualStyleBackColor = true;
            this.cb_quickTest.CheckedChanged += new System.EventHandler(this.cb_quickTest_CheckedChanged);
            // 
            // Form_Autotest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(790, 762);
            this.Controls.Add(this.cb_quickTest);
            this.Controls.Add(this.btn_updates);
            this.Controls.Add(this.tbBarcode);
            this.Controls.Add(this.btn_inputMode);
            this.Controls.Add(this.btn_Go);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.grp_Test5);
            this.Controls.Add(this.grp_Ruota);
            this.Controls.Add(this.grp_Test1);
            this.Controls.Add(this.grp_Test3);
            this.Controls.Add(this.grp_Test4);
            this.Controls.Add(this.grp_Test2);
            this.MaximumSize = new System.Drawing.Size(806, 800);
            this.MinimumSize = new System.Drawing.Size(806, 726);
            this.Name = "Form_Autotest";
            this.Text = "Form_Autotest";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Autotest_FormClosing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.Form_Autotest_HelpRequested);
            this.grp_Test2.ResumeLayout(false);
            this.grp_Test2.PerformLayout();
            this.grp_Test4.ResumeLayout(false);
            this.grp_Test4.PerformLayout();
            this.grp_Test3.ResumeLayout(false);
            this.grp_Test3.PerformLayout();
            this.grp_Test1.ResumeLayout(false);
            this.grp_Test1.PerformLayout();
            this.grp_Ruota.ResumeLayout(false);
            this.grp_Ruota.PerformLayout();
            this.grp_Test5.ResumeLayout(false);
            this.grp_Test5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MaskedTextBox tb_CNCTest2;
        private System.Windows.Forms.MaskedTextBox tb_FCTest2;
        private System.Windows.Forms.MaskedTextBox tb_SNTest2;
        private System.Windows.Forms.ComboBox cmb_ModelloTest2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private LedBulb ledBulbTest4;
        private LedBulb ledBulbTest2;
        private System.Windows.Forms.GroupBox grp_Test2;
        private System.Windows.Forms.GroupBox grp_Test4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmb_ModelloTest4;
        private System.Windows.Forms.MaskedTextBox tb_SNTest4;
        private System.Windows.Forms.MaskedTextBox tb_FCTest4;
        private System.Windows.Forms.MaskedTextBox tb_CNCTest4;
        private System.Windows.Forms.GroupBox grp_Test3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmb_ModelloTest3;
        private System.Windows.Forms.MaskedTextBox tb_SNTest3;
        private System.Windows.Forms.MaskedTextBox tb_FCTest3;
        private System.Windows.Forms.MaskedTextBox tb_CNCTest3;
        private LedBulb ledBulbTest3;
        private System.Windows.Forms.GroupBox grp_Test1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmb_ModelloTest1;
        private System.Windows.Forms.MaskedTextBox tb_SNTest1;
        private System.Windows.Forms.MaskedTextBox tb_FCTest1;
        private System.Windows.Forms.MaskedTextBox tb_CNCTest1;
        private LedBulb ledBulbTest1;
        private System.Windows.Forms.GroupBox grp_Ruota;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmb_Ruota;
        private LedBulb ledBulbRuota;
        private System.Windows.Forms.GroupBox grp_Test5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmb_ModelloTest5;
        private System.Windows.Forms.MaskedTextBox tb_SNTest5;
        private System.Windows.Forms.MaskedTextBox tb_FCTest5;
        private System.Windows.Forms.MaskedTextBox tb_CNCTest5;
        private LedBulb ledBulbTest5;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.Button btn_Go;
        private System.Windows.Forms.Button btn_inputMode;
        private System.Windows.Forms.TextBox tbBarcode;
        private System.Windows.Forms.Button btn_updates;
        private System.Windows.Forms.Timer timerBarcode;
        private System.Windows.Forms.CheckBox cb_quickTest;
    }
}