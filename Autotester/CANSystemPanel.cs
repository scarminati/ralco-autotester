﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Xceed.Words.NET;

namespace Autotester
{
    public partial class CANSystemPanel : UserControl, ICollimatorPanel
    {
        private CANSystemProtocolAnalyzer protocol;
        private SQLConnector conn;
        private FormControls formRuota = null;
        private ConfigurazioneTest cfg;
        private int testLuce = 0;
        private int dsc1TempVaue = -1;
        private int testdsc1 = 0;
        private int dsc2TempVaue = -1;
        private int testdsc2 = 0;
        private int numCicli;
        private int testLaserCenter = 0;
        private int testLaserSID = 0;

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public string collimatorName { get; set; }

        public string pathScriptFile { get; set; }

        public List<List<SingoloComando>> scripts { get; set; }

        private int _numPanel;

        public int NumPanel { get { return _numPanel; } set { this._numPanel = value; }  }

        public CANSystemPanel()
        {
            InitializeComponent();
        }


        //public void setConnector(CANusb_Connector connector, string deviceName)
        //{
        //    CANSystemProtocolAnalyzer CANSystemProtocol = new CANSystemProtocolAnalyzer();

        //    CANSystemProtocol.Connector = connector;

        //    CANSystemProtocol.Handler += ConnectorOnHandler();

        //    protocol = CANSystemProtocol;
        //}

        public void setConnector(testCollimatore t)
        {
            CANSystemProtocolAnalyzer CANSystemProtocol = new CANSystemProtocolAnalyzer();

            CANSystemProtocol.testColl = t;

            CANSystemProtocol.Connector = t.connettore;

            CANSystemProtocol.Handler += ConnectorOnHandler();

            protocol = CANSystemProtocol;
        }

        public void setFormRuota(FormControls formRuota)
        {
            this.formRuota = formRuota;
        }

        public void enableControl(ConfigurazioneTest c)
        {
            this.cfg = c;
            if (cfg.testDSC.Equals(true))
            {
                grpDSC.Enabled = true;
            }
            if (cfg.testLuce.Equals(true))
            {
                grpLuce.Enabled = true;
            }
            if (cfg.testLaser.Equals(true))
            {
                grpLaser.Enabled = true;
                if (cfg.numLasers.Equals(1))
                {
                    btnLaser2.Enabled = false;
                    ledLaser2.On = false;
                    ledLaser2Test.On = false;
                }
            }
            if (cfg.testInclin.Equals(true))
            {
                grpInclin.Enabled = true;
            }
            if (cfg.testVita.Equals(true))
            {
                grpVita.Enabled = true;
            }    
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return protocol;
        }

        public bool isManualTestCompleted()
        {
            bool rv = true;
            if (grpLuce.Enabled)
                rv = rv & ledLuceTest.On;
            if (grpLaser.Enabled)
                rv = rv & (ledLaser1Test.On) & (btnLaser2.Enabled.Equals(true) ? (ledLaser2Test.On) : true);
            return (rv);
        }

        public string getTipoTest()
        {
            if (cmb_tipoTest.SelectedItem == null)
                return "";
            else
                return cmb_tipoTest.SelectedItem.ToString();
        }

        private Action<ProtocolChange> ConnectorOnHandler()
        {
            return delegate(ProtocolChange value) { this.CollimatorValuesChanged(value); };
        }

        private void CollimatorValuesChanged(ProtocolChange value)
        {
            int tmp;

            switch (value)
            {
                case ProtocolChange.Motori1_4:
                    changeMotori(1, protocol.Motore1, protocol.Motore2, protocol.Motore3, protocol.Motore4);
                    break;

                case ProtocolChange.Motori5_8:
                    changeMotori(2, protocol.Motore5, protocol.Motore6, protocol.Motore7, protocol.Motore8);
                    break;

                case ProtocolChange.Motori9_12:
                    changeMotori(3, protocol.Motore9, protocol.Motore10, protocol.Motore11, protocol.Motore12);
                    break;

                case ProtocolChange.Errors:
                    ledError.On = true;
                    if (this.formRuota != null)
                    {
                        switch (this.formRuota.getActualAngleRotation())
                        {
                            case 0:
                                protocol.error0 = true;
                                break;
                            case 90:
                                protocol.error90 = true;
                                break;
                            case 180:
                                protocol.error180 = true;
                                break;
                            case 270:
                                protocol.error270 = true;
                                break;
                        }
                    }
                    changeChkEnableValue(false);
                    break;
                    
                case ProtocolChange.Info:
                    changeFirmware(protocol.FwCollimator);
                    changeBootloader(protocol.FwBootloader);
                    changePCB(protocol.PcbCollimator); 
                    break;

                case ProtocolChange.DSC:
                    ledCrossDSC.On = protocol.DSC1Status;
                    ledLongDSC.On = protocol.DSC2Status;
                    if (cfg.testDSC.Equals(true))
                    {
                        if (ledCrossDSC.On.Equals(false) && !ledCrossDSCTest.On)
                        {
                            if (protocol.DSC1 >= dsc1TempVaue + 2000 || protocol.DSC1 <= dsc1TempVaue - 2000)
                            {
                                testdsc1++;
                                dsc1TempVaue = protocol.DSC1;
                                if (testdsc1 >= 2)
                                    ledCrossDSCTest.On = true;
                            }
                        }
                        if (ledLongDSC.On.Equals(false && !ledCrossDSCTest.On))
                        {
                            if (protocol.DSC2 >= dsc2TempVaue + 2000 || protocol.DSC2 <= dsc2TempVaue - 2000)
                            {
                                testdsc2++;
                                dsc2TempVaue = protocol.DSC2;
                                if (testdsc2 >= 2)
                                    ledLongDSCTest.On = true;
                            }
                        }
                    }
                    break;

                case ProtocolChange.Light:
                    if (cfg.testLuce.Equals(true))
                    {
                        ledLuce.On = protocol.Light;
                        testLuce++;
                        if (protocol.Light)
                        {
                            changeButtonLight("OFF");
                        }
                        else
                        {
                            changeButtonLight("ON");
                        }
                        if (testLuce == 2)
                        {
                            ledLuceTest.On = true;
                        }
                    }
                    break;

                case ProtocolChange.Laser1:
                    if (cfg.testLaser.Equals(true))
                    {
                        ledLaser1.On = protocol.Laser1;
                        if (protocol.Laser1)
                        {
                            changeButtonLaserCenter("OFF");
                        }
                        else
                        {
                            changeButtonLaserCenter("ON");
                        }
                    }
                    break;

                case ProtocolChange.Laser2:
                    if (cfg.testLaser.Equals(true))
                    {
                        ledLaser2.On = protocol.Laser2;
                        if (protocol.Laser2)
                        {
                            changeButtonLaserSID("OFF");
                        }
                        else
                        {
                            changeButtonLaserSID("ON");
                        }
                    }
                    break;
                case ProtocolChange.Incl:
                    if (cfg.testInclin.Equals(true))
                        changeInclinometer(protocol.InclX, protocol.InclY, protocol.InclZ);
                    break;

                case ProtocolChange.Counter:
                    changeCicli(protocol.CounterIteration);
                    break;

                default:
                    break;
            }
        }

        private void changeButtonLight(string value)
        {
            if (this.btnLuce.InvokeRequired)
            {
                this.btnLuce.BeginInvoke((MethodInvoker)delegate () { this.btnLuce.Text = value; });
            }
            else
            {
                this.btnLuce.Text = value;
            }
        }

        private void changeButtonLaserCenter(string value)
        {
            if (this.btnLaser1.InvokeRequired)
            {
                this.btnLaser1.BeginInvoke((MethodInvoker)delegate () {
                    this.btnLaser1.Text = value;
                    testLaserCenter++;
                    if (testLaserCenter == 2)
                    {
                        ledLaser1Test.On = true;
                    }
                });
            }
            else
            {
                this.btnLaser1.Text = value;
                testLaserCenter++;
                if (testLaserCenter == 2)
                {
                    ledLaser1Test.On = true;
                }
            }
        }

        private void changeButtonLaserSID(string value)
        {
            if (this.btnLaser2.InvokeRequired)
            {
                this.btnLaser2.BeginInvoke((MethodInvoker)delegate () {
                    this.btnLaser2.Text = value;
                    testLaserSID++;
                    if (testLaserSID == 2)
                    {
                        ledLaser2Test.On = true;
                    }
                });
            }
            else
            {
                this.btnLaser2.Text = value;
                testLaserSID++;
                if (testLaserSID == 2)
                {
                    ledLaser2Test.On = true;
                }
            }
        }

        private void changeInclinometer(int inclX, int inclY, int inclZ)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate () {
                    this.lblinclX.Text = inclX.ToString();
                    this.lblinclY.Text = inclY.ToString();
                    this.lblinclZ.Text = inclZ.ToString();
                });
            }
            else
            {
                this.lblinclX.Text = inclX.ToString();
                this.lblinclY.Text = inclY.ToString();
                this.lblinclZ.Text = inclZ.ToString();
            }
        }

        private void changeChkEnableValue(bool value)
        {
            if (this.chkEnable.InvokeRequired)
            {
                this.chkEnable.BeginInvoke((MethodInvoker)delegate () { this.chkEnable.Checked = value; });
            }
            else
            {
                this.chkEnable.Checked = value;
            }
        }

        private void changeCicli(int value)
        {
            this.numCicli = value;
            //if (this.lblCicli.InvokeRequired)
            //{
            //    this.lblCicli.BeginInvoke((MethodInvoker)delegate() { this.lblCicli.Text = value.ToString(); ;});
            //    
            //}
            //else
            //{
            //    this.lblCicli.Text = value.ToString();
            //    
            //}
        }

        public int getCicli()
        {
            return this.numCicli;
        }

        private void changeFirmware(string value)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate() {
                    this.lblFirmware.Text = value;
                    if (!lblFirmware.Text.Equals(""))
                    {
                        if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblFirmware.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblFirmware.Text = value;
                if (!lblFirmware.Text.Equals(""))
                {
                    if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblFirmware.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeBootloader(string value)
        {
            if (this.lblBootloader.InvokeRequired)
            {
                this.lblBootloader.BeginInvoke((MethodInvoker)delegate() {
                    this.lblBootloader.Text = value;
                    if (!lblBootloader.Text.Equals(""))
                    {
                        if (!lblBootloader.Text.Equals(cfg.BLAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblBootloader.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblBootloader.Text = value;
                if (!lblBootloader.Text.Equals(""))
                {
                    if (!lblBootloader.Text.Equals(cfg.BLAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblBootloader.BackColor = Color.Red;
                    }
                }
            }
        }
        private void changePCB(string value)
        {
            if (this.lblPCB.InvokeRequired)
            {
                this.lblPCB.BeginInvoke((MethodInvoker)delegate() {
                    this.lblPCB.Text = value;
                    if (!lblPCB.Text.Equals(""))
                    {
                        if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblPCB.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblPCB.Text = value;
                if (!lblPCB.Text.Equals(""))
                {
                    if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblPCB.BackColor = Color.Red;
                    }
                }
            }
        }

        //private void changeSerialNumber(string value)
        //{
        //    if (this.lblSN.InvokeRequired)
        //    {
        //        this.lblSN.BeginInvoke((MethodInvoker)delegate () { this.lblSN.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblSN.Text = value;
        //    }
        //}

        private void changeMotori(int main, int motore1, int motore2, int motore3, int motore4)
        {
            if (this.lblMot1Value.InvokeRequired)
            {
                this.lblMot1Value.BeginInvoke((MethodInvoker) delegate() {
                    switch (main)
                    {
                        case 1:
                            this.lblMot1Value.Text = motore1.ToString();
                            this.lblMot2Value.Text = motore2.ToString();
                            this.lblMot3Value.Text = motore3.ToString();
                            this.lblMot4Value.Text = motore4.ToString();
                            break;
                        case 2:
                            this.lblMot5Value.Text = motore1.ToString();
                            this.lblMot6Value.Text = motore2.ToString();
                            this.lblMot7Value.Text = motore3.ToString();
                            this.lblMot8Value.Text = motore4.ToString();
                            break;
                        case 3:
                            this.lblMot9Value.Text = motore1.ToString();
                            this.lblMot10Value.Text = motore2.ToString();
                            this.lblMot11Value.Text = motore3.ToString();
                            this.lblMot12Value.Text = motore4.ToString();
                            break;
                    }
                });
            }
            else
            {
                switch (main)
                {
                    case 1:
                        this.lblMot1Value.Text = motore1.ToString();
                        this.lblMot2Value.Text = motore2.ToString();
                        this.lblMot3Value.Text = motore3.ToString();
                        this.lblMot4Value.Text = motore4.ToString();
                        break;
                    case 2:
                        this.lblMot5Value.Text = motore1.ToString();
                        this.lblMot6Value.Text = motore2.ToString();
                        this.lblMot7Value.Text = motore3.ToString();
                        this.lblMot8Value.Text = motore4.ToString();
                        break;
                    case 3:
                        this.lblMot9Value.Text = motore1.ToString();
                        this.lblMot10Value.Text = motore2.ToString();
                        this.lblMot11Value.Text = motore3.ToString();
                        this.lblMot12Value.Text = motore4.ToString();
                        break;
                }
            }
        }

        private void changeFilter(string value)
        {
            if (this.lblMot3Value.InvokeRequired)
            {
                this.lblMot3Value.BeginInvoke((MethodInvoker)delegate() { this.lblMot3Value.Text = value; });
            }
            else
            {
                this.lblMot3Value.Text = value;
            }
        }

        private void CANSystemPanel_Load(object sender, EventArgs e)
        {
            initValues();
        }

        private void initValues()
        {
            ledCrossDSC.On = false;
            ledCrossDSCTest.On = false;
            ledLongDSC.On = false;
            ledLongDSCTest.On = false;
            ledLuce.On = false;
            ledLuceTest.On = false;
            ledLaser1.On = false;
            ledLaser1Test.On = false;
            ledLaser2.On = false;
            ledLaser2Test.On = false;
            ledError.On = false;

            lblCollimatore.Text = collimatorName;
            lblSN.Text = SN;
            lblFC.Text = FC;
            lblCNC.Text = CNC;

            lblFirmware.Text = "";
            lblBootloader.Text = "";
            lblPCB.Text = "";

            lblMot1.Text = cfg.motore1;
            lblMot2.Text = cfg.motore2;
            lblMot3.Text = cfg.motore3;
            lblMot4.Text = cfg.motore4;
            lblMot5.Text = cfg.motore5;
            lblMot6.Text = cfg.motore6;
            lblMot7.Text = cfg.motore7;
            lblMot8.Text = cfg.motore8;
            lblMot9.Text = cfg.motore9;
            lblMot10.Text = cfg.motore10;
            lblMot11.Text = cfg.motore11;
            lblMot12.Text = cfg.motore12;
            lblMot1Value.Text = "";
            lblMot2Value.Text = "";
            lblMot3Value.Text = "";
            lblMot4Value.Text = "";
            lblMot5Value.Text = "";
            lblMot6Value.Text = "";
            lblMot7Value.Text = "";
            lblMot8Value.Text = "";
            lblMot9Value.Text = "";
            lblMot10Value.Text = "";
            lblMot11Value.Text = "";
            lblMot12Value.Text = "";

            if (protocol != null)
            {
                protocol.setSerialNumberOnPCB(lblSN.Text.PadLeft(8, '0'));
                protocol.initCollimator();
            }

            chkEnable.Checked = true;

            this.scripts = new List<List<SingoloComando>>();
            //if (collimatorName.Equals("R 806 Q DHHS"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R806Q\scripts.txt";
            //else if (collimatorName.Equals("R 1100/886B"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R1100\scripts.txt";
            //else if (collimatorName.Equals("R915/460B"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R915_460B\scripts.txt";
            //else if (collimatorName.Equals("R 221/027/ACS"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R221_027\scripts.txt";
            //else if (collimatorName.Equals("R 650 CV"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\R_650_CV\scripts.txt";
        }

        public bool isChecked()
        {
            return chkEnable.Checked;
        }

        public bool hasErrors()
        {
            return ledError.On;
        }

        private void btnErrori_Click(object sender, EventArgs e)
        {
            FormCANSystemErrors form = new FormCANSystemErrors();

            form.setErrors(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        private void cmb_tipoTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tipoTest = cmb_tipoTest.SelectedItem.ToString();
        }

        public void saveCalibTable()
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                string fileName = "";
                if (infoCNC[0].Equals("    "))
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt";
                else
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt";
                File.AppendAllText(fileName, DateTime.Now + "\n");
                for (int i = 0; i < ProtocolAnalyzer().CalibTable.Count; i++)
                {
                    File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].PassiMassimi + "\n");
                    File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].PuntiTaratura + "\n");
                    for (int j = 0; j < ProtocolAnalyzer().CalibTable[i].PuntiTaratura; j++)
                        File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].Aperture[j].ToString() + "," + ProtocolAnalyzer().CalibTable[i].Passi[j] + "\n");
                }
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt", true);
                    }

                    else
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt", true);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void saveReport(Utente user)
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string testPass = "PASS";
                string errorDescription = "";
                var doc = DocX.Load(@"\\ralcosrv2\Public\Templates_Autotest\\TemplateFinalReport_" + templateReportName + ".docx");
                #region Test Report
                if (lblFirmware.Text.Equals(cfg.FWAtteso) && lblBootloader.Text.Equals(cfg.BLAtteso) && lblPCB.Text.Equals(cfg.PCBAtteso))
                {
                    doc.ReplaceText("@@GENTEST@@", "PASS");
                    doc.ReplaceText("@@GENNOTES@@", "");
                }  
                else
                {
                    doc.ReplaceText("@@GENTEST@@", "FAIL");
                    doc.ReplaceText("@@GENNOTES@@", "FW Version Expected = " + cfg.FWAtteso + " FW Version Retrieved = " + lblFirmware.Text + " BL Version Expected = " + cfg.BLAtteso + " BL Version Retrieved = " + lblBootloader.Text + " PCB Version Expected = " + cfg.PCBAtteso + " PCB Version Retrieved = " + lblPCB.Text);
                    testPass = "FAIL";
                }
                if (grpLuce.Enabled.Equals(true))
                {
                    if (ledLuceTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@LIGTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIGTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpLaser.Enabled.Equals(true))
                {
                    switch (cfg.numLasers)
                    {
                        case 1:
                            {
                                if (ledLaser1Test.On.Equals(true))
                                {
                                    doc.ReplaceText("@@LASTEST@@", "PASS");
                                }
                                else
                                {
                                    doc.ReplaceText("@@LASTEST@@", "FAIL");
                                    testPass = "FAIL";
                                }
                            }
                            break;
                        case 2:
                            {
                                if (ledLaser1Test.On.Equals(true) && ledLaser2Test.On.Equals(true))
                                {
                                    doc.ReplaceText("@@LASTEST@@", "PASS");
                                }
                                else
                                {
                                    doc.ReplaceText("@@LASTEST@@", "FAIL");
                                    testPass = "FAIL";
                                }
                            }
                            break;
                    }
                }
                if (grpVita.Enabled.Equals(true))
                {
                    if (ledError.On.Equals(false))
                    {
                        doc.ReplaceText("@@LIFTEST@@", "PASS");
                        doc.ReplaceText("@@LIFNOTES@@", "");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIFTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";
                        if (protocol.ErrMot1)
                            errorDescription += " Error on Motor 1";
                        if (protocol.ErrMot2)
                            errorDescription += " Error on Motor 2";
                        if (protocol.ErrMot3)
                            errorDescription += " Error on Motor 3";
                        if (protocol.ErrMot4)
                            errorDescription += " Error on Motor 4";
                        if (protocol.ErrMot5)
                            errorDescription += " Error on Motor 5";
                        if (protocol.ErrMot6)
                            errorDescription += " Error on Motor 6";
                        if (protocol.ErrMot7)
                            errorDescription += " Error on Motor 7";
                        if (protocol.ErrMot8)
                            errorDescription += " Error on Motor 8";
                        if (protocol.ErrMot9)
                            errorDescription += " Error on Motor 9";
                        if (protocol.ErrMot10)
                            errorDescription += " Error on Motor 10";
                        if (protocol.ErrMot11)
                            errorDescription += " Error on Motor 11";
                        if (protocol.ErrMot12)
                            errorDescription += " Error on Motor 12";
                        if (protocol.ErrTimeoutRxCAN)
                            errorDescription += " Error Timeout Rx CAN Messages";
                        if (protocol.ErrReset)
                            errorDescription += " Error Timeout Reset";
                        if (protocol.ErrTempLow)
                            errorDescription += " Error Temperature Too Low";
                        if (protocol.ErrTempHigh)
                            errorDescription += " Error Temperature Too High";
                        if (protocol.ErrContEncoder)
                            errorDescription += " Encoders Counting Error";
                        if (protocol.PassiMot1 != 0)
                        errorDescription = "Step Loss Motor 1 = " + protocol.PassiMot1.ToString();
                        if (protocol.PassiMot1 != 0)
                            errorDescription = "Step Loss Motor 1 = " + protocol.PassiMot1.ToString();
                        if (protocol.PassiMot2 != 0)
                            errorDescription = "Step Loss Motor 2 = " + protocol.PassiMot2.ToString();
                        if (protocol.PassiMot3 != 0)
                            errorDescription = "Step Loss Motor 3 = " + protocol.PassiMot3.ToString();
                        if (protocol.PassiMot4 != 0)
                            errorDescription = "Step Loss Motor 4 = " + protocol.PassiMot4.ToString();
                        if (protocol.PassiMot5 != 0)
                            errorDescription = "Step Loss Motor 5 = " + protocol.PassiMot5.ToString();
                        if (protocol.PassiMot6 != 0)
                            errorDescription = "Step Loss Motor 6 = " + protocol.PassiMot6.ToString();
                        if (protocol.PassiMot7 != 0)
                            errorDescription = "Step Loss Motor 7 = " + protocol.PassiMot7.ToString();
                        if (protocol.PassiMot8 != 0)
                            errorDescription = "Step Loss Motor 8 = " + protocol.PassiMot8.ToString();
                        if (protocol.PassiMot9 != 0)
                            errorDescription = "Step Loss Motor 9 = " + protocol.PassiMot9.ToString();
                        if (protocol.PassiMot10 != 0)
                            errorDescription = "Step Loss Motor 10 = " + protocol.PassiMot10.ToString();
                        if (protocol.PassiMot11 != 0)
                            errorDescription = "Step Loss Motor 11 = " + protocol.PassiMot11.ToString();
                        if (protocol.PassiMot12 != 0)
                            errorDescription = "Step Loss Motor 12 = " + protocol.PassiMot12.ToString();
                        doc.ReplaceText("@@LIFNOTES@@", errorDescription);
                    }
                }
                #endregion
                #region General Information
                doc.ReplaceText("@@MODEL@@", lblCollimatore.Text);
                doc.ReplaceText("@@SERNR@@", lblSN.Text);
                doc.ReplaceText("@@FWVER@@", lblFirmware.Text);
                doc.ReplaceText("@@BLVER@@", lblBootloader.Text);
                doc.ReplaceText("@@PCBVER@@", lblPCB.Text);
                doc.ReplaceText("@@FLOW@@", lblFC.Text);
                conn = new SQLConnector();
                Int64 fromCollSN = 0;
                Int64 toCollSN = 0;
                fromCollSN = (Int64)conn.CreateCommand("SELECT Da_N FROM [archivi].[dbo].[listaFlowChart] WHERE IDFlow = '" + lblFC.Text.Split('/')[0] + "' AND anno = '" + lblFC.Text.Split('/')[1] + "'", fromCollSN);
                toCollSN = (Int64)conn.CreateCommand("SELECT A_N FROM [archivi].[dbo].[listaFlowChart] WHERE IDFlow = '" + lblFC.Text.Split('/')[0] + "' AND anno = '" + lblFC.Text.Split('/')[1] + "'", toCollSN);
                doc.ReplaceText("@@FROMTOSN@@", fromCollSN + "-" + toCollSN);
                doc.ReplaceText("@@CNC@@", lblCNC.Text);
                doc.ReplaceText("@@DATE@@", DateTime.Now.ToString());
                doc.ReplaceText("@@OUTCOME@@", testPass);
                #endregion
                #region Operator
                doc.ReplaceText("@@OPERATOR@@", user.nome + " " + user.cognome);
                #endregion
                #region CAlibrationTable
                for (int i = 0; i < ProtocolAnalyzer().CalibTable.Count-1; i++)
                {
                    for (int j = 0; j < ProtocolAnalyzer().CalibTable[i].PuntiTaratura; j++)
                    {
                        doc.ReplaceText("@@L" + (i+1) + "AP" + j + "@@", ProtocolAnalyzer().CalibTable[i].Aperture[j].ToString());
                        doc.ReplaceText("@@L" + (i+1) + "ST" + j + "@@", ProtocolAnalyzer().CalibTable[i].Passi[j].ToString());
                    }
                }
                #endregion
                string filePathFirma = Utility.searchPathDigitalSignatureOperator(user);

                Xceed.Words.NET.Image img;
                if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                {
                    //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                    img = doc.AddImage(filePathFirma);
                    Xceed.Words.NET.Picture picture = img.CreatePicture();
                    Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                    p1.AppendPicture(picture);
                    p1.Alignment = Alignment.right;
                }

                //salvo in locale il report
                //infoFC[0] = numero FC; infoFC[1] = anno FC;
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                //Console.WriteLine(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                if (infoCNC[0].Equals("    "))
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                else
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                //salvo il report sul server solo se il tipo di test è diverso da Test
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                    }

                    else
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                    }
                    SQLConnector conn = new SQLConnector();
                    conn.CreateCommand("INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[FW_Version],[BL_Version],[PCB_Version],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + collimatorName + "','" + SN + "','" + ProtocolAnalyzer().FwCollimator + "','" + ProtocolAnalyzer().FwBootloader + "','" + ProtocolAnalyzer().PcbCollimator + "','" + FC + "','" + CNC + "','" + Convert.ToString(DateTime.Now.Year) + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errorDescription + "','" + tipoTest + "','" + user.nome + " " + user.cognome + "','" + user.location + "')", null);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLuce_Click(object sender, EventArgs e)
        {
            protocol.sendLightCommand(!protocol.Light);
        }

        private void btnCenter_Click(object sender, EventArgs e)
        {
            protocol.sendLaserCommand(1, !protocol.Laser1);
        }

        private void btnSid_Click(object sender, EventArgs e)
        {
            protocol.sendLaserCommand(2, !protocol.Laser2);
        }
    }
}
