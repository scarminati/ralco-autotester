﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Autotester
{
    public partial class FormCANUSBConfig : Form
    {
        private Dictionary<string, Dictionary<int, string>> CANUSBmapping;
        public FormCANUSBConfig(List<String> connectorsDetected,  Dictionary<string, Dictionary<int, string>> CANUSBmapping)
        {
            InitializeComponent();
            this.CANUSBmapping = CANUSBmapping;
            int i = 0;
            foreach (var v in CANUSBmapping)
            {
                foreach (var v2 in v.Value)
                {
                    dgv_CANUSBConfig.Rows.Add(v.Key, v2.Key, v2.Value);      
                }
            }
                
            foreach (string s in connectorsDetected)
            {
                dgv_CANUSBConfig.Rows[i++].Cells[2].Value = s;    
            }
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgv_CANUSBConfig.Rows)
            {
                CANUSBmapping[row.Cells[0].Value.ToString()][(int)row.Cells[1].Value] = row.Cells[2].Value.ToString();
            }
            string json = JsonConvert.SerializeObject(CANUSBmapping);
            System.IO.File.WriteAllText(Environment.CurrentDirectory + @"\config\CANUSBMapping.txt", json);
            MessageBox.Show("Configurazione Salvata. Premi 'Reload' per ricaricare i dispositivi.","Configurazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Dispose();
        }

        private void FormCANUSBConfig_Load(object sender, EventArgs e)
        {

        }
    }
}
