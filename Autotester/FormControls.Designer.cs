﻿namespace Autotester
{
    partial class FormControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormControls));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_rot0 = new System.Windows.Forms.Button();
            this.btn_rot270 = new System.Windows.Forms.Button();
            this.btn_rot180 = new System.Windows.Forms.Button();
            this.btn_rot90 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numUpDw_rotAngle = new System.Windows.Forms.NumericUpDown();
            this.btn_rotVar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_ROT_RemainingTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numUpDw_rotTiming = new System.Windows.Forms.NumericUpDown();
            this.btn_setTiming = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.grp_testWheel = new System.Windows.Forms.GroupBox();
            this.rotationTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDw_rotAngle)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDw_rotTiming)).BeginInit();
            this.grp_testWheel.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_reset);
            this.groupBox1.Controls.Add(this.btn_rot0);
            this.groupBox1.Controls.Add(this.btn_rot270);
            this.groupBox1.Controls.Add(this.btn_rot180);
            this.groupBox1.Controls.Add(this.btn_rot90);
            this.groupBox1.Location = new System.Drawing.Point(22, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 108);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rotazioni Fisse";
            // 
            // btn_reset
            // 
            this.btn_reset.Location = new System.Drawing.Point(361, 30);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(60, 60);
            this.btn_reset.TabIndex = 4;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_rot0
            // 
            this.btn_rot0.Location = new System.Drawing.Point(21, 30);
            this.btn_rot0.Name = "btn_rot0";
            this.btn_rot0.Size = new System.Drawing.Size(60, 60);
            this.btn_rot0.TabIndex = 0;
            this.btn_rot0.Text = "0°";
            this.btn_rot0.UseVisualStyleBackColor = true;
            this.btn_rot0.Click += new System.EventHandler(this.btn_rot0_Click);
            // 
            // btn_rot270
            // 
            this.btn_rot270.Location = new System.Drawing.Point(276, 30);
            this.btn_rot270.Name = "btn_rot270";
            this.btn_rot270.Size = new System.Drawing.Size(60, 60);
            this.btn_rot270.TabIndex = 3;
            this.btn_rot270.Text = "270°";
            this.btn_rot270.UseVisualStyleBackColor = true;
            this.btn_rot270.Click += new System.EventHandler(this.btn_rot270_Click);
            // 
            // btn_rot180
            // 
            this.btn_rot180.Location = new System.Drawing.Point(191, 30);
            this.btn_rot180.Name = "btn_rot180";
            this.btn_rot180.Size = new System.Drawing.Size(60, 60);
            this.btn_rot180.TabIndex = 2;
            this.btn_rot180.Text = "180°";
            this.btn_rot180.UseVisualStyleBackColor = true;
            this.btn_rot180.Click += new System.EventHandler(this.btn_rot180_Click);
            // 
            // btn_rot90
            // 
            this.btn_rot90.Location = new System.Drawing.Point(106, 30);
            this.btn_rot90.Name = "btn_rot90";
            this.btn_rot90.Size = new System.Drawing.Size(60, 60);
            this.btn_rot90.TabIndex = 1;
            this.btn_rot90.Text = "90°";
            this.btn_rot90.UseVisualStyleBackColor = true;
            this.btn_rot90.Click += new System.EventHandler(this.btn_rot90_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numUpDw_rotAngle);
            this.groupBox2.Controls.Add(this.btn_rotVar);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(22, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(444, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rotazioni Variabili";
            // 
            // numUpDw_rotAngle
            // 
            this.numUpDw_rotAngle.Location = new System.Drawing.Point(191, 48);
            this.numUpDw_rotAngle.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numUpDw_rotAngle.Name = "numUpDw_rotAngle";
            this.numUpDw_rotAngle.Size = new System.Drawing.Size(91, 20);
            this.numUpDw_rotAngle.TabIndex = 5;
            // 
            // btn_rotVar
            // 
            this.btn_rotVar.Location = new System.Drawing.Point(329, 26);
            this.btn_rotVar.Name = "btn_rotVar";
            this.btn_rotVar.Size = new System.Drawing.Size(60, 60);
            this.btn_rotVar.TabIndex = 6;
            this.btn_rotVar.Text = "Ruota";
            this.btn_rotVar.UseVisualStyleBackColor = true;
            this.btn_rotVar.Click += new System.EventHandler(this.btn_rotVar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Angolo Di Rotazione (°)";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_ROT_RemainingTime);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.numUpDw_rotTiming);
            this.groupBox3.Controls.Add(this.btn_setTiming);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(22, 252);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(444, 93);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Setting dei Parametri";
            this.groupBox3.Visible = false;
            // 
            // tb_ROT_RemainingTime
            // 
            this.tb_ROT_RemainingTime.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tb_ROT_RemainingTime.Location = new System.Drawing.Point(190, 63);
            this.tb_ROT_RemainingTime.Name = "tb_ROT_RemainingTime";
            this.tb_ROT_RemainingTime.ReadOnly = true;
            this.tb_ROT_RemainingTime.Size = new System.Drawing.Size(91, 20);
            this.tb_ROT_RemainingTime.TabIndex = 111;
            this.tb_ROT_RemainingTime.Text = "00d 00h 00m 00s";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tempo Rimanente";
            // 
            // numUpDw_rotTiming
            // 
            this.numUpDw_rotTiming.Location = new System.Drawing.Point(190, 27);
            this.numUpDw_rotTiming.Maximum = new decimal(new int[] {
            142560,
            0,
            0,
            0});
            this.numUpDw_rotTiming.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUpDw_rotTiming.Name = "numUpDw_rotTiming";
            this.numUpDw_rotTiming.Size = new System.Drawing.Size(91, 20);
            this.numUpDw_rotTiming.TabIndex = 7;
            this.numUpDw_rotTiming.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // btn_setTiming
            // 
            this.btn_setTiming.Location = new System.Drawing.Point(329, 19);
            this.btn_setTiming.Name = "btn_setTiming";
            this.btn_setTiming.Size = new System.Drawing.Size(60, 60);
            this.btn_setTiming.TabIndex = 8;
            this.btn_setTiming.Text = "Set Tempo";
            this.btn_setTiming.UseVisualStyleBackColor = true;
            this.btn_setTiming.Click += new System.EventHandler(this.btn_setTiming_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tempo Di Rotazione (m)";
            // 
            // grp_testWheel
            // 
            this.grp_testWheel.Controls.Add(this.groupBox1);
            this.grp_testWheel.Controls.Add(this.groupBox3);
            this.grp_testWheel.Controls.Add(this.groupBox2);
            this.grp_testWheel.Enabled = false;
            this.grp_testWheel.Location = new System.Drawing.Point(13, 13);
            this.grp_testWheel.Name = "grp_testWheel";
            this.grp_testWheel.Size = new System.Drawing.Size(474, 352);
            this.grp_testWheel.TabIndex = 3;
            this.grp_testWheel.TabStop = false;
            this.grp_testWheel.Text = "Test Ruota";
            // 
            // rotationTimer
            // 
            this.rotationTimer.Interval = 550;
            this.rotationTimer.Tick += new System.EventHandler(this.rotationTimer_Tick);
            // 
            // FormControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 372);
            this.Controls.Add(this.grp_testWheel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormControls";
            this.Text = "Console Dispositivi Esterni";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDw_rotAngle)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDw_rotTiming)).EndInit();
            this.grp_testWheel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_rot270;
        private System.Windows.Forms.Button btn_rot180;
        private System.Windows.Forms.Button btn_rot90;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_rotVar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_setTiming;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grp_testWheel;
        private System.Windows.Forms.NumericUpDown numUpDw_rotAngle;
        private System.Windows.Forms.NumericUpDown numUpDw_rotTiming;
        private System.Windows.Forms.Button btn_rot0;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_ROT_RemainingTime;
        private System.Windows.Forms.Timer rotationTimer;



    }
}