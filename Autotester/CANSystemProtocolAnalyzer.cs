﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Autotester
{
    public class CANSystemProtocolAnalyzer : IProtocolDataChanges<ProtocolChange>, IProtocolAnalyzer
    {
        private CANusb_Connector connector = null;

        // Host Variables
        public string protocolName { get { return "CANSYSTEM"; } }
        private bool requestForCalTable = false;
        List<CalibTable> listCalTable = new List<CalibTable>();

        // variabili locali

        private string fwCollimator = "";
        private string fwBootloader = "";
        private string pcbCollimator = "";

        //features

        private bool light;
        private bool laser1;
        private bool laser2;
        private int motore1;
        private int motore2;
        private int motore3;
        private int motore4;
        private int motore5;
        private int motore6;
        private int motore7;
        private int motore8;
        private int motore9;
        private int motore10;
        private int motore11;
        private int motore12;
        private int dsc1;
        private int dsc2;
        private bool dsc1Status;
        private bool dsc2Status;
        private int inclX;
        private int inclY;
        private int inclZ;

        //errori
        private bool errTimeoutRxCAN;
        private bool errReset;
        private bool errTempLow;
        private bool errTempHigh;
        private bool errMot1;
        private bool errMot2;
        private bool errMot3;
        private bool errMot4;
        private bool errMot5;
        private bool errMot6;
        private bool errMot7;
        private bool errMot8;
        private bool errMot9;
        private bool errMot10;
        private bool errMot11;
        private bool errMot12;
        private bool errContEncoder;

        //eventi
        private Int32 errFormNonRaggMot1;
        private Int32 errFormNonRaggMot2;
        private Int32 errFormNonRaggMot3;
        private Int32 errFormNonRaggMot4;
        private Int32 errFormNonRaggMot5;
        private Int32 errFormNonRaggMot6;
        private Int32 errFormNonRaggMot7;
        private Int32 errFormNonRaggMot8;
        private Int32 errFormNonRaggMot9;
        private Int32 errFormNonRaggMot10;
        private Int32 errFormNonRaggMot11;
        private Int32 errFormNonRaggMot12;

        //passi motore
        private Int32 passiMot1;
        private Int32 passiMot2;
        private Int32 passiMot3;
        private Int32 passiMot4;
        private Int32 passiMot5;
        private Int32 passiMot6;
        private Int32 passiMot7;
        private Int32 passiMot8;
        private Int32 passiMot9;
        private Int32 passiMot10;
        private Int32 passiMot11;
        private Int32 passiMot12;

        //variabili pubbliche implementazioni dell'interfaccia

        public int SpatialFilter1_Pos { get; } //spostamento filtro spaziale 1
        public int SpatialFilter1_Rot { get; } //rotazione filtro spaziale 1
        public int IrisPos { get; } //spostamento iride
        public bool error0 { get; set; }
        public bool error90 { get; set; }
        public bool error180 { get; set; }
        public bool error270 { get; set; }

        //tasti membrana
        public bool TastiMembrActive { get; set; }
        public bool TestTastiMemb { get; set; }
        public byte TastiMembPressed { get; set; }

        public int CounterIteration {get;set;}

        public bool CollimationDone { get;}

        public bool CollimationFailed {get;}

        public bool Light
        {
            get { return light; }
        }

        public bool Laser1
        {
            get { return laser1; }
        }

        public bool Laser2
        {
            get { return laser2; }
        }

        public string FwCollimator
        {
            get { return fwCollimator; }
        }

        public string FwBootloader
        {
            get { return fwBootloader; }
        }

        public string PcbCollimator
        {
            get { return pcbCollimator; }
        }

        public int Motore1
        {
            get { return motore1; }
        }

        public int Motore2
        {
            get { return motore2; }
        }

        public int Motore3
        {
            get { return motore3; }
        }

        public int Motore4
        {
            get { return motore4; }
        }

        public int Motore5
        {
            get { return motore5; }
        }

        public int Motore6
        {
            get { return motore6; }
        }

        public int Motore7
        {
            get { return motore7; }
        }

        public int Motore8
        {
            get { return motore8; }
        }

        public int Motore9
        {
            get { return motore9; }
        }

        public int Motore10
        {
            get { return motore10; }
        }

        public int Motore11
        {
            get { return motore11; }
        }

        public int Motore12
        {
            get { return motore12; }
        }

        public bool ErrTimeoutRxCAN
        {
           get { return errTimeoutRxCAN; }
        }

        public bool ErrReset
        {
            get { return errReset; }
        }

        public bool ErrTempLow
        {
            get { return errTempLow; }
        }

        public bool ErrTempHigh
        {
            get { return errTempHigh; }
        }

        public bool ErrMot1
        {
            get { return errMot1; }
        }

        public bool ErrMot2
        {
            get { return errMot2; }
        }

        public bool ErrMot3
        {
            get { return errMot3; }
        }

        public bool ErrMot4
        {
            get { return errMot4; }
        }

        public bool ErrMot5
        {
            get { return errMot5; }
        }

        public bool ErrMot6
        {
            get { return errMot6; }
        }

        public bool ErrMot7
        {
            get { return errMot7; }
        }

        public bool ErrMot8
        {
            get { return errMot8; }
        }

        public bool ErrMot9
        {
            get { return errMot9; }
        }

        public bool ErrMot10
        {
            get { return errMot10; }
        }

        public bool ErrMot11
        {
            get { return errMot11; }
        }

        public bool ErrMot12
        {
            get { return errMot12; }
        }

        public Int32 ErrFormNonRaggMot1
        {
            get { return errFormNonRaggMot1; }
        }

        public Int32 ErrFormNonRaggMot2
        {
            get { return errFormNonRaggMot2; }
        }

        public Int32 ErrFormNonRaggMot3
        {
            get { return errFormNonRaggMot3; }
        }

        public Int32 ErrFormNonRaggMot4
        {
            get { return errFormNonRaggMot4; }
        }

        public Int32 ErrFormNonRaggMot5
        {
            get { return errFormNonRaggMot5; }
        }

        public Int32 ErrFormNonRaggMot6
        {
            get { return errFormNonRaggMot6; }
        }

        public Int32 ErrFormNonRaggMot7
        {
            get { return errFormNonRaggMot7; }
        }

        public Int32 ErrFormNonRaggMot8
        {
            get { return errFormNonRaggMot8; }
        }

        public Int32 ErrFormNonRaggMot9
        {
            get { return errFormNonRaggMot9; }
        }

        public Int32 ErrFormNonRaggMot10
        {
            get { return errFormNonRaggMot10; }
        }

        public Int32 ErrFormNonRaggMot11
        {
            get { return errFormNonRaggMot11; }
        }

        public Int32 ErrFormNonRaggMot12
        {
            get { return errFormNonRaggMot12; }
        }

        public Int32 PassiMot1
        {
            get { return passiMot1; }
        }

        public Int32 PassiMot2
        {
            get { return passiMot2; }
        }

        public Int32 PassiMot3
        {
            get { return passiMot3; }
        }

        public Int32 PassiMot4
        {
            get { return passiMot4; }
        }

        public Int32 PassiMot5
        {
            get { return passiMot5; }
        }

        public Int32 PassiMot6
        {
            get { return passiMot6; }
        }

        public Int32 PassiMot7
        {
            get { return passiMot7; }
        }

        public Int32 PassiMot8
        {
            get { return passiMot8; }
        }

        public Int32 PassiMot9
        {
            get { return passiMot9; }
        }

        public Int32 PassiMot10
        {
            get { return passiMot10; }
        }

        public Int32 PassiMot11
        {
            get { return passiMot11; }
        }

        public Int32 PassiMot12
        {
            get { return passiMot12; }
        }

        public int DSC1
        {
            get { return dsc1; }
        }

        public int DSC2
        {
            get { return dsc2; }
        }

        public bool DSC1Status
        {
            get { return dsc1Status; }
        }

        public bool DSC2Status
        {
            get { return dsc2Status; }
        }

        public bool ErrContEncoder
        {
            get { return errContEncoder; }
        }

        public int InclX
        {
            get { return inclX; }
        }

        public int InclY
        {
            get { return inclY; }
        }

        public int InclZ
        {
            get { return inclZ; }
        }

        public List<CalibTable> CalibTable { get { return listCalTable; } }

        public CANusb_Connector Connector
        {
            get { return connector; }
            set
            {
                if (value == null)
                {
                    if (this.connector != null)
                    {
                        connector.Handler -= ConnectorOnHandler();
                    }
                    connector = null;

                }
                else
                {
                    connector = value;
                    connector.Handler += ConnectorOnHandler();                    
                }
            }
        }

        public testCollimatore  testColl { get; set; }


        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate(CANCommand command) { this.processMessage(command); };
        }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<ProtocolChange> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<ProtocolChange> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<ProtocolChange> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<ProtocolChange>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */
        
        public void initCollimator()
        {
            CANCommand msg;
            //setto i valori di formato minimo e massimo singolo motore per la movimentazione manuale mediante manopole
            if (!testColl.configTest.nomeCollim.Contains("230"))
            {
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x04), 0, 8, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x04), 0, 8, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x04), 0, 8, 0x05, 0x01, 0x10, 0xCC, 0x00, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x04), 0, 8, 0x05, 0x02, 0x10, 0xCC, 0x00, 0x00, 0x00, 0x00);
                connector.enqueueMessage(msg);
            }
            //attivo il test passi (il collimatore esegue un reset)
            msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0xF1, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            emitChanges(ProtocolChange.ConfigurationComplete);
            sendVersionRequests();
            readCalibTable();
        }

        public void setSerialNumberOnPCB(string SN)
        {
            //unlock scrittura S/N
            CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0xFE, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            byte[] bytes = new byte[SN.Length];
            for (int i = 0; i < SN.Length; i += 1)
                bytes[i] = Convert.ToByte(SN.Substring(i, 1), 16);
            msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x7D, bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
            connector.enqueueMessage(msg);
        }

        public void readCalibTable()
        {
            if (!requestForCalTable)
            {
                requestForCalTable = true;
                for (byte motore = 1; motore <= testColl.configTest.numMotori; motore++)
                {
                    listCalTable.Add(new CalibTable());
                    //Richiesta passi massimi motore
                    CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x70, motore, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                    connector.enqueueMessage(msg);
                }
            }
        }

        public void setCollimatorForShipment()
        {

        }

        public void playSoundEndTest()
        {

        }

        private void processMessage(CANCommand msg)
        {
            processReadMsg(msg);
        }

        public void sendLightCommand(bool status)
        {
            CANCommand msg = new CANCommand(0x000, 0, 8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            switch (status)
            {
                case false:
                    msg = new CANCommand(0x7A3, 0, 8, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                    break;
                case true://attivo la luce per un tempo di 60s
                    msg = new CANCommand(0x7A3, 0, 8, 0x05, 0xEA, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00);
                    break;
            }
            connector.enqueueMessage(msg);
        }

        public void sendLaserCommand(byte laser, bool status)
        {
            CANCommand msg;
            if (!testColl.configTest.nomeCollim.Contains("230"))
            {
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x10, laser, (byte)(status ? 2 : 0), 0, 0, 0, 0, 0);
            }
            else
            {
                //accendo il laser per 10s
                msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x10, 0x00, (byte)(status ? 0x64 : 0x00), 0, 0, 0, 0, 0);
            }
            connector.enqueueMessage(msg);
        }

        //valorizza le variabili di istanza del protocollo che vengono poi analizzate dalla R225Panel
        private void processReadMsg(CANCommand msg)
        {
            CANCommand mex;
            if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03) || msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x0E))
            {
                switch (msg.getDatum(0))
                {
                    case 0x70://EOS
                    case 0xF0:
                        listCalTable[msg.getDatum(1) - 1].PassiMassimi = (msg.getDatum(2) << 24) + (msg.getDatum(3) << 16) + (msg.getDatum(4) << 8) + msg.getDatum(5);
                        //Richiesta numero punti di taratura
                        mex = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x71, msg.getDatum(1), 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
                        connector.enqueueMessage(mex);
                        break;
                    case 0x71://EOS
                    case 0xF1:
                        listCalTable[msg.getDatum(1) - 1].PuntiTaratura = msg.getDatum(2);
                        for (int i = 0; i < listCalTable[msg.getDatum(1) - 1].PuntiTaratura; i++)
                        {
                            mex = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x72, msg.getDatum(1), Convert.ToByte(i), 0x00, 0x00, 0x00, 0x00, 0x00);
                            connector.enqueueMessage(mex);
                        }
                        break;
                    case 0x72://EOS
                    case 0xF2:
                        {
                            listCalTable[msg.getDatum(1) - 1].Aperture.Add(Convert.ToInt32((msg.getDatum(3) << 8) + msg.getDatum(4)));
                            listCalTable[msg.getDatum(1) - 1].Passi.Add(Convert.ToInt32((msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + msg.getDatum(7)));
                        }
                        break;
                    case 0x7F:
                    case 0xFF:
                        switch (msg.getDatum(1))
                        {
                            case 0x00:
                                fwBootloader = Convert.ToInt32((msg.getDatum(2) << 8) + msg.getDatum(3)) + "." + Convert.ToInt32(msg.getDatum(4)) + "." + Convert.ToInt32(msg.getDatum(5));
                                emitChanges(ProtocolChange.Info);
                                break;
                            case 0x01:
                                fwCollimator = Convert.ToInt32((msg.getDatum(2) << 8) + msg.getDatum(3)) + "." + Convert.ToInt32(msg.getDatum(4)) + "." + Convert.ToInt32(msg.getDatum(5));
                                emitChanges(ProtocolChange.Info);
                                break;
                        }
                        break;
                }
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x00))
            {
                motore1 = (Int16)Convert.ToInt32((msg.getDatum(0) << 8) + msg.getDatum(1));
                motore2 = (Int16)Convert.ToInt32((msg.getDatum(2) << 8) + msg.getDatum(3));
                motore3 = (Int16)Convert.ToInt32((msg.getDatum(4) << 8) + msg.getDatum(5));
                motore4 = (Int16)Convert.ToInt32((msg.getDatum(6) << 8) + msg.getDatum(7));
                emitChanges(ProtocolChange.Motori1_4);
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x01))
            {
                motore5 = (Int16)Convert.ToInt32((msg.getDatum(0) << 8) + msg.getDatum(1));
                motore6 = (Int16)Convert.ToInt32((msg.getDatum(2) << 8) + msg.getDatum(3));
                motore7 = (Int16)Convert.ToInt32((msg.getDatum(4) << 8) + msg.getDatum(5));
                motore8 = (Int16)Convert.ToInt32((msg.getDatum(6) << 8) + msg.getDatum(7));
                emitChanges(ProtocolChange.Motori5_8);
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x02))
            {
                motore9 = (Int16)Convert.ToInt32((msg.getDatum(0) << 8) + msg.getDatum(1));
                motore10 = (Int16)Convert.ToInt32((msg.getDatum(2) << 8) + msg.getDatum(3));
                motore11 = (Int16)Convert.ToInt32((msg.getDatum(4) << 8) + msg.getDatum(5));
                motore12 = (Int16)Convert.ToInt32((msg.getDatum(6) << 8) + msg.getDatum(7));
                emitChanges(ProtocolChange.Motori9_12);
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x03))
            {
                if ((msg.getDatum(2) & 0x80) == 0x80)
                {
                    errTimeoutRxCAN = true;
                    emitChanges(ProtocolChange.Errors);
                }
                else if ((msg.getDatum(2) & 0x08) == 0x08)
                {
                    errReset = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if ((msg.getDatum(3) & 0x80) == 0x80)
                {
                    errTempLow = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if ((msg.getDatum(3) & 0x40) == 0x40)
                {
                    errTempHigh = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(3) & 0x08) == 0x08) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot12 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(3) & 0x04) == 0x04) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot11 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(3) & 0x02) == 0x02) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot10 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(3) & 0x01) == 0x01) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot9 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x80) == 0x80) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot8 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x40) == 0x40) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot7 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x20) == 0x20) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot6 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x10) == 0x10) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot5 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x08) == 0x08) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot4 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x04) == 0x04) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot3 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x02) == 0x02) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot2 = true;
                    emitChanges(ProtocolChange.Errors);
                }

                else if (((msg.getDatum(4) & 0x01) == 0x01) && (msg.getDatum(2) & 0x80) == 0x80)
                {
                    errMot1 = true;
                    emitChanges(ProtocolChange.Errors);
                }
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x04))
            {
                switch (msg.getDLC())
                {
                    case 1:
                        //switch (msg.getDatum(0))
                        //{
                        //    case 0x00:
                        //        TastiMembrActive = false;
                        //        break;
                        //    default:
                        //        TastiMembrActive = true;
                        //        testTastiMembr = (byte)(testTastiMembr | msg.getDatum(0));
                        //        TastiMembPressed = testTastiMembr;
                        //        switch (testTastiMembr)
                        //        {
                        //            case 0x3F:
                        //                TestTastiMemb = true;
                        //                break;
                        //            default:
                        //                TestTastiMemb = false;
                        //                break;
                        //        }
                        //        break;
                        //}
                        //emitChanges(ProtocolChange.TastiMembrana);
                        break;
                    case 8:
                        switch (msg.getDatum(0))
                        {
                            case 0x04:
                                switch (msg.getDatum(1))
                                {
                                    case 0x01:
                                        dsc1 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                        dsc1Status = true;
                                        break;
                                    case 0x02:
                                        dsc2 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                        dsc2Status = true;
                                        break;
                                }
                                emitChanges(ProtocolChange.DSC);
                                break;
                            case 0x05:
                                switch (msg.getDatum(1))
                                {
                                    case 0x01:
                                        dsc1 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                        dsc1Status = false;
                                        break;
                                    case 0x02:
                                        dsc2 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                        dsc2Status = false;
                                        break;
                                }
                                emitChanges(ProtocolChange.DSC);
                                break;
                            //EV_LIGHT
                            case 0x08:
                                switch (msg.getDatum(7))
                                {
                                    case 0x00:
                                        light = false;
                                        break;
                                    case 0x01:
                                        light = true;
                                        break;
                                }
                                emitChanges(ProtocolChange.Light);
                                break;
                            //EV_CHIAVE
                            case 0x0C:
                                //switch (msg.getDatum(7))
                                //{
                                //    case 0x00:
                                //        key = false;
                                //        break;
                                //    case 0x01:
                                //        key = true;
                                //        break;
                                //}
                                //emitChanges(ProtocolChange.Key);
                                break;
                            //EV_FILTER_READY
                            case 0x0D:
                                //filter = msg.getDatum(7);
                                //emitChanges(ProtocolChange.Filter);
                                break;
                            //EV_PASSI_PHOTO
                            case 0x0E:
                                switch(msg.getDatum(1))
                                {
                                    case 0x01:
                                        passiMot1 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot1.Equals(-1) || (Math.Abs(50 - passiMot1) > 50))
                                        {
                                            errMot1 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x02:
                                        passiMot2 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot2.Equals(-1) || (Math.Abs(50 - passiMot2) > 50))
                                        {
                                            errMot2 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x03:
                                        passiMot3 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot3.Equals(-1) || (Math.Abs(50 - passiMot3) > 50))
                                        {
                                            errMot3 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x04:
                                        passiMot4 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot4.Equals(-1) || (Math.Abs(50 - passiMot4) > 50))
                                        {
                                            errMot4 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x05:
                                        passiMot5 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot5.Equals(-1) || (Math.Abs(50 - passiMot5) > 50))
                                        {
                                            errMot5 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x06:
                                        passiMot6 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot6.Equals(-1) || (Math.Abs(50 - passiMot6) > 50))
                                        {
                                            errMot6 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x07:
                                        passiMot7 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot7.Equals(-1) || (Math.Abs(50 - passiMot7) > 50))
                                        {
                                            errMot7 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x08:
                                        passiMot8 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot8.Equals(-1) || (Math.Abs(50 - passiMot8) > 50))
                                        {
                                            errMot8 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x09:
                                        passiMot9 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot9.Equals(-1) || (Math.Abs(50 - passiMot9) > 50))
                                        {
                                            errMot9 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x0A:
                                        passiMot10 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot10.Equals(-1) || (Math.Abs(50 - passiMot10) > 50))
                                        {
                                            errMot10 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x0B:
                                        passiMot11 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot11.Equals(-1) || (Math.Abs(50 - passiMot11) > 50))
                                        {
                                            errMot11 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                    case 0x0C:
                                        passiMot12 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                        if (passiMot12.Equals(-1) || (Math.Abs(50 - passiMot12) > 50))
                                        {
                                            errMot12 = true;
                                            emitChanges(ProtocolChange.Errors);
                                        }
                                        break;
                                }
                                break;
                            //EV_STATO_USCITA
                            case 0x15:
                                switch (msg.getDatum(1))
                                {
                                    case 0x01:
                                        {
                                            laser1 = Convert.ToBoolean(msg.getDatum(7));
                                            emitChanges(ProtocolChange.Laser1);
                                        }
                                        break;
                                    case 0x02:
                                        {
                                            laser2 = Convert.ToBoolean(msg.getDatum(7));
                                            emitChanges(ProtocolChange.Laser2);
                                        }
                                        break;
                                }
                                break;
                            //EV_STATUS_IO_GC009
                            case 0x2F:
                                switch (msg.getDatum(1))
                                {
                                    case 0x09:
                                        {
                                            laser1 = Convert.ToBoolean(msg.getDatum(7));
                                            emitChanges(ProtocolChange.Laser1);
                                        }
                                        break;
                                    case 0x0A:
                                        {
                                            laser2 = Convert.ToBoolean(msg.getDatum(7));
                                            emitChanges(ProtocolChange.Laser2);
                                        }
                                        break;
                                }
                                break;
                            case 0x10:
                                //switch (msg.getDatum(1))
                                //{
                                //    case 0x01://TASTO FILTRO
                                //        switch (msg.getDatum(7))
                                //        {
                                //            case 0x00:
                                //                tastoFiltro = false;
                                //                break;
                                //            case 0x01:
                                //                tastoFiltro = true;
                                //                break;
                                //        }
                                //        emitChanges(ProtocolChange.TastoFiltro);
                                //        break;
                                //    case 0x02://TASTO LUCE
                                //        switch (msg.getDatum(7))
                                //        {
                                //            case 0x00:
                                //                tastoLuce = false;
                                //                break;
                                //            case 0x01:
                                //                tastoLuce = true;
                                //                break;
                                //        }
                                //        emitChanges(ProtocolChange.TastoLuce);
                                //        break;
                                //}
                                break;
                            case 0x40://EV_ERROR
                                switch (msg.getDatum(2))
                                {
                                    //case 0x01://FORMATO NON RAGGIUNGIBILE
                                    //    switch (msg.getDatum(1))
                                    //    {
                                    //        case 0x01:
                                    //            errFormNonRaggMot1 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x02:
                                    //            errFormNonRaggMot2 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x03:
                                    //            errFormNonRaggMot3 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x04:
                                    //            errFormNonRaggMot4 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x05:
                                    //            errFormNonRaggMot5 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x06:
                                    //            errFormNonRaggMot6 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x07:
                                    //            errFormNonRaggMot7 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x08:
                                    //            errFormNonRaggMot8 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x09:
                                    //            errFormNonRaggMot9 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x0A:
                                    //            errFormNonRaggMot10 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x0B:
                                    //            errFormNonRaggMot11 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //        case 0x0C:
                                    //            errFormNonRaggMot12 = (msg.getDatum(4) << 24) + (msg.getDatum(5) << 16) + (msg.getDatum(6) << 8) + (msg.getDatum(7));
                                    //            emitChanges(ProtocolChange.Errors);
                                    //            break;
                                    //    }
                                    //    break;
                                }
                                break;
                        }
                        break;
                }
            }
            else if (msg.getID() == Convert.ToUInt32(testColl.configTest.baseID_Recv + testColl.configTest.offsetID_Recv + 0x07))
            {
                switch (msg.getDatum(0))
                {
                    //VALORI X, Y, Z INCLINOMETRO
                    case 0x01:
                        {
                            inclX = (Int16)((msg.getDatum(1) << 8) + (msg.getDatum(2)));
                            inclY = (Int16)((msg.getDatum(3) << 8) + (msg.getDatum(4)));
                            inclZ = (Int16)((msg.getDatum(5) << 8) + (msg.getDatum(6)));
                            emitChanges(ProtocolChange.Incl);
                        }
                        break;
                }
            }
        }

        private void emitChanges(ProtocolChange value)
        {
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
            {
                LogManager.GetLogger(connector.Logger.Name).Warn(connector.Logger.Name + " processor messages not available ");
            }
        }

        public void sendVersionRequests()
        {
            //Richiesta Versione Firmware
            CANCommand msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x7F, 0x12, 0x34, 0x56, 0x01, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            //Richiesta Versione Bootloader
            msg = new CANCommand(Convert.ToUInt32(testColl.configTest.baseID_Send + testColl.configTest.offsetID_Send + 0x03), 0, 8, 0x7F, 0x12, 0x34, 0x56, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            //emitChanges(ProtocolChange.Info);
        }
    }
}
