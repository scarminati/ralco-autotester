﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autotester
{
	public partial class Form_Login : Form
	{
        private List<Utente> users;
        private SQLConnector conn; 
        public Form_Login()
		{
            InitializeComponent();
            users = new List<Utente>();
            conn = new SQLConnector();
            try
            {
                //users = (List<Utente>)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[Utente2] JOIN [AutoTest].[dbo].[Applicativo] ON [AutoTest].[dbo].[Utente].[idUtente] = [AutoTest].[dbo].[Applicativo].[idUtente] WHERE [AutoTest].[dbo].[Applicativo].[idApplicativo] = [AutoTest].[dbo].[Utente].[idApplicativo] AND [Autotest].[dbo].[Applicativo].[nomeApplicativo] = 'Ralco Autotester'", users);
                users = (List<Utente>)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[Utente2] JOIN [AutoTest].[dbo].[Applicativo] ON [AutoTest].[dbo].[Utente2].[idUtente] = [AutoTest].[dbo].[Applicativo].[idUtente] WHERE [AutoTest].[dbo].[Applicativo].[idUtente] = [AutoTest].[dbo].[Utente2].[idUtente] AND [Autotest].[dbo].[Applicativo].[nomeApplicativo] = 'Ralco Autotester'", users);
                foreach (var u in users)
                    cmb_UserName.Items.Add(u.nome + " " + u.cognome);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
		{
            try
            {
                if (cmb_UserName.SelectedItem != null)
                {
                    foreach (var usr in users)
                    {
                        if (cmb_UserName.SelectedItem.ToString().Equals(usr.nome + " " + usr.cognome) && usr.idApplicativo.Equals(2))//idApplicativo = 2 => Autotest
                        {
                            if (txt_password.Text.Equals(usr.password))
                            {
                                Form_Autotest formAutotest = new Form_Autotest(usr);
                                formAutotest.Show();
                                this.Hide();
                            }
                            else
                                MessageBox.Show("Password errata", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                    MessageBox.Show("Inserisci un nome utente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
