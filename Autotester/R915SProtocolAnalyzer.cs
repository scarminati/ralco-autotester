﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Media;

namespace Autotester
{
    public class R915SProtocolAnalyzer : IProtocolDataChanges<ProtocolChange>, IProtocolAnalyzer
    {
        private CANusb_Connector connector = null;
        private int flag = -1;//variabile usata per determinare se i messaggi da analizzare contengono le stringhe per la versione FW, BL, PCB
        public string protocolName { get { return "CAN-OPEN"; } }
        // attributi collimatore
        List<CalibTable> listCalTable = new List<CalibTable>();
        private bool requestForCalTable = false;

        private string fwCollimator = "";
        private string fwBootloader = "";
        private string pcbCollimator = "";
        private string serialNumber;

        private int front;
        private int right;
        private int rear;
        private int left;
        private int filter;
        private int focalSpot;
        private bool light;
        private bool configComplete = false;

        public bool error0 { get; set; }
        public bool error90 { get; set; }
        public bool error180 { get; set; }
        public bool error270 { get; set; }

        private bool postError = false;
        private bool emcyBufferFull = false;
        private bool busOff = false;
        private bool memoryError = false;
        private bool overVoltage = false;
        private bool underVoltage = false;
        private bool tempOutOfRange = false;
        private bool canOverRun = false;
        private bool errMotFront = false;
        private bool errMotRight = false;
        private bool errMotRear = false;
        private bool errMotLeft = false;
        private bool errMotFiltro = false;
        private bool errHomFront = false;
        private bool errHomRight = false;
        private bool errHomRear = false;
        private bool errHomLeft = false;
        private bool errHomFiltro = false;
        private bool errLimHWFront = false;
        private bool errLimHWRight = false;
        private bool errLimHWRear = false;
        private bool errLimHWLeft = false;
        private bool errLimHWFiltro = false;

        private bool evGCLED_tLedAlta = false;
        private bool evGCLED_tSchedaAlta = false;
        private bool evGCLED_tastoPremutoPiuDi5s = false;
        private bool evGCLED_laserError = false;
        private bool evGCLED_fanError = false;
        private bool evGCLED_LEDcc = false;
        private bool evGCLED_LEDdisconnesso = false;

        //passi motore
        private Int32 passiMot1;
        private Int32 passiMot2;
        private Int32 passiMot3;
        private Int32 passiMot4;

        public int filterWhenError { get; set; }
        public int crossPosWhenError { get; set; }
        public int longPosWhenError { get; set; }

        public int passiLong { get; set; }
        public int deltaPotenziometriLong { get; set; }
        public int passiCross { get; set; }
        public int deltaPotenziometriCross { get; set; }
        public int passiIride { get; set; }
        public int passiSpatialFilter { get; set; }
        public int tentativiPosFiltro { get; set; }

        public bool TastiMembrActive { get; set; }
        public bool TestTastiMemb { get; set; }

        private int _counterIteration = 0;

        public int CounterIteration
        {
            get { return _counterIteration; }
            set
            {
                if (this._counterIteration != value)
                {
                    this._counterIteration = value;
                    emitChanges(ProtocolChange.Counter);
                }

            }
        }

        public bool CollimationDone
        {
            get;
        }

        public bool CollimationFailed
        {
            get;
        }

        public bool DscCrossActive
        {
            get;
        }

        public bool DscLongActive
        {
            get;
        }

        public bool CrossJam
        {
            get;
        }

        public bool LongJam
        {
            get;
        }

        public bool CrossCalib
        {
            get;
        }

        public bool LongCalib
        {
            get;
        }

        public bool FilterCalib
        {
            get;
        }

        public bool CrossPot
        {
            get;
        }

        public bool LongPot
        {
            get;
        }

        public bool Light
        {
            get { return light; }
        }

        public bool Key
        {
            get;
        }

        public bool TastoFiltro
        {
            get;
        }

        public bool TastoLuce
        {
            get;
        }

        public int Meter
        {
            get;
        }

        public int MaxSpeed
        {
            get;
        }

        public int Filter
        {
            get { return filter; }
        }

        public int CrossPos
        {
            get;
        }

        public int LongPos
        {
            get;
        }

        public int Temperature
        {
            get;
        }

        public int CrossEncoder
        {
            get;
        }

        public int LongEncoder
        {
            get;
        }

        public string FwCollimator
        {
            get { return fwCollimator; }
        }

        public string FwBootloader
        {
            get { return fwBootloader; }
        }

        public string PcbCollimator
        {
            get { return pcbCollimator; }
        }

        public string SerialNumber
        {
            get { return serialNumber; }
        }

        public bool HeartbeatStatus
        {
            get;
        }

        public bool HeartbeatError
        {
            get;
        }


        // da modificare

        public bool generalError
        {
            get;
        }

        public bool communicationError
        {
            get;
        }

        public bool AEPError
        {
            get;
        }

       //implementati appositamente per protocollo CANOPEN (R 915 S DHHS)

        public int FrontPos
        {
            get { return front; }
        }

        public int RightPos
        {
            get { return right; }
        }

        public int RearPos
        {
            get { return rear; }
        }

        public int LeftPos
        {
            get { return left; }
        }

        public int FocalSpot
        {
            get { return focalSpot; }
        }

        public bool POSTerror
        {
            get { return postError; }
        }

        public bool EMCYBufferFull
        {
            get { return emcyBufferFull; }
        }

        public bool BUSOff
        {
            get { return busOff; }
        }

        public bool MemoryError
        {
            get { return memoryError; }
        }

        public bool OverVoltage
        {
            get { return overVoltage; }
        }

        public bool UnderVoltage
        {
            get { return underVoltage; }
        }

        public bool TempOutOfRange
        {
            get { return tempOutOfRange; }
        }

        public bool CANOverRun
        {
            get { return canOverRun; }
        }

        public bool ErrMotFront
        {
            get { return errMotFront; }
        }

        public bool ErrMotRight
        {
            get { return errMotRight; }
        }

        public bool ErrMotRear
        {
            get { return errMotRear; }
        }

        public bool ErrMotLeft
        {
            get { return errMotLeft; }
        }

        public bool ErrMotFiltro
        {
            get { return errMotFiltro; }
        }

        public bool ErrLimHWFront
        {
            get { return errLimHWFront; }
        }

        public bool ErrLimHWRight
        {
            get { return errLimHWRight; }
        }

        public bool ErrLimHWRear
        {
            get { return errLimHWRear; }
        }

        public bool ErrLimHWLeft
        {
            get { return errLimHWLeft; }
        }

        public bool ErrLimHWFiltro
        {
            get { return errLimHWFiltro; }
        }

        public bool ErrHomFront
        {
            get { return errHomFront; }
        }

        public bool ErrHomRight
        {
            get { return errHomRight; }
        }

        public bool ErrHomRear
        {
            get { return errHomRear; }
        }

        public bool ErrHomLeft
        {
            get { return errHomLeft; }
        }

        public bool ErrHomFiltro
        {
            get { return errHomFiltro; }
        }

        public bool EVGCLED_tLedAlta
        {
            get { return evGCLED_tLedAlta; }
        }

        public bool EVGCLED_tSchedaAlta
        {
            get { return evGCLED_tSchedaAlta; }
        }

        public bool EVGCLED_tastoPremutoPiuDi5s
        {
            get { return evGCLED_tastoPremutoPiuDi5s; }
        }

        public bool EVGCLED_laserError
        {
            get { return evGCLED_laserError; }
        }

        public bool EVGCLED_fanError
        {
            get { return evGCLED_fanError; }
        }

        public bool EVGCLED_LEDcc
        {
            get { return evGCLED_LEDcc; }
        }

        public bool EVGCLED_LEDdisconnesso
        {
            get { return evGCLED_LEDdisconnesso; }
        }

        public Int32 PassiMot1
        {
            get { return passiMot1; }
        }

        public Int32 PassiMot2
        {
            get { return passiMot2; }
        }

        public Int32 PassiMot3
        {
            get { return passiMot3; }
        }

        public Int32 PassiMot4
        {
            get { return passiMot4; }
        }

        public CANusb_Connector Connector
        {
            get { return connector; }
            set
            {
                if (value == null)
                {
                    if (this.connector != null)
                    {
                        connector.Handler -= ConnectorOnHandler();
                    }
                    connector = null;

                }
                else
                {
                    connector = value;
                    connector.Handler += ConnectorOnHandler();                    
                }
            }
        }

        public testCollimatore testColl { get; set; }

        public List<CalibTable> CalibTable { get { return listCalTable; } }


        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate(CANCommand command) { this.processMessage(command); };
        }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<ProtocolChange> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<ProtocolChange> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<ProtocolChange> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<ProtocolChange>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */
        
        public void initCollimator()
        {
            /*Configurazioni necessarie per l'attivazione del collimatore*/
            //NMT Start
            CANCommand msg = new CANCommand(0x000, 0, 2, 0x01, 0x07, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            //emitChanges(ProtocolChange.ConfigurationComplete);
        }

        public void setSerialNumberOnPCB(string SN)
        {
            CANCommand msg = new CANCommand(0x607, 0, 8, 0x23, 0x00, 0x28, 0x01, 0x65, 0x87, 0x21, 0x43);
            connector.enqueueMessage(msg);
            byte[] bytes = new byte[SN.Length / 2];
            for (int i = 0; i < SN.Length; i += 2)
                bytes[i / 2] = Convert.ToByte(SN.Substring(i, 2), 16);
            msg = new CANCommand(0x607, 0, 8, 0x23, 0x00, 0x28, 0x02, bytes[3], bytes[2], bytes[1], bytes[0]);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x607, 0, 8, 0x23, 0x00, 0x28, 0x01, 0x11, 0x11, 0x22, 0x22);
            connector.enqueueMessage(msg);
        }

        public void setCollimatorForShipment()
        {
            //lamelle tutte aperte
            CANCommand msg = new CANCommand(0x207, 0, 8, 0x82, 0x00, 0x54, 0x06, 0x5A, 0x0A, 0x54, 0x06);
            connector.enqueueMessage(msg);
            //filtro in posizione 4
            msg = new CANCommand(0x307, 0, 1, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            //Luce Spenta
            msg = new CANCommand(0x407, 0, 5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            //Punto focale 0
            msg = new CANCommand(0x507, 0, 1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            connector.enqueueMessage(msg);
            playSoundEndTest();
        }

        public void playSoundEndTest()
        {
            SoundPlayer simpleSound = new SoundPlayer(Environment.CurrentDirectory + @"\sounds\alarmEndTest.wav");
            simpleSound.Play();
        }


        public void sendVersionRequests()
        {
            //Richiesta Versione Firmware
            CANCommand msg = new CANCommand(0x607, 0, 8, 0x40, 0x0A, 0x10, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x607, 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Richiesta Versione PCB
            msg = new CANCommand(0x607, 0, 8, 0x40, 0x09, 0x10, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x607, 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x607, 0, 8, 0x70, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Richiesta Versione Bootloader
            msg = new CANCommand(0x607, 0, 8, 0x40, 0x09, 0x22, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x607, 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
        }

        public void readCalibTable()
        {
            if (!requestForCalTable)
            {
                requestForCalTable = true;
                for (byte motore = 1; motore <= testColl.configTest.numMotori; motore++)
                {
                    listCalTable.Add(new CalibTable());
                    //Richiesta numero punti di taratura motore
                    CANCommand msg = new CANCommand(0x607, 0, 8, 0x40, 0x50, 0x28, motore, 0, 0, 0, 0);
                    connector.enqueueMessage(msg);
                }
            }
        }

        public void readCalibPointApSteps()
        {

        }

        public void sendLightCommand(bool status)
        {
            CANCommand msg = new CANCommand(0x407, 0, 5, 0x00, 0x00, 0x00, 0x00, (byte)(status ? 1 : 0), 0, 0, 0);
            connector.enqueueMessage(msg);
        }

        private void processMessage(CANCommand msg)
        {
            processReadMsg(msg);
        }

        //valorizza le variabili di istanza del protocollo che vengono poi analizzate dalla R915SPanel
        private void processReadMsg(CANCommand msg)
        {
            if (msg.getID().Equals(0x087))
            {
                if (msg.getDatum(1) == 0xF0 && msg.getDatum(0) == 0x70)
                {
                    postError = true;
                }
                if (msg.getDatum(1) == 0x10 && msg.getDatum(0) == 0x00)
                {
                    emcyBufferFull = true;
                }
                if (msg.getDatum(1) == 0x81 && msg.getDatum(0) == 0x40)
                {
                    busOff = true;
                }
                if (msg.getDatum(1) == 0x51 && msg.getDatum(0) == 0x00)
                {
                    memoryError = true;
                }
                if (msg.getDatum(1) == 0x31 && msg.getDatum(0) == 0x00)
                {
                    overVoltage = true;
                }
                if (msg.getDatum(1) == 0x31 && msg.getDatum(0) == 0x01)
                {
                    underVoltage = true;
                }
                if (msg.getDatum(1) == 0x42 && msg.getDatum(0) == 0x00)
                {
                    tempOutOfRange = true;
                }
                if (msg.getDatum(1) == 0x81 && msg.getDatum(0) == 0x10)
                {
                    canOverRun = true;
                }
                if (msg.getDatum(1) == 0x53 && msg.getDatum(0) == 0x00)
                {
                    if (msg.getDatum(7) == 0x00)
                    {
                        if ((msg.getDatum(6) & 0x01).Equals(0x01))
                            errMotFront = true;
                        if ((msg.getDatum(6) & 0x02).Equals(0x02))
                            errMotLeft = true;
                        if ((msg.getDatum(6) & 0x04).Equals(0x04))
                            errMotRear = true;
                        if ((msg.getDatum(6) & 0x08).Equals(0x08))
                            errMotRight = true;
                        if ((msg.getDatum(6) & 0x10).Equals(0x10))
                            errMotFiltro = true;
                    }
                    else if (msg.getDatum(7) == 0x01)//errori provenienti dalla GC-LED 4A
                    {
                        if ((msg.getDatum(4) & 0x01) == 0x01)
                        {
                            evGCLED_LEDdisconnesso = true;
                        }
                        if ((msg.getDatum(4) & 0x02) == 0x02)
                        {
                            evGCLED_LEDcc = true;
                        }
                        if ((msg.getDatum(4) & 0x04) == 0x04)
                        {
                            evGCLED_fanError = true;
                        }
                        if ((msg.getDatum(4) & 0x08) == 0x08)
                        {
                            evGCLED_laserError = true;
                        }
                        if ((msg.getDatum(4) & 0x10) == 0x10)
                        {
                            evGCLED_tastoPremutoPiuDi5s = true;
                        }
                        if ((msg.getDatum(4) & 0x20) == 0x20)
                        {
                            evGCLED_tSchedaAlta = true;
                        }
                        if ((msg.getDatum(4) & 0x40) == 0x40)
                        {
                            evGCLED_tLedAlta = true;
                        }
                        emitChanges(ProtocolChange.Errors);
                    }
                }
                if (msg.getDatum(1) == 0x52 && msg.getDatum(0) == 0x00)
                {
                    if ((msg.getDatum(6) & 0x01).Equals(0x01))
                        errHomFront = true;
                    if ((msg.getDatum(6) & 0x02).Equals(0x02))
                        errHomLeft = true;
                    if ((msg.getDatum(6) & 0x04).Equals(0x04))
                        errHomRear = true;
                    if ((msg.getDatum(6) & 0x08).Equals(0x08))
                        errHomRight = true;
                    if ((msg.getDatum(6) & 0x10).Equals(0x10))
                        errHomFiltro = true;
                }
                if (msg.getDatum(1) == 0xA0 && msg.getDatum(0) == 0x30)
                {
                    if ((msg.getDatum(4) & 0x01).Equals(0x01))
                        errLimHWFront = true;
                    if ((msg.getDatum(4) & 0x02).Equals(0x02))
                        errLimHWLeft = true;
                    if ((msg.getDatum(4) & 0x04).Equals(0x04))
                        errLimHWRear = true;
                    if ((msg.getDatum(4) & 0x08).Equals(0x08))
                        errLimHWRight = true;
                    if ((msg.getDatum(4) & 0x10).Equals(0x10))
                        errLimHWFiltro = true;
                }
                emitChanges(ProtocolChange.Errors);
            }
            else if (msg.getID().Equals(0x187))
            {
                front = (Int16)Convert.ToInt32(msg.getDatum(0) + (msg.getDatum(1) << 8));
                left = (Int16)Convert.ToInt32(msg.getDatum(2) + (msg.getDatum(3) << 8));
                rear = (Int16)Convert.ToInt32(msg.getDatum(4) + (msg.getDatum(5) << 8));
                right = (Int16)Convert.ToInt32(msg.getDatum(6) + (msg.getDatum(7) << 8));
                emitChanges(ProtocolChange.Shutters);
            }
            else if (msg.getID().Equals(0x287))
            {
                filter = Convert.ToInt32(msg.getDatum(0));
                emitChanges(ProtocolChange.Filter);
            }
            else if (msg.getID().Equals(0x387))
            {
                light = (msg.getDatum(4) == 0x01 ? true : false);
                emitChanges(ProtocolChange.Light);
            }
            else if (msg.getID().Equals(0x487))
            {
                focalSpot = Convert.ToInt32(msg.getDatum(0));
                emitChanges(ProtocolChange.FocalSpot);
            }
            //Generali
            else if (msg.getID().Equals(0x587))
            {
                switch (msg.getDatum(0))
                {
                    case (0x41):
                        switch (msg.getDatum(1))
                        {
                            case (0x09):
                                switch (msg.getDatum(2))
                                {
                                    case (0x10):
                                        pcbCollimator = "";
                                        flag = 0;//Versione PCB
                                        break;
                                    case (0x22):
                                        fwBootloader = "";
                                        flag = 1;//Versione Bootloader
                                        break;
                                }
                                break;
                            case (0x0A):
                                fwCollimator = "";
                                flag = 2;//Versione Firmware
                                break;
                        }
                        break;
                    case (0x43)://4 byte
                        {
                            //Aperture
                            if (msg.getDatum(2) == 0x28)
                            {
                                listCalTable[msg.getDatum(1) - 1].Passi.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                //Console.WriteLine("Passi motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                            }
                                
                            //Passi
                            if (msg.getDatum(2) == 0x29)
                            {
                                listCalTable[msg.getDatum(1) - 1].Aperture.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                //Console.WriteLine("Apertura motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                            }
                                
                            
                        }
                        break;
                    case (0x47)://3 byte
                        break;
                    case (0x4B)://2 byte
                        break;
                    case (0x4F)://1 byte
                        {
                            //numero punti di taratura
                            if (msg.getDatum(2) == 0x28 && msg.getDatum(1) == 0x50)
                            {
                                listCalTable[msg.getDatum(3) - 1].PuntiTaratura = msg.getDatum(4);
                                for (byte i = 1; i <= msg.getDatum(4); i++)
                                {
                                    //Richiesta aperture motore per il punto di taratura
                                    CANCommand mex = new CANCommand(0x607, 0, 8, 0x40, msg.getDatum(3), 0x28, i, 0, 0, 0, 0);
                                    connector.enqueueMessage(mex);
                                    //Richiesta passi motore per il punto di taratura
                                    mex = new CANCommand(0x607, 0, 8, 0x40, msg.getDatum(3), 0x29, i, 0, 0, 0, 0);
                                    connector.enqueueMessage(mex);
                                }
                            }
                        }
                        break;
                    default:
                        switch (flag)
                        {
                            case 0:
                                pcbCollimator = pcbCollimator + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                pcbCollimator = pcbCollimator.Replace('\0', ' ').Trim();
                                break;
                            case 1:
                                fwBootloader = fwBootloader + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                fwBootloader = fwBootloader.Replace('\0', ' ').Trim();
                                break;
                            case 2:
                                fwCollimator = fwCollimator + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                                fwCollimator = fwCollimator.Replace('\0', ' ').Trim();
                                break;
                        }
                        emitChanges(ProtocolChange.Info);
                        break;
                }
                ////POST error
                //if (msg.getDatum(0) == 0x70 && msg.getDatum(1) == 0xF0 && msg.getDatum(2) == 0x00 && msg.getDatum(3) == 0x02)
                //{
                //    postError = true;
                //    emitChanges(ProtocolChange.Errors);
                //}
            }
            else if (msg.getID().Equals(0x707))
            {
                switch (msg.getDatum(0))
                {
                    case 0x05:
                        if (!configComplete)
                        {
                            readCalibTable();
                            configComplete = !configComplete;
                            emitChanges(ProtocolChange.ConfigurationComplete);
                        }
                        break;
                }
            }
            else if (msg.getID().Equals(0x7F4))
            {
                switch (msg.getDatum(0))
                {
                    //EV_PASSI_PHOTO
                    case 0x0E:
                        switch (msg.getDatum(1))
                        {
                            case 0x01://front
                                passiMot1 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                if (passiMot1.Equals(-1) || (Math.Abs(50 - passiMot1) > 50))
                                    emitChanges(ProtocolChange.Errors);
                                break;
                            case 0x02://Right
                                passiMot2 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                if (passiMot2.Equals(-1) || (Math.Abs(50 - passiMot2) > 50))
                                    emitChanges(ProtocolChange.Errors);
                                break;
                            case 0x03://Rear
                                passiMot3 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                if (passiMot3.Equals(-1) || (Math.Abs(50 - passiMot3) > 50))
                                    emitChanges(ProtocolChange.Errors);
                                break;
                            case 0x04://Left
                                passiMot4 = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                if (passiMot4.Equals(-1) || (Math.Abs(50 - passiMot4) > 50))
                                    emitChanges(ProtocolChange.Errors);
                                break;
                        }
                        break;
                }
            }
        }

        private void emitChanges(ProtocolChange value)
        {
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
            {
                LogManager.GetLogger(connector.Logger.Name).Warn(connector.Logger.Name + " processor messages not available ");
            }
        }
    }
}
