﻿namespace Autotester
{
    partial class FormR225Errors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbl_deltaPassiLong = new System.Windows.Forms.Label();
            this.lbl_deltaPassiCross = new System.Windows.Forms.Label();
            this.lbl_deltaPassiIride = new System.Windows.Forms.Label();
            this.lbl_numRiposizFiltro = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ledErr270 = new Autotester.LedBulb();
            this.ledErr180 = new Autotester.LedBulb();
            this.ledErr90 = new Autotester.LedBulb();
            this.ledErr0 = new Autotester.LedBulb();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(340, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Filtro";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Errore Perdita Passi";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(254, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Long";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(165, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Cross";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "0°";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "90°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(276, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "180°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(415, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "270°";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(417, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "Iride";
            // 
            // lbl_deltaPassiLong
            // 
            this.lbl_deltaPassiLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiLong.Location = new System.Drawing.Point(234, 51);
            this.lbl_deltaPassiLong.Name = "lbl_deltaPassiLong";
            this.lbl_deltaPassiLong.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiLong.TabIndex = 75;
            // 
            // lbl_deltaPassiCross
            // 
            this.lbl_deltaPassiCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiCross.Location = new System.Drawing.Point(153, 51);
            this.lbl_deltaPassiCross.Name = "lbl_deltaPassiCross";
            this.lbl_deltaPassiCross.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiCross.TabIndex = 74;
            // 
            // lbl_deltaPassiIride
            // 
            this.lbl_deltaPassiIride.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiIride.Location = new System.Drawing.Point(403, 51);
            this.lbl_deltaPassiIride.Name = "lbl_deltaPassiIride";
            this.lbl_deltaPassiIride.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiIride.TabIndex = 77;
            // 
            // lbl_numRiposizFiltro
            // 
            this.lbl_numRiposizFiltro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_numRiposizFiltro.Location = new System.Drawing.Point(327, 102);
            this.lbl_numRiposizFiltro.Name = "lbl_numRiposizFiltro";
            this.lbl_numRiposizFiltro.Size = new System.Drawing.Size(60, 19);
            this.lbl_numRiposizFiltro.TabIndex = 76;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 13);
            this.label12.TabIndex = 78;
            this.label12.Text = "Errore Riposizionamento";
            // 
            // ledErr270
            // 
            this.ledErr270.Color = System.Drawing.Color.Red;
            this.ledErr270.Location = new System.Drawing.Point(417, 48);
            this.ledErr270.Name = "ledErr270";
            this.ledErr270.On = true;
            this.ledErr270.Size = new System.Drawing.Size(27, 23);
            this.ledErr270.TabIndex = 57;
            this.ledErr270.Text = "ledBulb9";
            // 
            // ledErr180
            // 
            this.ledErr180.Color = System.Drawing.Color.Red;
            this.ledErr180.Location = new System.Drawing.Point(280, 48);
            this.ledErr180.Name = "ledErr180";
            this.ledErr180.On = true;
            this.ledErr180.Size = new System.Drawing.Size(25, 23);
            this.ledErr180.TabIndex = 56;
            this.ledErr180.Text = "ledBulb4";
            // 
            // ledErr90
            // 
            this.ledErr90.Color = System.Drawing.Color.Red;
            this.ledErr90.Location = new System.Drawing.Point(153, 48);
            this.ledErr90.Name = "ledErr90";
            this.ledErr90.On = true;
            this.ledErr90.Size = new System.Drawing.Size(22, 23);
            this.ledErr90.TabIndex = 55;
            this.ledErr90.Text = "ledBulb5";
            // 
            // ledErr0
            // 
            this.ledErr0.Color = System.Drawing.Color.Red;
            this.ledErr0.Location = new System.Drawing.Point(22, 48);
            this.ledErr0.Name = "ledErr0";
            this.ledErr0.On = true;
            this.ledErr0.Size = new System.Drawing.Size(23, 23);
            this.ledErr0.TabIndex = 54;
            this.ledErr0.Text = "ledBulb6";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ledErr0);
            this.groupBox1.Controls.Add(this.ledErr90);
            this.groupBox1.Controls.Add(this.ledErr180);
            this.groupBox1.Controls.Add(this.ledErr270);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 85);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rotazione Collimatori";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_numRiposizFiltro);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lbl_deltaPassiIride);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.lbl_deltaPassiLong);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lbl_deltaPassiCross);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(475, 135);
            this.groupBox2.TabIndex = 80;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Allarmi Motore";
            // 
            // FormR225Errors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 251);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormR225Errors";
            this.Text = "R225 Errori";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private LedBulb ledErr270;
        private LedBulb ledErr180;
        private LedBulb ledErr90;
        private LedBulb ledErr0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbl_deltaPassiLong;
        private System.Windows.Forms.Label lbl_deltaPassiCross;
        private System.Windows.Forms.Label lbl_deltaPassiIride;
        private System.Windows.Forms.Label lbl_numRiposizFiltro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}