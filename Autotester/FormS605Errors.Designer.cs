﻿namespace Autotester
{
    partial class FormS6055Errors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ledErr270 = new Autotester.LedBulb();
            this.ledErr180 = new Autotester.LedBulb();
            this.ledErr90 = new Autotester.LedBulb();
            this.ledErr0 = new Autotester.LedBulb();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ledPOST = new Autotester.LedBulb();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl_deltaPassiLamelle = new System.Windows.Forms.Label();
            this.lbl_deltaPassiIride = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ledSpFiltJam = new Autotester.LedBulb();
            this.label1 = new System.Windows.Forms.Label();
            this.ledIrisJam = new Autotester.LedBulb();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(177, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Iride";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(87, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Lamelle";
            // 
            // ledErr270
            // 
            this.ledErr270.Color = System.Drawing.Color.Red;
            this.ledErr270.Location = new System.Drawing.Point(197, 54);
            this.ledErr270.Name = "ledErr270";
            this.ledErr270.On = true;
            this.ledErr270.Size = new System.Drawing.Size(23, 23);
            this.ledErr270.TabIndex = 57;
            this.ledErr270.Text = "ledBulb9";
            // 
            // ledErr180
            // 
            this.ledErr180.Color = System.Drawing.Color.Red;
            this.ledErr180.Location = new System.Drawing.Point(143, 54);
            this.ledErr180.Name = "ledErr180";
            this.ledErr180.On = true;
            this.ledErr180.Size = new System.Drawing.Size(23, 23);
            this.ledErr180.TabIndex = 56;
            this.ledErr180.Text = "ledBulb4";
            // 
            // ledErr90
            // 
            this.ledErr90.Color = System.Drawing.Color.Red;
            this.ledErr90.Location = new System.Drawing.Point(90, 54);
            this.ledErr90.Name = "ledErr90";
            this.ledErr90.On = true;
            this.ledErr90.Size = new System.Drawing.Size(23, 23);
            this.ledErr90.TabIndex = 55;
            this.ledErr90.Text = "ledBulb5";
            // 
            // ledErr0
            // 
            this.ledErr0.Color = System.Drawing.Color.Red;
            this.ledErr0.Location = new System.Drawing.Point(38, 54);
            this.ledErr0.Name = "ledErr0";
            this.ledErr0.On = true;
            this.ledErr0.Size = new System.Drawing.Size(23, 23);
            this.ledErr0.TabIndex = 54;
            this.ledErr0.Text = "ledBulb6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "0°";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "90°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "180°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "270°";
            // 
            // ledPOST
            // 
            this.ledPOST.Color = System.Drawing.Color.Red;
            this.ledPOST.Location = new System.Drawing.Point(38, 50);
            this.ledPOST.Name = "ledPOST";
            this.ledPOST.On = true;
            this.ledPOST.Size = new System.Drawing.Size(23, 23);
            this.ledPOST.TabIndex = 67;
            this.ledPOST.Text = "ledBulb1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(31, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "POST";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 70;
            this.label16.Text = "Delta Passi";
            // 
            // lbl_deltaPassiLamelle
            // 
            this.lbl_deltaPassiLamelle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiLamelle.Location = new System.Drawing.Point(79, 41);
            this.lbl_deltaPassiLamelle.Name = "lbl_deltaPassiLamelle";
            this.lbl_deltaPassiLamelle.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiLamelle.TabIndex = 72;
            // 
            // lbl_deltaPassiIride
            // 
            this.lbl_deltaPassiIride.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiIride.Location = new System.Drawing.Point(160, 41);
            this.lbl_deltaPassiIride.Name = "lbl_deltaPassiIride";
            this.lbl_deltaPassiIride.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiIride.TabIndex = 73;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ledErr270);
            this.groupBox1.Controls.Add(this.ledErr0);
            this.groupBox1.Controls.Add(this.ledErr90);
            this.groupBox1.Controls.Add(this.ledErr180);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 182);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 100);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rotazione Collimatore";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ledIrisJam);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ledSpFiltJam);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.ledPOST);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(12, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(241, 84);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Allarmi Vari";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl_deltaPassiIride);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.lbl_deltaPassiLamelle);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 74);
            this.groupBox3.TabIndex = 76;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Allarmi Motore";
            // 
            // ledSpFiltJam
            // 
            this.ledSpFiltJam.Color = System.Drawing.Color.Red;
            this.ledSpFiltJam.Location = new System.Drawing.Point(93, 50);
            this.ledSpFiltJam.Name = "ledSpFiltJam";
            this.ledSpFiltJam.On = true;
            this.ledSpFiltJam.Size = new System.Drawing.Size(23, 23);
            this.ledSpFiltJam.TabIndex = 69;
            this.ledSpFiltJam.Text = "ledBulb1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Lamelle";
            // 
            // ledIrisJam
            // 
            this.ledIrisJam.Color = System.Drawing.Color.Red;
            this.ledIrisJam.Location = new System.Drawing.Point(143, 50);
            this.ledIrisJam.Name = "ledIrisJam";
            this.ledIrisJam.On = true;
            this.ledIrisJam.Size = new System.Drawing.Size(23, 23);
            this.ledIrisJam.TabIndex = 71;
            this.ledIrisJam.Text = "ledBulb1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(141, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 70;
            this.label7.Text = "Iride";
            // 
            // FormS6055Errors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 294);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormS6055Errors";
            this.Text = "S605 Errori";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private LedBulb ledErr270;
        private LedBulb ledErr180;
        private LedBulb ledErr90;
        private LedBulb ledErr0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private LedBulb ledPOST;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbl_deltaPassiLamelle;
        private System.Windows.Forms.Label lbl_deltaPassiIride;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private LedBulb ledIrisJam;
        private System.Windows.Forms.Label label7;
        private LedBulb ledSpFiltJam;
        private System.Windows.Forms.Label label1;
    }
}