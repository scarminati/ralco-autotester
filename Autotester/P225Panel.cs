﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Xceed.Words.NET;

namespace Autotester
{
    public partial class P225Panel : UserControl, ICollimatorPanel
    {
        private P225ProtocolAnalyzer protocol;
        private ConfigurazioneTest cfg;
        private FormControls formRuota = null;
        private int startLong;
        private int startCross;
        private int minLong;
        private int minCross;
        private int maxLong;
        private int maxCross;
        private int lastLong;
        private int lastCross;
        private int lastDir;
        private int dirLong;
        private int dirCross;
        private int testLuce = 0;
        private int testLaserCenter = 0;
        private int testLaserSID = 0;
        private int numCicli;

        private int _numPanel;

        public int NumPanel { get { return _numPanel; } set { this._numPanel = value; }  }

        public P225Panel()
        {
            InitializeComponent();
            this.scripts = new List<List<SingoloComando>>();
            pathScriptFile = Environment.CurrentDirectory + @"\script\P225\scripts.txt";
        }

        //public void setConnector(CANusb_Connector connector, string deviceName)
        //{
        //    P225ProtocolAnalyzer p225Protocol = new P225ProtocolAnalyzer();
        //    p225Protocol.setDBTmessages(deviceName);
        //    p225Protocol.Connector = connector;

        //    p225Protocol.Handler += ConnectorOnHandler();

        //    protocol = p225Protocol;
        //}

        public void setConnector(testCollimatore t)
        {
            P225ProtocolAnalyzer p225Protocol = new P225ProtocolAnalyzer();
            p225Protocol.testColl = t;
            p225Protocol.setDBTmessages(t.configTest.nomeCollim);
            p225Protocol.Connector = t.connettore;

            p225Protocol.Handler += ConnectorOnHandler();

            protocol = p225Protocol;
        }

        public string collimatorName { get; set; }

        public void setFormRuota(FormControls formRuota)
        {
            this.formRuota = formRuota;
        }

        public void enableControl(ConfigurazioneTest cfg)
        {
            this.cfg = cfg;
            if (cfg.testMetro.Equals(true))
            {
                grpMetro.Enabled = true;
            } 
            if (cfg.testLuce.Equals(true))
            {
                grpLuce.Enabled = true;
            }
            if (cfg.testDSC.Equals(true))
            {
                grpDSC.Enabled = true;
            }  
            if (cfg.testLaser.Equals(true))
            {
                grpLaser.Enabled = true;
            }  
            if (cfg.testVita.Equals(true))
            {
                grpVita.Enabled = true;
            }    
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return protocol;
        }

        public bool isManualTestCompleted()
        {
            bool rv = true;
            if (grpMetro.Enabled)
                rv = rv & ledMetro.On;
            if (grpLuce.Enabled)
                rv = rv & ledLuceTest.On;
            if (grpDSC.Enabled)
                rv = rv & ledCrossDSCTest.On & ledLongDSCTest.On;
            if (grpLaser.Enabled)
                rv = rv & ledCenter.On & ledSID.On;
            return (rv);
        }

        public string getTipoTest()
        {
            if (cmb_tipoTest.SelectedItem == null)
                return "";
            else
                return cmb_tipoTest.SelectedItem.ToString();
        }

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public string pathScriptFile { get; set; }


        public List<List<SingoloComando>> scripts { get; set; }

        public bool testMetro()
        {
            return true;
        }


        private Action<ProtocolChange> ConnectorOnHandler()
        {
            return delegate(ProtocolChange value) { this.CollimatorValuesChanged(value); };
        }

        private void CollimatorValuesChanged(ProtocolChange value)
        {
            int tmp;

            switch (value)
            {
                case ProtocolChange.Shutters:
                    changeCross(protocol.CrossPos / 10);
                    changeLong(protocol.LongPos / 10);
                    
                    break;

                case ProtocolChange.Filter:
                    switch(protocol.Filter)
                    {
                        case 0:
                            changeFilter("0 Al");
                            break;
                        case 1:
                            changeFilter("1 Al + 0,1 Cu");
                            break;
                        case 2:
                            changeFilter("1 Al + 0,2 Cu");
                            break;
                        case 3:
                            changeFilter("2 Al");
                            break;

                        default:
                            changeFilter("Unknown");
                            break;
                    }
                    
                    break;

                case ProtocolChange.Errors:
                    ledError.On = true;
                    if (this.formRuota != null)
                    {
                        switch (this.formRuota.getActualAngleRotation())
                        {
                            case 0:
                                protocol.error0 = true;
                                break;
                            case 90:
                                protocol.error90 = true;
                                break;
                            case 180:
                                protocol.error180 = true;
                                break;
                            case 270:
                                protocol.error270 = true;
                                break;
                        }
                    }
                    if (protocol.crossPosWhenError.Equals (-1))
                        protocol.crossPosWhenError = protocol.CrossPos;
                    if (protocol.longPosWhenError.Equals(-1))
                        protocol.longPosWhenError = protocol.LongPos;
                    if (protocol.filterWhenError.Equals(-1))
                        protocol.filterWhenError = protocol.Filter;
                    changeChkEnableValue(false);
                    break;

                case ProtocolChange.DSC:
                    ledCrossDSCActive.On = protocol.DscCrossActive;
                    ledLongDSCActive.On = protocol.DscLongActive;
                    if (ledCrossDSCActive.On && ledLongDSCActive.On)
                    {
                        startLong = 0;
                        startCross = 0;
                        minCross = 99999;
                        minLong = 99999;
                        maxCross = 0;
                        maxLong = 0;
                        dirLong = 0;
                        dirCross = 0;
                        lastDir = 0;
                    }
                    else
                    {
                        if (protocol.DscCrossActive)
                        {
                            dirLong = 0;
                            startLong = 0;

                            tmp = protocol.CrossPos - lastCross;
                            if (startCross == 0)
                            {
                                dirCross = Math.Sign(tmp);

                                startCross = protocol.CrossPos;
                            }
                            
                            if (tmp > 0)
                            {
                                 
                                //if (lastDir < 0)
                                //{
                                //    startCross = protocol.CrossPos;
                                //} 
                                //tmp = protocol.CrossPos - startCross;
                                //if (tmp > 100 && dirCross <= 0)
                                //{
                                //    dirCross += 1;
                                //    if (dirCross == 0)
                                //    {
                                //        ledCrossDSCTest.On = true;
                                //    }
                                //}

                                if (protocol.CrossPos > maxCross)
                                {
                                    maxCross = protocol.CrossPos;
                                }

                                lastDir = 1;

                            }
                            else if (tmp < 0)
                            {
                                //if (lastDir > 0)
                                //{
                                //    startCross = protocol.CrossPos;
                                //}
                                //if ((protocol.CrossPos - startCross) < -100 && dirCross >= 0)
                                //{
                                //    dirCross -= 1;
                                //    if (dirCross == 0)
                                //    {
                                //        ledCrossDSCTest.On = true;
                                //    }
                                //}

                                //lastDir = -1;

                                if (protocol.CrossPos < minCross)
                                {
                                    minCross = protocol.CrossPos;
                                }

                                lastDir = -1;
                            }

                            lastCross = protocol.CrossPos;

                            if (minCross < 10 && maxCross > 400)
                            {
                                ledCrossDSCTest.On = true;
                            }
                            //if (dirCross == -1)
                            //{

                            //    if ((startCross - minCross) > 100 && (maxCross - minCross) > 100)
                            //    {
                            //        ledCrossDSCTest.On = true;
                            //    }
                            //}
                            //else if (dirCross == 1)
                            //{
                            //    if ((maxCross - minCross) > 100 && (maxCross - startCross) > 100)
                            //    {
                            //        ledCrossDSCTest.On = true;
                            //    }
                            //}

                        }
                        else if (protocol.DscLongActive)
                        {
                            //if (startLong == 0)
                            //{
                            //    startLong = protocol.LongPos;
                            //}
                            //tmp = protocol.LongPos - startLong;
                            //if (tmp > 0)
                            //{
                            //    if (tmp > 200 && dirLong <= 0)
                            //    {
                            //        dirLong += 1;
                            //        if (dirLong == 0)
                            //        {
                            //            ledLongDSCTest.On = true;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (dirLong > 0)
                            //        {
                            //            startLong = protocol.LongPos;
                            //        }
                            //    }
                            //}
                            //else if (tmp < 0)
                            //{
                            //    if (tmp < -200 && dirLong >= 0)
                            //    {
                            //        dirLong -= 1;
                            //        if (dirLong == 0)
                            //        {
                            //            ledLongDSCTest.On = true;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (dirLong > 0)
                            //        {
                            //            startLong = protocol.LongPos;
                            //        }
                            //    }
                            //}
                            //lastLong = protocol.LongPos;

                            dirLong = 0;
                            startLong = 0;

                            tmp = protocol.LongPos - lastLong;
                            if (startLong == 0)
                            {
                                dirLong = Math.Sign(tmp);

                                startLong = protocol.LongPos;
                            }

                            if (tmp > 0)
                            {


                                if (protocol.LongPos > maxLong)
                                {
                                    maxLong = protocol.LongPos;
                                }

                                lastDir = 1;

                            }
                            else if (tmp < 0)
                            {

                                if (protocol.LongPos < minLong)
                                {
                                    minLong = protocol.LongPos;
                                }

                                lastDir = -1;
                            }

                            lastLong = protocol.LongPos;

                            if (minLong < 10 && maxLong > 400)
                            {
                                ledLongDSCTest.On = true;
                            }

                        }
                    }

                    break;

                case ProtocolChange.Meter:
                    if (protocol.Meter > 1000)
                    {
                        ledMetro.On = true;
                    }
                    changeMeterValue(protocol.Meter / 10);
                    break;
                    
                case ProtocolChange.Info:
                    changeFirmware(protocol.FwCollimator);
                    changeBootloader(protocol.FwBootloader);
                    changePCB(protocol.PcbCollimator);
                    break;

                case ProtocolChange.Light:
                    ledLuce.On = protocol.Light;
                    testLuce++;
                    if (protocol.Light)
                    {
                        changeButtonLight("Spegni");
                    }
                    else
                    {
                        changeButtonLight("Accendi");
                    }
                    if (testLuce >= 2)
                    {
                        ledLuceTest.On = true;
                    }   
                    break;            
        
                case ProtocolChange.Counter:
                    changeCicli(protocol.CounterIteration);
                    break;

                case ProtocolChange.DBTError:
                    MessageBox.Show("Problemi nella configurazione del collimatore: " + this._numPanel);
                    break;

                case ProtocolChange.ConfigurationComplete:
                    protocol.sendVersionRequests();
                    break;

                default:
                    break;
            }
        }

        private void changeMeterValue(int value)
        {
            if (this.lblMetro.InvokeRequired)
            {
                this.lblMetro.BeginInvoke((MethodInvoker)delegate() { this.lblMetro.Text = value.ToString(); ;});    
            }
            else
            {
                this.lblMetro.Text = value.ToString(); ;
            }
        }

        private void changeChkEnableValue(bool value)
        {
            if (this.chkEnable.InvokeRequired)
            {
                this.chkEnable.BeginInvoke((MethodInvoker)delegate () { this.chkEnable.Checked = value ; });
            }
            else
            {
                this.chkEnable.Checked = value; 
            }
        }

        private void changeCicli(int value)
        {
            this.numCicli = value;
            //if (this.lblCicli.InvokeRequired)
            //{
            //    this.lblCicli.BeginInvoke((MethodInvoker)delegate() { this.lblCicli.Text = value.ToString(); ;});
            //    
            //}
            //else
            //{
            //    this.lblCicli.Text = value.ToString();
            //    
            //}
        }

        public int getCicli()
        {
            return this.numCicli;
        }

        private void changeFirmware(string value)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate () {
                    this.lblFirmware.Text = value;
                    if (!lblFirmware.Text.Equals(""))
                    {
                        if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblFirmware.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblFirmware.Text = value;
                if (!lblFirmware.Text.Equals(""))
                {
                    if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblFirmware.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeBootloader(string value)
        {
            if (this.lblBootloader.InvokeRequired)
            {
                this.lblBootloader.BeginInvoke((MethodInvoker)delegate () {
                    this.lblBootloader.Text = value;
                    if (!lblBootloader.Text.Equals(""))
                    {
                        if (!lblBootloader.Text.Equals(cfg.BLAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblBootloader.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblBootloader.Text = value;
                if (!lblBootloader.Text.Equals(""))
                {
                    if (!lblBootloader.Text.Equals(cfg.BLAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblBootloader.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changePCB(string value)
        {
            if (this.lblPCB.InvokeRequired)
            {
                this.lblPCB.BeginInvoke((MethodInvoker)delegate () {
                    this.lblPCB.Text = value;
                    if (!lblPCB.Text.Equals(""))
                    {
                        if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblPCB.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblPCB.Text = value;
                if (!lblPCB.Text.Equals(""))
                {
                    if (!lblPCB.Text.Equals(cfg.PCBAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblPCB.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeButtonLight(string value)
        {
            if (this.btnLuce.InvokeRequired)
            {
                this.btnLuce.BeginInvoke((MethodInvoker)delegate() { this.btnLuce.Text = value; });
            }
            else
            {
                this.btnLuce.Text = value;
            }
        }

        //private void changeFirmware(string value)
        //{
        //    if (this.lblFirmware.InvokeRequired)
        //    {
        //        this.lblFirmware.BeginInvoke((MethodInvoker)delegate() { this.lblFirmware.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblFirmware.Text = value;
        //    }
        //}

        //private void changeBootloader(string value)
        //{
        //    if (this.lblBootloader.InvokeRequired)
        //    {
        //        this.lblBootloader.BeginInvoke((MethodInvoker)delegate() { this.lblBootloader.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblBootloader.Text = value;
        //    }
        //}


        //private void changePCB(string value)
        //{
        //    if (this.lblPCB.InvokeRequired)
        //    {
        //        this.lblPCB.BeginInvoke((MethodInvoker)delegate() { this.lblPCB.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblPCB.Text = value;
        //    }
        //}

        private void changeCross(int value)
        {
            if (this.lblCross.InvokeRequired)
            {
                this.lblCross.BeginInvoke((MethodInvoker)delegate() { this.lblCross.Text = value.ToString(); });
            }
            else
            {
                this.lblCross.Text = value.ToString();
            }
        }

        private void changeLong(int value)
        {
            if (this.lblLong.InvokeRequired)
            {
                this.lblLong.BeginInvoke((MethodInvoker)delegate() { this.lblLong.Text = value.ToString(); });
            }
            else
            {
                this.lblLong.Text = value.ToString();
            }
        }

        private void changeFilter(string value)
        {
            if (this.lblFilter.InvokeRequired)
            {
                this.lblFilter.BeginInvoke((MethodInvoker)delegate() { this.lblFilter.Text = value; });
            }
            else
            {
                this.lblFilter.Text = value;
            }
        }

        private void P225Panel_Load(object sender, EventArgs e)
        {
            initValues();
        }

        private void initValues()
        {
            ledMetro.On = false;
            ledCrossDSCActive.On = false;
            ledCrossDSCTest.On = false;
            ledLongDSCActive.On = false;
            ledLongDSCTest.On = false;
            ledError.On = false;
            ledLuce.On = false;
            ledLuceTest.On = false;
            ledCenter.On = false;
            ledSID.On = false;

            lblCollimatore.Text = collimatorName;
            lblSN.Text = SN;
            lblFC.Text = FC;
            lblCNC.Text = CNC;



            btnLuce.Text = "Accendi";

            lblMetro.Text = "0";
            lblFirmware.Text = "";
            lblBootloader.Text = "";
            lblPCB.Text = "";
            //lblCicli.Text = "";

            lblCross.Text = "";
            lblLong.Text = "";
            lblFilter.Text = "";

            if(protocol != null)
                protocol.initCollimator();
//                protocol.sendVersionRequests();

            chkEnable.Checked = true;

            startLong = 0;
            startCross = 0;
            dirLong = 0;
            dirCross = 0;
            minCross = 99999;
            minLong = 99999;
            maxCross = 0;
            maxLong = 0;
            dirLong = 0;
            dirCross = 0;
            //if (collimatorName.Equals("P 225 ACS DHHS"))
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\P225\scripts.txt";
            //else
            //    pathScriptFile = Environment.CurrentDirectory + @"\script\P225_078A\scripts.txt";
        }

        public bool isChecked()
        {
            return chkEnable.Checked;
        }

        public bool hasErrors()
        {
            return ledError.On;
        }

        private void btnErrori_Click(object sender, EventArgs e)
        {
            FormP225Errors form = new FormP225Errors();

            form.setErrors(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        private void btnLuce_Click(object sender, EventArgs e)
        {
            protocol.sendLightCommand(!protocol.Light);
        }

        private void btnCenterOn_Click(object sender, EventArgs e)
        {
            testLaserCenter++;
            if (btnCenter.Text.Equals("CENTER On"))
            {
                protocol.sendLaserCommand(1, true);
                btnCenter.Text = "CENTER Off";
            }
            else if (btnCenter.Text.Equals("CENTER Off"))
            {
                protocol.sendLaserCommand(1, false);
                btnCenter.Text = "CENTER On";
            }
            if (testLaserCenter >= 2)
                ledCenter.On = true;
        }

        private void btnSidOn_Click(object sender, EventArgs e)
        {
            testLaserSID++;
            if (btnSid.Text.Equals("SID On"))
            {
                protocol.sendLaserCommand(2, true);
                btnSid.Text = "SID Off";
            }
            else if (btnSid.Text.Equals("SID Off"))
            {
                protocol.sendLaserCommand(2, false);
                btnSid.Text = "SID On";
            }
            if (testLaserSID >= 2)
                ledSID.On = true;
        }

        private void cmb_tipoTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tipoTest = cmb_tipoTest.SelectedItem.ToString();
        }

        public void saveCalibTable()
        {

        }

        public void saveReport(Utente user)
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string testPass = "PASS";
                string errorDescription = "";
                var doc = DocX.Load(@"\\ralcosrv2\Public\Templates_Autotest\\TemplateFinalReport_" + templateReportName + ".docx");
                #region Test Report
                if (!lblFirmware.Text.Equals(-1) && !lblBootloader.Equals(-1) && !lblPCB.Equals(-1))
                    doc.ReplaceText("@@GENTEST@@", "PASS");
                else
                {
                    doc.ReplaceText("@@GENTEST@@", "FAIL");
                    testPass = "FAIL";
                }
                if (grpMetro.Enabled.Equals(true))
                {
                    if (ledMetro.On.Equals(true))
                    {
                        doc.ReplaceText("@@RLRTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@RLRTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpLuce.Enabled.Equals(true))
                {
                    if (ledLuceTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@LIGTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIGTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpDSC.Enabled.Equals(true))
                {
                    if (ledCrossDSCTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@KNOTEST1@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@KNOTEST1@@", "FAIL");
                        testPass = "FAIL";
                    }
                    if (ledLongDSCTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@KNOTEST2@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@KNOTEST2@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpLaser.Enabled.Equals(true))
                {
                    if (ledCenter.On.Equals(true) && ledSID.On.Equals(true))
                    {
                        doc.ReplaceText("@@LASTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@LASTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpVita.Enabled.Equals(true))
                {
                    if (ledError.On.Equals(false))
                    {
                        doc.ReplaceText("@@LIFTEST@@", "PASS");
                        doc.ReplaceText("@@LIFNOTES@@", "");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIFTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";
                        if (protocol.CrossJam)
                            errorDescription += " Motor Jammed Cross Error";
                        if (protocol.LongJam)
                            errorDescription += " Motor Jammed Long Error";
                        if (protocol.FilterJam)
                            errorDescription += " Motor Jammed Spectral Filter Error";
                        if (protocol.CrossCalib)
                            errorDescription += " Motor Calibration Cross Error";
                        if (protocol.LongCalib)
                            errorDescription += " Motor Calibration Long Error";
                        if (protocol.FilterCalib)
                            errorDescription += " Motor Calibration Spectral Filter Error";
                        if (protocol.CrossPot)
                            errorDescription += " Potentiometer / Encoders Cross Error";
                        if (protocol.LongPot)
                            errorDescription += " Potentiometer / Encoders Long Error";
                        if (protocol.generalError)
                            errorDescription += " General Error";
                        if (protocol.AEPError)
                            errorDescription += " AEP Error";
                        if (protocol.communicationError)
                            errorDescription += " Communication Error";
                        errorDescription += " Steps Error Cross = " + protocol.passiCross.ToString()
                                          + " Steps Error Long = " + protocol.passiLong.ToString()
                                          + " Potentiometer Error Cross = " + protocol.deltaPotenziometriCross.ToString()
                                          + " Potentiometer Error Long = " + protocol.deltaPotenziometriLong.ToString();
                        doc.ReplaceText("@@LIFNOTES@@", errorDescription);
                    }
                }
                #endregion
                #region General Information
                doc.ReplaceText("@@MODEL@@", lblCollimatore.Text);
                doc.ReplaceText("@@SERNR@@", lblSN.Text);
                doc.ReplaceText("@@FWVER@@", lblFirmware.Text);
                doc.ReplaceText("@@BLVER@@", lblBootloader.Text);
                doc.ReplaceText("@@PCBVER@@", lblPCB.Text);
                doc.ReplaceText("@@FLOW@@", lblFC.Text);
                doc.ReplaceText("@@CNC@@", lblCNC.Text);
                doc.ReplaceText("@@DATE@@", DateTime.Now.ToString());
                doc.ReplaceText("@@OUTCOME@@", testPass);
                #endregion
                #region Operator
                doc.ReplaceText("@@OPERATOR@@", user.nome + " " + user.cognome);
                #endregion
                string filePathFirma = Utility.searchPathDigitalSignatureOperator(user);

                Xceed.Words.NET.Image img;
                if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                {
                    //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                    img = doc.AddImage(filePathFirma);
                    Xceed.Words.NET.Picture picture = img.CreatePicture();
                    Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                    p1.AppendPicture(picture);
                    p1.Alignment = Alignment.right;
                }

                //salvo in locale il report
                //infoFC[0] = numero FC; infoFC[1] = anno FC;
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                //Console.WriteLine(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                if (infoCNC[0].Equals("    "))
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                else
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                //salvo il report sul server solo se il tipo di test è diverso da Test
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                    }

                    else
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                    }
                    SQLConnector conn = new SQLConnector();
                    conn.CreateCommand("INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[FW_Version],[BL_Version],[PCB_Version],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + collimatorName + "','" + SN + "','" + ProtocolAnalyzer().FwCollimator + "','" + ProtocolAnalyzer().FwBootloader + "','" + ProtocolAnalyzer().PcbCollimator + "','" + FC + "','" + CNC + "','" + Convert.ToString(DateTime.Now.Year) + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errorDescription + "','" + tipoTest + "','" + user.nome + " " + user.cognome + "','" + user.location + "')", null);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
