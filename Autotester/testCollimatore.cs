﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    public class testCollimatore
    {
        public string SN { get; set; }
        public string FC { get; set; }
        public string CNC { get; set; }
        public CANusb_Connector connettore { get; set; }
        public ConfigurazioneTest configTest { get; set; }
    }
}
