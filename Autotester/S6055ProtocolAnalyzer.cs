﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Autotester
{
    public class S605ProtocolAnalyzer : IProtocolDataChanges<ProtocolChange>, IProtocolAnalyzer
    {
        private CANusb_Connector connector = null;
        public string protocolName { get { return "CAN-OPEN"; } }

        // attributi collimatore
        private bool requestForCalTable = false;
        List<CalibTable> listCalTable = new List<CalibTable>();

        private string fwCollimator;
        private string fwBootloader;
        private string pcbCollimator;
        private string serialNumber;

        private bool postError = false;

        private int spatialFilter1Pos;
        private int spatialFilter1Rot;
        private int irisPos;
        private int resetCount = 0;

        private bool irisJam = false;
        private bool spFiltJam = false;

        public bool error0 { get; set; }
        public bool error90 { get; set; }
        public bool error180 { get; set; }
        public bool error270 { get; set; }

        public bool IrisJam { get { return irisJam; } }

        public bool SpFiltJam { get { return spFiltJam; } }

        public int filterWhenError { get; set; }
        public int crossPosWhenError { get; set; }
        public int longPosWhenError { get; set; }

        public int passiLong { get; set; }
        public int deltaPotenziometriLong { get; set; }
        public int passiCross { get; set; }
        public int deltaPotenziometriCross { get; set; }
        public int passiIride { get; set; }
        public int passiSpatialFilter { get; set; }
        public int tentativiPosFiltro { get; set; }

        public bool TastiMembrActive { get; set; }
        public bool TestTastiMemb { get; set; }

        private int _counterIteration;

        public int CounterIteration
        {
            get { return _counterIteration; }
            set
            {
                if (this._counterIteration != value)
                {
                    this._counterIteration = value;
                    emitChanges(ProtocolChange.Counter);
                }

            }
        }

        public bool CollimationDone
        {
            get;
        }

        public bool CollimationFailed
        {
            get;
        }

        public bool DscCrossActive
        {
            get;
        }

        public bool DscLongActive
        {
            get;
        }

        public bool CrossJam
        {
            get;
        }

        public bool LongJam
        {
            get;
        }

        public bool CrossCalib
        {
            get;
        }

        public bool LongCalib
        {
            get;
        }

        public bool FilterCalib
        {
            get;
        }

        public bool CrossPot
        {
            get;
        }

        public bool LongPot
        {
            get;
        }

        public bool Light
        {
            get;
        }

        public bool Key
        {
            get;
        }

        public bool TastoFiltro
        {
            get;
        }

        public bool TastoLuce
        {
            get;
        }

        public int Meter
        {
            get;
        }

        public int MaxSpeed
        {
            get;
        }

        public int Filter
        {
            get;
        }

        public int CrossPos
        {
            get;
        }

        public int LongPos
        {
            get;
        }

        public int Temperature
        {
            get;
        }

        public int CrossEncoder
        {
            get;
        }

        public int LongEncoder
        {
            get;
        }

        public string FwCollimator
        {
            get { return fwCollimator; }
        }

        public string FwBootloader
        {
            get { return fwBootloader; }
        }

        public string PcbCollimator
        {
            get { return pcbCollimator; }
        }

        public string SerialNumber
        {
            get { return serialNumber; }
        }

        public bool HeartbeatStatus
        {
            get;
        }

        public bool HeartbeatError
        {
            get;
        }

        // da modificare

        public bool generalError
        {
            get;
        }

        public bool communicationError
        {
            get;
        }

        public bool AEPError
        {
            get;
        }

       //implementati appositamente per protocollo CANOPEN (S605 DASM DHHS)
        public bool POSTerror
        {
            get { return postError; }
        }

        //spostamento filtro spaziale 1
        public int SpatialFilter1_Pos
        {
            get { return spatialFilter1Pos; }
        }

        //rotazione filtro spaziale 1
        public int SpatialFilter1_Rot
        {
            get { return spatialFilter1Rot; }
        }

        //spostamento iride
        public int IrisPos
        {
            get { return irisPos; }
        }

        public CANusb_Connector Connector
        {
            get { return connector; }
            set
            {
                if (value == null)
                {
                    if (this.connector != null)
                    {
                        connector.Handler -= ConnectorOnHandler();
                    }
                    connector = null;

                }
                else
                {
                    connector = value;
                    connector.Handler += ConnectorOnHandler();                    
                }
            }
        }

        public testCollimatore testColl { get; set; }


        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate(CANCommand command) { this.processMessage(command); };
        }

        public List<CalibTable> CalibTable { get { return listCalTable; } }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<ProtocolChange> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<ProtocolChange> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<ProtocolChange> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<ProtocolChange>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<ProtocolChange> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<ProtocolChange> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */
        
        public void initCollimator()
        {
            /*Configurazioni necessarie per l'attivazione del collimatore*/
            //NMT Start
            CANCommand msg = new CANCommand(0x000, 0, 2, 0x01, 0x40, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Abilitazione lamelle
            msg = new CANCommand(0x640, 0, 8, 0x2F, 0x50, 0x60, 0x01, 0x11, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Abilitazione iride
            msg = new CANCommand(0x640, 0, 8, 0x2F, 0x30, 0x60, 0x01, 0x11, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Abilitazione test passi
            msg = new CANCommand(0x640, 0, 8, 0x23, 0x00, 0x28, 0x01, 0x37, 0x78, 0x78, 0x83);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x640, 0, 8, 0x23, 0x00, 0x28, 0x01, 0x78, 0x83, 0x37, 0x78);
            connector.enqueueMessage(msg);
            emitChanges(ProtocolChange.ConfigurationComplete);
            readCalibTable();
            sendVersionRequests();
        }

        public void sendVersionRequests()
        {
            //Richiesta Versione Firmware
            CANCommand msg = new CANCommand(0x640, 0, 8, 0x40, 0x0A, 0x10, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x640, 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            //Richiesta Versione Bootloader
            msg = new CANCommand(0x640, 0, 8, 0x40, 0x09, 0x22, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
            msg = new CANCommand(0x640, 0, 8, 0x60, 0, 0, 0, 0, 0, 0, 0);
            connector.enqueueMessage(msg);
        }

        public void readCalibTable()
        {
            if (!requestForCalTable)
            {
                requestForCalTable = true;
                for (byte motore = 1; motore <= testColl.configTest.numMotori; motore++)
                {
                    listCalTable.Add(new CalibTable());
                    //Richiesta numero punti di taratura motore
                    CANCommand msg = new CANCommand(0x640, 0, 8, 0x40, 0x50, 0x28, motore, 0, 0, 0, 0);
                    connector.enqueueMessage(msg);
                }
            }
        }

        public void setCollimatorForShipment()
        {

        }

        public void playSoundEndTest()
        {

        }

        private void processMessage(CANCommand msg)
        {
            processReadMsg(msg);
        }

        //valorizza le variabili di istanza del protocollo che vengono poi analizzate dalla S605Panel
        private void processReadMsg(CANCommand msg)
        {
            if (msg.getID().Equals(0xC0))
            {
                //POST error
                if (msg.getDatum(0) == 0x70 && msg.getDatum(1) == 0xF0 && msg.getDatum(2) == 0x00 && msg.getDatum(3) == 0x02)
                {
                    postError = true;
                    emitChanges(ProtocolChange.Errors);
                }
            }
            //Movimentazione Iride
            else if (msg.getID() == 0x1C1)
            {
                irisPos = msg.getDatum(2) << 8 | msg.getDatum(1);
                emitChanges(ProtocolChange.Iris);
            }
            //Movimentazione Lamelle
            else if (msg.getID() == 0x3C1)
            {
                spatialFilter1Pos = msg.getDatum(2) << 8 | msg.getDatum(1);
                emitChanges(ProtocolChange.SpatialFilter1Pos);
                spatialFilter1Rot = msg.getDatum(4) << 8 | msg.getDatum(3);
                emitChanges(ProtocolChange.SpatialFilter1Rot);
            }
            else if (msg.getID() == 0x5C0)
            {
                switch (msg.getDatum(0))
                {
                    case 0x00:
                        {
                            fwCollimator = "" + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                            emitChanges(ProtocolChange.Info);
                        }
                        break;
                    case (0x43)://4 byte
                        {
                            //Aperture
                            if (msg.getDatum(2) == 0x28)
                            {
                                listCalTable[msg.getDatum(1) - 1].Passi.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                //Console.WriteLine("Passi motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                            }

                            //Passi
                            if (msg.getDatum(2) == 0x29)
                            {
                                listCalTable[msg.getDatum(1) - 1].Aperture.Add(msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24));
                                //Console.WriteLine("Apertura motore" + msg.getDatum(1) + " = " + (msg.getDatum(4) + (msg.getDatum(5) << 8) + (msg.getDatum(6) << 16) + (msg.getDatum(7) << 24)));
                            }
                        }
                        break;
                    case (0x4F)://1 byte
                        {
                            //numero punti di taratura
                            if (msg.getDatum(2) == 0x28 && msg.getDatum(1) == 0x50)
                            {
                                listCalTable[msg.getDatum(3) - 1].PuntiTaratura = msg.getDatum(4);
                                for (byte i = 1; i <= msg.getDatum(4); i++)
                                {
                                    //Richiesta aperture motore per il punto di taratura
                                    CANCommand mex = new CANCommand(0x640, 0, 8, 0x40, msg.getDatum(3), 0x28, i, 0, 0, 0, 0);
                                    connector.enqueueMessage(mex);
                                    //Richiesta passi motore per il punto di taratura
                                    mex = new CANCommand(0x640, 0, 8, 0x40, msg.getDatum(3), 0x29, i, 0, 0, 0, 0);
                                    connector.enqueueMessage(mex);
                                }
                            }
                        }
                        break;
                }
                //if (msg.getDatum(0) == 0x00)
                //{
                //    fwCollimator = "" + Convert.ToChar(msg.getDatum(1)) + Convert.ToChar(msg.getDatum(2)) + Convert.ToChar(msg.getDatum(3)) + Convert.ToChar(msg.getDatum(4)) + Convert.ToChar(msg.getDatum(5)) + Convert.ToChar(msg.getDatum(6)) + Convert.ToChar(msg.getDatum(7));
                //    emitChanges(ProtocolChange.Info);
                //}
            }
            else if (msg.getID() == 0x740)
            {
                if (msg.getDLC() == 1 && msg.getDatum(0) == 0x00)
                {
                    resetCount++;
                    if (resetCount == 8)//8 reset = test di reset completato
                        emitChanges(ProtocolChange.TestReset);
                }
            }
            else if (msg.getID() == 0x7F4)
            {
                switch (msg.getDatum(0))
                {
                    //EV_PASSI_PHOTO
                    case 0x0E:
                        switch (msg.getDatum(1))
                        {
                            case 0x01://IRIDE
                                passiIride = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                emitChanges(ProtocolChange.StepsIris);
                                if (passiIride.Equals(-1) || (Math.Abs(50 - passiIride) > 50))
                                {
                                        irisJam = true;
                                }
                                break;
                            case 0x03://LAMELLE
                                passiSpatialFilter = Convert.ToInt32((msg.getDatum(4) << 24) | (msg.getDatum(5) << 16) | (msg.getDatum(6) << 8) | msg.getDatum(7));
                                emitChanges(ProtocolChange.StepsSpatialFilter1);
                                if (passiSpatialFilter.Equals(-1) || (Math.Abs(200 - passiSpatialFilter) > 200))
                                {
                                        spFiltJam = true;
                                }
                                break;
                        }
                        if (irisJam || spFiltJam)
                            emitChanges(ProtocolChange.Errors);
                        break;
                }
            }
        }

        private void emitChanges(ProtocolChange value)
        {
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
            {
                LogManager.GetLogger(connector.Logger.Name).Warn(connector.Logger.Name + " processor messages not available ");
            }
        }
    }
}
