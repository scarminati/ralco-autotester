﻿namespace Autotester
{
    partial class FormR225DettagliTastiMembrana
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ledBtnBarellaIn = new Autotester.LedBulb();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ledBtnBarellaOut = new Autotester.LedBulb();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ledBtnScanSx = new Autotester.LedBulb();
            this.ledBtnScanDx = new Autotester.LedBulb();
            this.ledBtnPendAntior = new Autotester.LedBulb();
            this.ledBtnPendOr = new Autotester.LedBulb();
            this.SuspendLayout();
            // 
            // ledBtnBarellaIn
            // 
            this.ledBtnBarellaIn.Location = new System.Drawing.Point(157, 40);
            this.ledBtnBarellaIn.Name = "ledBtnBarellaIn";
            this.ledBtnBarellaIn.On = true;
            this.ledBtnBarellaIn.Size = new System.Drawing.Size(25, 23);
            this.ledBtnBarellaIn.TabIndex = 27;
            this.ledBtnBarellaIn.Text = "ledBulb4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Barella IN";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Barella OUT";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(154, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Test";
            // 
            // ledBtnBarellaOut
            // 
            this.ledBtnBarellaOut.Location = new System.Drawing.Point(157, 75);
            this.ledBtnBarellaOut.Name = "ledBtnBarellaOut";
            this.ledBtnBarellaOut.On = true;
            this.ledBtnBarellaOut.Size = new System.Drawing.Size(25, 23);
            this.ledBtnBarellaOut.TabIndex = 28;
            this.ledBtnBarellaOut.Text = "ledBulb3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Scanning Sinistro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Scanning Destro";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Pendolazione Antioraria";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Pendolazione Oraria";
            // 
            // ledBtnScanSx
            // 
            this.ledBtnScanSx.Location = new System.Drawing.Point(157, 110);
            this.ledBtnScanSx.Name = "ledBtnScanSx";
            this.ledBtnScanSx.On = true;
            this.ledBtnScanSx.Size = new System.Drawing.Size(25, 23);
            this.ledBtnScanSx.TabIndex = 37;
            this.ledBtnScanSx.Text = "ledBulb4";
            // 
            // ledBtnScanDx
            // 
            this.ledBtnScanDx.Location = new System.Drawing.Point(157, 145);
            this.ledBtnScanDx.Name = "ledBtnScanDx";
            this.ledBtnScanDx.On = true;
            this.ledBtnScanDx.Size = new System.Drawing.Size(25, 23);
            this.ledBtnScanDx.TabIndex = 38;
            this.ledBtnScanDx.Text = "ledBulb3";
            // 
            // ledBtnPendAntior
            // 
            this.ledBtnPendAntior.Location = new System.Drawing.Point(157, 180);
            this.ledBtnPendAntior.Name = "ledBtnPendAntior";
            this.ledBtnPendAntior.On = true;
            this.ledBtnPendAntior.Size = new System.Drawing.Size(25, 23);
            this.ledBtnPendAntior.TabIndex = 39;
            this.ledBtnPendAntior.Text = "ledBulb4";
            // 
            // ledBtnPendOr
            // 
            this.ledBtnPendOr.Location = new System.Drawing.Point(157, 215);
            this.ledBtnPendOr.Name = "ledBtnPendOr";
            this.ledBtnPendOr.On = true;
            this.ledBtnPendOr.Size = new System.Drawing.Size(25, 23);
            this.ledBtnPendOr.TabIndex = 40;
            this.ledBtnPendOr.Text = "ledBulb3";
            // 
            // FormR225DettagliTastiMembrana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 251);
            this.Controls.Add(this.ledBtnPendAntior);
            this.Controls.Add(this.ledBtnPendOr);
            this.Controls.Add(this.ledBtnScanSx);
            this.Controls.Add(this.ledBtnScanDx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ledBtnBarellaIn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ledBtnBarellaOut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormR225DettagliTastiMembrana";
            this.Text = "R225 Tasti Membrana";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LedBulb ledBtnBarellaIn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private LedBulb ledBtnBarellaOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private LedBulb ledBtnScanSx;
        private LedBulb ledBtnScanDx;
        private LedBulb ledBtnPendAntior;
        private LedBulb ledBtnPendOr;
    }
}