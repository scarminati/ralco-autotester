﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Xceed.Words.NET;

namespace Autotester
{
    public partial class S605Panel : UserControl, ICollimatorPanel
    {
        private S605ProtocolAnalyzer protocol;
        private ConfigurazioneTest cfg;
        private FormControls formRuota = null;
        private int startLong;
        private int startCross;
        private int minLong;
        private int minCross;
        private int maxLong;
        private int maxCross;
        private int lastLong;
        private int lastCross;
        private int lastDir;
        private int dirLong;
        private int dirCross;
        private int testLuce = 0;
        private int testLaserCenter = 0;
        private int testLaserSID = 0;
        private int numCicli;

        private int _numPanel;

        public int NumPanel { get { return _numPanel; } set { this._numPanel = value; }  }

        public S605Panel()
        {
            InitializeComponent();
            this.scripts = new List<List<SingoloComando>>();
            //pathScriptFile = Environment.CurrentDirectory + @"\script\S605\scripts.txt";
        }

        //public void setConnector(CANusb_Connector connector, string deviceName)
        //{
        //    S605ProtocolAnalyzer s605Protocol = new S605ProtocolAnalyzer();
        //    s605Protocol.Connector = connector;

        //    s605Protocol.Handler += ConnectorOnHandler();

        //    protocol = s605Protocol;
        //}

        public void setConnector(testCollimatore t)
        {
            S605ProtocolAnalyzer s605Protocol = new S605ProtocolAnalyzer();

            s605Protocol.testColl = t;

            s605Protocol.Connector = t.connettore;

            s605Protocol.Handler += ConnectorOnHandler();

            protocol = s605Protocol;
        }

        public string collimatorName { get; set; }

        public void setFormRuota(FormControls formRuota)
        {
            this.formRuota = formRuota;
        }

        public void enableControl(ConfigurazioneTest cfg)
        {
            this.cfg = cfg;
            if (cfg.testVita.Equals(true))
            {
                grpVita.Enabled = true;
            }
            if (cfg.testReset.Equals(true))
            {
                grpReset.Enabled = true;
            }
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return protocol;
        }

        public bool isManualTestCompleted()
        {
            bool rv = true;
            if (grpReset.Enabled)
                rv = rv & ledTestReset.On;
            return (rv);
        }

        public string getTipoTest()
        {
            if (cmb_tipoTest.SelectedItem == null)
                return "";
            else
                return cmb_tipoTest.SelectedItem.ToString();
        }

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public string pathScriptFile { get; set; }


        public List<List<SingoloComando>> scripts { get; set; }

        public bool testMetro()
        {
            return true;
        }


        private Action<ProtocolChange> ConnectorOnHandler()
        {
            return delegate(ProtocolChange value) { this.CollimatorValuesChanged(value); };
        }

        private void CollimatorValuesChanged(ProtocolChange value)
        {
            int tmp;

            switch (value)
            {
                case ProtocolChange.Iris:
                    changeIride(protocol.IrisPos / 100);
                    break;

                case ProtocolChange.SpatialFilter1Pos:
                    changeSpatFilt1Pos(protocol.SpatialFilter1_Pos / 100);
                    break;

                case ProtocolChange.SpatialFilter1Rot:
                    changeSpatFilt1Rot(protocol.SpatialFilter1_Rot / 10);
                    break;

                case ProtocolChange.TestReset:
                    ledTestReset.On = true;
                    protocol.initCollimator();
                    break;

                case ProtocolChange.Errors:
                    ledError.On = true;
                    
                    if (this.formRuota != null)
                    {
                        switch (this.formRuota.getActualAngleRotation())
                        {
                            case 0:
                                protocol.error0 = true;
                                break;
                            case 90:
                                protocol.error90 = true;
                                break;
                            case 180:
                                protocol.error180 = true;
                                break;
                            case 270:
                                protocol.error270 = true;
                                break;
                        }
                    }
                    changeChkEnableValue(false);
                   // this.chkEnable.Checked = false;
                    break;
                    
                case ProtocolChange.Info:
                    changeFirmware(protocol.FwCollimator);
                    changeBootloader(protocol.FwBootloader);
                    changePCB(protocol.PcbCollimator);
                    break;          
        
                case ProtocolChange.Counter:
                    changeCicli(protocol.CounterIteration);
                    break;

                case ProtocolChange.DBTError:
                    MessageBox.Show("Problemi nella configurazione del collimatore: " + this._numPanel);
                    break;

                case ProtocolChange.ConfigurationComplete:
                    protocol.sendVersionRequests();
                    break;

                default:
                    break;
            }
        }

        private void changeChkEnableValue(bool value)
        {
            if (this.chkEnable.InvokeRequired)
            {
                this.chkEnable.BeginInvoke((MethodInvoker)delegate () { this.chkEnable.Checked = value ; });
            }
            else
            {
                this.chkEnable.Checked = value; 
            }
        }

        private void changeCicli(int value)
        {
            this.numCicli = value;
        }

        public int getCicli()
        {
            return this.numCicli;
        }

        private void changeFirmware(string value)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate () {
                    this.lblFirmware.Text = value;
                    if (!lblFirmware.Text.Equals(""))
                    {
                        if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblFirmware.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblFirmware.Text = value;
                if (!lblFirmware.Text.Equals(""))
                {
                    if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblFirmware.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeBootloader(string value)
        {
            if (this.lblBootloader.InvokeRequired)
            {
                this.lblBootloader.BeginInvoke((MethodInvoker)delegate () {
                    this.lblBootloader.Text = value;
                    if (!lblBootloader.Text.Equals(""))
                    {
                        if (!lblBootloader.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblBootloader.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblBootloader.Text = value;
                if (!lblBootloader.Text.Equals(""))
                {
                    if (!lblBootloader.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblBootloader.BackColor = Color.Red;
                    }
                }
            }
        }


        private void changePCB(string value)
        {
            if (this.lblPCB.InvokeRequired)
            {
                this.lblPCB.BeginInvoke((MethodInvoker)delegate () {
                    this.lblPCB.Text = value;
                    if (!lblPCB.Text.Equals(""))
                    {
                        if (!lblPCB.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblPCB.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblPCB.Text = value;
                if (!lblPCB.Text.Equals(""))
                {
                    if (!lblPCB.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblPCB.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeIride(int value)
        {
            if (this.lblIride.InvokeRequired)
            {
                this.lblIride.BeginInvoke((MethodInvoker)delegate() { this.lblIride.Text = value.ToString(); });
            }
            else
            {
                this.lblIride.Text = value.ToString();
            }
        }

        private void changeSpatFilt1Pos(int value)
        {
            if (this.lblSpatFlt1Pos.InvokeRequired)
            {
                this.lblSpatFlt1Pos.BeginInvoke((MethodInvoker)delegate () { this.lblSpatFlt1Pos.Text = value.ToString(); });
            }
            else
            {
                this.lblSpatFlt1Pos.Text = value.ToString(); ;
            }
        }

        private void changeSpatFilt1Rot(int value)
        {
            if (this.lblSpatFlt1Rot.InvokeRequired)
            {
                this.lblSpatFlt1Rot.BeginInvoke((MethodInvoker)delegate() { this.lblSpatFlt1Rot.Text = value.ToString(); });
            }
            else
            {
                this.lblSpatFlt1Rot.Text = value.ToString(); ;
            }
        }

        private void S605Panel_Load(object sender, EventArgs e)
        {
            initValues();
        }

        private void initValues()
        {
            ledError.On = false;
            ledTestReset.On = false;

            lblCollimatore.Text = collimatorName;
            lblSN.Text = SN;
            lblFC.Text = FC;
            lblCNC.Text = CNC;


            lblFirmware.Text = "";
            lblBootloader.Text = "";
            lblPCB.Text = "";
            //lblCicli.Text = "";

            lblSpatFlt1Pos.Text = "";
            lblIride.Text = "";
            lblSpatFlt1Rot.Text = "";

            if(protocol != null)
                protocol.initCollimator();
//                protocol.sendVersionRequests();

            chkEnable.Checked = true;

            startLong = 0;
            startCross = 0;
            dirLong = 0;
            dirCross = 0;
            minCross = 99999;
            minLong = 99999;
            maxCross = 0;
            maxLong = 0;
            dirLong = 0;
            dirCross = 0;


        }

        public bool isChecked()
        {
            return chkEnable.Checked;
        }

        public bool hasErrors()
        {
            return ledError.On;
        }

        private void btnErrori_Click(object sender, EventArgs e)
        {
            FormS6055Errors form = new FormS6055Errors();

            form.setErrors(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        private void cmb_tipoTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tipoTest = cmb_tipoTest.SelectedItem.ToString();
        }

        public void saveCalibTable()
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                string fileName = "";
                if (infoCNC[0].Equals("    "))
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt";
                else
                    fileName = Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt";
                File.AppendAllText(fileName, DateTime.Now + "\n");
                for (int i = 0; i < ProtocolAnalyzer().CalibTable.Count; i++)
                {
                    File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].PuntiTaratura + "\n");
                    for (int j = 0; j < ProtocolAnalyzer().CalibTable[i].PuntiTaratura; j++)
                        File.AppendAllText(fileName, ProtocolAnalyzer().CalibTable[i].Aperture[j].ToString() + "," + ProtocolAnalyzer().CalibTable[i].Passi[j] + "\n");
                }
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".txt", true);
                    }

                    else
                    {
                        File.Copy(fileName, @"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\CalibTable_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".txt", true);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void saveReport(Utente user)
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string testPass = "PASS";
                string errorDescription = "";
                var doc = DocX.Load(@"\\ralcosrv2\Public\Templates_Autotest\\TemplateFinalReport_" + templateReportName + ".docx");
                #region Test Report
                if (!lblFirmware.Text.Equals(-1))// && !lblBootloader.Equals(-1) && !lblPCB.Equals(-1))
                    doc.ReplaceText("@@GENTEST@@", "PASS");
                else
                {
                    doc.ReplaceText("@@GENTEST@@", "FAIL");
                    testPass = "FAIL";
                }
                if (grpReset.Enabled.Equals(true))
                {
                    errorDescription = "";
                    if (ledTestReset.On.Equals(true))
                    {
                        doc.ReplaceText("@@RESTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@RESTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";
                    }
                    doc.ReplaceText("@@RESNOTES@@", errorDescription);
                    errorDescription = "";
                }
                if (grpVita.Enabled.Equals(true))
                {
                    if (ledError.On.Equals(false))
                    {
                        doc.ReplaceText("@@LIFTEST@@", "PASS");
                        doc.ReplaceText("@@LIFNOTES@@", "");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIFTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";
                        if (protocol.POSTerror)
                            errorDescription += " POST Error";
                        errorDescription += " Steps Error Spatial Filter = " + protocol.passiSpatialFilter.ToString()
                                          + " Steps Error Iris = " + protocol.passiIride.ToString();
                        doc.ReplaceText("@@LIFNOTES@@", errorDescription);
                    }
                }
                #endregion
                #region General Information
                doc.ReplaceText("@@MODEL@@", lblCollimatore.Text);
                doc.ReplaceText("@@SERNR@@", lblSN.Text);
                doc.ReplaceText("@@FWVER@@", lblFirmware.Text);
                doc.ReplaceText("@@BLVER@@", lblBootloader.Text);
                doc.ReplaceText("@@PCBVER@@", lblPCB.Text);
                doc.ReplaceText("@@FLOW@@", lblFC.Text);
                doc.ReplaceText("@@CNC@@", lblCNC.Text);
                doc.ReplaceText("@@DATE@@", DateTime.Now.ToString());
                doc.ReplaceText("@@OUTCOME@@", testPass);
                #endregion
                #region Operator
                doc.ReplaceText("@@OPERATOR@@", user.nome + " " + user.cognome);
                #endregion
                string filePathFirma = Utility.searchPathDigitalSignatureOperator(user);

                Xceed.Words.NET.Image img;
                if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                {
                    //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                    img = doc.AddImage(filePathFirma);
                    Xceed.Words.NET.Picture picture = img.CreatePicture();
                    Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                    p1.AppendPicture(picture);
                    p1.Alignment = Alignment.right;
                }

                //salvo in locale il report
                //infoFC[0] = numero FC; infoFC[1] = anno FC;
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                //Console.WriteLine(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                if (infoCNC[0].Equals("    "))
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                else
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                //salvo il report sul server solo se il tipo di test è diverso da Test
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                    }

                    else
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                    }
                    SQLConnector conn = new SQLConnector();
                    conn.CreateCommand("INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[FW_Version],[BL_Version],[PCB_Version],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + collimatorName + "','" + SN + "','" + ProtocolAnalyzer().FwCollimator + "','" + ProtocolAnalyzer().FwBootloader + "','" + ProtocolAnalyzer().PcbCollimator + "','" + FC + "','" + CNC + "','" + Convert.ToString(DateTime.Now.Year) + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errorDescription + "','" + tipoTest + "','" + user.nome + " " + user.cognome + "','" + user.location + "')", null);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
