﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotester
{
    public class ConfigurazioneTest
    {
        public int ID_tipoCollim { get; set; }
        public string nomeCollim { get; set; }
        public string baudRate { get; set; }
        public int baseID_Send { get; set; }
        public int offsetID_Send { get; set; }
        public int baseID_Recv { get; set; }
        public int offsetID_Recv { get; set; }
        public string nomePanel { get; set; }
        public string protocolName { get; set; }
        public int numMotori { get; set; }
        public int numLasers { get; set; }
        public string FWAtteso { get; set; }
        public string BLAtteso { get; set; }
        public string PCBAtteso { get; set; }
        public string motore1 { get; set; }
        public string motore2 { get; set; }
        public string motore3 { get; set; }
        public string motore4 { get; set; }
        public string motore5 { get; set; }
        public string motore6 { get; set; }
        public string motore7 { get; set; }
        public string motore8 { get; set; }
        public string motore9 { get; set; }
        public string motore10 { get; set; }
        public string motore11 { get; set; }
        public string motore12 { get; set; }
        public bool testMetro { get; set; }
        public bool testLuce { get; set; }
        public bool testDSC { get; set; }
        public bool testLaser { get; set; }
        public bool testBtnFiltro { get; set; }
        public bool testBtnLuce { get; set; }
        public bool testBtnIride { get; set; }
        public bool testBtnMembrana { get; set; }
        public bool testChiave { get; set; }
        public bool testVita { get; set; }
        public bool testReset { get; set; }
        public bool testInclin { get; set; }
    }
}
