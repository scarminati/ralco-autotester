﻿using System;

namespace Autotester
{
    public class SingoloComando
    {
        public uint id;
        public byte dlc;
        public byte[] dati;

        public int pause;
        public bool increment;
        public bool decrement;

        public SingoloComando()
        {
            id = 0;
            dlc = 8;
            dati = new byte[8];

            for(int i=0; i<8; i++) 
            {
                dati[i]=0;
            }

            pause = 5;
            increment = false;
        }

    }
}
