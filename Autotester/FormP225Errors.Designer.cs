﻿namespace Autotester
{
    partial class FormP225Errors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ledCrossJam = new Autotester.LedBulb();
            this.ledLongJam = new Autotester.LedBulb();
            this.ledFilterJam = new Autotester.LedBulb();
            this.ledFilterCalib = new Autotester.LedBulb();
            this.ledLongCalib = new Autotester.LedBulb();
            this.ledCrossCalib = new Autotester.LedBulb();
            this.ledLongPot = new Autotester.LedBulb();
            this.ledCrossPot = new Autotester.LedBulb();
            this.ledErr270 = new Autotester.LedBulb();
            this.ledErr180 = new Autotester.LedBulb();
            this.ledErr90 = new Autotester.LedBulb();
            this.ledErr0 = new Autotester.LedBulb();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ledComunicazione = new Autotester.LedBulb();
            this.ledAEP = new Autotester.LedBulb();
            this.ledGeneral = new Autotester.LedBulb();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbl_deltaPassiCross = new System.Windows.Forms.Label();
            this.lbl_deltaPassiLong = new System.Windows.Forms.Label();
            this.lbl_deltaPotLong = new System.Windows.Forms.Label();
            this.lbl_deltaPotCross = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbl_deltaEncLong = new System.Windows.Forms.Label();
            this.lbl_deltaEncCross = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(306, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Filtro";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Errore potenziomentro";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Motor calibration";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Motor jammed";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(229, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Long";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Cross";
            // 
            // ledCrossJam
            // 
            this.ledCrossJam.Color = System.Drawing.Color.Red;
            this.ledCrossJam.Location = new System.Drawing.Point(151, 47);
            this.ledCrossJam.Name = "ledCrossJam";
            this.ledCrossJam.On = true;
            this.ledCrossJam.Size = new System.Drawing.Size(23, 23);
            this.ledCrossJam.TabIndex = 37;
            this.ledCrossJam.Text = "ledBulb1";
            // 
            // ledLongJam
            // 
            this.ledLongJam.Color = System.Drawing.Color.Red;
            this.ledLongJam.Location = new System.Drawing.Point(232, 47);
            this.ledLongJam.Name = "ledLongJam";
            this.ledLongJam.On = true;
            this.ledLongJam.Size = new System.Drawing.Size(23, 23);
            this.ledLongJam.TabIndex = 38;
            this.ledLongJam.Text = "ledBulb2";
            // 
            // ledFilterJam
            // 
            this.ledFilterJam.Color = System.Drawing.Color.Red;
            this.ledFilterJam.Location = new System.Drawing.Point(309, 47);
            this.ledFilterJam.Name = "ledFilterJam";
            this.ledFilterJam.On = true;
            this.ledFilterJam.Size = new System.Drawing.Size(23, 23);
            this.ledFilterJam.TabIndex = 39;
            this.ledFilterJam.Text = "ledBulb3";
            // 
            // ledFilterCalib
            // 
            this.ledFilterCalib.Color = System.Drawing.Color.Red;
            this.ledFilterCalib.Location = new System.Drawing.Point(309, 76);
            this.ledFilterCalib.Name = "ledFilterCalib";
            this.ledFilterCalib.On = true;
            this.ledFilterCalib.Size = new System.Drawing.Size(23, 23);
            this.ledFilterCalib.TabIndex = 42;
            this.ledFilterCalib.Text = "ledBulb4";
            // 
            // ledLongCalib
            // 
            this.ledLongCalib.Color = System.Drawing.Color.Red;
            this.ledLongCalib.Location = new System.Drawing.Point(232, 76);
            this.ledLongCalib.Name = "ledLongCalib";
            this.ledLongCalib.On = true;
            this.ledLongCalib.Size = new System.Drawing.Size(23, 23);
            this.ledLongCalib.TabIndex = 41;
            this.ledLongCalib.Text = "ledBulb5";
            // 
            // ledCrossCalib
            // 
            this.ledCrossCalib.Color = System.Drawing.Color.Red;
            this.ledCrossCalib.Location = new System.Drawing.Point(151, 76);
            this.ledCrossCalib.Name = "ledCrossCalib";
            this.ledCrossCalib.On = true;
            this.ledCrossCalib.Size = new System.Drawing.Size(23, 23);
            this.ledCrossCalib.TabIndex = 40;
            this.ledCrossCalib.Text = "ledBulb6";
            // 
            // ledLongPot
            // 
            this.ledLongPot.Color = System.Drawing.Color.Red;
            this.ledLongPot.Location = new System.Drawing.Point(232, 104);
            this.ledLongPot.Name = "ledLongPot";
            this.ledLongPot.On = true;
            this.ledLongPot.Size = new System.Drawing.Size(23, 23);
            this.ledLongPot.TabIndex = 44;
            this.ledLongPot.Text = "ledBulb8";
            // 
            // ledCrossPot
            // 
            this.ledCrossPot.Color = System.Drawing.Color.Red;
            this.ledCrossPot.Location = new System.Drawing.Point(151, 104);
            this.ledCrossPot.Name = "ledCrossPot";
            this.ledCrossPot.On = true;
            this.ledCrossPot.Size = new System.Drawing.Size(23, 23);
            this.ledCrossPot.TabIndex = 43;
            this.ledCrossPot.Text = "ledBulb9";
            // 
            // ledErr270
            // 
            this.ledErr270.Color = System.Drawing.Color.Red;
            this.ledErr270.Location = new System.Drawing.Point(298, 41);
            this.ledErr270.Name = "ledErr270";
            this.ledErr270.On = true;
            this.ledErr270.Size = new System.Drawing.Size(23, 23);
            this.ledErr270.TabIndex = 57;
            this.ledErr270.Text = "ledBulb9";
            // 
            // ledErr180
            // 
            this.ledErr180.Color = System.Drawing.Color.Red;
            this.ledErr180.Location = new System.Drawing.Point(215, 41);
            this.ledErr180.Name = "ledErr180";
            this.ledErr180.On = true;
            this.ledErr180.Size = new System.Drawing.Size(23, 23);
            this.ledErr180.TabIndex = 56;
            this.ledErr180.Text = "ledBulb4";
            // 
            // ledErr90
            // 
            this.ledErr90.Color = System.Drawing.Color.Red;
            this.ledErr90.Location = new System.Drawing.Point(122, 41);
            this.ledErr90.Name = "ledErr90";
            this.ledErr90.On = true;
            this.ledErr90.Size = new System.Drawing.Size(23, 23);
            this.ledErr90.TabIndex = 55;
            this.ledErr90.Text = "ledBulb5";
            // 
            // ledErr0
            // 
            this.ledErr0.Color = System.Drawing.Color.Red;
            this.ledErr0.Location = new System.Drawing.Point(30, 41);
            this.ledErr0.Name = "ledErr0";
            this.ledErr0.On = true;
            this.ledErr0.Size = new System.Drawing.Size(23, 23);
            this.ledErr0.TabIndex = 54;
            this.ledErr0.Text = "ledBulb6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "0°";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "90°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "180°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "270°";
            // 
            // ledComunicazione
            // 
            this.ledComunicazione.Color = System.Drawing.Color.Red;
            this.ledComunicazione.Location = new System.Drawing.Point(146, 54);
            this.ledComunicazione.Name = "ledComunicazione";
            this.ledComunicazione.On = true;
            this.ledComunicazione.Size = new System.Drawing.Size(23, 23);
            this.ledComunicazione.TabIndex = 69;
            this.ledComunicazione.Text = "ledBulb3";
            // 
            // ledAEP
            // 
            this.ledAEP.Color = System.Drawing.Color.Red;
            this.ledAEP.Location = new System.Drawing.Point(80, 54);
            this.ledAEP.Name = "ledAEP";
            this.ledAEP.On = true;
            this.ledAEP.Size = new System.Drawing.Size(23, 23);
            this.ledAEP.TabIndex = 68;
            this.ledAEP.Text = "ledBulb2";
            // 
            // ledGeneral
            // 
            this.ledGeneral.Color = System.Drawing.Color.Red;
            this.ledGeneral.Location = new System.Drawing.Point(30, 54);
            this.ledGeneral.Name = "ledGeneral";
            this.ledGeneral.On = true;
            this.ledGeneral.Size = new System.Drawing.Size(23, 23);
            this.ledGeneral.TabIndex = 67;
            this.ledGeneral.Text = "ledBulb1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(77, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "AEP";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(119, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 64;
            this.label14.Text = "Comunicazione";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "Generale";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(63, 146);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 70;
            this.label16.Text = "Delta Passi";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 172);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 13);
            this.label17.TabIndex = 71;
            this.label17.Text = "Delta Potenziometri";
            // 
            // lbl_deltaPassiCross
            // 
            this.lbl_deltaPassiCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiCross.Location = new System.Drawing.Point(134, 140);
            this.lbl_deltaPassiCross.Name = "lbl_deltaPassiCross";
            this.lbl_deltaPassiCross.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiCross.TabIndex = 72;
            // 
            // lbl_deltaPassiLong
            // 
            this.lbl_deltaPassiLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPassiLong.Location = new System.Drawing.Point(215, 140);
            this.lbl_deltaPassiLong.Name = "lbl_deltaPassiLong";
            this.lbl_deltaPassiLong.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPassiLong.TabIndex = 73;
            // 
            // lbl_deltaPotLong
            // 
            this.lbl_deltaPotLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPotLong.Location = new System.Drawing.Point(215, 171);
            this.lbl_deltaPotLong.Name = "lbl_deltaPotLong";
            this.lbl_deltaPotLong.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPotLong.TabIndex = 75;
            // 
            // lbl_deltaPotCross
            // 
            this.lbl_deltaPotCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaPotCross.Location = new System.Drawing.Point(134, 170);
            this.lbl_deltaPotCross.Name = "lbl_deltaPotCross";
            this.lbl_deltaPotCross.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaPotCross.TabIndex = 74;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ledErr0);
            this.groupBox1.Controls.Add(this.ledErr90);
            this.groupBox1.Controls.Add(this.ledErr180);
            this.groupBox1.Controls.Add(this.ledErr270);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 356);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 73);
            this.groupBox1.TabIndex = 76;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rotazione Collimatore";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ledComunicazione);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ledGeneral);
            this.groupBox2.Controls.Add(this.ledAEP);
            this.groupBox2.Location = new System.Drawing.Point(12, 250);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(345, 100);
            this.groupBox2.TabIndex = 77;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Allarmi Vari";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl_deltaEncLong);
            this.groupBox3.Controls.Add(this.lbl_deltaEncCross);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.lbl_deltaPotLong);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.lbl_deltaPotCross);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.lbl_deltaPassiLong);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lbl_deltaPassiCross);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.ledCrossJam);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.ledLongJam);
            this.groupBox3.Controls.Add(this.ledFilterJam);
            this.groupBox3.Controls.Add(this.ledCrossCalib);
            this.groupBox3.Controls.Add(this.ledLongPot);
            this.groupBox3.Controls.Add(this.ledLongCalib);
            this.groupBox3.Controls.Add(this.ledCrossPot);
            this.groupBox3.Controls.Add(this.ledFilterCalib);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(345, 232);
            this.groupBox3.TabIndex = 78;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Allarmi Motore";
            // 
            // lbl_deltaEncLong
            // 
            this.lbl_deltaEncLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaEncLong.Location = new System.Drawing.Point(215, 202);
            this.lbl_deltaEncLong.Name = "lbl_deltaEncLong";
            this.lbl_deltaEncLong.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaEncLong.TabIndex = 78;
            // 
            // lbl_deltaEncCross
            // 
            this.lbl_deltaEncCross.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_deltaEncCross.Location = new System.Drawing.Point(134, 201);
            this.lbl_deltaEncCross.Name = "lbl_deltaEncCross";
            this.lbl_deltaEncCross.Size = new System.Drawing.Size(60, 19);
            this.lbl_deltaEncCross.TabIndex = 77;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(48, 203);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 13);
            this.label18.TabIndex = 76;
            this.label18.Text = "Delta Encoder";
            // 
            // FormP225Errors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 438);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormP225Errors";
            this.Text = "P225 Errori";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private LedBulb ledCrossJam;
        private LedBulb ledLongJam;
        private LedBulb ledFilterJam;
        private LedBulb ledFilterCalib;
        private LedBulb ledLongCalib;
        private LedBulb ledCrossCalib;
        private LedBulb ledLongPot;
        private LedBulb ledCrossPot;
        private LedBulb ledErr270;
        private LedBulb ledErr180;
        private LedBulb ledErr90;
        private LedBulb ledErr0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private LedBulb ledComunicazione;
        private LedBulb ledAEP;
        private LedBulb ledGeneral;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbl_deltaPassiCross;
        private System.Windows.Forms.Label lbl_deltaPassiLong;
        private System.Windows.Forms.Label lbl_deltaPotLong;
        private System.Windows.Forms.Label lbl_deltaPotCross;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_deltaEncLong;
        private System.Windows.Forms.Label lbl_deltaEncCross;
        private System.Windows.Forms.Label label18;
    }
}