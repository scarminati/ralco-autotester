﻿namespace Autotester
{
    partial class R915SPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnErrori = new System.Windows.Forms.Button();
            this.lblFront = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblFiltro = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblRear = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.grpVita = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPuntoFocale = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblFC = new System.Windows.Forms.Label();
            this.lblCNC = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblCollimatore = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cmb_tipoTest = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.grpLuce = new System.Windows.Forms.GroupBox();
            this.btnLuce = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.ledLuce = new Autotester.LedBulb();
            this.label16 = new System.Windows.Forms.Label();
            this.ledLuceTest = new Autotester.LedBulb();
            this.ledError = new Autotester.LedBulb();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPCB = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBootloader = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFirmware = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.grpVita.SuspendLayout();
            this.grpLuce.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(4, 4);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(15, 14);
            this.chkEnable.TabIndex = 0;
            this.chkEnable.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1101, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Errori";
            // 
            // btnErrori
            // 
            this.btnErrori.Location = new System.Drawing.Point(1161, 35);
            this.btnErrori.Name = "btnErrori";
            this.btnErrori.Size = new System.Drawing.Size(75, 41);
            this.btnErrori.TabIndex = 23;
            this.btnErrori.Text = "Mostra errori";
            this.btnErrori.UseVisualStyleBackColor = true;
            this.btnErrori.Click += new System.EventHandler(this.btnErrori_Click);
            // 
            // lblFront
            // 
            this.lblFront.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFront.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFront.Location = new System.Drawing.Point(87, 29);
            this.lblFront.Name = "lblFront";
            this.lblFront.Size = new System.Drawing.Size(69, 16);
            this.lblFront.TabIndex = 35;
            this.lblFront.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Front (cm)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFiltro
            // 
            this.lblFiltro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFiltro.Location = new System.Drawing.Point(87, 83);
            this.lblFiltro.Name = "lblFiltro";
            this.lblFiltro.Size = new System.Drawing.Size(69, 16);
            this.lblFiltro.TabIndex = 37;
            this.lblFiltro.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Filtro";
            // 
            // lblRear
            // 
            this.lblRear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRear.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRear.Location = new System.Drawing.Point(87, 56);
            this.lblRear.Name = "lblRear";
            this.lblRear.Size = new System.Drawing.Size(69, 16);
            this.lblRear.TabIndex = 39;
            this.lblRear.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "Rear (cm)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(135, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 48;
            this.label15.Text = "Test Vita";
            // 
            // grpVita
            // 
            this.grpVita.Controls.Add(this.label2);
            this.grpVita.Controls.Add(this.lblPuntoFocale);
            this.grpVita.Controls.Add(this.label1);
            this.grpVita.Controls.Add(this.lblRight);
            this.grpVita.Controls.Add(this.label3);
            this.grpVita.Controls.Add(this.lblLeft);
            this.grpVita.Controls.Add(this.label15);
            this.grpVita.Controls.Add(this.label17);
            this.grpVita.Controls.Add(this.lblFront);
            this.grpVita.Controls.Add(this.label19);
            this.grpVita.Controls.Add(this.label21);
            this.grpVita.Controls.Add(this.lblRear);
            this.grpVita.Controls.Add(this.lblFiltro);
            this.grpVita.Enabled = false;
            this.grpVita.Location = new System.Drawing.Point(727, 0);
            this.grpVita.Name = "grpVita";
            this.grpVita.Size = new System.Drawing.Size(347, 110);
            this.grpVita.TabIndex = 52;
            this.grpVita.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(183, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Punto Focale";
            // 
            // lblPuntoFocale
            // 
            this.lblPuntoFocale.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPuntoFocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuntoFocale.Location = new System.Drawing.Point(261, 81);
            this.lblPuntoFocale.Name = "lblPuntoFocale";
            this.lblPuntoFocale.Size = new System.Drawing.Size(69, 16);
            this.lblPuntoFocale.TabIndex = 54;
            this.lblPuntoFocale.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(183, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Right (cm)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRight
            // 
            this.lblRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRight.Location = new System.Drawing.Point(261, 25);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(69, 16);
            this.lblRight.TabIndex = 50;
            this.lblRight.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(183, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Left (cm)";
            // 
            // lblLeft
            // 
            this.lblLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeft.Location = new System.Drawing.Point(261, 52);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(69, 16);
            this.lblLeft.TabIndex = 52;
            this.lblLeft.Text = "0";
            // 
            // lblFC
            // 
            this.lblFC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFC.Location = new System.Drawing.Point(69, 84);
            this.lblFC.Name = "lblFC";
            this.lblFC.Size = new System.Drawing.Size(74, 21);
            this.lblFC.TabIndex = 72;
            // 
            // lblCNC
            // 
            this.lblCNC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNC.Location = new System.Drawing.Point(184, 85);
            this.lblCNC.Name = "lblCNC";
            this.lblCNC.Size = new System.Drawing.Size(74, 21);
            this.lblCNC.TabIndex = 71;
            // 
            // lblSN
            // 
            this.lblSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSN.Location = new System.Drawing.Point(184, 52);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(74, 21);
            this.lblSN.TabIndex = 70;
            // 
            // lblCollimatore
            // 
            this.lblCollimatore.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCollimatore.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollimatore.Location = new System.Drawing.Point(69, 21);
            this.lblCollimatore.Name = "lblCollimatore";
            this.lblCollimatore.Size = new System.Drawing.Size(189, 20);
            this.lblCollimatore.TabIndex = 69;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(147, 86);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 68;
            this.label25.Text = "CNC";
            // 
            // cmb_tipoTest
            // 
            this.cmb_tipoTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tipoTest.FormattingEnabled = true;
            this.cmb_tipoTest.Items.AddRange(new object[] {
            "Analisi",
            "Produzione",
            "Riparazione",
            "Test"});
            this.cmb_tipoTest.Location = new System.Drawing.Point(69, 51);
            this.cmb_tipoTest.Name = "cmb_tipoTest";
            this.cmb_tipoTest.Size = new System.Drawing.Size(74, 21);
            this.cmb_tipoTest.TabIndex = 67;
            this.cmb_tipoTest.SelectedIndexChanged += new System.EventHandler(this.cmb_tipoTest_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 66;
            this.label26.Text = "Tipo Test";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 24);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 13);
            this.label27.TabIndex = 65;
            this.label27.Text = "Collimatore";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(20, 13);
            this.label28.TabIndex = 64;
            this.label28.Text = "FC";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(147, 55);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 13);
            this.label29.TabIndex = 63;
            this.label29.Text = "S/N";
            // 
            // grpLuce
            // 
            this.grpLuce.Controls.Add(this.btnLuce);
            this.grpLuce.Controls.Add(this.label13);
            this.grpLuce.Controls.Add(this.ledLuce);
            this.grpLuce.Controls.Add(this.label16);
            this.grpLuce.Controls.Add(this.ledLuceTest);
            this.grpLuce.Enabled = false;
            this.grpLuce.Location = new System.Drawing.Point(608, 0);
            this.grpLuce.Name = "grpLuce";
            this.grpLuce.Size = new System.Drawing.Size(111, 110);
            this.grpLuce.TabIndex = 74;
            this.grpLuce.TabStop = false;
            // 
            // btnLuce
            // 
            this.btnLuce.Location = new System.Drawing.Point(6, 33);
            this.btnLuce.Name = "btnLuce";
            this.btnLuce.Size = new System.Drawing.Size(54, 23);
            this.btnLuce.TabIndex = 25;
            this.btnLuce.Text = "Accendi";
            this.btnLuce.UseVisualStyleBackColor = true;
            this.btnLuce.Click += new System.EventHandler(this.btnLuce_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(37, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Luce";
            // 
            // ledLuce
            // 
            this.ledLuce.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ledLuce.Location = new System.Drawing.Point(15, 63);
            this.ledLuce.Name = "ledLuce";
            this.ledLuce.On = true;
            this.ledLuce.Size = new System.Drawing.Size(36, 39);
            this.ledLuce.TabIndex = 26;
            this.ledLuce.Text = "ledBulb1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(73, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Test";
            // 
            // ledLuceTest
            // 
            this.ledLuceTest.Location = new System.Drawing.Point(76, 69);
            this.ledLuceTest.Name = "ledLuceTest";
            this.ledLuceTest.On = true;
            this.ledLuceTest.Size = new System.Drawing.Size(25, 23);
            this.ledLuceTest.TabIndex = 41;
            this.ledLuceTest.Text = "ledBulb4";
            // 
            // ledError
            // 
            this.ledError.Color = System.Drawing.Color.Red;
            this.ledError.Location = new System.Drawing.Point(1086, 30);
            this.ledError.Name = "ledError";
            this.ledError.On = true;
            this.ledError.Size = new System.Drawing.Size(69, 67);
            this.ledError.TabIndex = 22;
            this.ledError.Text = "ledBulb1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblPCB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblBootloader);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblFirmware);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(263, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 105);
            this.panel1.TabIndex = 74;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Firmware Version";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPCB
            // 
            this.lblPCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCB.Location = new System.Drawing.Point(48, 81);
            this.lblPCB.Name = "lblPCB";
            this.lblPCB.Size = new System.Drawing.Size(55, 16);
            this.lblPCB.TabIndex = 5;
            this.lblPCB.Text = "-1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "PCB";
            // 
            // lblBootloader
            // 
            this.lblBootloader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBootloader.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBootloader.Location = new System.Drawing.Point(48, 53);
            this.lblBootloader.Name = "lblBootloader";
            this.lblBootloader.Size = new System.Drawing.Size(55, 16);
            this.lblBootloader.TabIndex = 3;
            this.lblBootloader.Text = "-1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "BL";
            // 
            // lblFirmware
            // 
            this.lblFirmware.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFirmware.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirmware.Location = new System.Drawing.Point(48, 27);
            this.lblFirmware.Name = "lblFirmware";
            this.lblFirmware.Size = new System.Drawing.Size(55, 16);
            this.lblFirmware.TabIndex = 1;
            this.lblFirmware.Text = "-1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "FW";
            // 
            // R915SPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grpLuce);
            this.Controls.Add(this.lblFC);
            this.Controls.Add(this.lblCNC);
            this.Controls.Add(this.lblSN);
            this.Controls.Add(this.lblCollimatore);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.cmb_tipoTest);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.grpVita);
            this.Controls.Add(this.btnErrori);
            this.Controls.Add(this.ledError);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.chkEnable);
            this.Name = "R915SPanel";
            this.Size = new System.Drawing.Size(1246, 115);
            this.Tag = "Philips";
            this.Load += new System.EventHandler(this.R915SPanel_Load);
            this.grpVita.ResumeLayout(false);
            this.grpVita.PerformLayout();
            this.grpLuce.ResumeLayout(false);
            this.grpLuce.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEnable;
        private System.Windows.Forms.Label label12;
        private LedBulb ledError;
        private System.Windows.Forms.Button btnErrori;
        private System.Windows.Forms.Label lblFront;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblFiltro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblRear;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox grpVita;
        private System.Windows.Forms.Label lblFC;
        private System.Windows.Forms.Label lblCNC;
        private System.Windows.Forms.Label lblSN;
        private System.Windows.Forms.Label lblCollimatore;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmb_tipoTest;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox grpLuce;
        private System.Windows.Forms.Button btnLuce;
        private System.Windows.Forms.Label label13;
        private LedBulb ledLuce;
        private System.Windows.Forms.Label label16;
        private LedBulb ledLuceTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPuntoFocale;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBootloader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFirmware;
        private System.Windows.Forms.Label label7;
    }
}
