﻿namespace Autotester
{
    partial class FormCANUSBConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_CANUSBConfig = new System.Windows.Forms.DataGridView();
            this.btnConfigure = new System.Windows.Forms.Button();
            this.cln_devType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_devNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CANUSB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANUSBConfig)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_CANUSBConfig
            // 
            this.dgv_CANUSBConfig.AllowUserToAddRows = false;
            this.dgv_CANUSBConfig.AllowUserToResizeRows = false;
            this.dgv_CANUSBConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CANUSBConfig.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_devType,
            this.cln_devNumber,
            this.CANUSB});
            this.dgv_CANUSBConfig.Location = new System.Drawing.Point(12, 12);
            this.dgv_CANUSBConfig.Name = "dgv_CANUSBConfig";
            this.dgv_CANUSBConfig.RowHeadersVisible = false;
            this.dgv_CANUSBConfig.Size = new System.Drawing.Size(344, 178);
            this.dgv_CANUSBConfig.TabIndex = 1;
            // 
            // btnConfigure
            // 
            this.btnConfigure.Location = new System.Drawing.Point(383, 93);
            this.btnConfigure.Name = "btnConfigure";
            this.btnConfigure.Size = new System.Drawing.Size(75, 23);
            this.btnConfigure.TabIndex = 2;
            this.btnConfigure.Text = "Configura";
            this.btnConfigure.UseVisualStyleBackColor = true;
            this.btnConfigure.Click += new System.EventHandler(this.btnConfigure_Click);
            // 
            // cln_devType
            // 
            this.cln_devType.HeaderText = "Tipo di Dispositivo";
            this.cln_devType.Name = "cln_devType";
            this.cln_devType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_devType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cln_devType.Width = 150;
            // 
            // cln_devNumber
            // 
            this.cln_devNumber.HeaderText = "# Dispositivo";
            this.cln_devNumber.Name = "cln_devNumber";
            this.cln_devNumber.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_devNumber.Width = 110;
            // 
            // CANUSB
            // 
            this.CANUSB.HeaderText = "CANUSB";
            this.CANUSB.Name = "CANUSB";
            this.CANUSB.ReadOnly = true;
            this.CANUSB.Width = 80;
            // 
            // FormCANUSBConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 202);
            this.Controls.Add(this.btnConfigure);
            this.Controls.Add(this.dgv_CANUSBConfig);
            this.Name = "FormCANUSBConfig";
            this.Text = "FormCANUSBConfig";
            this.Load += new System.EventHandler(this.FormCANUSBConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANUSBConfig)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_CANUSBConfig;
        private System.Windows.Forms.Button btnConfigure;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_devType;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_devNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn CANUSB;
    }
}