﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormR225DettagliTastiMembrana : Form
    {
        public FormR225DettagliTastiMembrana()
        {
            InitializeComponent();
        }

        public void setTests(R225ProtocolAnalyzer protocol)
        {
            if ((protocol.TastiMembPressed & 0x01) == 0x01)
                ledBtnBarellaIn.On = true;
            else
                ledBtnBarellaIn.On = false;
            if ((protocol.TastiMembPressed & 0x02) == 0x02)
                ledBtnBarellaOut.On = true;
            else
                ledBtnBarellaOut.On = false;
            if ((protocol.TastiMembPressed & 0x04) == 0x04)
                ledBtnScanSx.On = true;
            else
                ledBtnScanSx.On = false;
            if ((protocol.TastiMembPressed & 0x08) == 0x08)
                ledBtnScanDx.On = true;
            else
                ledBtnScanDx.On = false;
            if ((protocol.TastiMembPressed & 0x10) == 0x10)
                ledBtnPendAntior.On = true;
            else
                ledBtnPendAntior.On = false;
            if ((protocol.TastiMembPressed & 0x20) == 0x20)
                ledBtnPendOr.On = true;
            else
                ledBtnPendOr.On = false;
        }
    }
}
