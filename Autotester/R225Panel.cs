﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Xceed.Words.NET;

namespace Autotester
{
    public partial class R225Panel : UserControl, ICollimatorPanel
    {
        private R225ProtocolAnalyzer protocol;
        private FormControls formRuota = null;
        private ConfigurazioneTest cfg;
        private int startLong;
        private int startCross;
        private int minLong;
        private int minCross;
        private int maxLong;
        private int maxCross;
        private int lastLong;
        private int lastCross;
        private int lastDir;
        private int dirLong;
        private int dirCross;
        private int changeLuce = 0;
        private int changeBtnLuce = 0;
        private int changeFiltro = 0;
        private int changeBtnFiltro = 0;
        private int changeKey = 0;
        private int testIride = 0;
        private int testChiave = 0;
        private int numCicli;

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public string collimatorName { get; set; }

        public string pathScriptFile { get; set; }

        public List<List<SingoloComando>> scripts { get; set; }

        private int _numPanel;

        public int NumPanel { get { return _numPanel; } set { this._numPanel = value; }  }

        public R225Panel()
        {
            InitializeComponent();
            this.scripts = new List<List<SingoloComando>>();
            //pathScriptFile = Environment.CurrentDirectory + @"\script\R225\scripts.txt";
        }


        //public void setConnector(CANusb_Connector connector, string deviceName)
        //{
        //    R225ProtocolAnalyzer r225Protocol = new R225ProtocolAnalyzer();

        //    r225Protocol.Connector = connector;

        //    r225Protocol.Handler += ConnectorOnHandler();

        //    protocol = r225Protocol;
        //}

        public void setConnector(testCollimatore t)
        {
            R225ProtocolAnalyzer r225Protocol = new R225ProtocolAnalyzer();

            r225Protocol.testColl = t;

            r225Protocol.Connector = t.connettore;

            r225Protocol.Handler += ConnectorOnHandler();

            protocol = r225Protocol;

        }

        public void setFormRuota(FormControls formRuota)
        {
            this.formRuota = formRuota;
        }

        public void enableControl(ConfigurazioneTest c)
        {
            this.cfg = c;
            if (cfg.testBtnLuce.Equals(true))
            {
                grpTastoLuce.Enabled = true;
            }
            if (cfg.testBtnFiltro.Equals(true))
            {
                grpTastoFiltro.Enabled = true;
            }
            if (cfg.testBtnIride.Equals(true))
            {
                grpTastoIride.Enabled = true;
            }
            if (cfg.testBtnMembrana.Equals(true))
            {
                grpTastiMemb.Enabled = true;
            }
            if (cfg.testChiave.Equals(true))
            {
                grpChiave.Enabled = true;
            }
            if (cfg.testDSC.Equals(true))
            {
                grpDSC.Enabled = true;
            }  
            if (cfg.testVita.Equals(true))
            {
                grpVita.Enabled = true;
            }    
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return protocol;
        }

        public bool isManualTestCompleted()
        {
            bool rv = true;
            if (grpTastoLuce.Enabled)
                rv = rv & ledLuceTest.On;
            if (grpTastoFiltro.Enabled)
                rv = rv & ledFiltroTest.On;
            if (grpTastoIride.Enabled)
                rv = rv & ledIrideTest.On;
            if (grpTastiMemb.Enabled)
                rv = rv & ledTastiMemb.On;
            if (grpChiave.Enabled)
                rv = rv & ledChiave.On;
            if (grpDSC.Enabled)
                rv = rv & ledCrossDSCTest.On & ledLongDSCTest.On;
            return (rv);
        }

        public string getTipoTest()
        {
            if (cmb_tipoTest.SelectedItem == null)
                return "";
            else
                return cmb_tipoTest.SelectedItem.ToString();
        }

        private Action<ProtocolChange> ConnectorOnHandler()
        {
            return delegate(ProtocolChange value) { this.CollimatorValuesChanged(value); };
        }

        private void CollimatorValuesChanged(ProtocolChange value)
        {
            int tmp;

            switch (value)
            {
                case ProtocolChange.Shutters:
                    changeCross(protocol.CrossPos / 10);
                    changeLong(protocol.LongPos / 10);
                    break;

                case ProtocolChange.Iris:
                    changeIris(protocol.IrisPos / 10);
                    break;

                case ProtocolChange.Filter:
                    switch(protocol.Filter)
                    {
                        case 0:
                            if (collimatorName.Equals("R 225/460A/ACS") || collimatorName.Equals("R 225/460E/ACS"))
                                changeFilter("0");
                            else
                                changeFilter("0 Al");
                            changeFiltro++;
                            break;
                        case 1:
                            if(collimatorName.Equals("R 225/460A/ACS") || collimatorName.Equals("R 225/460E/ACS"))
                                changeFilter("0,1 Cu");
                            else
                                changeFilter("1 Al + 0,1 Cu");
                            changeFiltro++;
                            break;
                        case 2:
                            if (collimatorName.Equals("R 225/460A/ACS") || collimatorName.Equals("R 225/460E/ACS"))
                                changeFilter("0,2 Cu");
                            else
                                changeFilter("1 Al + 0,2 Cu");
                            changeFiltro++;
                            break;
                        case 3:
                            if (collimatorName.Equals("R 225/460A/ACS") || collimatorName.Equals("R 225/460E/ACS"))
                                changeFilter("0,3 Cu");
                            else
                                changeFilter("2 Al");
                            changeFiltro++;
                            break;
                        default:
                            changeFilter("Unknown");
                            break;
                    }
                    break;

                case ProtocolChange.Errors:
                    ledError.On = true;
                    if (this.formRuota != null)
                    {
                        switch (this.formRuota.getActualAngleRotation())
                        {
                            case 0:
                                protocol.error0 = true;
                                break;
                            case 90:
                                protocol.error90 = true;
                                break;
                            case 180:
                                protocol.error180 = true;
                                break;
                            case 270:
                                protocol.error270 = true;
                                break;
                        }
                    }
                    changeChkEnableValue(false);
                    break;

                case ProtocolChange.DSC:
                    if (cfg.testDSC.Equals(true))
                    {
                        ledCrossDSCActive.On = protocol.DscCrossActive;
                        ledLongDSCActive.On = protocol.DscLongActive;
                        if (ledCrossDSCActive.On && ledLongDSCActive.On)
                        {
                            startLong = 0;
                            startCross = 0;
                            minCross = 99999;
                            minLong = 99999;
                            maxCross = 0;
                            maxLong = 0;
                            dirLong = 0;
                            dirCross = 0;
                            lastDir = 0;
                        }
                        else
                        {
                            if (protocol.DscCrossActive)
                            {
                                dirLong = 0;
                                startLong = 0;

                                tmp = protocol.CrossPos - lastCross;
                                if (startCross == 0)
                                {
                                    dirCross = Math.Sign(tmp);

                                    startCross = protocol.CrossPos;
                                }

                                if (tmp > 0)
                                {

                                    if (protocol.CrossPos > maxCross)
                                    {
                                        maxCross = protocol.CrossPos;
                                    }

                                    lastDir = 1;

                                }
                                else if (tmp < 0)
                                {
                                    if (protocol.CrossPos < minCross)
                                    {
                                        minCross = protocol.CrossPos;
                                    }

                                    lastDir = -1;
                                }

                                lastCross = protocol.CrossPos;

                                if (minCross < 10 && maxCross > 400)
                                {
                                    ledCrossDSCTest.On = true;
                                }
                            }
                            else if (protocol.DscLongActive)
                            {
                                dirLong = 0;
                                startLong = 0;

                                tmp = protocol.LongPos - lastLong;
                                if (startLong == 0)
                                {
                                    dirLong = Math.Sign(tmp);

                                    startLong = protocol.LongPos;
                                }

                                if (tmp > 0)
                                {


                                    if (protocol.LongPos > maxLong)
                                    {
                                        maxLong = protocol.LongPos;
                                    }

                                    lastDir = 1;

                                }
                                else if (tmp < 0)
                                {

                                    if (protocol.LongPos < minLong)
                                    {
                                        minLong = protocol.LongPos;
                                    }

                                    lastDir = -1;
                                }

                                lastLong = protocol.LongPos;

                                if (minLong < 10 && maxLong > 400)
                                {
                                    ledLongDSCTest.On = true;
                                }

                            }
                        }
                    }
                    break;

                case ProtocolChange.Key:
                    if (cfg.testChiave.Equals(true))
                    {
                        changeKey++;
                        changeKeyValue(protocol.Key);
                        if (changeKey.Equals(2))
                        {
                            ledChiave.On = true;
                        }
                    }
                    break;
                    
                case ProtocolChange.Info:
                    changeFirmware(protocol.FwCollimator);
                    //changeSerialNumber(protocol.SerialNumber);
                    //changeBootloader(protocol.FwBootloader);
                    //changePCB(protocol.PcbCollimator); 
                    break;

                case ProtocolChange.TastoLuce:
                    if (cfg.testBtnLuce.Equals(true))
                    {
                        changeBtnLuce++;
                        ledTastoLuceActive.On = protocol.TastoLuce;
                        changeLightButtonValue(protocol.TastoLuce);
                    }
                    break;

                case ProtocolChange.TastoFiltro:
                    if (cfg.testBtnFiltro.Equals(true))
                    {
                        changeBtnFiltro++;
                        ledTastoFiltroActive.On = protocol.TastoFiltro;
                        changeFilterButtonValue(protocol.TastoFiltro);
                    }
                    break;

                case ProtocolChange.Light:
                    if (cfg.testLuce.Equals(true))
                    {
                        ledLuce.On = protocol.Light;
                        changeLuce++;
                    }
                    break;            
        
                case ProtocolChange.Counter:
                    changeCicli(protocol.CounterIteration);
                    break;

                case ProtocolChange.TastiMembrana:
                    ledTastiMembActive.On = (protocol.TastiMembrActive);
                    ledTastiMemb.On = (protocol.TestTastiMemb);
                    break;

                default:
                    break;
            }
        }

        private void changeKeyValue(bool value)
        {
            if (this.lblChiave.InvokeRequired)
            {
                this.lblChiave.BeginInvoke((MethodInvoker)delegate() {
                    {
                        if (value.Equals(false))
                            this.lblChiave.Text = "Man";
                        else if (value.Equals(true))
                            this.lblChiave.Text = "Aut";
                    };
                    ;});
            }
            else
            {
                this.lblChiave.Text = value.ToString(); ;
            }
        }

        private void changeLightButtonValue(bool value)
        {
            if (this.ledTastoLuceActive.InvokeRequired)
            {
                this.ledLuceTest.BeginInvoke((MethodInvoker)delegate () { if (changeBtnLuce == 4)  this.ledLuceTest.On = true; });
            }
            else
            {
                if (changeBtnLuce == 4)
                    this.ledLuceTest.On = true;
            }
        }

        private void changeFilterButtonValue(bool value)
        {
            if (this.ledTastoFiltroActive.InvokeRequired)
            {
                this.ledFiltroTest.BeginInvoke((MethodInvoker)delegate () { if (changeBtnFiltro == 4) this.ledFiltroTest.On = true; });
            }
            else
            {
                if (changeBtnFiltro == 4)
                    this.ledFiltroTest.On = true;
            }
        }

        private void changeChkEnableValue(bool value)
        {
            if (this.chkEnable.InvokeRequired)
            {
                this.chkEnable.BeginInvoke((MethodInvoker)delegate () { this.chkEnable.Checked = value; });
            }
            else
            {
                this.chkEnable.Checked = value;
            }
        }

        private void changeCicli(int value)
        {
            this.numCicli = value;
            //if (this.lblCicli.InvokeRequired)
            //{
            //    this.lblCicli.BeginInvoke((MethodInvoker)delegate() { this.lblCicli.Text = value.ToString(); ;});
            //    
            //}
            //else
            //{
            //    this.lblCicli.Text = value.ToString();
            //    
            //}
        }

        public int getCicli()
        {
            return this.numCicli;
        }

        private void changeFirmware(string value)
        {
            if (this.lblFirmware.InvokeRequired)
            {
                this.lblFirmware.BeginInvoke((MethodInvoker)delegate () {
                    this.lblFirmware.Text = value;
                    if (!lblFirmware.Text.Equals(""))
                    {
                        if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblFirmware.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblFirmware.Text = value;
                if (!lblFirmware.Text.Equals(""))
                {
                    if (!lblFirmware.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblFirmware.BackColor = Color.Red;
                    }
                }
            }
        }

        private void changeBootloader(string value)
        {
            if (this.lblBootloader.InvokeRequired)
            {
                this.lblBootloader.BeginInvoke((MethodInvoker)delegate () {
                    this.lblBootloader.Text = value;
                    if (!lblBootloader.Text.Equals(""))
                    {
                        if (!lblBootloader.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblBootloader.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblBootloader.Text = value;
                if (!lblBootloader.Text.Equals(""))
                {
                    if (!lblBootloader.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblBootloader.BackColor = Color.Red;
                    }
                }
            }
        }


        private void changePCB(string value)
        {
            if (this.lblPCB.InvokeRequired)
            {
                this.lblPCB.BeginInvoke((MethodInvoker)delegate () {
                    this.lblPCB.Text = value;
                    if (!lblPCB.Text.Equals(""))
                    {
                        if (!lblPCB.Text.Equals(cfg.FWAtteso))
                        {
                            ledError.On = true;
                            changeChkEnableValue(false);
                            lblPCB.BackColor = Color.Red;
                        }
                    }
                });
            }
            else
            {
                this.lblPCB.Text = value;
                if (!lblPCB.Text.Equals(""))
                {
                    if (!lblPCB.Text.Equals(cfg.FWAtteso))
                    {
                        ledError.On = true;
                        changeChkEnableValue(false);
                        lblPCB.BackColor = Color.Red;
                    }
                }
            }
        }

        //private void changeSerialNumber(string value)
        //{
        //    if (this.lblSN.InvokeRequired)
        //    {
        //        this.lblSN.BeginInvoke((MethodInvoker)delegate () { this.lblSN.Text = value; });
        //    }
        //    else
        //    {
        //        this.lblSN.Text = value;
        //    }
        //}

        private void changeCross(int value)
        {
            if (this.lblCross.InvokeRequired)
            {
                this.lblCross.BeginInvoke((MethodInvoker)delegate() { this.lblCross.Text = value.ToString(); });
            }
            else
            {
                this.lblCross.Text = value.ToString();
            }
        }

        private void changeLong(int value)
        {
            if (this.lblLong.InvokeRequired)
            {
                this.lblLong.BeginInvoke((MethodInvoker)delegate() { this.lblLong.Text = value.ToString(); });
            }
            else
            {
                this.lblLong.Text = value.ToString();
            }
        }

        private void changeIris(int value)
        {
            if (this.lblIris.InvokeRequired)
            {
                this.lblIris.BeginInvoke((MethodInvoker)delegate () { this.lblIris.Text = value.ToString(); });
            }
            else
            {
                this.lblIris.Text = value.ToString();
            }
        }

        private void changeFilter(string value)
        {
            if (this.lblFilter.InvokeRequired)
            {
                this.lblFilter.BeginInvoke((MethodInvoker)delegate() { this.lblFilter.Text = value; });
            }
            else
            {
                this.lblFilter.Text = value;
            }
        }

        private void R225Panel_Load(object sender, EventArgs e)
        {
            initValues();
        }

       
        private void initValues()
        {
            ledLuce.On = false;
            ledLuceTest.On = false;
            ledTastoFiltroActive.On = false;
            ledFiltroTest.On = false;
            ledTastoIrideActive.On = false;
            ledIrideTest.On = false;
            ledTastiMemb.On = false;
            ledTastiMembActive.On = false;
            ledChiave.On = false;
            ledCrossDSCActive.On = false;
            ledTastoLuceActive.On = false;
            ledCrossDSCTest.On = false;
            ledLongDSCActive.On = false;
            ledLongDSCTest.On = false;
            ledError.On = false;

            lblCollimatore.Text = collimatorName;
            lblSN.Text = SN;
            lblFC.Text = FC;
            lblCNC.Text = CNC;

            lblFirmware.Text = "";
            lblBootloader.Text = "";
            lblPCB.Text = "";

            lblChiave.Text = "";
            lblCross.Text = "";
            lblLong.Text = "";
            lblFilter.Text = "";
            lblIris.Text = "";

            if (protocol != null)
                protocol.initCollimator();

            chkEnable.Checked = true;

            startLong = 0;
            startCross = 0;
            dirLong = 0;
            dirCross = 0;
            minCross = 99999;
            minLong = 99999;
            maxCross = 0;
            maxLong = 0;
            dirLong = 0;
            dirCross = 0;


        }

        public bool isChecked()
        {
            return chkEnable.Checked;
        }

        public bool hasErrors()
        {
            return ledError.On;
        }

        private void btnErrori_Click(object sender, EventArgs e)
        {
            FormR225Errors form = new FormR225Errors();

            form.setErrors(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        private void cmb_tipoTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.tipoTest = cmb_tipoTest.SelectedItem.ToString();
        }

        private void btnDettagliTastiMemb_Click(object sender, EventArgs e)
        {
            FormR225DettagliTastiMembrana form = new FormR225DettagliTastiMembrana();

            form.setTests(protocol);

            form.ShowDialog(this);

            form.Dispose();
        }

        public void saveCalibTable()
        {

        }

        public void saveReport(Utente user)
        {
            try
            {
                string templateReportName = Utility.getTemplateFileName(lblCollimatore.Text);
                string testPass = "PASS";
                string errorDescription = "";
                var doc = DocX.Load(@"\\ralcosrv2\Public\Templates_Autotest\\TemplateFinalReport_" + templateReportName + ".docx");
                #region Test Report
                if (!lblFirmware.Text.Equals(-1))// && !lblBootloader.Equals(-1) && !lblPCB.Equals(-1))
                    doc.ReplaceText("@@GENTEST@@", "PASS");
                else
                {
                    doc.ReplaceText("@@GENTEST@@", "FAIL");
                    testPass = "FAIL";
                }
                if (grpTastoLuce.Enabled.Equals(true))
                {
                    if (ledLuceTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@BTNLIGTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@BTNLIGTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpTastoFiltro.Enabled.Equals(true))
                {
                    if (ledFiltroTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@BTNFILTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@BTNFILTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpTastoIride.Enabled.Equals(true))
                {
                    if (ledIrideTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@BTNIRSTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@BTNIRSTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpTastiMemb.Enabled.Equals(true))
                {
                    if (ledTastiMemb.On.Equals(true))
                    {
                        doc.ReplaceText("@@BTNMEMTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@BTNMEMTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpChiave.Enabled.Equals(true))
                {
                    if (ledChiave.On.Equals(true))
                    {
                        doc.ReplaceText("@@KEYTEST@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@KEYTEST@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpDSC.Enabled.Equals(true))
                {
                    if (ledCrossDSCTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@KNOTEST1@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@KNOTEST1@@", "FAIL");
                        testPass = "FAIL";
                    }
                    if (ledLongDSCTest.On.Equals(true))
                    {
                        doc.ReplaceText("@@KNOTEST2@@", "PASS");
                    }
                    else
                    {
                        doc.ReplaceText("@@KNOTEST2@@", "FAIL");
                        testPass = "FAIL";
                    }
                }
                if (grpVita.Enabled.Equals(true))
                {
                    if (ledError.On.Equals(false))
                    {
                        doc.ReplaceText("@@LIFTEST@@", "PASS");
                        doc.ReplaceText("@@LIFNOTES@@", "");
                    }
                    else
                    {
                        doc.ReplaceText("@@LIFTEST@@", "FAIL");
                        testPass = "FAIL";
                        if (protocol.error0)
                            errorDescription += " Errors at 0 degrees";
                        if (protocol.error90)
                            errorDescription += " Errors at 90 degrees";
                        if (protocol.error180)
                            errorDescription += " Errors at 180 degrees";
                        if (protocol.error270)
                            errorDescription += " Errors at 270 degrees";

                        errorDescription += " Steps Error Cross = " + protocol.passiCross.ToString()
                                          + " Steps Error Long = " + protocol.passiLong.ToString()
                                          + " Steps Error Iris = " + protocol.passiIride.ToString()
                                          + " Steps Spatial Filter = " + protocol.passiSpatialFilter.ToString();
                        doc.ReplaceText("@@LIFNOTES@@", errorDescription);
                    }
                }
                #endregion
                #region General Information
                doc.ReplaceText("@@MODEL@@", lblCollimatore.Text);
                doc.ReplaceText("@@SERNR@@", lblSN.Text);
                doc.ReplaceText("@@FWVER@@", lblFirmware.Text);
                doc.ReplaceText("@@BLVER@@", lblBootloader.Text);
                doc.ReplaceText("@@PCBVER@@", lblPCB.Text);
                doc.ReplaceText("@@FLOW@@", lblFC.Text);
                doc.ReplaceText("@@CNC@@", lblCNC.Text);
                doc.ReplaceText("@@DATE@@", DateTime.Now.ToString());
                doc.ReplaceText("@@OUTCOME@@", testPass);
                #endregion
                #region Operator
                doc.ReplaceText("@@OPERATOR@@", user.nome + " " + user.cognome);
                #endregion
                string filePathFirma = Utility.searchPathDigitalSignatureOperator(user);

                Xceed.Words.NET.Image img;
                if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                {
                    //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                    img = doc.AddImage(filePathFirma);
                    Xceed.Words.NET.Picture picture = img.CreatePicture();
                    Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                    p1.AppendPicture(picture);
                    p1.Alignment = Alignment.right;
                }

                //salvo in locale il report
                //infoFC[0] = numero FC; infoFC[1] = anno FC;
                string[] infoFC = FC.Split('/');
                string[] infoCNC = CNC.Split('/');
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                //Console.WriteLine(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                if (infoCNC[0].Equals("    "))
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                else
                    doc.SaveAs(Environment.CurrentDirectory + @"\Reports\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                //salvo il report sul server solo se il tipo di test è diverso da Test
                if (!tipoTest.Equals("Test"))
                {
                    if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0]);
                    if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN))
                        Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN);
                    if (infoCNC[0].Equals("    "))
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + ".docx");
                    }

                    else
                    {
                        doc.SaveAs(@"\\ralcosrv2\Public\Test Report\" + templateReportName + @"\Collaudi\" + infoFC[1] + @"\" + infoFC[0] + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                        doc.SaveAs(@"\\ralcosrv2\Public\DHR_Collimatori\\" + infoFC[1] + "\\" + infoFC[0] + "\\" + SN + @"\FinalReport_" + templateReportName + "_SN = " + SN + "_FC = " + infoFC[1] + " " + infoFC[0] + "_CNC " + infoCNC[0] + " " + infoCNC[1] + ".docx");
                    }
                    SQLConnector conn = new SQLConnector();
                    conn.CreateCommand("INSERT INTO [dbo].[esitiTest] ([Modello],[SN],[FW_Version],[BL_Version],[PCB_Version],[nFlow],[CNC],[anno],[dataTest],[esitotest],[note],[tipoTest],[nomeTester],[location])" + "VALUES ('" + collimatorName + "','" + SN + "','" + ProtocolAnalyzer().FwCollimator + "','" + ProtocolAnalyzer().FwBootloader + "','" + ProtocolAnalyzer().PcbCollimator + "','" + FC + "','" + CNC + "','" + Convert.ToString(DateTime.Now.Year) + "','" + DateTime.Now.ToString() + "','" + testPass + "','" + errorDescription + "','" + tipoTest + "','" + user.nome + " " + user.cognome + "','" + user.location + "')", null);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
