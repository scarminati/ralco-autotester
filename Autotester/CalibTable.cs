﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Autotester
{
    public class CalibTable
    {
        private int passiMassimi;
        private int numPuntiTaratura;
        private List<int> aperture = new List<int>();
        private List<int> passi = new List<int>();
        public int PassiMassimi { get { return this.passiMassimi; } set { this.passiMassimi = value; } }
        public int PuntiTaratura { get { return this.numPuntiTaratura; } set { this.numPuntiTaratura = value; } }
        public List<int> Aperture { get { return this.aperture; } set { this.aperture = value; } }
        public List<int> Passi { get { return this.passi; } set { this.passi = value; } }
    }
}
