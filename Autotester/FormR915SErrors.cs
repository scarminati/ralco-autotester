﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Autotester
{
    public partial class FormR915SErrors : Form
    {
        public FormR915SErrors()
        {
            InitializeComponent();
        }

        public void setErrors(R915SProtocolAnalyzer protocol)
        {

            ledPOST.On = protocol.POSTerror;
            ledBuffFull.On = protocol.EMCYBufferFull;
            ledBusOff.On = protocol.BUSOff;
            ledMemErr.On = protocol.MemoryError;
            ledOverVolt.On = protocol.OverVoltage;
            ledUnderVolt.On = protocol.UnderVoltage;
            ledTempErr.On = protocol.TempOutOfRange;
            ledCanOverRun.On = protocol.CANOverRun;
            ledErrMotFront.On = protocol.ErrMotFront;
            ledErrMotRight.On = protocol.ErrMotRight;
            ledErrMotRear.On = protocol.ErrMotRear;
            ledErrMotLeft.On = protocol.ErrMotLeft;
            ledErrMotFiltro.On = protocol.ErrMotFiltro;
            ledErrHomFront.On = protocol.ErrHomFront;
            ledErrHomRight.On = protocol.ErrHomRight;
            ledErrHomRear.On = protocol.ErrHomRear;
            ledErrHomLeft.On = protocol.ErrHomLeft;
            ledErrHomFiltro.On = protocol.ErrHomFiltro;
            ledErrLimHwFront.On = protocol.ErrLimHWFront;
            ledErrLimHwRight.On = protocol.ErrLimHWRight;
            ledErrLimHwRear.On = protocol.ErrLimHWRear;
            ledErrLimHwLeft.On = protocol.ErrLimHWLeft;
            ledErrLimHwFiltro.On = protocol.ErrLimHWFiltro;

            ledErr0.On = protocol.error0;
            ledErr90.On = protocol.error90;
            ledErr180.On = protocol.error180;
            ledErr270.On = protocol.error270;

            led_tLED_Alta.On = protocol.EVGCLED_tLedAlta;
            led_tGCLED_Alta.On = protocol.EVGCLED_tSchedaAlta;
            led_tastoPremPiuDi5sa.On = protocol.EVGCLED_tastoPremutoPiuDi5s;
            led_laserError.On = protocol.EVGCLED_laserError;
            led_fanError.On = protocol.EVGCLED_fanError;
            led_LED_cc.On = protocol.EVGCLED_LEDcc;
            led_LEDdisconnesso.On = protocol.EVGCLED_LEDdisconnesso;

            if (protocol.PassiMot1.Equals(-1))
            {
                lbl_passiFront.BackColor = Color.Red;
                lbl_passiFront.Text = protocol.PassiMot1.ToString();
            }
            else
            {
                if ((Math.Abs(50 - protocol.PassiMot1) > 50))
                    lbl_passiFront.BackColor = Color.Red;
                lbl_passiFront.Text = (Math.Abs(50 - protocol.PassiMot1)).ToString();
            }

            if (protocol.PassiMot2.Equals(-1))
            {
                lbl_passiLeft.BackColor = Color.Red;
                lbl_passiLeft.Text = protocol.PassiMot2.ToString();
            }
            else
            {
                if ((Math.Abs(50 - protocol.PassiMot2) > 50))
                    lbl_passiLeft.BackColor = Color.Red;
                lbl_passiLeft.Text = (Math.Abs(50 - protocol.PassiMot2)).ToString();
            }
            if (protocol.PassiMot3.Equals(-1))
            {
                lbl_passiRear.BackColor = Color.Red;
                lbl_passiRear.Text = protocol.PassiMot3.ToString();
            }
            else
            {
                if ((Math.Abs(50 - protocol.PassiMot3) > 50))
                    lbl_passiRear.BackColor = Color.Red;
                lbl_passiRear.Text = (Math.Abs(50 - protocol.PassiMot3)).ToString();
            }
            if (protocol.PassiMot4.Equals(-1))
            {
                lbl_passiRight.BackColor = Color.Red;
                lbl_passiRight.Text = protocol.PassiMot4.ToString();
            }
            else
            {
                if ((Math.Abs(50 - protocol.PassiMot4) > 50))
                    lbl_passiRight.BackColor = Color.Red;
                lbl_passiRight.Text = (Math.Abs(50 - protocol.PassiMot4)).ToString();
            }
        }
    }
}
