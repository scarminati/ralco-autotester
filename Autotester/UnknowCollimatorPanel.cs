﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autotester
{
    public partial class UnknowCollimatorPanel : UserControl, ICollimatorPanel
    {
        public UnknowCollimatorPanel()
        {
            InitializeComponent();
        }

        //public void setConnector(CANusb_Connector connector, string collName)
        //{

        //}

        public void setConnector(testCollimatore t)
        {

        }

        public void setFormRuota(FormControls formRuota)
        {

        }

        public void enableControl(ConfigurazioneTest cfg)
        {

        }

        public int getCicli()
        {
            return -1;
        }

        public void setCollimatorCode(string code)
        {
            lblCode.Text = code;
        }

        public IProtocolAnalyzer ProtocolAnalyzer()
        {
            return null;
        }

        public bool isManualTestCompleted()
        {
            return false;
        }

        public bool isChecked()
        {
            return false;
        }

        public bool hasErrors()
        {
            return true;
        }

        public int NumPanel { get; set; }

        public string collimatorType { get; set; }

        public string collimatorName { get; set; }

        public string SN { get; set; }

        public string FC { get; set; }

        public string CNC { get; set; }

        public string tipoTest { get; set; }

        public List<List<SingoloComando>> scripts { get; set; }

        public string pathScriptFile { get; set; }

        public void saveCalibTable() { }

        public void saveReport(Utente user)
        {

        }
    }
}
