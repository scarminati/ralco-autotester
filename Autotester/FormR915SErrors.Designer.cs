﻿namespace Autotester
{
    partial class FormR915SErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ledErr270 = new Autotester.LedBulb();
            this.ledErr180 = new Autotester.LedBulb();
            this.ledErr90 = new Autotester.LedBulb();
            this.ledErr0 = new Autotester.LedBulb();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ledPOST = new Autotester.LedBulb();
            this.label15 = new System.Windows.Forms.Label();
            this.ledBuffFull = new Autotester.LedBulb();
            this.label7 = new System.Windows.Forms.Label();
            this.ledBusOff = new Autotester.LedBulb();
            this.label10 = new System.Windows.Forms.Label();
            this.ledUnderVolt = new Autotester.LedBulb();
            this.label11 = new System.Windows.Forms.Label();
            this.ledOverVolt = new Autotester.LedBulb();
            this.label12 = new System.Windows.Forms.Label();
            this.ledTempErr = new Autotester.LedBulb();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.ledErrMotLeft = new Autotester.LedBulb();
            this.ledErrMotRear = new Autotester.LedBulb();
            this.ledErrMotRight = new Autotester.LedBulb();
            this.ledErrMotFront = new Autotester.LedBulb();
            this.label22 = new System.Windows.Forms.Label();
            this.ledErrMotFiltro = new Autotester.LedBulb();
            this.ledErrHomFiltro = new Autotester.LedBulb();
            this.ledErrHomLeft = new Autotester.LedBulb();
            this.ledErrHomRear = new Autotester.LedBulb();
            this.ledErrHomRight = new Autotester.LedBulb();
            this.ledErrHomFront = new Autotester.LedBulb();
            this.ledMemErr = new Autotester.LedBulb();
            this.label23 = new System.Windows.Forms.Label();
            this.ledCanOverRun = new Autotester.LedBulb();
            this.label25 = new System.Windows.Forms.Label();
            this.ledErrLimHwFiltro = new Autotester.LedBulb();
            this.ledErrLimHwLeft = new Autotester.LedBulb();
            this.ledErrLimHwRear = new Autotester.LedBulb();
            this.ledErrLimHwRight = new Autotester.LedBulb();
            this.ledErrLimHwFront = new Autotester.LedBulb();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.led_LED_cc = new Autotester.LedBulb();
            this.label27 = new System.Windows.Forms.Label();
            this.led_laserError = new Autotester.LedBulb();
            this.led_tastoPremPiuDi5sa = new Autotester.LedBulb();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.led_LEDdisconnesso = new Autotester.LedBulb();
            this.led_tLED_Alta = new Autotester.LedBulb();
            this.led_tGCLED_Alta = new Autotester.LedBulb();
            this.led_fanError = new Autotester.LedBulb();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl_passiLeft = new System.Windows.Forms.Label();
            this.lbl_passiRear = new System.Windows.Forms.Label();
            this.lbl_passiRight = new System.Windows.Forms.Label();
            this.lbl_passiFront = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ledErr270
            // 
            this.ledErr270.Color = System.Drawing.Color.Red;
            this.ledErr270.Location = new System.Drawing.Point(332, 39);
            this.ledErr270.Name = "ledErr270";
            this.ledErr270.On = true;
            this.ledErr270.Size = new System.Drawing.Size(23, 23);
            this.ledErr270.TabIndex = 57;
            this.ledErr270.Text = "ledBulb9";
            // 
            // ledErr180
            // 
            this.ledErr180.Color = System.Drawing.Color.Red;
            this.ledErr180.Location = new System.Drawing.Point(231, 39);
            this.ledErr180.Name = "ledErr180";
            this.ledErr180.On = true;
            this.ledErr180.Size = new System.Drawing.Size(23, 23);
            this.ledErr180.TabIndex = 56;
            this.ledErr180.Text = "ledBulb4";
            // 
            // ledErr90
            // 
            this.ledErr90.Color = System.Drawing.Color.Red;
            this.ledErr90.Location = new System.Drawing.Point(124, 39);
            this.ledErr90.Name = "ledErr90";
            this.ledErr90.On = true;
            this.ledErr90.Size = new System.Drawing.Size(23, 23);
            this.ledErr90.TabIndex = 55;
            this.ledErr90.Text = "ledBulb5";
            // 
            // ledErr0
            // 
            this.ledErr0.Color = System.Drawing.Color.Red;
            this.ledErr0.Location = new System.Drawing.Point(21, 39);
            this.ledErr0.Name = "ledErr0";
            this.ledErr0.On = true;
            this.ledErr0.Size = new System.Drawing.Size(23, 23);
            this.ledErr0.TabIndex = 54;
            this.ledErr0.Text = "ledBulb6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "0°";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "90°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "180°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(329, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 61;
            this.label5.Text = "270°";
            // 
            // ledPOST
            // 
            this.ledPOST.Color = System.Drawing.Color.Red;
            this.ledPOST.Location = new System.Drawing.Point(21, 56);
            this.ledPOST.Name = "ledPOST";
            this.ledPOST.On = true;
            this.ledPOST.Size = new System.Drawing.Size(23, 23);
            this.ledPOST.TabIndex = 67;
            this.ledPOST.Text = "ledBulb1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "POST";
            // 
            // ledBuffFull
            // 
            this.ledBuffFull.Color = System.Drawing.Color.Red;
            this.ledBuffFull.Location = new System.Drawing.Point(121, 55);
            this.ledBuffFull.Name = "ledBuffFull";
            this.ledBuffFull.On = true;
            this.ledBuffFull.Size = new System.Drawing.Size(23, 23);
            this.ledBuffFull.TabIndex = 75;
            this.ledBuffFull.Text = "ledBulb1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 26);
            this.label7.TabIndex = 74;
            this.label7.Text = "Buffer\r\nFull";
            // 
            // ledBusOff
            // 
            this.ledBusOff.Color = System.Drawing.Color.Red;
            this.ledBusOff.Location = new System.Drawing.Point(231, 56);
            this.ledBusOff.Name = "ledBusOff";
            this.ledBusOff.On = true;
            this.ledBusOff.Size = new System.Drawing.Size(23, 23);
            this.ledBusOff.TabIndex = 77;
            this.ledBusOff.Text = "ledBulb2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(233, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 26);
            this.label10.TabIndex = 76;
            this.label10.Text = "Bus\r\nOff";
            // 
            // ledUnderVolt
            // 
            this.ledUnderVolt.Color = System.Drawing.Color.Red;
            this.ledUnderVolt.Location = new System.Drawing.Point(124, 129);
            this.ledUnderVolt.Name = "ledUnderVolt";
            this.ledUnderVolt.On = true;
            this.ledUnderVolt.Size = new System.Drawing.Size(23, 23);
            this.ledUnderVolt.TabIndex = 81;
            this.ledUnderVolt.Text = "ledBulb3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(118, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 26);
            this.label11.TabIndex = 80;
            this.label11.Text = "Under\r\nVoltage";
            // 
            // ledOverVolt
            // 
            this.ledOverVolt.Color = System.Drawing.Color.Red;
            this.ledOverVolt.Location = new System.Drawing.Point(21, 130);
            this.ledOverVolt.Name = "ledOverVolt";
            this.ledOverVolt.On = true;
            this.ledOverVolt.Size = new System.Drawing.Size(23, 23);
            this.ledOverVolt.TabIndex = 79;
            this.ledOverVolt.Text = "ledBulb4";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 26);
            this.label12.TabIndex = 78;
            this.label12.Text = "Over\r\nVoltage";
            // 
            // ledTempErr
            // 
            this.ledTempErr.Color = System.Drawing.Color.Red;
            this.ledTempErr.Location = new System.Drawing.Point(231, 130);
            this.ledTempErr.Name = "ledTempErr";
            this.ledTempErr.On = true;
            this.ledTempErr.Size = new System.Drawing.Size(23, 23);
            this.ledTempErr.TabIndex = 83;
            this.ledTempErr.Text = "ledBulb5";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(229, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 26);
            this.label13.TabIndex = 82;
            this.label13.Text = "Temp\r\nError\r\n";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(14, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 84;
            this.label14.Text = "Errori Homing";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(17, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 13);
            this.label17.TabIndex = 85;
            this.label17.Text = "Errori Motore";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(282, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 13);
            this.label18.TabIndex = 93;
            this.label18.Text = "Left";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(227, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 92;
            this.label19.Text = "Rear";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(174, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 91;
            this.label20.Text = "Right";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(121, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 90;
            this.label21.Text = "Front";
            // 
            // ledErrMotLeft
            // 
            this.ledErrMotLeft.Color = System.Drawing.Color.Red;
            this.ledErrMotLeft.Location = new System.Drawing.Point(285, 40);
            this.ledErrMotLeft.Name = "ledErrMotLeft";
            this.ledErrMotLeft.On = true;
            this.ledErrMotLeft.Size = new System.Drawing.Size(23, 23);
            this.ledErrMotLeft.TabIndex = 89;
            this.ledErrMotLeft.Text = "ledBulb9";
            // 
            // ledErrMotRear
            // 
            this.ledErrMotRear.Color = System.Drawing.Color.Red;
            this.ledErrMotRear.Location = new System.Drawing.Point(231, 40);
            this.ledErrMotRear.Name = "ledErrMotRear";
            this.ledErrMotRear.On = true;
            this.ledErrMotRear.Size = new System.Drawing.Size(23, 23);
            this.ledErrMotRear.TabIndex = 88;
            this.ledErrMotRear.Text = "ledBulb4";
            // 
            // ledErrMotRight
            // 
            this.ledErrMotRight.Color = System.Drawing.Color.Red;
            this.ledErrMotRight.Location = new System.Drawing.Point(178, 40);
            this.ledErrMotRight.Name = "ledErrMotRight";
            this.ledErrMotRight.On = true;
            this.ledErrMotRight.Size = new System.Drawing.Size(23, 23);
            this.ledErrMotRight.TabIndex = 87;
            this.ledErrMotRight.Text = "ledBulb5";
            // 
            // ledErrMotFront
            // 
            this.ledErrMotFront.Color = System.Drawing.Color.Red;
            this.ledErrMotFront.Location = new System.Drawing.Point(126, 40);
            this.ledErrMotFront.Name = "ledErrMotFront";
            this.ledErrMotFront.On = true;
            this.ledErrMotFront.Size = new System.Drawing.Size(23, 23);
            this.ledErrMotFront.TabIndex = 86;
            this.ledErrMotFront.Text = "ledBulb6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(329, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 13);
            this.label22.TabIndex = 95;
            this.label22.Text = "Filtro";
            // 
            // ledErrMotFiltro
            // 
            this.ledErrMotFiltro.Color = System.Drawing.Color.Red;
            this.ledErrMotFiltro.Location = new System.Drawing.Point(332, 40);
            this.ledErrMotFiltro.Name = "ledErrMotFiltro";
            this.ledErrMotFiltro.On = true;
            this.ledErrMotFiltro.Size = new System.Drawing.Size(23, 23);
            this.ledErrMotFiltro.TabIndex = 94;
            this.ledErrMotFiltro.Text = "ledBulb9";
            // 
            // ledErrHomFiltro
            // 
            this.ledErrHomFiltro.Color = System.Drawing.Color.Red;
            this.ledErrHomFiltro.Location = new System.Drawing.Point(332, 78);
            this.ledErrHomFiltro.Name = "ledErrHomFiltro";
            this.ledErrHomFiltro.On = true;
            this.ledErrHomFiltro.Size = new System.Drawing.Size(23, 23);
            this.ledErrHomFiltro.TabIndex = 100;
            this.ledErrHomFiltro.Text = "ledBulb9";
            // 
            // ledErrHomLeft
            // 
            this.ledErrHomLeft.Color = System.Drawing.Color.Red;
            this.ledErrHomLeft.Location = new System.Drawing.Point(285, 78);
            this.ledErrHomLeft.Name = "ledErrHomLeft";
            this.ledErrHomLeft.On = true;
            this.ledErrHomLeft.Size = new System.Drawing.Size(23, 23);
            this.ledErrHomLeft.TabIndex = 99;
            this.ledErrHomLeft.Text = "ledBulb9";
            // 
            // ledErrHomRear
            // 
            this.ledErrHomRear.Color = System.Drawing.Color.Red;
            this.ledErrHomRear.Location = new System.Drawing.Point(231, 78);
            this.ledErrHomRear.Name = "ledErrHomRear";
            this.ledErrHomRear.On = true;
            this.ledErrHomRear.Size = new System.Drawing.Size(23, 23);
            this.ledErrHomRear.TabIndex = 98;
            this.ledErrHomRear.Text = "ledBulb4";
            // 
            // ledErrHomRight
            // 
            this.ledErrHomRight.Color = System.Drawing.Color.Red;
            this.ledErrHomRight.Location = new System.Drawing.Point(178, 78);
            this.ledErrHomRight.Name = "ledErrHomRight";
            this.ledErrHomRight.On = true;
            this.ledErrHomRight.Size = new System.Drawing.Size(23, 23);
            this.ledErrHomRight.TabIndex = 97;
            this.ledErrHomRight.Text = "ledBulb5";
            // 
            // ledErrHomFront
            // 
            this.ledErrHomFront.Color = System.Drawing.Color.Red;
            this.ledErrHomFront.Location = new System.Drawing.Point(126, 78);
            this.ledErrHomFront.Name = "ledErrHomFront";
            this.ledErrHomFront.On = true;
            this.ledErrHomFront.Size = new System.Drawing.Size(23, 23);
            this.ledErrHomFront.TabIndex = 96;
            this.ledErrHomFront.Text = "ledBulb6";
            // 
            // ledMemErr
            // 
            this.ledMemErr.Color = System.Drawing.Color.Red;
            this.ledMemErr.Location = new System.Drawing.Point(333, 55);
            this.ledMemErr.Name = "ledMemErr";
            this.ledMemErr.On = true;
            this.ledMemErr.Size = new System.Drawing.Size(23, 23);
            this.ledMemErr.TabIndex = 102;
            this.ledMemErr.Text = "ledBulb16";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(328, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 26);
            this.label23.TabIndex = 101;
            this.label23.Text = "Memory\r\nError";
            // 
            // ledCanOverRun
            // 
            this.ledCanOverRun.Color = System.Drawing.Color.Red;
            this.ledCanOverRun.Location = new System.Drawing.Point(332, 129);
            this.ledCanOverRun.Name = "ledCanOverRun";
            this.ledCanOverRun.On = true;
            this.ledCanOverRun.Size = new System.Drawing.Size(23, 23);
            this.ledCanOverRun.TabIndex = 104;
            this.ledCanOverRun.Text = "ledBulb17";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(325, 91);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 26);
            this.label25.TabIndex = 103;
            this.label25.Text = "CAN\r\nOverrun";
            // 
            // ledErrLimHwFiltro
            // 
            this.ledErrLimHwFiltro.Color = System.Drawing.Color.Red;
            this.ledErrLimHwFiltro.Location = new System.Drawing.Point(332, 119);
            this.ledErrLimHwFiltro.Name = "ledErrLimHwFiltro";
            this.ledErrLimHwFiltro.On = true;
            this.ledErrLimHwFiltro.Size = new System.Drawing.Size(23, 23);
            this.ledErrLimHwFiltro.TabIndex = 110;
            this.ledErrLimHwFiltro.Text = "ledBulb9";
            // 
            // ledErrLimHwLeft
            // 
            this.ledErrLimHwLeft.Color = System.Drawing.Color.Red;
            this.ledErrLimHwLeft.Location = new System.Drawing.Point(285, 119);
            this.ledErrLimHwLeft.Name = "ledErrLimHwLeft";
            this.ledErrLimHwLeft.On = true;
            this.ledErrLimHwLeft.Size = new System.Drawing.Size(23, 23);
            this.ledErrLimHwLeft.TabIndex = 109;
            this.ledErrLimHwLeft.Text = "ledBulb9";
            // 
            // ledErrLimHwRear
            // 
            this.ledErrLimHwRear.Color = System.Drawing.Color.Red;
            this.ledErrLimHwRear.Location = new System.Drawing.Point(231, 119);
            this.ledErrLimHwRear.Name = "ledErrLimHwRear";
            this.ledErrLimHwRear.On = true;
            this.ledErrLimHwRear.Size = new System.Drawing.Size(23, 23);
            this.ledErrLimHwRear.TabIndex = 108;
            this.ledErrLimHwRear.Text = "ledBulb4";
            // 
            // ledErrLimHwRight
            // 
            this.ledErrLimHwRight.Color = System.Drawing.Color.Red;
            this.ledErrLimHwRight.Location = new System.Drawing.Point(178, 119);
            this.ledErrLimHwRight.Name = "ledErrLimHwRight";
            this.ledErrLimHwRight.On = true;
            this.ledErrLimHwRight.Size = new System.Drawing.Size(23, 23);
            this.ledErrLimHwRight.TabIndex = 107;
            this.ledErrLimHwRight.Text = "ledBulb5";
            // 
            // ledErrLimHwFront
            // 
            this.ledErrLimHwFront.Color = System.Drawing.Color.Red;
            this.ledErrLimHwFront.Location = new System.Drawing.Point(126, 119);
            this.ledErrLimHwFront.Name = "ledErrLimHwFront";
            this.ledErrLimHwFront.On = true;
            this.ledErrLimHwFront.Size = new System.Drawing.Size(23, 23);
            this.ledErrLimHwFront.TabIndex = 106;
            this.ledErrLimHwFront.Text = "ledBulb6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 105;
            this.label6.Text = "Errori Limiti HW";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.ledErrLimHwFiltro);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.ledErrMotFront);
            this.groupBox1.Controls.Add(this.ledErrLimHwLeft);
            this.groupBox1.Controls.Add(this.ledErrMotRight);
            this.groupBox1.Controls.Add(this.ledErrLimHwRear);
            this.groupBox1.Controls.Add(this.lbl_passiLeft);
            this.groupBox1.Controls.Add(this.ledErrMotRear);
            this.groupBox1.Controls.Add(this.lbl_passiRear);
            this.groupBox1.Controls.Add(this.ledErrLimHwRight);
            this.groupBox1.Controls.Add(this.lbl_passiRight);
            this.groupBox1.Controls.Add(this.ledErrMotLeft);
            this.groupBox1.Controls.Add(this.lbl_passiFront);
            this.groupBox1.Controls.Add(this.ledErrLimHwFront);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.ledErrHomFiltro);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.ledErrHomLeft);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.ledErrHomRear);
            this.groupBox1.Controls.Add(this.ledErrMotFiltro);
            this.groupBox1.Controls.Add(this.ledErrHomRight);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.ledErrHomFront);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 189);
            this.groupBox1.TabIndex = 111;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allarmi Motore";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ledErr270);
            this.groupBox2.Controls.Add(this.ledErr0);
            this.groupBox2.Controls.Add(this.ledErr90);
            this.groupBox2.Controls.Add(this.ledErr180);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 501);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(374, 73);
            this.groupBox2.TabIndex = 112;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rotazione Collimatore";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ledCanOverRun);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.ledPOST);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.ledBuffFull);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.ledBusOff);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.ledOverVolt);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.ledUnderVolt);
            this.groupBox3.Controls.Add(this.ledMemErr);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.ledTempErr);
            this.groupBox3.Location = new System.Drawing.Point(12, 337);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(374, 158);
            this.groupBox3.TabIndex = 112;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Allarmi Vari";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.led_LED_cc);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.led_laserError);
            this.groupBox4.Controls.Add(this.led_tastoPremPiuDi5sa);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.led_LEDdisconnesso);
            this.groupBox4.Controls.Add(this.led_tLED_Alta);
            this.groupBox4.Controls.Add(this.led_tGCLED_Alta);
            this.groupBox4.Controls.Add(this.led_fanError);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(12, 205);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(374, 126);
            this.groupBox4.TabIndex = 113;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Errori GC-LED 4A";
            // 
            // led_LED_cc
            // 
            this.led_LED_cc.Color = System.Drawing.Color.Red;
            this.led_LED_cc.Location = new System.Drawing.Point(255, 87);
            this.led_LED_cc.Name = "led_LED_cc";
            this.led_LED_cc.On = true;
            this.led_LED_cc.Size = new System.Drawing.Size(23, 23);
            this.led_LED_cc.TabIndex = 66;
            this.led_LED_cc.Text = "ledBulb9";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(252, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 52);
            this.label27.TabIndex = 67;
            this.label27.Text = "LED\r\nin\r\nCorto\r\nCircuito";
            // 
            // led_laserError
            // 
            this.led_laserError.Color = System.Drawing.Color.Red;
            this.led_laserError.Location = new System.Drawing.Point(158, 87);
            this.led_laserError.Name = "led_laserError";
            this.led_laserError.On = true;
            this.led_laserError.Size = new System.Drawing.Size(23, 23);
            this.led_laserError.TabIndex = 63;
            this.led_laserError.Text = "ledBulb9";
            // 
            // led_tastoPremPiuDi5sa
            // 
            this.led_tastoPremPiuDi5sa.Color = System.Drawing.Color.Red;
            this.led_tastoPremPiuDi5sa.Location = new System.Drawing.Point(102, 87);
            this.led_tastoPremPiuDi5sa.Name = "led_tastoPremPiuDi5sa";
            this.led_tastoPremPiuDi5sa.On = true;
            this.led_tastoPremPiuDi5sa.Size = new System.Drawing.Size(23, 23);
            this.led_tastoPremPiuDi5sa.TabIndex = 62;
            this.led_tastoPremPiuDi5sa.Text = "ledBulb6";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(102, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 52);
            this.label24.TabIndex = 64;
            this.label24.Text = "Tasto\r\nPremuto\r\npiù di\r\n5s";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(155, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 39);
            this.label26.TabIndex = 65;
            this.label26.Text = "Laser\r\nIn \r\nErrore";
            // 
            // led_LEDdisconnesso
            // 
            this.led_LEDdisconnesso.Color = System.Drawing.Color.Red;
            this.led_LEDdisconnesso.Location = new System.Drawing.Point(304, 87);
            this.led_LEDdisconnesso.Name = "led_LEDdisconnesso";
            this.led_LEDdisconnesso.On = true;
            this.led_LEDdisconnesso.Size = new System.Drawing.Size(23, 23);
            this.led_LEDdisconnesso.TabIndex = 57;
            this.led_LEDdisconnesso.Text = "ledBulb9";
            // 
            // led_tLED_Alta
            // 
            this.led_tLED_Alta.Color = System.Drawing.Color.Red;
            this.led_tLED_Alta.Location = new System.Drawing.Point(9, 87);
            this.led_tLED_Alta.Name = "led_tLED_Alta";
            this.led_tLED_Alta.On = true;
            this.led_tLED_Alta.Size = new System.Drawing.Size(23, 23);
            this.led_tLED_Alta.TabIndex = 54;
            this.led_tLED_Alta.Text = "ledBulb6";
            // 
            // led_tGCLED_Alta
            // 
            this.led_tGCLED_Alta.Color = System.Drawing.Color.Red;
            this.led_tGCLED_Alta.Location = new System.Drawing.Point(55, 87);
            this.led_tGCLED_Alta.Name = "led_tGCLED_Alta";
            this.led_tGCLED_Alta.On = true;
            this.led_tGCLED_Alta.Size = new System.Drawing.Size(23, 23);
            this.led_tGCLED_Alta.TabIndex = 55;
            this.led_tGCLED_Alta.Text = "ledBulb5";
            // 
            // led_fanError
            // 
            this.led_fanError.Color = System.Drawing.Color.Red;
            this.led_fanError.Location = new System.Drawing.Point(204, 87);
            this.led_fanError.Name = "led_fanError";
            this.led_fanError.On = true;
            this.led_fanError.Size = new System.Drawing.Size(23, 23);
            this.led_fanError.TabIndex = 56;
            this.led_fanError.Text = "ledBulb4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 52);
            this.label1.TabIndex = 58;
            this.label1.Text = "Temp \r\nLED\r\nTroppo \r\nAlta\r\n";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 52);
            this.label8.TabIndex = 59;
            this.label8.Text = "Temp\r\nScheda\r\nTroppo\r\nAlta";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(201, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 39);
            this.label9.TabIndex = 60;
            this.label9.Text = "Ventola\r\nIn\r\nErrore";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(301, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 52);
            this.label16.TabIndex = 61;
            this.label16.Text = "LED\r\nDisconnesso\r\no Driver\r\nFault";
            // 
            // lbl_passiLeft
            // 
            this.lbl_passiLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiLeft.Location = new System.Drawing.Point(276, 159);
            this.lbl_passiLeft.Name = "lbl_passiLeft";
            this.lbl_passiLeft.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiLeft.TabIndex = 83;
            // 
            // lbl_passiRear
            // 
            this.lbl_passiRear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiRear.Location = new System.Drawing.Point(222, 159);
            this.lbl_passiRear.Name = "lbl_passiRear";
            this.lbl_passiRear.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiRear.TabIndex = 82;
            // 
            // lbl_passiRight
            // 
            this.lbl_passiRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiRight.Location = new System.Drawing.Point(168, 158);
            this.lbl_passiRight.Name = "lbl_passiRight";
            this.lbl_passiRight.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiRight.TabIndex = 81;
            // 
            // lbl_passiFront
            // 
            this.lbl_passiFront.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_passiFront.Location = new System.Drawing.Point(115, 158);
            this.lbl_passiFront.Name = "lbl_passiFront";
            this.lbl_passiFront.Size = new System.Drawing.Size(40, 19);
            this.lbl_passiFront.TabIndex = 80;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(31, 160);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 13);
            this.label28.TabIndex = 111;
            this.label28.Text = "Test Passi";
            // 
            // FormR915SErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 581);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(413, 619);
            this.MinimumSize = new System.Drawing.Size(413, 619);
            this.Name = "FormR915SErrors";
            this.Text = "R915S Errori";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private LedBulb ledErr270;
        private LedBulb ledErr180;
        private LedBulb ledErr90;
        private LedBulb ledErr0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private LedBulb ledPOST;
        private System.Windows.Forms.Label label15;
        private LedBulb ledBuffFull;
        private System.Windows.Forms.Label label7;
        private LedBulb ledBusOff;
        private System.Windows.Forms.Label label10;
        private LedBulb ledUnderVolt;
        private System.Windows.Forms.Label label11;
        private LedBulb ledOverVolt;
        private System.Windows.Forms.Label label12;
        private LedBulb ledTempErr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private LedBulb ledErrMotLeft;
        private LedBulb ledErrMotRear;
        private LedBulb ledErrMotRight;
        private LedBulb ledErrMotFront;
        private System.Windows.Forms.Label label22;
        private LedBulb ledErrMotFiltro;
        private LedBulb ledErrHomFiltro;
        private LedBulb ledErrHomLeft;
        private LedBulb ledErrHomRear;
        private LedBulb ledErrHomRight;
        private LedBulb ledErrHomFront;
        private LedBulb ledMemErr;
        private System.Windows.Forms.Label label23;
        private LedBulb ledCanOverRun;
        private System.Windows.Forms.Label label25;
        private LedBulb ledErrLimHwFiltro;
        private LedBulb ledErrLimHwLeft;
        private LedBulb ledErrLimHwRear;
        private LedBulb ledErrLimHwRight;
        private LedBulb ledErrLimHwFront;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private LedBulb led_LED_cc;
        private System.Windows.Forms.Label label27;
        private LedBulb led_laserError;
        private LedBulb led_tastoPremPiuDi5sa;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private LedBulb led_LEDdisconnesso;
        private LedBulb led_tLED_Alta;
        private LedBulb led_tGCLED_Alta;
        private LedBulb led_fanError;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lbl_passiLeft;
        private System.Windows.Forms.Label lbl_passiRear;
        private System.Windows.Forms.Label lbl_passiRight;
        private System.Windows.Forms.Label lbl_passiFront;
    }
}