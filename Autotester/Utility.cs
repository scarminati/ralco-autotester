﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Configuration;

namespace Autotester
{
    static class Utility
    {
        /*Definizione Protocolli usati*/
        public const int PROTOCOL_CAN_STEP = 1;

        /*Definizione Operazioni Effettuate*/
        public const int OPERATION_MOVE = 1;
        public const int OPERATION_RESET = 2;

        public static void visualizeSwInformation()
        {
            try
            {
                MessageBox.Show("********************************************************\n" +
                                "*********************AUTOTESTER*********************\n" +
                                "********************************************************\n" +
                                "________________________________________________________\n" +
                                "|                                                                                            |\n" +
                                "| PANEL NAME           VERSION              LAST UPDATE |\n" +
                                "| CANSystemPanel      V1.00.00             02/05/2019       |\n" +
                                "| P225Panel                   V1.00.00             02/05/2019       |\n" +
                                "| R225Panel                   V1.00.00             02/05/2019       |\n" +
                                "| R915Panel                   V1.00.01             03/05/2019       |\n" +
                                "| S605Panel                   V1.00.00             02/05/2019       |\n" +
                                "|_______________________________________________________|\n" +
                                "\n" +
                                "********************************************************"
                , "Autotester Information Panel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string getTemplateFileName(string collName)
        {
            string[] pattern = { "/", " " };
            Regex rgx = new Regex(pattern[0]);
            string[] replacement = { "_" };
            string line = collName;
            for (int i = 0; i < pattern.Length; i++)
            {
                rgx = new Regex(pattern[i]);
                line = rgx.Replace(line, replacement[0]);
            }
            return (line);
        }

        public static string searchPathDigitalSignatureOperator(Utente usr)
        {
            string filePathFirma = "";
            string[] nomeFileFirma = new string[] { (usr.nome + usr.cognome).ToLower(), (usr.cognome + usr.nome).ToLower() };
            string[] listaFirmeDigitali = Directory.GetFiles(@"\\ralcosrv2\Public\Firme");
            ////faccio i lowercase della dei nomi dei file delle firme
            listaFirmeDigitali = listaFirmeDigitali.Select(f => f.ToLowerInvariant()).ToArray();
            for (int j = 0; j < nomeFileFirma.Length; j++)
            {
                for (int i = 0; i < listaFirmeDigitali.Length; i++)
                {
                    if (listaFirmeDigitali[i].Contains(nomeFileFirma[j]))
                    {
                        filePathFirma = filePathFirma + listaFirmeDigitali[i];
                        break;
                    }
                }
                if (!filePathFirma.Equals(""))
                    break;
            }
            return filePathFirma;
        }

        public static List<List<SingoloComando>> loadScripts(IProtocolAnalyzer protocol, ConfigurazioneTest testConfig)
        {
            try
            {
                //string pathScriptFile = Environment.CurrentDirectory + @"\script\" + testConfig.nomeCollim.Replace("/", "_").Replace(" ", "_") + @"\scripts.txt";
                string pathScriptFile = "";
                if (ConfigurationManager.AppSettings["LOCAL_TEST"].Equals("false"))
                    pathScriptFile = ConfigurationManager.AppSettings["SCRIPT_PATH"] + @"\" + testConfig.nomeCollim.Replace("/", "_").Replace(" ", "_") + @"\scripts.txt";
                else
                    pathScriptFile = Environment.CurrentDirectory + @"\script\" + testConfig.nomeCollim.Replace("/", "_").Replace(" ", "_") + @"\scripts.txt";
                if (File.Exists(pathScriptFile))
                {
                    List<List<SingoloComando>> scripts = new List<List<SingoloComando>>();
                    //carico il file di script
                    String[] scriptsFileName = File.ReadAllLines(Path.GetFullPath(pathScriptFile));
                    bool ok = true;
                    foreach (string s in scriptsFileName)
                        if (!s.Contains(".txt"))
                        {
                            ok = false;
                            break;
                        }
                    if (ok.Equals(true))
                    {
                        foreach (string script in scriptsFileName)
                        {
                            List<SingoloComando> elenco = readScriptFile(Path.GetFullPath(pathScriptFile).Replace(Path.GetFileName(pathScriptFile), script), protocol, testConfig, true);
                            if (elenco.Count > 0)
                            {
                                scripts.Add(elenco);
                            }
                        }
                        return (scripts);
                    }
                    else
                    {
                        MessageBox.Show("Il file caricato non contiene una lista di script validi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return null;
                    }
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private static List<SingoloComando> readScriptFile(string filename, IProtocolAnalyzer protocol, ConfigurazioneTest testConfig, bool readIncludes)
        {
            String[] lines;
            String row;

            lines = File.ReadAllLines(filename);
            List<SingoloComando> elenco = new List<SingoloComando>();
            SingoloComando msg = null;
            String data;

            foreach (string s in lines)
            {
                // # = commento
                if (s.StartsWith("#"))
                    continue;

                if (s.StartsWith(">"))
                {
                    row = s.Trim().ToUpper();
                    if (!row.StartsWith(">INIT"))
                    {
                        msg = new SingoloComando();

                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            int start = row.IndexOf(':');
                            int end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);

                            msg.id = (uint)Int16.Parse(row.Substring(0, start), NumberStyles.AllowHexSpecifier);

                            String[] dati = data.Split(',');
                            msg.dlc = 0;
                            for (int i = 0; i < dati.Length; i++)
                            {
                                if (i == 8)
                                    break;

                                msg.dati[i] = (Byte)Int16.Parse(dati[i], NumberStyles.AllowHexSpecifier);
                                msg.dlc++;
                            }
                            //msg.dlc = dati.Length;

                            if (end != row.Length)
                            {
                                if (row.IndexOf('@') != -1)
                                {
                                    data = row.Substring(end + 1, (row.Length - end) - 2);
                                    msg.decrement = true;
                                }
                                else
                                {
                                    data = row.Substring(end + 1);
                                }

                                msg.pause = Int32.Parse(data);
                            }
                            elenco.Add(msg);
                        }

                    }
                }
                else
                {
                    CANCommand.SplitUlong val;
                    byte dato;
                    msg = null;
                    int start = 0;
                    int end = 0;
                    row = s.Trim().ToUpper();
                    // interprete comandi
                    // A = Apertura  
                    //              -> CAN-CAL, CAN-BUS: cross, long : pausa
                    //              -> CAN-OPEN : posizione, rotazione : pausa (spatial filter)
                    // M = Movimentazione
                    //              -> CANSYSTEM : M0 -> Per messaggi con id 0x7A0
                    //              -> CANSYSTEM : M1 -> Per messaggi con id 0x7A0
                    //              -> CANSYSTEM : M2 -> Per messaggi con id 0x7A0
                    //              -> posMot1,posMot2,posMot3,posMot4 : tempo
                    // F = Filtro -> filtro : pausa
                    // L = Luce  -> off(0)/on(1)/toggle(2),tempo : pausa
                    // O = fOcal Spot  -> focalSpot : pausa
                    // I = Iride -> iride : pausa
                    // E = lasEr
                    // R = Reset
                    // S = Start
                    // C_A = Calibrazione Apertura -> lamella, punto di taratura, apertura : pausa
                    // C_P = Calibrazione Passi -> lamella, punto di taratura, passi : pausa
                    if (row.StartsWith("A"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {

                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x006);
                                msg.dlc = 8;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.d3 = 0;

                                val.data = (uint)(Int32.Parse(dati[0]));
                                val.data |= (uint)(Int32.Parse(dati[1]) << 10);
                                msg.dati[0] = 0x01;
                                msg.dati[1] = val.d0;
                                msg.dati[2] = val.d1;
                                msg.dati[3] = val.d2;
                                msg.dati[4] = val.d3;
                                msg.dati[5] = 0xFF;
                                msg.dati[6] = 0xFF;
                                msg.dati[7] = 0xFE;
                            }
                            else if (protocol.protocolName.Equals("CAN-BUS"))
                            {
                                if (testConfig.nomeCollim.Equals("R 221/237/ACS DHHS") || testConfig.nomeCollim.Equals("R 221/237G/ACS DHHS"))
                                {
                                    val.data = (uint)(Int32.Parse(dati[0]));
                                    val.data |= (uint)(Int32.Parse(dati[1]) << 16);
                                    //if (val.data.Equals(32768500))
                                    if (val.data <= 1310740)
                                        msg.dati[0] = 0x14;
                                    else
                                        msg.dati[0] = 0x28;
                                    msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x000);
                                    msg.dlc = 8;
                                    msg.dati[1] = 0x00;
                                    msg.dati[2] = 0x00;
                                    msg.dati[3] = 0x00;
                                    msg.dati[4] = 0x00;
                                    msg.dati[5] = 0x00;
                                    msg.dati[6] = 0x00;
                                    msg.dati[7] = 0x00;

                                }
                                else
                                {
                                    msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x000);
                                    msg.dlc = 8;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.d2 = 0;
                                    val.d3 = 0;

                                    val.data = (uint)(Int32.Parse(dati[0]));
                                    val.data |= (uint)(Int32.Parse(dati[1]) << 16);
                                    msg.dati[0] = 0x80;
                                    msg.dati[1] = 0x00;
                                    msg.dati[2] = 0x00;
                                    msg.dati[3] = 0x64;
                                    msg.dati[4] = val.d1;
                                    msg.dati[5] = val.d0;
                                    msg.dati[6] = val.d3;
                                    msg.dati[7] = val.d2;
                                }
                            }
                            //al momento uso solo i parametri del siemens S605 DASM DHHS; nel caso, usare la variabile collimatorName passata al metodo per differenziare caso per caso.
                            else if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                if (testConfig.nomeCollim.Equals("S 605 DASM DHHS") || testConfig.nomeCollim.Equals("S 605/146/DASM DHHS"))
                                {
                                    msg.id = 0x441;
                                    msg.dlc = 5;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.d2 = 0;
                                    val.d3 = 0;

                                    val.data = (uint)(Int32.Parse(dati[0]));
                                    val.data |= (uint)(Int32.Parse(dati[1]) << 16);
                                    msg.dati[0] = 0x11;
                                    msg.dati[1] = val.d0;
                                    msg.dati[2] = val.d1;
                                    msg.dati[3] = val.d2;
                                    msg.dati[4] = val.d3;
                                    msg.dati[5] = 0x00;
                                    msg.dati[6] = 0x00;
                                    msg.dati[7] = 0x00;
                                }
                                else if (testConfig.nomeCollim.Contains("R 915 S DHHS"))
                                {
                                    msg.id = 0x207;
                                    msg.dlc = 8;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.d2 = 0;
                                    val.d3 = 0;
                                    val.d4 = 0;
                                    val.d5 = 0;
                                    val.d6 = 0;
                                    val.d7 = 0;

                                    val.data = (UInt64)(Int64.Parse(dati[0]) & 0xFFFF);
                                    val.data |= (UInt64)((Int64.Parse(dati[1]) & 0xFFFF) << 16);
                                    val.data |= (UInt64)((Int64.Parse(dati[2]) & 0xFFFF) << 32);
                                    val.data |= (UInt64)((Int64.Parse(dati[3]) & 0xFFFF) << 48);
                                    msg.dati[0] = val.d0;
                                    msg.dati[1] = val.d1;
                                    msg.dati[2] = val.d2;
                                    msg.dati[3] = val.d3;
                                    msg.dati[4] = val.d4;
                                    msg.dati[5] = val.d5;
                                    msg.dati[6] = val.d6;
                                    msg.dati[7] = val.d7;
                                }
                            }
                        }
                    }

                    if (row.StartsWith("M"))
                    {
                        msg = new SingoloComando();
                        if (row.StartsWith("M0"))
                            msg.id = 0x7A0;
                        else if (row.StartsWith("M1"))
                            msg.id = 0x7A1;
                        else if (row.StartsWith("M2"))
                            msg.id = 0x7A2;
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CANSYSTEM"))
                            {
                                //if (testConfig.nomeCollim.Equals("R 915 S DHHS"))
                                //{
                                //    msg.id = 0x207;
                                msg.dlc = 8;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.d3 = 0;
                                val.d4 = 0;
                                val.d5 = 0;
                                val.d6 = 0;
                                val.d7 = 0;

                                val.data = (UInt64)(Int64.Parse(dati[0]) & 0xFFFF);
                                val.data |= (UInt64)((Int64.Parse(dati[1]) & 0xFFFF) << 16);
                                val.data |= (UInt64)((Int64.Parse(dati[2]) & 0xFFFF) << 32);
                                val.data |= (UInt64)((Int64.Parse(dati[3]) & 0xFFFF) << 48);
                                msg.dati[0] = val.d1;
                                msg.dati[1] = val.d0;
                                msg.dati[2] = val.d3;
                                msg.dati[3] = val.d2;
                                msg.dati[4] = val.d5;
                                msg.dati[5] = val.d4;
                                msg.dati[6] = val.d7;
                                msg.dati[7] = val.d6;
                                //}
                            }
                            else if (protocol.protocolName.Equals("CANSYSTEM_PHILIPS"))
                            {
                                //if (testConfig.nomeCollim.Equals("R 915 S DHHS"))
                                //{
                                //    msg.id = 0x207;
                                msg.dlc = 8;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.d3 = 0;
                                val.d4 = 0;
                                val.d5 = 0;
                                val.d6 = 0;
                                val.d7 = 0;

                                val.data = (UInt64)(Int64.Parse(dati[0]) | 0x8000);
                                val.data |= (UInt64)((Int64.Parse(dati[1]) | 0x8000) << 16);
                                val.data |= (UInt64)((Int64.Parse(dati[2]) | 0x8000) << 32);
                                val.data |= (UInt64)((Int64.Parse(dati[3]) | 0x8000) << 48);
                                msg.dati[0] = val.d1;
                                msg.dati[1] = val.d0;
                                msg.dati[2] = val.d3;
                                msg.dati[3] = val.d2;
                                msg.dati[4] = val.d5;
                                msg.dati[5] = val.d4;
                                msg.dati[6] = val.d7;
                                msg.dati[7] = val.d6;
                                //}
                            }
                        }
                    }
                    if (row.StartsWith("CV"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CANSYSTEM") || protocol.protocolName.Equals("CANSYSTEM_EOS"))
                            {
                                //if (testConfig.nomeCollim.Equals("R 915 S DHHS"))
                                //{
                                //    msg.id = 0x207;
                                msg.id = 0x7A3;
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x03;
                                msg.dati[1] = (byte)Int16.Parse(dati[0]);
                            }
                        }
                    }
                    if (row.StartsWith("SD"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CANSYSTEM") || protocol.protocolName.Equals("CANSYSTEM_EOS"))
                            {
                                msg.id = 0x7A3;
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x11;
                            }
                        }
                    }
                    if (row.StartsWith("RC"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CANSYSTEM") || protocol.protocolName.Equals("CANSYSTEM_EOS"))
                            {
                                msg.id = 0x7A3;
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x55;
                            }
                        }
                    }
                    else if (row.StartsWith("F"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x006);
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x20;
                                msg.dati[1] = (byte)Int16.Parse(dati[0]);
                            }
                            else if (protocol.protocolName.Equals("CAN-BUS"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x003);
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x01;
                                msg.dati[1] = (byte)Int16.Parse(dati[0]);
                            }
                            else if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                if (testConfig.nomeCollim.Contains("R 915 S DHHS"))
                                {
                                    msg.id = 0x307;
                                    msg.dlc = 1;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.data = (uint)(Int16.Parse(dati[0]));
                                    msg.dati[0] = val.d0;
                                    msg.dati[1] = val.d1;
                                }
                            }
                        }
                    }
                    else if (row.StartsWith("L"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            dato = (byte)Int16.Parse(dati[0]);

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x006);
                                msg.dlc = 8;
                                if (dato == 0 || dato == 1)
                                {
                                    msg.dati[0] = 0x31;
                                    msg.dati[1] = dato;

                                }
                                else
                                {
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.data = (ulong)Int32.Parse(dati[1]);
                                    msg.dati[0] = 0x32;
                                    msg.dati[1] = val.d0;
                                    msg.dati[2] = val.d1;
                                    elenco.Add(msg);

                                    msg = new SingoloComando();
                                    msg.id = 0x1C6;
                                    msg.dlc = 8;
                                    msg.dati[0] = 0x30;

                                }
                            }

                            else if (protocol.protocolName.Equals("CAN-BUS"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x003);
                                msg.dlc = 8;
                                if (dato == 0 || dato == 1)
                                {
                                    msg.dati[0] = 0x08;
                                    msg.dati[1] = dato;

                                }
                                else
                                {
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.data = (ulong)Int32.Parse(dati[1]);
                                    msg.dati[0] = 0x14;
                                    msg.dati[1] = val.d0;
                                }
                            }

                            else if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                if (testConfig.nomeCollim.Contains("R 915 S DHHS"))
                                {
                                    msg.id = 0x407;
                                    msg.dlc = 5;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.data = (ulong)Int32.Parse(dati[0]);
                                    msg.dati[4] = val.d0;
                                }
                            }
                            else if (protocol.protocolName.Equals("CANSYSTEM"))
                            {
                                msg.id = 0x7A3;
                                msg.dlc = 8;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.data = (UInt64)(Int16.Parse(dati[0]) * 0xFFFF);
                                msg.dati[0] = 0x05;
                                msg.dati[1] = val.d1;
                                msg.dati[2] = val.d0;
                                msg.dati[3] = 0x00;
                                msg.dati[4] = 0x00;
                                msg.dati[5] = 0x00;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                            }

                        }
                    }
                    else if (row.StartsWith("O"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');
                            if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                if (testConfig.nomeCollim.Contains("R 915 S DHHS"))
                                {
                                    msg.id = 0x507;
                                    msg.dlc = 1;
                                    val.data = 0;
                                    val.d0 = 0;
                                    val.d1 = 0;
                                    val.data = (uint)(Int16.Parse(dati[0]));
                                    msg.dati[0] = val.d0;
                                    msg.dati[1] = val.d1;
                                }
                            }
                        }
                    }
                    else if (row.StartsWith("I"))
                    {

                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');
                            //al momento uso solo i parametri del siemens S605 DASM DHHS; nel caso, usare la variabile collimatorName passata al metodo per differenziare caso per caso.
                            if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                msg.id = 0x241;
                                msg.dlc = 3;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.data = (uint)(Int32.Parse(dati[0]));
                                msg.dati[0] = 0x11;
                                msg.dati[1] = val.d0;
                                msg.dati[2] = val.d1;
                            }
                            else if (protocol.protocolName.Equals("CAN-BUS"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x000);
                                msg.dlc = 6;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;

                                val.data = (uint)(Int32.Parse(dati[0]));
                                val.data |= (uint)(Int32.Parse(dati[0]) << 16);
                                msg.dati[0] = 0x01;
                                msg.dati[1] = val.d1;
                                msg.dati[2] = val.d0;
                                msg.dati[3] = 0x00;
                                msg.dati[4] = 0x00;
                                msg.dati[5] = 0x00;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                            }
                        }
                    }
                    else if (row.StartsWith("R"))
                    {

                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');
                            //al momento uso solo i parametri del siemens S605 DASM DHHS; nel caso, usare la variabile collimatorName passata al metodo per differenziare caso per caso.
                            if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                msg.id = 0x000;
                                msg.dlc = 2;
                                val.data = 0;
                                val.d0 = 0;
                                val.data = (uint)(Int32.Parse(dati[0]));
                                msg.dati[0] = val.d0;
                                msg.dati[1] = 0x40;
                                msg.dati[2] = 0x00;
                                msg.dati[3] = 0x00;
                                msg.dati[4] = 0x00;
                                msg.dati[5] = 0x00;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                            }
                        }
                    }
                    else if (row.StartsWith("S"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');
                            //al momento uso solo i parametri del siemens S605 DASM DHHS; nel caso, usare la variabile collimatorName passata al metodo per differenziare caso per caso.
                            if (protocol.protocolName.Equals("CAN-OPEN"))
                            {
                                msg.id = 0x000;
                                msg.dlc = 2;
                                val.data = 0;
                                val.d0 = 0;
                                val.data = (uint)(Int32.Parse(dati[0]));
                                msg.dati[0] = val.d0;
                                msg.dati[1] = 0x40;
                                msg.dati[2] = 0x00;
                                msg.dati[3] = 0x00;
                                msg.dati[4] = 0x00;
                                msg.dati[5] = 0x00;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                            }
                        }
                    }
                    else if (row.StartsWith("E"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x006);
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x34;
                                msg.dati[1] = (byte)Int16.Parse(dati[0]);
                                msg.dati[2] = (byte)Int16.Parse(dati[1]);
                            }
                            else if (protocol.protocolName.Equals("CANSYSTEM"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x003);
                                msg.dlc = 8;
                                val.data = 0;
                                msg.dati[0] = 0x10;
                                msg.dati[1] = (byte)Int16.Parse(dati[0]);
                                msg.dati[2] = (byte)Int16.Parse(dati[1]);
                            }
                        }
                    }
                    else if (row.StartsWith("C_A"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x007);
                                msg.dlc = 8;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.d3 = 0;
                                val.d4 = 0;
                                val.d5 = 0;

                                val.data = (uint)(Int32.Parse(dati[0]));
                                val.data |= (uint)(Int32.Parse(dati[1]) << 8);
                                val.data |= (uint)(Int32.Parse(dati[2]) << 16);
                                msg.dati[0] = 0x7F;
                                msg.dati[1] = 0x14;
                                msg.dati[2] = val.d0;
                                msg.dati[3] = val.d1;
                                msg.dati[4] = val.d2;
                                msg.dati[5] = val.d3;
                                msg.dati[6] = 0x00;
                                msg.dati[7] = 0x00;
                            }

                        }
                    }

                    else if (row.StartsWith("C_P"))
                    {
                        msg = new SingoloComando();
                        if (row.IndexOf(':') > 0)
                        {
                            row = row.Substring(1, row.Length - 1);
                            start = row.IndexOf(':');
                            end = row.IndexOf(':', start + 1);
                            if (end == -1)
                                end = row.Length;
                            data = row.Substring(start + 1, (end - start) - 1);
                            String[] dati = data.Split(',');

                            if (protocol.protocolName.Equals("CAN-CAL"))
                            {
                                msg.id = Convert.ToUInt32(testConfig.baseID_Send + testConfig.offsetID_Send + 0x007);
                                msg.dlc = 8;
                                val.data = 0;
                                val.d0 = 0;
                                val.d1 = 0;
                                val.d2 = 0;
                                val.d3 = 0;
                                val.d4 = 0;
                                val.d5 = 0;
                                val.d6 = 0;

                                val.data = (uint)(Int32.Parse(dati[0]));
                                val.data |= (uint)(Int32.Parse(dati[1]) << 8);
                                val.data |= (uint)(Int32.Parse(dati[2]) << 16);
                                msg.dati[0] = 0x7F;
                                msg.dati[1] = 0x15;
                                msg.dati[2] = val.d0;
                                msg.dati[3] = val.d1;
                                msg.dati[4] = val.d2;
                                msg.dati[5] = val.d3;
                                msg.dati[6] = val.d4;
                                msg.dati[7] = 0x00;
                            }

                        }
                    }
                    if (msg != null)
                    {
                        if (end != row.Length)
                        {
                            if (row.IndexOf('@') != -1)
                            {
                                data = row.Substring(end + 1, (row.Length - end) - 2);
                                msg.decrement = true;
                            }
                            else
                            {
                                data = row.Substring(end + 1);
                            }

                            msg.pause = Int32.Parse(data);
                            msg.increment = true;
                        }

                        elenco.Add(msg);
                    }
                }
            }

            if (elenco.Count == 0)
            {
                MessageBox.Show("Lo script " + filename + " non contiene comandi validi", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                elenco[elenco.Count - 1].increment = false;
            return elenco;
        }

    }
}
